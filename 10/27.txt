[EXH. NVLES CATHOLIQUES]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Vendredi Saint
II Vendredi Saint
III Vendredi Saint
IV Vendredi Saint
I Pâques Jour
II Pâques Jour
III Pâques Jour
IV Pâques Jour
Pâques Jour abrg
Quasimodo
Pâques IIIe Dim.
Pâques IIIe Dim. abr.
Pâques Ve Dim.
Ascension
Pentecôte I
Pentecôte II
Pentecôte III
Pentecôte abr.
Trinité
III Pentecôte
V Pentecôte
IX Pentecôte
XXI Pentecôte abr.
Sainte Croix I
Sainte Croix II
Précis Ste Croix
Exh. Nvles Catholiques
Vie Xtienne
Ursulines Meaux I
Ursulines Meaux II
Ord. Ursul. Meaux
Ursulines Meaux III
Ursulines Meaux IV
Conf. Ursul. Meaux
Instr. Ursul. Meaux
Profess. Ursuline
Disc. Visitandines
Disc. Union J-C / Épouse
Pensées Xtiennes
Pensées Détachées

 

 



EXHORTATION AUX NOUVELLES CATHOLIQUES (_A_).


 

_Deus tentavit eos et invenit illos dignos se_.

Dieu les a mis à l'épreuve et les a trouvés dignes de lui. _Sapient.,_
III, 5.

 

Le serviteur est bienheureux , lorsque son maître daigne éprouver sa
fidélité ; et le soldat doit avoir beaucoup d'espérance, lorsqu'il voit
aussi que son capitaine met son courage à l'épreuve. Car comme on
n'éprouve pas en vain la vertu , l'essai qu'on fait de la leur, leur est
un gage assuré et des emplois qu'on leur veut donner, et des grâces
qu'on leur prépare ; d'où il est aisé de comprendre combien l'Apôtre a
raison de dire que « l'épreuve produit l'espérance : » _Probatio verò
spem_ (1). C'est ce qui m'oblige, Messieurs, pour fortifier l'espérance
dans laquelle doivent vivre les enfants de Dieu, de vous parler des
épreuves qui en sont le fondement immuable ; et je vous exposerai plus
au long les raisons particulières qui m'engagent à en traiter dans cette
assemblée , après avoir imploré le secours d'en haut par l'intercession
de la sainte Vierge. _Ave, Maria_.

 

Comme c'était de l'or le plus affiné (_b_) que les enfants, d'Israël
con-sacroient à Dieu pour faire l'ornement de son sanctuaire, la vertu
doit être la plus épurée qui servira d'ornement au sanctuaire céleste

 

1 _Rom.,_ V, 4.

 

(_a_)  Prêché à Paris, chez les nouvelles Catholiques, en 1662.

Le lieu est clairement indiqué dans tout le sermon. Quant à la date,
elle ressort de ce passage : O Dieu clément et juste!... vous avez
départi aux riches du monde quelque écoulement de votre abondance. Vous
les avez bits grande pour servir de pères à vos pauvres... Et leur
grandeur les rend dédaigneux, leur abondance secs, leur félicité
insensibles, encore qu'ils voient tous les jours, non tant des pauvres
et des misérables que la misère elle-même et la pauvreté en personne
pleurante et gémissante à leur porte. » Ces paroles peignent au naturel
la disette de 1661 et de 1662.

Quand on aura lu notre sermon, on voudra bien nous dire s'il est vrai
que Bossuet était le flatteur des riches et l'ennemi des pauvres :
Jamais grand homme ne fut plus méconnu.

(_b_)  _Var._ : Le plus fin.

 

469

 

et au temple qui n'est point bâti de main d'homme. Dieu a dessein
d'épurer les âmes, afin de les rendre, dignes de la gloire, delà
sainteté, de la magnificence du siècle futur ; mais afin de les épurer
et d'en tirer tout le fin, si je puis parler de la sorte, il leur
prépare aussi de grandes épreuves. Et remarquez, Messieurs, qu'il y en a
de deux genres : l'épreuve de la pauvreté et celle de l'abondance. Car
non-seulement les afflictions, mais encore les prospérités sont une
pierre de touche à laquelle la vertu se peut reconnaître. Je l'ai appris
du grand saint Basile dans cette excellente _Homélie_ qu'il a faite sur
l'avarice (1), et saint Basile l'a appris lui-même des Écritures
divines.

Nous lisons dans le livre du _Deutéronome_ (_a_) : — « Le Seigneur vous
a conduit par le désert, afin de vous affliger et de vous éprouver tout
ensemble : » _Adduxit te Dominus tuus per desertum, ut affligent te
atque tentaret_ (2) : voilà l'épreuve par l'affliction. Mais nous lisons
aussi en l' Exode, lorsque Dieu fit pleuvoir la manne, qu'il parle ainsi
à Moïse : « Je pleuvrai, dit-il, des pains du ciel : » _Ecce, ego pluam
vobis panes de cœlo_ (3) ; et il ajoute aussitôt après : « C'est afin
d'éprouver mon peuple et de voir s'il marchera dans toutes mes voies
(_b_) : » et voilà en termes formels l'épreuve des prospérités et de
l'abondance : _Ut tentera eum utrùm ambulet in lege meâ, annon_ (4).

« Toutes choses, dit le saint Apôtre (5), arrivaient en figure au peuple
ancien , » et nous devons rechercher la vérité de ces deux épreuves dans
la nouvelle Alliance. Je vous en dirai ma pensée pour servir de
fondement à tout ce discours.

Je ne vois dans le Nouveau Testament que deux voies pour arriver au
royaume : ou celle de la patience qui souffre les maux, ou celle de la
charité qui les soulage. La grande voie et la voie royale , par laquelle
Jésus-Christ a marché lui-même, c'est celle des afflictions. Le Sauveur
n'appelle à son banquet que les faibles, que les malades, que les
languissants (6) ; il ne veut voir en sa compagnie que ceux qui portent
sa marque, c'est-à-dire la pauvreté

 

1 S. Basil., hom. _de Avarit_., n. 1. — 2 _Deuter.,_ VIII, 2. — 3
_Exod_., XVI, 4. — 4 _Ibid._ — 5 I _Cor_., X, 11. — 6 _Luc.,_ XIV, 21.

 

(_a_) _Var._ ; Au _Deutéronome_. — (_b_) Dans ma voie : dans ma loi.

 

470

 

et la croix. Tel était son premier dessein, lorsqu'il a formé (_a_) son
Église. Mais si tout le monde était pauvre , qui pourrait soulager les
pauvres, et leur aider à soutenir le fardeau qui les accable? C'est pour
cela, chrétiens, qu'outre la voie des afflictions qui est la plus
assurée, il a plu à notre Sauveur d'ouvrir un autre chemin aux riches et
aux fortunés, qui est celui de la charité et de la communication
fraternelle. Si vous n'avez pas cette gloire de vivre avec Jésus-Christ
dans l'humiliation et dans l'indigence (_b_), voici une autre voie qui
vous est montrée, une seconde espérance qui vous est offerte ; c'est de
secourir les misérables et d'adoucir leurs douleurs et leurs amertumes.
Ainsi Dieu nous éprouve en ces deux manières. Si vous vivez dans
l'affliction, croyez que le Seigneur vous éprouve pour reconnaître votre
patience ; si vous êtes dans l'abondance, croyez que le Seigneur vous
éprouve pour reconnaître votre charité : _Tentat vos Dominus Deus
vester_ (1). Et par là vous voyez, mes Frères, les deux épreuves
diverses dont je vous ai fait l'ouverture.

La vue de mon auditoire me jette profondément dans cette pensée (_c_).
Car que vois-je dans cette assemblée, sinon l'exercice de ces deux
épreuves? Deux objets attirent mes yeux, et doivent aujourd'hui partager
mes soins. Je vois d'un côté des âmes souffrantes que la profession de
la foi expose à de grands périls, et de l'autre des personnes de
condition qui semblent ici accourir pour soulager leurs misères (_d_).
Je suis redevable aux uns et aux autres ; et pour m'acquitter envers
tous, j'exhorterai en particulier chacun de mes auditeurs à être fidèle
à son épreuve. Je vous dirai, mes très-chères Sœurs : Souffrez avec
soumission, et votre foi sera épurée par l'épreuve de la patience. Je
vous dirai, Messieurs et Mesdames : Donnez libéralement, et votre
charité sera épurée par l'épreuve de la compassion. Ainsi cette
exhortation sera partagée entre les deux sortes de personnes qui
composent cette assemblée , et le partage que je vois dans mon auditoire
fera celui de ce discours (_e_).

 

1 _Deuter.,_ XIII, 3.

 

(_a_) _Var_. : Construit. — (_b_) Et dans les angoisses. — (_c_) Me fait
penser à ces choses. — (_d_) Leurs calamités.— (_e_) Par l'épreuve de la
compassion. C'est le sujet de ce discours.

 

471

 


PREMIER POINT.

 

Je commence par vous, mes très-chères Sœurs, nouveaux en-fans de
l'Église et ses plus chères délices, nouveaux arbres qu'elle a plantés
et nouveaux fruits qu'elle goûte. Je ne puis m'empêcher d'abord de vous
témoigner devant Dieu que je suis touché de vos maux : la séparation de
vos proches, les outrages dont ils vous accablent, les dures
persécutions qu'ils font à votre innocence, les misères et les périls où
votre foi vous expose m'affligent sensiblement ; et comme de si grands
besoins et des extrémités si pressantes demandent un secours réel, j'ai
peine, je vous l'avoue , à ne vous donner que des paroles. Mais comme
votre foi en Jésus-Christ ne vous permet pas de compter pour rien les
paroles de ses ministres, ou plutôt ses propres paroles dont ses
ministres sont établis les dispensateurs, je vous donnerai avec joie un
trésor de consolation dans des paroles saintes et évangéliques, et je
vous dirai avant toutes choses avec le grand saint Basile (1) :Vous
souffrez , mes très-chères Sœurs, devez-vous vous en étonner (_a_) étant
chrétiennes ? Le soldat se reconnaît par les hasards (_b_), le marchand
par la vigilance, le laboureur par son travail opiniâtre, le courtisan
par ses assiduités et le chrétien par les douleurs et par les
afflictions. Ce n'est pas assez de le dire ; il faut établir cette
vérité par quelque principe solide, et faire voir en peu de paroles que
l'épreuve de la foi c'est la patience. Mais afin de le bien entendre ,
examinons, je vous prie, quelle est la nature de la foi et la manière
divine dont elle veut être prouvée.

La foi est une adhérence de cœur à la vérité éternelle, malgré les
témoignages des sens et delà raison. De là vous pouvez comprendre
qu'elle dédaigne tous les arguments que peut inventer la sagesse
humaine. Mais si les raisons lui manquent, le ciel même lui fournit des
preuves , et elle est suffisamment établie par les miracles et par les
martyres.

C'est, mes Frères, par ces deux moyens qu'a été soutenue la foi

 

1 Hom. _in fam. et siccit.,_ n. 5.

 

(_a_) _Var._ : Vous en affliger. — (_b_) Ecoutez le grand saint Basile :
Le soldat se reconnaît par les périls, le marchand par la vigilance, le
laboureur par son travail assidu.

 

472

 

chrétienne. Elle est venue sur la terre troubler tout le monde par sa
nouveauté, étonner tous les esprits par sa hauteur et effrayer tous les
sens par la sévérité inouïe de sa discipline. Tout l'univers s'est uni
contre elle et a conjuré sa perte. Mais malgré toute la nature elle a
été établie par les choses prodigieuses que Dieu a faites pour
l'autoriser (_a_), et par les cruelles extrémités que les hommes ont
endurées pour la défendre. Dieu et les hommes ont fait leurs efforts
pour appuyer le christianisme. Quel a dû être l'effort de Dieu, sinon
d'étendre sa main à des signes et à des prodiges? Quel a dû être
l'effort des hommes, sinon de souffrir avec soumission des peines et des
tourments ? Chacun a fait ce qui lui est propre. Car il n'y avait rien
de plus convenable, ni à la puissance divine que de faire de grands
miracles pour autoriser la foi chrétienne, ni à la faiblesse humaine que
de souffrir de grands maux pour en soutenir la vérité. Voilà donc la
preuve de Dieu, faire des miracles ; (_b_) voici la preuve des hommes,
souffrir des tourments : l'homme étant si faible, ne pouvait rien faire
de grand, ni de remarquable, que de s'abandonner à souffrir. Ainsi ce
que Dieu a opéré, et ce que les hommes ont souffert, a également
concouru à prouver la vérité de la foi. Les miracles que Dieu a faits
ont montré que la doctrine du christianisme surpassait toute la nature ;
et les cruautés inouïes auxquelles se sont soumis les fidèles pour
défendre cette doctrine, ont fait voir (_c_) jusqu'où doit aller le
glorieux ascendant qui appartient à la vérité sur tous les esprits et
sur tous les cœurs.

Et en effet, chrétiens, jamais nous ne rendrons à la vérité l'hommage
qui lui est dû, jusqu'à ce que nous soyons résolus à souffrir pour elle ;
et c'est ce qui a fait dire à Tertullien que « la foi est obligée au
martyre, » _debitricem martyrii fidem_ ». Oui, sainte vérité de Dieu,
souveraine de tous les esprits et arbitre de la vie humaine, le
témoignage de la parole est une preuve trop faible de ma servitude ; je
dois vous prouver ma foi par l'épreuve des souffrances. O vérité
éternelle, si j'endure pour l'amour de vous, si

 

1 Scorp., n. 8.

 

(_a_) _Var._ : Pour la soutenir. — (_b_) _Note marg. : In eo quod manum
tuam exendas ad sanitates,et signa et prodigia fieri per nomen sancti
Filii tui Jesu_ (Act. IV, 30). — (_c_) _Var._ : Et l'ardeur qu'out eue
les fidèles à défendre cette doctrine a fait voir!

 

473

 

mes sens sont noyés pour l'amour de vous dans la douleur et dans
l'amertume, ce vous sera une preuve que j'y ai renoncé de bon cœur pour
m'attacher à vos ordres (_a_). Pour faire voir à toute la terre que je
m'abaisse volontairement sous le joug que vous m'imposez, je veux bien
m'abaisser encore jusqu'aux dernières humiliations. Qu'on me jette dans
les prisons, et qu'on charge mes mains de fers, je regarderai ma
captivité comme une image glorieuse (_b_) de ces chaînes intérieures par
lesquelles j'ai lié ma volonté toute entière et assujetti mon
entendement à l'obéissance de Jésus-Christ et de sa sainte doctrine :
_In captivitatem redigentes intellectum in obsequium Christi_ (1).

Consolez-vous donc, mes très-chères Sœurs, dans la preuve que vous
donnez par vos peines de la pureté de votre foi. Vous êtes un grand
spectacle à Dieu, aux anges et aux hommes. Vos souffrances font
l'honneur de la sainte Église, qui se glorifie de voir en vous, même au
milieu de sa paix et de son triomphe, une image de ses combats et une
peinture animée des martyres qu'elle a soufferts. Ne vous occupez pas
tellement des maux que vous endurez, que vous ne laissiez épancher vos
cœurs dans le souvenir agréable des récompenses qui vous attendent.
Encore un peu, encore un peu, dit le Seigneur, et je viendrai moi-même
essuyer vos larmes ; et je m'approcherai de vous pour vous consoler, et
vous verrez le feu de ma vengeance dévorer vos persécuteurs ; et
cependant je vous recevrai en ma paix et en mon repos, au sein de mes
éternelles miséricordes.

Vous endurez pour la foi, ne vous découragez pas ; songez que la sainte
Église s'est fortifiée par les tourments, accrue par la patience ,
établie par l'effort (_c_) des persécutions. Et à ce propos, chrétiens,
je me souviens que saint Augustin se représente que les fidèles étonnés
de voir durer si longtemps ces cruelles persécutions par lesquelles
l'Église était agitée, s'adressent à elle-même et lui en demandent la
cause (2). Il y a longtemps, ô Église, que l'on frappe sur vos pasteurs
et que l'on dissipe vos troupeaux : Dieu vous

 

1 II _Cor.,_ X, 5. — 2 _In__ Psal._ CXXVIII, n. 2, 3.

 

(_a_) _Var._ : Que je les ai quittés pour vous suivre. — (_b_) Sacrée. —
(_c_) Par la violence.

 

474

 

a-t-il oubliée? Les vents grondent, les flots se soulèvent, vous flottez
deçà et delà battue des ondes et de la tempête : ne craignez-vous pas à
la fin d'être entièrement abîmée et ensevelie sous les eaux? Le même
saint Augustin ayant ainsi fait parler les fidèles, fait aussi répondre
l'Église par ces paroles du divin Psalmiste : _Sœpè expugnaverunt me à
juventute meà, dicat nunc Israël_ (1). Mes enfants, dit la sainte
Église, je ne m'étonne pas de tant de traverses ; j'y suis accoutumée dès
ma tendre enfance. Les ennemis qui m'attaquent n'ont jamais cessé de me
tourmenter dès ma première jeunesse ; et ils n'ont rien gagné contre
moi, et leurs efforts ont été toujours inutiles : _Etenim non potuerunt
mihi_ (2).

Et certainement, chrétiens, l'Église a toujours été sur la terre, et
jamais elle n'a été sans afflictions (_a_). Elle était représentée en
Abel ; et il a été tué par Caïn son frère. Elle a été représentée en
Enoch ; et il a fallu le séparer (_b_) du milieu des iniques et des
impies, qui ne pou voient compatir avec son innocence : _Et translatas
est ab iniquis_ (3). Elle nous a paru dans la famille de Noé ; et il a
fallu un miracle pour la délivrer, non-seulement des eaux du déluge,
mais encore des contradictions des enfants du siècle. Le jour me
manquerait, comme dit l'Apôtre 4, si j'entreprenais de vous raconter ce
qu'ont souffert des impies Abraham et les patriarches , Moïse et tous
les prophètes, Jésus-Christ et ses saints apôtres. Par conséquent, dit
la sainte Église par la bouche du saint Psalmiste, je ne m'étonne pas de
ces violences : _Sœpè expugnavenmt me à juventute meà ; numquid ideò non
perverti ad senectutem_ (5)? Regardez, mes enfants, mon antiquité,
considérez ces cheveux gris ; « ces cruelles persécutions dont a été
tourmentée mon enfance, m'ont-elles pu empêcher de parvenir heureusement
à cette vieillesse vénérable ? » Ainsi je ne m'étonne plus des
persécutions ; si c'était la première fois, j'en serais peut-être
troublée ; maintenant la longue habitude fait que je ne m'en émeus pas ;
je laisse agir les pécheurs : _Supra dorsum meum_

 

1 _Psal._ CXXVIII, 1. — 2 _Ibid.,_ 2. — 3 _Hebr_., XI, 5. — 4 _Ibid.,_
32. — 5 S. Aug., _in Psal._ CXXVIII, n. 3.

 

(_a_) _Var._ : Sans persécuteurs. — (_b_) Le tirer.

 

475

 

_fabricaverunt peccatores_ (1). Je ne tourne pas ma face contre eux pour
m'opposer à leurs violences, je ne fais que tendre le dos pour porter
les coups qu'ils me donnent ; ils frappent cruellement, et je souffre
sans murmurer. C'est pourquoi ils prolongent leurs iniquités, et ne
mettent point de bornes à leur furie : _Prolongaverunt iniquitatem suam_
(2). Ma patience sert de jouet à leur injustice, mais je ne me lasse pas
de souffrir ; je suis bien aise de prouver ma foi à celui qui m'a
appelée, et de me montrer (_a_) digne de son choix par une si noble
épreuve d'un amour constant et fidèle : _Tentavit eos Deus, et invenit
illos dignos se_.

Entrez, mes Sœurs, dans ces sentiments ; souffrez pour l'amour de la
sainte Église ; la grâce que Dieu vous a faite de vous ramener à son
unité ne vous semblerait pas (_b_) assez précieuse, si elle ne vous
coûtait quelque chose. Songez à ce qu'ont souffert les saints
personnages dont je vous ai récité les noms et rappelé le souvenir.
Joignez-vous à cette troupe bienheureuse (_c_) de ceux qui ont souffert
pour la vérité, et « qui ont blanchi leurs étoles dans le sang de
l'Agneau sans tache (5). » Autant de peines qu'on souffre, autant de
larmes qu'on verse pour avoir embrassé la foi (_d_), autant de fois on
se lave dans le sang du Sauveur Jésus, et on y nettoie ses péchés, et on
sort de ce bain sacré avec une splendeur immortelle. Et c'est alors que
Jésus nous dit : Voici mes fidèles et mes bien-aimés ; « et ils
marcheront avec moi ornés d'une céleste blancheur, parce qu'ils sont
dignes d'une telle gloire : » _Et ambulabunt meum in albis, quia digni
sunt_ (6). Voyez donc, mes très-chères Sœurs ; voyez Jésus-Christ qui
vous tend les bras, qui soutient votre faiblesse, qui admire aussi votre
force et prépare votre couronne : il vous a éprouvées par la patience et
vous a trouvées dignes de lui : _Tentavit eos et invenit illos dignos
se_.

Mais nous, que ferons-nous, chrétiens? Demeurerons-nous insensibles, et
serons-nous spectateurs oisifs d'un combat si célèbre et si glorieux? Ne
donnerons-nous que des  paroles et

 

1 _Psal._ CXXVIII, 3. — 2 _Ibid._ — 3 _Apoc.,_ VII, 14. — 4 _Ibid.,_
III, 4.

 

(_a_) _Var._ : De me rendre. — (_b_) De vous rappeler à son unité : la
grâce que Dieu vous a faite ne vous semblerait pas. — (_c_) Invincible :
— généreuse : — sacrée. — (_d_) Pour la cause de la vérité et pour la
foi.

 

476

 

quelques frivoles consolations à des peines si effectives ? Et pendant
que ces filles innocentes, qui souffrent persécution pour la justice,
sont dans le feu de l'affliction où Dieu épure leur foi, ne ferons-nous
point distiller sur elles quelque rosée de nos charités pour les
rafraîchir dans cette fournaise, et les aider à souffrir une épreuve si
violente ? C'est de quoi il faut vous entretenir dans le reste de ce
discours, que je tranche en peu de paroles.

 


SECOND  POINT.

 

Je parle donc maintenant à vous qui vivez dans les richesses et dans
l'abondance. Ne vous persuadez pas que Dieu vous ait ouvert ses trésors
avec une telle libéralité, pour contenter votre luxe ; c'est qu'il a
dessein d'éprouver si vous avez un cœur chrétien, c'est-à-dire un cœur
fraternel et un cœur compatissant.

David, considérant autrefois les immenses profusions de Dieu envers lui,
se sentit obligé par reconnaissance de faire de magnifiques préparatifs
pour orner son temple ; et lui offrant de grands dons (_a_), il y ajouta
ces paroles : « Je sais, dit-il, ô mon Dieu, que vous éprouvez les cœurs
et que vous aimez la simplicité ; et c'est pourquoi, Seigneur
tout-puissant, je vous ai consacré ces choses avec grande joie en la
simplicité de mon cœur : » _Scio, Deus meus, quòd probes corda et
simplicitatem diligas ; undè et ego in simplicitate cordis met lœtus
obtuli universa hœc_ (1). Vous voyez comme il reconnaît que les bontés
de Dieu étaient une épreuve (_b_) ; et qu'il voulait éprouver, en lui
donnant, s'il avait un cœur libéral qui offrît à Dieu volontairement ce
qu'il reçevait de sa main.

Croyez, ô riches du siècle, qu'il vous ouvre ses mains dans la même vue.
S'il est libéral envers vous, c'est qu'il a dessein d'éprouver si votre
âme sera attendrie par ses bontés, et sera touchée du désir de les
imiter. De là cette abondance dans votre maison ; de là cette affluence
de biens ; de là ce bonheur, ce succès, ce cours fortuné de vos affaires.
Il veut voir, chrétien, si ton cœur avide

 

1 I _Paral_., XXIX, 17.

 

(_a_) _Var._ : Fit de magnifiques préparatifs pour orner son temple, et
lui offrant tous ses dons. — (_b_) Comme il reconnaît que les
libéralités que Dieu lui a faites lui tenaient lieu d'une épreuve.

 

477

 

engloutira tous ces biens pour ta propre satisfaction ; ou bien si se
dilatant par la charité, il fera couler ses ruisseaux sur les pauvres et
les misérables, comme parle l'Écriture sainte (1). Car ce sont les
temples qu'il aime, et c'est là qu'il veut recevoir les effets de ta
gratitude.

Voici, Messieurs, une grande épreuve ; c'est ici qu'il nous faut
entendre la malédiction des grandes fortunes. L'abondance, la prospérité
a coutume d'endurcir le cœur de l'homme ; l'aise, la joie, l'afflucnce
(_a_) Remplissent l'âme de sorte qu'elles en éloignent tout le sentiment
de la misère des autres, et mettent à sec, si l'on n'y prend garde, la
source delà compassion. C'est pourquoi le divin Apôtre parlant des
fortunés de la terre, de ceux qui s'aiment eux-mêmes et qui vivent dans
les plaisirs, dans la bonne chère, dans le luxe, dans les vanités, les
appelle « cruels et impitoyables, sans affection, sans miséricorde,
amateurs de leurs voluptés : » _Homines seipsos amantes, immites, sine
affectione, sine benignitate, voluptatum amatores_ (2). Voilà une
merveilleuse contexture de qualités différentes, (_b_) Mais c'est que le
saint Apôtre pénétrant par l'Esprit de Dieu dans les plus intimes replis
de nos cœurs, voyait que ces hommes voluptueux, attachés excessivement à
leurs propres satisfactions, deviennent insensibles aux maux de leurs
frères. C'est pourquoi il dit qu'ils sont sans affection, sans tendresse
et sans miséricorde ; ils ne regardent qu'eux-mêmes. Et le prophète
Isaïe représente au naturel leurs véritables sentiments, lorsqu'il leur
attribue ces paroles (_c_) : _Ego sum, et prœter me non est altera_ (3)
: « Je suis, il n'y a que moi sur la terre. » Qu'est-ce que toute cette
multitude ? Têtes de nul prix et gens de néant. Penser aux intérêts des
autres, leur délicatesse ne le permet pas. Chacun ne compte que soi ; et
tenant tous les autres dans l'indifférence, on tâche de vivre à son aise
dans une souveraine tranquillité des fléaux qui affligent le reste des
hommes.

O Dieu clément et juste! ce n'est pas pour cette raison que vous

 

1  _Isa.,_ LVIII, 10, 11. — 2 II _Timoth.,_ III, 3. — 3 _Isa.,_ XLVII,
10.

 

(_a_) _Var._ : Félicité. — (_b_) _Note marg_. : Vous croyiez peut-être,
Messieurs, que cet amour des plaisirs ne fût que tendre et délicat, ou
bien plaisant et flatteur, mata vous n'aviez pas encore songé qu'il fût
cruel et impitoyable. — (_c_) _Var._: Les fait parler admirablement dans
la véritable disposition de leur cœur.

 

478

 

avez départi aux riches du monde quelque écoulement (_a_) de votre
abondance. Vous les avez faits grands pour servir de pères à vos
pauvres ; votre providence a pris soin de détourner les maux de dessus
leurs têtes, afin qu'ils pensassent à ceux du prochain ; vous les avez
mis à leur aise et en liberté, afin qu'ils fissent leur affaire du
soulagement de vos enfants. Telle est l'épreuve où vous les mettez ; et
leur grandeur au contraire les rend dédaigneux, leur abondance secs,
leur félicité insensibles, encore qu'ils voient tous les jours non tant
des pauvres et des misérables que la misère elle-même et la pauvreté en
personne, pleurante et gémissante à leur porte.

O riches, voilà votre épreuve ; et afin d'y être fidèles, écoutez
attentivement cette parole du Sauveur des âmes : « Donnez-vous garde de
toute avarice : » _Cavete ab omni avaritiâ_ (1). Cette parole du Fils de
Dieu demande un auditeur attentif. Donnez-vous garde de toute avarice ;
c'est qu'il y en a de plus d'une sorte. Il y a une avarice sordide, une
avarice noire et ténébreuse, qui enfouit ses trésors, qui n'en repaît
que sa vue et qui en interdit l'usage à ses mains. _Quid prodest
possessori, nisi quòd cernit divitias ocidis suis_ (2)? Mais il y a
encore une autre avarice, qui dépense, qui fait bonne chère, qui
n'épargne rien à ses appétits. Je me trompe peut-être, mes Frères,
d'appeler cela avarice, puisque c'est une extrême prodigalité. Elle
mérite néanmoins le nom d'avarice, parce que c'est une avidité qui veut
dévorer tous ses biens, qui donne tout à ses appétits et qui ne veut
rien donner aux nécessités des pauvres et des misérables ; et je parle en
cela selon l'Évangile (_b_). Jésus-Christ ayant dit ces mots :
Donnez-vous garde de toute avarice, apporte l'exemple d'un homme qui
ravi de son abondance, veut agrandir ses greniers et augmenter sa
dépense. Car il paraît bien, chrétiens, qu'il voulait user de ses
richesses, puisqu'il se dit à lui-même : « Mon âme, voilà de grands
biens ; repose-toi, fais grande chère, mange et bois longtemps à ton aise
: » _Requiesce, comede, bibe, epulare_ (3). Encore qu'il donne tout à
son plaisir et

 

1 _Luc._,__ XII, 15. — 2 _Eccles_., V. 10. — 3 _Luc,_ XII, 19.

 

(_a_) _Var._ : Un rayon. — (_b_) Je parle néanmoins avec l'Évangile :
elle mérite le nom d'avarice, parce que c'est une avidité qui veut... et
qui ne veut rien donner aux nécessités des pauvres et des misérables.
Jésus-Christ ayant dit...

 

479

 

qu'il tienne une table si abondante et si délicate, Jésus-Christ
néanmoins le traite d'avare, condamnant l'avidité de son cœur, qui
consume tous ses biens pour soi, qui donne tout à ses excès et à ses
débauches, et n'ouvre point ses mains aux nécessités ni aux besoins de
ses frères. Prenez garde à cette avarice de cœur, à cette avidité ;
modérez vos passions, (_a_) et laites un fonds aux pauvres sur la
modération de vos vanités : _Manum inferre rei suœ in causa eleemosynœ_
(1).

Pourquoi agrandir tes greniers? Jeté montre un lieu convenable où tu
mettras tes richesses plus en sûreté : laisse un peu déborder ce fleuve,
laisse-le se répandre sur les misérables. Mais pourquoi tout donner à
tes appétits? Mon âme, dis-tu, repose-toi, mange et bois longtemps à ton
aise. Regarde de quels biens tu repais ton âme, de même, dit saint
Basile, que si tu avais une âme de bête (2). Ne me dis point : Que
ferai-je ? Il faut te.... Si vous ne le faites, mes Frères, il n'y a
point d'espérance de salut pour vous. Car pour arriver à la gloire que
Jésus-Christ nous a méritée, il faut porter son image, il faut être
marqué à son caractère, il faut en un mot lui être conforme. Quelle
ressemblance avez-vous avec sa pauvreté dans votre abondance ; avec ses
délaisserons dans vos joies ; avec sa croix, avec ses épines, avec son
fiel et ses amertumes parmi vos délices dissolues? Est-ce là une
ressemblance, ou plutôt une manifeste contrariété? Voici néanmoins
quelque ressemblance et quelques ressources pour vous : c'est que la
croix de notre Sauveur n'est pas seulement une souffrance (_b_), mais
encore une inondation d'une libéralité infinie. Il donne pour nous son
âme et son corps, il prodigue tout son sang pour notre salut. Imitez du
moins quelque trait, sinon de ses souffrances affreuses, du moins d'une
libéralité si aimable et si attirante ; donnez au prochain, sinon vos
peines, du moins vos commodités ; sinon votre vie et votre substance, du
moins le superflu de vos biens ou le reste de vos excès (_c_). Entrez
dans les saints désirs du Sauveur et dans les empressements de sa
charité pour les hommes.

 

1   Tertull., _de Patient_., n. 7. — 2 Homil. de _Avar_., n. 6.

 

(_a_) _Note marg_. : Voyez saint Thomas de Villeneuve : Pauvres
intérieurs ; Carême du Louvre, I^(er) et II^(me) serm. — (_b_) _Var._ :
Un exercice. — (_c_) Sinon votre sang et votre vie, du moins quelque
partie de vos biens.

 

480

 

Il a guéri les malades, il a repu les faméliques, il a soutenu les
désespérés. C'est là sans doute la moindre partie que vous puissiez
imiter de la vie de notre Sauveur. Soyez les imitateurs, sinon des
souffrances qu'il a endurées à la croix, du moins des libéralités qu'il
y exerce, (_a_) Venez travailler au salut des âmes. Considérez ces
filles non moins innocentes qu'affligées. Faut-il vous représenter et
les périls de ce sexe, et les dangereuses suites de sa pauvreté,
l'écueil le plus ordinaire où sa pudeur fait naufrage ? Faut-il vous
dire les tentations où leur foi se trouve exposée dans les extrémités
qui les pressent?...

Considérez le ravage qu'a fait l'hérésie. Quelle plaie, quelle ruine,
quelle funeste désolation! La terre est désolée, le ciel est en deuil et
tout couvert de ténèbres, après qu'un si grand nombre d'étoiles qui
devaient briller dans son firmament, a été traîné au fond de l'abîme
avec la queue du dragon (1). L'Église gémit et soupire de se voir
arracher si cruellement une si grande partie de ses entrailles. Asile
pour recueillir quelque reste de son naufrage, cette maison depuis si
longtemps n'a pas encore de pain. Qu'attendez-vous, mes chers Frères?
Quoi? que leurs parents, qu'elles ont quittés, viennent offrir le pain
que votre dureté leur dénie? Horrible tentation ! Dans le schisme , le
plus grand malheur c'est la charité éteinte. Le diable pour leur
imposer, image de charité dans le secours mutuel qu'ils se donnent les
uns aux autres. Voulez-vous donc qu'elles pensent qu'il n'y a point de
charité dans l'Église , et qu'elles tirent cette conséquence : Donc
l'Esprit de Dieu s'en est retiré ? Vous leur vantez votre foi ; et
l'apôtre saint Jacques vous dit : Montre ta foi par tes œuvres (2).
C'est ainsi que le malin s'efforce de les séduire, et de les replonger
dans l'abîme d'où elles ne sont encore qu'à demi sorties. Veux-tu être
aujourd'hui par ta dureté coopérateur de sa malice, autoriser ses
tromperies et donner efficace à ses tentations? Sois plutôt coopérateur
de la charité de Jésus pour sauver lésâmes! Maintenant que je vous
parle, ce divin Sauveur vous éprouve. Si vous aimez les âmes, si vous
désirez

 

1 _Apoc.,_ XII, 4. — 2 _Jacob_., II, 18.

 

(_a_) _Note marg_. : Jésus-Christ demande  une  partie  des biens qu'il
vous a donnés, pour sauver sou bien et son trésor : son trésor ce sont
les âmes

 

481

 

leur salut, si vous êtes effrayés de leurs périls, vous êtes ses
véritables disciples. Si vous sortez de cet oratoire sans être touchés
de si grands malheurs, vous reposant du soin de cette maison sur ces
dames si charitables, comme si cette œuvre importante ne vous regardent
pas autant qu'elles. Funeste épreuve pour vous, qui prouvera votre
dureté, convaincra votre obstination, condamnera votre ingratitude !

[Précédente]

[Accueil]

[Suivante]

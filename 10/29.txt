[URSULINES MEAUX I]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Vendredi Saint
II Vendredi Saint
III Vendredi Saint
IV Vendredi Saint
I Pâques Jour
II Pâques Jour
III Pâques Jour
IV Pâques Jour
Pâques Jour abrg
Quasimodo
Pâques IIIe Dim.
Pâques IIIe Dim. abr.
Pâques Ve Dim.
Ascension
Pentecôte I
Pentecôte II
Pentecôte III
Pentecôte abr.
Trinité
III Pentecôte
V Pentecôte
IX Pentecôte
XXI Pentecôte abr.
Sainte Croix I
Sainte Croix II
Précis Ste Croix
Exh. Nvles Catholiques
Vie Xtienne
Ursulines Meaux I
Ursulines Meaux II
Ord. Ursul. Meaux
Ursulines Meaux III
Ursulines Meaux IV
Conf. Ursul. Meaux
Instr. Ursul. Meaux
Profess. Ursuline
Disc. Visitandines
Disc. Union J-C / Épouse
Pensées Xtiennes
Pensées Détachées

 



PREMIERE EXHORTATION AUX URSULINES  DE  MEAUX  (_A_).


 

_Si quis sitit, veniat ad me, et bibat._

 

« Si quelqu'un a soif, qu'il vienne à moi ; je lui donnerai à boire d'une
eau vive qui rejaillira jusqu'à la vie éternelle, et il n'aura plus
soif. » Ce sont les paroles sacrées que Jésus-Christ a prononcées dans
l'évangile de ce jour, parlant au peuple dans le temple de Jérusalem.

 

Ce n'est pas sans mystère que Jésus-Christ a proféré ces admirables
paroles au jour que les Juifs célébraient une fête parmi eux, où on
apportait de l'eau dans un bassin, pour certains usages, dans une
cérémonie : ce qu'il n'est pas nécessaire de vous expliquer ici, puisque
Jésus-Christ ne dit ces mêmes paroles que dans un sens mystique et
sublime, qui ne signifiait rien autre chose que l'eau de la grâce qu'il
voulait donner abondamment. Il parlait de cette eau mystérieuse qu'il
désirait répandre dans les âmes, et dont il voulait établir la source
dans son Église. Ces mêmes paroles signifiaient encore le zèle qu'avait
le Sauveur de voir venir à lui les hommes pour prendre ces eaux de salut
et de grâce, et la disposition qui est nécessaire pour les recevoir,
représentée par la soif qui marque aussi très-bien le désir et la
préparation qu'il faut que vous apportiez à la grâce qu'il vous veut
conférer dans cette occasion par mon ministère.

Remarquez, mes Filles, que Jésus-Christ jeta un grand cri, disant : « Si
quelqu'un a soif, qu'il vienne à moi ; et je lui donnerai à boire (1). »
Ce cri est en faveur des pécheurs, pour qui il

 

1 _Joan_., VII, 37.

 

(_a_) Prononcée à l'ouverture d'une visite pastorale faite au couvent de
Sainte-Ursule de Meaux, le 9 avril 1685.

Bossuet n’a pas écrit cette exhortation ; l'analyse imparfaite qu'on va
lire a été rédigée par une religieuse de la communauté. Nous n'avons ni
vu ni recherché le manuscrit, qui certainement n'existe plus: ici la
reproduction de Déforis vaut l'original.

Les indications données tout à l'heure se trouvaient dans l'analyse de
l'exhortation.

 

494

 

demande miséricorde ; il est en faveur des justes et des âmes fidèles,
dont il désire la perfection et la sainteté. Il crie pour les appeler à
lui, afin de répandre en elles avec plus d'abondance l'eau de ses
divines grâces. Mais ce cri nous représente encore ceux qu'il jette dans
l'Église et dans nos mystères. Il crie dans ce temps par la bouche des
prédicateurs, qui excitent les peuples à faire des fruits dignes de
pénitence. Il crie à l'autel, quand il dit par la bouche des prêtres : «
Faites ceci en mémoire de moi (1). » Ces paroles sont un cri de l'amour
de Jésus-Christ qui demande le nôtre. Il crie dans les mystères de ce
temps: il criera bientôt de la croix par toutes ses plaies et par son
sang, demandant à son Père le salut de tous les hommes, pour qui il va
donner sa vie adorable. Il crie spirituellement dans les âmes, par les
mouvements intérieurs que son divin Esprit y forme. Il a crié dans vos
cœurs, mes Filles ; c'est cet Esprit-Saint qui a formé ces cris qu'il y a
si longtemps que vous faites entendre, et qui sont parvenus jusqu'à mes
oreilles et qui m'ont l'ait connaître vos désirs. Combien y a-t-il, mes
chères Sœurs, que vous me demandez cette visite, et -que vous
reconnaissez vous-mêmes le besoin que vous en avez? Vous la souhaitez
toutes unanimement : vous vous êtes, sans doute, préparées à recevoir
les grâces de cette même visite, et les effets qu'elle doit produire
chez vous et pour lesquels je la viens faire. Je viens confirmer et je
désire accroître le bien que j'y trouverai, et détruire l'imperfection
jusqu'à la racine. Mais il fout que vous ayez un véritable esprit de
renouvellement, et un désir sincère de coopérer à nos soins de tout
votre pouvoir.

Va, dit Dieu autrefois au prophète Jonas (2), comme nous venons de lire
en la messe : lève-toi pour aller à Ninive vers mon peuple ; prêche-leur
la pénitence, et les avertis de ma part qu'ils aient à changer de vie ;
qu'ils se convertissent de tout leur cœur à moi, qui suis leur Dieu et
leur Seigneur : autrement que dans quarante jours Ninive sera renversée
et entièrement détruite. Si ces paroles donnèrent de la frayeur à ce
peuple et eurent tant de pouvoir et tant d'effet, celles que je viens de
vous dire de la part de Dieu ne vous doivent pas moins émouvoir de
respect et de crainte.

 

1 _Luc.,_ XXII, 19. — 2 _Joan_., III, 2 et seq.

 

495

 

Il y a ici plus que Jonas ; et celui qui m'envoie à vous est le même
Dieu, grand et redoutable.

Je viens donc aujourd'hui de sa part vous prêcher la pénitence, le
changement et le renouvellement de vie, le mépris du monde, le parfait
renoncement à vous-mêmes, la soumission d'esprit, la mortification des
sens : en un mot, je viens faire cette visite pour réparer tout ce qu'il
y aurait de déchet en la perfection religieuse dans votre maison, pour
éteindre, pour détruire et anéantir les plus petits restes de l'amour du
monde et des choses de la terre. Il faut faire périr les moindres
inclinations de ce monde corrompu ; il faut qu'il meure, qu'il y meure,
qu'il expire, qu'il y rende le dernier soupir. Venez donc, mes Filles,
travailler toutes avec moi, pour exterminer tout ce qui ressent encore
ce monde criminel. Venez m'aider à renverser Ninive : détruisons tout ce
qu'il y a encore de trop immortifié, de trop mondain, enfin tout ce qui
est trop naturel et imparfait en vous, sans pardonner à la moindre chose
et sans rien épargner.

Dites-moi, mes Sœurs, quelles sont maintenant vos inclinations et vos
pensées? Vous êtes par vos vœux mortes au monde et à tout ce qui est
créé ; que souhaitez-vous à présent? Avez-vous d'autres désirs que ceux
qui vous doivent élever sans cesse vers les biens de l'éternité
bienheureuse, et vous y faire aspirer à tout moment ? Si votre cœur a
encore quelque mouvement qui le possède, il faut désormais que ce soit
pour la justice, pour la perfection et la sainteté de chacune de vous en
particulier et de tout votre monastère, par le moyen de cette visite.
Souhaitez véritablement d'en recevoir les grâces ; demandez qu'elles
soient répandues en vos âmes. C'est là, mes Filles, désirer la justice,
comme dit Jésus-Christ dans son Évangile, lorsqu'il a prononcé cet
oracle sur la montagne : « Bienheureux ceux qui ont faim et soif de la
justice, ils seront rassasiés (1). » Vous serez parfaitement rassasiées,
si vous n'avez que cet unique désir. Il vous donnera à boire de cette
eau vive, qui éteindra votre soif. Demandez-lui comme la Samaritaine
(2) ; et il vous donnera cette eau dont je vous parle, «lui n'est autre
que la grâce, de laquelle il veut remplir vos âmes

 

1 _Matth.,_ V, 6.— 2 _Joan.,_ IV, 15.

 

496

 

dans cette fonction sainte que je viens exercer chez vous : car si nous
ne méritons pas que ces eaux soient en nous pour nous-mêmes, nous les
avons toutefois pour les répandre dans les autres. La source en est dans
l'Église : elle est dans mon ministère poulies épancher dans vos cœurs,
puisque par mon caractère et en qualité de son ministre, quoiqu'indigne,
je vous représente sa personne. Vous en serez toutes pénétrées dans
cette action sainte, si vous n'y apportez qu'un esprit soumis et détaché
de toutes choses.

La grâce est, selon la théologie, une qualité spirituelle que
Jésus-Christ répand dans nos âmes, laquelle pénètre le plus intime de
notre substance, qui s'imprime dans le plus secret de nous-mêmes, et qui
se répand dans toutes les puissances et les facultés de l'âme qui la
possède intérieurement, la rend pure et agréable aux yeux de ce divin
Sauveur, la fait être son sanctuaire, son tabernacle, son temple, enfin
son lieu de délices. Quand une âme est ainsi toute remplie, l'abondance
de ces eaux rejaillit jusqu'à la vie éternelle, c'est-à-dire qu'elle
élève cette âme jusqu'à l'heureux état de la perfection. N'est-ce pas ce
que dit Jésus-Christ : « Des fleuves sortiront de son ventre (1) : » la
fontaine de ces eaux vives rejaillissant jusqu'à la vie éternelle, qui
est précédée ici-bas de la grâce et de la sainteté. On voit
l'épanchement de ces eaux jusque sur les sens extérieurs : sur les yeux
par la modestie, dans les paroles par le silence religieux et par une
sainte circonspection et retenue à parler ; en un mot, une personne
paraît mortifiée en toutes ses actions ; elle se montre partout possédée
de la grâce au dedans d'elle-même, contraire à l'esprit du monde,
ennemie de la nature et des sens, mais toute pleine des vertus et de
l'esprit de Jésus-Christ.

Je ne sais, mes Filles, si vous avez assez bien pesé l'importante vérité
contenue en ces paroles de saint Paul (2), lorsqu'il dit qu'il est
crucifié au monde et que le monde est crucifié pour lui ? Ces paroles
renferment, si vous y prenez garde, toute la perfection religieuse, à
laquelle vous devez sans cesse aspirer. Etre crucifié au monde, c'est y
renoncer, n'y plus penser, n'avoir que du

 

1 _Joan.,_ VII, 38.— 2 _Galat.,_ VI, 14.

 

497

 

dégoût et de l'aversion de toutes ses maximes, avoir du mépris pour
l'honneur et pour tout ce qui est vain, mépriser le plaisir et tout ce
que le monde estime, n'avoir plus la moindre attache à tout ce qui
s'appelle complaisance en vous-mêmes ; au contraire, faire état partout
et en toutes choses de la simplicité chrétienne, et de l'esprit de la
croix de Jésus-Christ : voilà ce que c'est d'être crucifié au monde.
Mais ce n'est pas encore assez ; il faut que le monde soit crucifié pour
vous. C'est, mes Filles, que vous ne devez pas seulement oublier ce
malheureux monde, mais aussi le monde vous doit oublier ; et pour vivre
saintement dans votre état, vous devez souhaiter d'en être oubliées,
vous devez désirer d'être effacées de sa mémoire, comme des personnes
mortes et ensevelies avec Jésus-Christ.

Considérez-vous comme mortes au monde, et qu'il est pareillement mort
pour vous. Dès que vous vous êtes ensevelies dans le sépulcre de la
religion, vous séparant du monde, vous avez dû mourir à tout le sensible
par la mortification et un renoncement total à tout ce qui est mortel et
terrestre. Faites donc maintenant vivre Jésus-Christ en vous par sa
grâce : ne respirez que pour lui ; n'agissez que par son esprit, et
soyez-en parfaitement possédées : mourez tous les jours à votre esprit
propre et à votre jugement, le soumettant à l'obéissance : mourez à vos
désirs et à vos sens ; mourez à vous-mêmes ; étouffez le plus petit
mouvement de la concupiscence, dès qu'il s'élève en vous. Enfin, mes
Sœurs, rendez le dernier soupir de la vie imparfaite et encore tant soit
peu engagée dans les illusions du monde ; dites-lui un adieu général et
éternel : autrement, si vous ne mourez de cette mort mystique, prenez
garde que quelque reste dangereux de la corruption de ce monde
malheureux ne dessèche et ne détruise en vos âmes ces eaux de grâce que
je viens y verser par cette visite, ou même ne vous rende incapables de
les recevoir et ne les empêche d'entrer.

Il en est des objets du monde qui offusquent notre imagination, qui
occupent et amusent notre esprit, comme d'une fontaine pleine d'eau vive
qui ne pourrait rejaillir, ni même retenir ses eaux, si le conduit en
était  bouché, parce que la liberté de couler

 

498

 

et de se répandre lui étant ôtée, cette fontaine sans doute viendrait à
sécher et la source en tarirait. La même chose arrive à l'égard de ces
eaux de grâce dont je désire remplir votre cœur. Si ce même cœur est
encore prévenu d'inclinations inquiètes, ou occupé des objets de la
terre ; si le monde, ou quoi que ce soit de créé, vous remplit l'esprit
et possède votre affection ; s'il a quelque pouvoir d'y faire des
impressions, et s'il se propose encore à vos sens comme un objet
attrayant, vous deviendrez comme cette fontaine, vous ne pourrez
recevoir ces saintes et mystiques eaux, parce qu'il est impossible de
remplir ce qui est déjà plein : ou bien vous ne pourrez conserver
longtemps ces grâces dont nous vous parlons ; car l'esprit du monde et
l'esprit de Jésus-Christ ne sauraient compatir ensemble, et ne peuvent
demeurer dans une âme. Ces eaux divines ne rejailliront point jusqu'à la
vie éternelle , à moins que pour les conserver vous ne vous dégagiez
entièrement de tout ce qui vous empêche de vivre à Jésus-Christ et de sa
divine vie ; à moins que vous ne deveniez insensibles comme des
personnes mortes et crucifiées au monde, qui l'ont mis si fort en oubli,
qu'elles ne pensent jamais à lui qu'avec horreur ou avec compassion de
tant d'âmes qui sont emportées par sa corruption, et afin de vous
employer sans cesse à demander miséricorde pour ce monde malheureux, qui
retient tant de personnes continuellement exposées au danger de se
perdre et de se damner pour jamais.

Vous le devez, mes Filles ; ce sont les obligations de votre état. Je
vous exhorte de tout mon pouvoir à vous en acquitter avec grand soin.
Offrez sans cesse des prières à la divine Majesté pour toutes les
nécessités de l'Église : priez pour obtenir la conversion des infidèles,
des pécheurs et des mauvais chrétiens ; et demandez à Dieu qu'il touche
leurs cœurs. Gémissez devant lui pour tant de prêtres qui déshonorent
leur caractère, qui profanent les choses saintes, et qui ne vivent pas
conformément à leur dignité et à la sainteté de leur état. Affligez-vous
pour ces femmes et ces filles mondaines, qui n'ont point cette pudeur
qu'elles devraient avoir, qui est l'ornement de votre sexe ; pour tant
de chrétiens et de chrétiennes, qui s'abandonnent à toutes leurs
inclinations déréglées,

 

499

 

et qui suivent malheureusement les pernicieuses maximes du monde et ses
damnables impressions. Ayez, mes Filles, du zèle et de la charité pour
toutes ces personnes qui sont dans le chemin de perdition, prêtes à
tomber dans des abîmes éternels. Faites monter vos prières au ciel comme
un encens devant le trône de Dieu, pour apaiser sa colère irritée contre
tous ces pécheurs qui l'offensent si outrageusement. Revêtez-vous des
entrailles de miséricorde : pleurez sur ces grands maux, pour ces
nécessités, et pour tant de misères qui vraiment sont dignes de
compassion et de larmes. Voilà, mes Sœurs, de quelle manière vous devez
conserver le souvenir du monde ; c'est ainsi qu'il faut y penser, et non
autrement : hors de là il vous doit être à dégoût ; tout vous y doit
être fort indifférent, et ne doit point entrer dans vos pensées.

Que toute votre occupation d'esprit soit de vous appliquer sérieusement
à opérer votre salut, en travaillant pour vous avancer à la perfection
où vous êtes obligées de tendre sans cesse ; vous ne vous sauverez pas,
si vous n'y aspirez avec amour et ferveur le reste de vos jours.
Renouvelez donc en vous ce désir dans cette visite que je commence
aujourd'hui, à ce dessein de vous porter toutes à la perfection et pour
vous sanctifier. Pour correspondre de votre part à nos intentions,
souvenez-vous de ces paroles portées dans l'Évangile, que Jésus-Christ
prononça avec tant de zèle et tant de douceur : «Venez à moi, dit-il,
vous qui êtes travaillés et chargés de quelque peine, et je vous
soulagerai (1). » Je vous dis la même chose, mes Filles ; je vous
adresse les mêmes paroles, en vous conviant toutes de venir m'ouvrir vos
cœurs sans crainte : dites-moi avec confiance tout ce qui vous pèse,
tout ce qui vous fait peine, je vous soulagerai. Venez donc à moi sans
rien craindre ; apportez-moi un cœur sincère, un cœur parfaitement
soumis et un cœur simple : ce sont les dispositions que je veux voir, et
que je demande de vous toutes, et avec lesquelles vous devez venir en ma
présence. Déclarez-moi tout ce qu'en conscience vous voyez être
nécessaire ou utile que je connaisse pour le bien de votre communauté :
je vous y oblige ; je vous ordonne de ne me rien

 

1 _Matth.,_ XI, 28.

 

500

 

soustraire, par tout ce saint pouvoir que j'exerce en vertu de mon
caractère.

Je vous dénonce de la part de Dieu tout-puissant au nom duquel je vous
parle, par l'autorité que je tiens de lui et par tout l'empire qu'il me
donne sur vous toutes et sur chacune de vos âmes, que si vous êtes
sincères et sans déguisement, je demeurerai chargé de tout ce que vous
me direz : au contraire, ce que vous voudrez me cacher et me taire, je
vous déclare que je vous en charge vous-mêmes, et que ce sera un poids
qui vous écrasera. Prenez garde à ceci, mes Sœurs ; ne taisez pas ce
qu'il est utile de me dire, non tant pour vous décharger que pour nous
donner les connaissances nécessaires : ne m'apportez que des choses
véritables et utiles pour la communauté ou pour votre particulier ;
qu'il n'y ait rien d'inutile : mais parlez-moi avec franchise et ne
craignez point de me fatiguer, puisque je veux bien vous écouter, et
vous donner tout le temps que vous pouvez souhaiter pour votre
instruction et pour votre consolation. Vous ne me serez point à charge ,
tant que je verrai en ce que vous me direz de l'utilité pour vous ou
pour le public : au contraire je vous écouterai, je vous répondrai selon
les mouvements de Dieu et avec les paroles qu'il me mettra en la bouche.
Ainsi vous serez instruites, et vous recevrez les secours dont vous
pouvez avoir besoin ; et moi je vous dirai ce que son divin Esprit me
donnera pour vous, chacune selon ce que je verrai qui lui sera propre,
pour procurer votre perfection et votre paix : car je désire profiter à
tout le monde, et qu'il n'y ait pas une de vous qui ne prenne en cette
visite l'esprit d'un saint renouvellement en la perfection de son état.
Je vous y porterai toutes en général, et chacune en particulier. Dieu
m'envoie à vous pour détruire Ninive ; c'est-à-dire pour déraciner
jusqu'aux plus petites inclinations de la nature corrompue et toutes les
imperfections contraires à votre sainteté. Si ce peuple fit pénitence à
la voix d'un prophète et s'il se rendit docile à sa parole, comme nous
l'avons lu en la sainte Epître de ce jour, avec quelle docilité
devez-vous coopérer à notre dessein et n'y apporter nul obstacle ?

Venez donc à moi, mes Filles, avec un grand zèle de votre

 

501

 

avancement et un saint désir de la perfection : ne craignez point de me
découvrir vos besoins ; ouvrez-moi vos consciences, et n'hésitez pas de
me dire tout ce qui sera pour votre bien et même pour votre consolation.
Je sais que l'office des pasteurs des âmes est de confirmer les fortes,
et de compatir aux infirmes, de les consoler en leurs faiblesses, de les
soulever et de les charger sur leurs épaules : c'est ce que je me
propose de faire en cette visite. Les fortes , nous travaillerons à les
animer de plus en plus à la perfection et à les transporter jusqu'au
ciel ; les faibles, nous les encouragerons, nous nous abaisserons
jusqu'à leurs faiblesses pour les relever et les fortifier, nous les
porterons sur nos épaules ; et les unes et les autres, nous les
animerons et nous tâcherons de les faire marcher et de les élever toutes
à la perfection où elles sont appelées. En un mot, nous désirons réparer
tout ce qui serait déchu en l'observance régulière, rallumer ce qui
serait éteint en la charité et établir une ferme et solide paix. A cet
effet je prétends réunir tout ce qui serait tant soit peu divisé ; je
viens établir la concorde, en dissipant les plus faibles dispositions et
les plus légers sentiments contraires. Je veux ruiner et anéantir
jusqu'au plus petit défaut contraire à la charité, et détruire tous les
empêchements de la parfaite union jusqu'aux moindres fibres. Il faut
réparer toutes les ruines de cette vertu et remédier à tout ce qui s'y
oppose, pour faire fleurir l'ordre et la perfection dans votre
communauté. Pour cela ne négligez aucune des déclarations sincères et
véritables qui seront requises, puisque les connaissances que vous me
donnerez me serviront à faire régner Jésus-Christ par une charité
parfaite et une paix inaltérable en ce monde, qui vous conduira au repos
éternel de l'autre. C'est ce que je vous souhaite à toutes ; cependant
je prie Dieu qu'il vous bénisse et qu'il vous remplisse de ses grâces.

 

[Précédente]

[Accueil]

[Suivante]

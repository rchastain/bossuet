[III PENTECÔTE]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Vendredi Saint
II Vendredi Saint
III Vendredi Saint
IV Vendredi Saint
I Pâques Jour
II Pâques Jour
III Pâques Jour
IV Pâques Jour
Pâques Jour abrg
Quasimodo
Pâques IIIe Dim.
Pâques IIIe Dim. abr.
Pâques Ve Dim.
Ascension
Pentecôte I
Pentecôte II
Pentecôte III
Pentecôte abr.
Trinité
III Pentecôte
V Pentecôte
IX Pentecôte
XXI Pentecôte abr.
Sainte Croix I
Sainte Croix II
Précis Ste Croix
Exh. Nvles Catholiques
Vie Xtienne
Ursulines Meaux I
Ursulines Meaux II
Ord. Ursul. Meaux
Ursulines Meaux III
Ursulines Meaux IV
Conf. Ursul. Meaux
Instr. Ursul. Meaux
Profess. Ursuline
Disc. Visitandines
Disc. Union J-C / Épouse
Pensées Xtiennes
Pensées Détachées

 



SERMON POUR LE IIIE DIMANCHE APRÈS LA PENTECOTE (_A_).


 

_Dico vobis : Gaudium erit in cœlo super uno peccatore pœnitentiam
agente, plus quàm super nonaginia novem justis, qui non indigent
pœnitentià_.

 

Je vous dis qu'il y aura plus de joie au ciel devant les anges de Dieu
sur un pécheur faisant pénitence, que sur quatre-vingt-dix-neuf justes
qui n'ont pas besoin de pénitence. _Luc,_ XV, 7.

 

Si quelqu'un n'a pas encore assez entendu combien est grande la charité
des saints anges pour les misérables mortels, qu'il

 

(_a_) Prêché vers 1660.

Ce sermon renferme des expressions comme celles-ci : « Veux-tu pas
restituer? accoutumance, mondes et immondes, passer plus outre, les
forces se diminuent tous les jours, le bon berger va après sa brebis, »
etc. D'un autre côté l'écriture du manuscrit a plus de fermeté et de
régularité, la marche plus de mouvement et plus d'entrain, le style plus
de souplesse et d'ampleur, les pensées plus de force et plus d'élévation
que dans les premières compositions de l'auteur. Deux considérations qui
justifient notre date, en l'appelant pour ainsi dire entre l'époque de
Metz et l'époque de Paris.

 

371

 

considère (_a_) en notre évangile les aimables paroles du Sauveur des
âmes, par lesquelles il nous apprend que la conversion des pécheurs
réjouit tous les esprits bienheureux ; et qu'encore que Dieu les enivre
du torrent de ses éternelles délices, néanmoins ils sentent augmenter
leur joie, quand nous sommes renouvelés par la pénitence. Nous lisons
dans les Écritures ' qu'autrefois les esprits célestes se déclarèrent
visiblement contre nous, lorsqu'un chérubin envoyé de Dieu avec une
forme terrible, tenant en sa main un glaive de feu, gardait la porte du
paradis pour épouvanter nos parents rebelles, et leur interdire l'entrée
de ce jardin délicieux qu'ils avaient déshonoré par leur crime. Mais
après la naissance de ce Sauveur qui nous a réconciliés par son sang,
vous n'ignorez pas, chrétiens, que ces bienheureuses intelligences qui
nous avaient déclaré la guerre, nous vinrent aussi annoncer la paix : «
Que la paix, disent-ils, (_b_) (2), soit donnée aux hommes! » Et depuis
cette salutaire journée nous leur sommes devenus si chers, que
Jésus-Christ nous enseigne dans notre évangile qu'ils préfèrent nos
intérêts aux leurs propres. C'est ce que vous remarquerez aisément, si
vous pénétrez le sens des paroles que j'ai alléguées pour mon texte. «
Les anges, dit le Fils de Dieu, se réjouissent plus de la conversion
d'un pécheur que de la persévérance de quatre-vingt-dix-neuf justes qui
n'ont pas besoin de pénitence. » Je demande quels sont ces justes
auxquels le Sauveur ne craint pas de dire que la pénitence n'est pas
nécessaire. Certes nous ne les trouverons pas sur la terre, puisque tous
les hommes étant pécheurs, ce serait une témérité inouïe que d'assurer
qu'ils n'ont pas besoin du remède de la pénitence. « Si quelqu'un dit
qu'il ne pèche pas, il se trompe et la vérité n'est pas en lui, » dit le
Disciple bien-aimé de notre Sauveur (3).

Où chercherons-nous donc, chrétiens, cette innocence si pure et si
achevée, qu'elle n'a pas besoin de la pénitence? Sans doute puisqu'elle
est bannie du milieu des hommes, elle ne se peut rencontrer que parmi
les anges, qui détestant la rébellion et

 

1 _Genes_., III, 24. — 2 _Luc.,_ II, 14. — 3 I _Joan.,_ I, 8.

 

(a) _Var._ : Ecoute. — 2 Disaient-ils.

 

372

 

l'audace de Satan et de ses complices, demeurèrent immuablement dans le
bien où Dieu les avait établis dès leur origine. Vous êtes les seuls, ô
esprits célestes, parmi toutes les créatures, qui jamais n'avez été
souillés par aucun péché ; vous êtes ces justes de notre évangile,
auxquels la pénitence n'est pas nécessaire ; et ainsi lorsque notre
Sauveur nous apprend que vous recevez une joie plus grande de la
conversion des pécheurs que de la justice des innocents qui n'ont pas
besoin de se repentir, c'est de même que s'il nous disait que notre
pénitence vous réjouit plus que votre propre persévérance. Merveilleuse
vertu de la pénitence, qui oblige tous les saints anges à nous préférer
à eux-mêmes, qui répare si glorieusement les ruines des plus grands
pécheurs, qu'elle les met en quelque sorte au-dessus des justes, et qui
fait que la justice rendue a quelque avantage au-dessus de la justice
toujours conservée! Car puisque ces intelligences célestes, qui goûtent
le vrai bien dans sa source, ne peuvent avoir de ces joies déréglées que
l'opinion fait naître en nos âmes, ne voyez-vous pas, chrétiens,
qu'elles ne se peuvent réjouir que du bien? Et donc, si leur joie est
plus abondante, ne faut-il pas conclure nécessairement qu'il leur paraît
quelque bien plus considérable, d'autant plus que c'est le Sauveur
lui-même qui les excite par son exemple à cette sainte et divine joie?

En effet ne voyez-vous pas qu'il se présente à nous dans notre évangile
sous la figure de ce berger « qui laisse tous ses troupeaux au désert
pour chercher une brebis égarée, qui l'ayant trouvée au milieu des bois
seule et tremblante d'effroi, la rapporte sur ses épaules et appelant
ses amis et ses proches : Réjouissez-vous avec moi, dit-il, de ce que
j'ai rencontré ma brebis perdue (1).» De sorte que les anges et le
Sauveur même se réjouissant plus d'un pécheur sauvé que d'un juste qui
persévère, il paraît que l'innocence recouvrée a quelque chose de plus
agréable que l'innocence continuée. Réjouissons-nous, pécheurs
misérables ; admirons la force de la pénitence, qui nous rend avec
avantage ce que notre péché nous avait fait perdre ; et pour exciter en
nos cœurs les saints gémissements de la pénitence, recherchons les
véritables

 

1 _Luc,_ XV, 4 et suiv.

 

373

 

raisons de cette vérité si satisfaisante que Jésus-Christ nous enseigne
dans son Évangile.

Si je n'avais qu'à vous parler d'une joie humaine, je me contenterais de
vous dire que nous expérimentons tous les jours une certaine douceur
plus sensible à rentrer dans la possession de nos biens qu'à nous
maintenir dans la jouissance ; nous goûtons la santé par la maladie, et
la perte de nos amis nous apprend combien ils nous étaient nécessaires.
Car l'accoutumance nous ôte ce qu'il y a de plus vif dans le sentiment ;
et notre jugement est si faible que ne pouvant pénétrer les choses en
elles-mêmes, il ne les reconnaît jamais mieux que par leurs contraires :
tellement que cet excès de joie que nous ressentons, lorsque nous
pouvons réparer nos pertes, vient presque toujours de notre faiblesse.
Mais à Dieu ne plaise que nous croyions qu'il en soit ainsi de la joie
des anges et de celle du Fils de Dieu même, dont nous devons aujourd'hui
expliquer les causes! Il faut prendre des principes plus relevés, si
nous voulons pénétrer de si grands mystères. Entrons en matière, et
disons : Tout le motif de la joie du Fils, c'est la gloire de Dieu son
Père ; tout le motif de la joie des anges, c'est la gloire de leur
Créateur. Si donc ils se réjouissent si fort dans la conversion des
pécheurs, c'est que la gloire de Dieu y paraît avec plus de
magnificence. Prouvons solidement cette vérité.

La gloire de Dieu éclate singulièrement dans les natures intelligentes
par sa miséricorde et par sa justice : sa providence, son immensité, sa
toute-puissance paraissent dans les créatures inanimées ; mais il n'y a
que les raisonnables qui puissent ressentir les effets de sa miséricorde
et de sa justice ; et ce sont ces deux attributs qui établissent sa
gloire et son règne sur les natures intelligentes. C'est par la
miséricorde et par la justice que les anges et les hommes sont sujets à
Dieu ; la miséricorde règne sur les bons, la justice sur les criminels :
l'une par la communication de ses dons, l'autre par la sévérité de ses
lois ; l'une par douceur, et l'autre par force ; l'une se fait aimer,
l'autre se fait craindre ; l'une attire, et l'autre réprime ; l'une
récompense la fidélité, l'autre venge la rébellion : si bien que la
miséricorde et la justice sont en quelque sorte les deux mains de Dieu,
dont l'une donne, et

 

374

 

l'autre châtie : ce sont les deux colonnes qui soutiennent la majesté de
son règne ; l'une élève les innocents, l'autre accable les criminels,
afin que Dieu domine sur les uns et sur les autres avec une égale
puissance. C'est pourquoi le Prophète chante : « Toutes les voies du
Seigneur sont miséricorde et vérité (1) ; » c'est-à-dire miséricorde et
justice selon l'interprétation des docteurs, d'autant que la justice de
Dieu c'est sa vérité, parce que, comme dit le grand saint Thomas (2),
c'est à cause de sa vérité qu'il est la loi éternelle et qu'il est la
loi immuable qui règle toutes les créatures intelligentes. Que si toutes
les voies du Seigneur sont miséricorde et justice, si ce sont ces deux
divins attributs qui établissent sa gloire et son règne, je ne m'étonne
plus, ô saints anges, de ce que la pénitence vous comble de joie. C'est
que vous y voyez éclater magnifiquement la gloire de Dieu votre créateur
par sa miséricorde et par sa justice : la miséricorde dans la
conversion, la justice dans la satisfaction ; la première dans la
rémission des péchés, la seconde dans les gémissements des pécheurs.

 


PREMIER  POINT.

 

Pour entrer d'abord en matière, je remarquerai dans notre évangile trois
effets de la miséricorde divine dans la conversion des pécheurs : Dieu
les cherche, Dieu les trouve, Dieu les rapporte. C'est ce que nous
lisons clairement dans la parabole de notre évangile. «Le bon berger,
dit le Fils de Dieu, va après sa brebis perdue : » _Vadit ad illam quœ
perierat_ ; « et il va jusqu'à ce qu'il la trouve : » _donec inveniat
eam_ (3) ; « et après qu'il l'a retrouvée, il la charge sur ses épaules.
» C'est la véritable figure du Sauveur des âmes ; il cherche
charitablement les pécheurs, suivant ce qu'il dit dans son Évangile : «
Le Fils de l'homme est venu chercher ce qui était perdu (4) ; » il les
trouve par la vertu de sa grâce : car il est ce Samaritain
miséricordieux, « qui trouvant en son chemin le pauvre blessé, est
touché de miséricorde, et s'approche, et ne dédaigne pas de lier ses
plaies : » _Et alligavit vulnera ejus_ (5). Enfin il les porte sur ses
épaules, parce que c'est

 

1 _Psal._ XXIV, 10. — 2 I-II, Quaest. XCIII, art. 2. — 3 _Luc,_ XV, 4. —
4 _Ibid.,_ XIX, 10. — 5 _Ibid.,_ X, 34.

 

375

 

lui dont il est écrit : « Vraiment il a porté nos langueurs : » _Verè
languores nostros ipse tulit_ (1). Or cette triple miséricorde répond à
la triple misère en laquelle est précipitée l'âme pécheresse. Elle
s'écarte, elle fuit, elle perd ses forces et devient entièrement
impuissante. Elle s'éloigne du bon Pasteur, et s'en éloignant elle
oublie, elle ne connaît plus son visage ; tellement que lorsqu'il
approche, elle fuit, et fuyant elle se fatigue et tombe dans une extrême
impuissance. Mais le Pasteur infiniment bon, qui ne se plaît qu'à sauver
les âmes, oppose charitablement à ces trois misères trois effets
merveilleux de miséricorde. Car il cherche sa brebis éloignée ; il
trouve et il atteint sa brebis fuyante ; il rapporte sur ses épaules
cette pauvre brebis épuisée de forces. Apprenons ici à connaître la
miséricorde du Pasteur fidèle, qui nous a sauvés au péril de sa propre
vie.

Et premièrement remarquons ce qui est écrit dans notre évangile, que la
brebis que le Sauveur cherche n'est plus en la compagnie de tout le
troupeau, par conséquent elle est séparée ; mais entendons le sens de
cette parole. Le troupeau du Fils de Dieu, c'est l'Église, et celui qui
est séparé du troupeau semble être hors de la vraie Église. Dirons-nous
que le Fils de Dieu ne parle en ce lieu que des hérétiques qui ont rompu
le lien d'unité? Mais la suite de notre évangile réfutera manifestement
cette explication, puisque Jésus-Christ nous fuit bien entendre qu'il
parle généralement de tous les pécheurs, parce qu'il veut encourager
tous les pénitents. Mais pourrons-nous dire, fidèles, que tous les
pécheurs sont séparés du sacré troupeau et de la communion de l'Église?
Nullement ; il n'en est pas de la sorte : c'est l'erreur de Calvin et des
calvinistes, contre laquelle le Fils de Dieu nous a dit qu'il y a de
l'ivraie même dans son champ, qu'il y a du scandale même en sa maison,
qu'il y a de mauvais poissons même en ses filets (2). Mais d'où vient,
direz-vous, que notre Sauveur, nous figurant tous les pécheurs en notre
évangile, les représente comme séparés du troupeau? Entrons en sa
pensée, et disons avec l'incomparable saint Augustin : « Il y en a qui
sont dans la maison de Dieu, et qui ne sont pas la maison de Dieu ; il y
en a qui sont dans la maison

 

1 _Isa_., LIII, 4. — 2 _Matth.,_ XIII, 28, 41, 48.

 

376

 

de Dieu, et qui sont eux-mêmes la maison de Dieu : » _Altos ita esse in
domo Dei, ut ipsi etiam sint eadem domus Dei_ (1). Expliquons la
doctrine de ce grand évêque.

Les justes sont en la maison de Dieu, et ils sont eux-mêmes la maison de
Dieu, selon ce que dit le Prophète : « J'habiterai au milieu de vous
(2) ; » et l'Apôtre : « Ne savez-vous pas que vous êtes les temples de
l'Esprit de Dieu (3)? » Mais les méchants qui sont en l'Église qui est
la maison que Dieu a choisie, ne sont pas la maison choisie ; Dieu
n'habite pas en leurs cœurs ; ils ne sont pas les pierres vivantes de ce
miraculeux édifice, dont les fondements sont posés en terre, et dont le
sommet égale les cieux. « Ils sont dans l'Église, dit saint Augustin,
comme la paille est dans le froment, » _Sicut palea esse dicitur in
frumentis,_ « parce qu'encore qu'ils soient liés par les sacrements,
néanmoins ils sont séparés de cette invisible unité qui est assemblée
par la charité : » _Cùm intùs videantur, ab illâ invisibili charitatis
compage separati sunt_. — _Alios ita dici esse in domo, ut non
pertineant ad compagem domûs, nec ad societatem fructiferœ pacificœque
justitiœ ; sed sicut esse palea dicitur in frumentis : nam et istos esse
in domo negare non possumus, Apostolo dicente : In magnà autem domo non
solùm aurea vasa sunt vel argentea, sed et lignea et fictilia, et alia
quidem sunt in honorem, alia vero in contumeliam_ (4).

Par où nous voyons clairement qu'il y a double unité dans l'Église :
l'une est liée par les sacrements qui nous sont communs ; en celle-là
les mauvais y entrent, quoiqu'ils n'y entrent qu'à leur condamnation.
Mais il y a une autre unité invisible et spirituelle, qui joint les
saints par la charité, qui en fait les membres vivants : à cette paix, à
cette unité, à cette concorde, il n'y a que les justes qui y
participent ; les impies n'y ont point de place, ils en sont excommuniés.
Il y a une arche, à la vérité, qui renferme tous les animaux mondes et
immondes, il y a un champ qui porte le bon et le mauvais grain ; «mais
il y a une colombe et une parfaite, » qui ne reçoit en son sein que les
vrais fidèles, qui vivent en l'unité par la charité : _Una est columba
mea, perfecta mea_ (5). C'est

 

1 _De Bapt. cont. Donat_., lib. VII, n. 99. — 2 _Levit_. XXVI, 12 ; II
_Cor.,_ VI, 16. — 3 I _Cor.,_ III, 16. — 4 Loco mox citato. II
_Timoth.,_ II, 20. — 5 _Cant_., VI, 8.

 

377

 

pourquoi le Sauveur des âmes représente tous les pécheurs comme séparés
du troupeau, parce qu'ils sont exclus par leurs crimes de cette
invisible société qui unit les brebis fidèles en la charité de
Notre-Seigneur. Et pour vous faire voir, chrétiens, qu'ils ne sont plus
avec le troupeau, c'est que le céleste et divin Pasteur ne leur donne
plus la même pâture. Dites-moi, quel est le pain des fidèles, quelle est
la nourriture des enfants de Dieu? n'est-ce pas le pain de
l'Eucharistie, ce pain céleste et vivifiant que nous recevons de ces
saints autels? Cette sainte et divine table est-elle préparée aux
impies, dont les consciences sont infectées de péchés mortels?
Nullement, ils en sont exclus ; s'ils sont si téméraires que d'en
approcher, ils y prendront un poison mortel, au lieu d'une viande
d'immortalité.

Reconnais donc, pécheur misérable, que tu es séparé du troupeau fidèle,
puisque tu es privé de la nourriture que le vrai Pasteur lui a destinée
(_a_). Et ne me réponds pas : Je suis de l'Église, je demeure en ce
corps mystique. Car que sert au bras gangrené de tenir encore au reste
du corps par quelques nerfs qui n'ont plus de force? que lui sert,
dis-je, de tenir au corps, puisqu'il est si fort éloigné du cœur, qu'il
ne peut plus en recevoir aucune influence? Quelque union qui paroisse au
dehors, il y a une prodigieuse distance entre la partie vivante et la
partie morte. Il en est de même de toi, ô pécheur. Il ne te sert de rien
d'être dans le corps, puisque tu es entièrement séparé du cœur. Le cœur
de l'Église, c'est la charité. C'est là qu'est le principe de vie, c'est
de là que se répand la chaleur vitale : si bien que n'étant pas en la
charité, bien qu'il te soit permis d'entrer au dehors, tu es excommunié
du dedans. Ne me vante point ta foi, qui est morte ; ne me dis pas que tu
t'assembles avec les fidèles. Les hommes t'y reçoivent, mais Dieu t'en
sépare ; le corps s'en approche, il est vrai, mais l'âme en est
infiniment éloignée ; la vie et la mort ne s'accordent pas. Considère
donc, misérable ! combien tu es loin des membres vivants, puisqu'il est
certain que tu perds la vie. C'est pour cette raison que le Fils de Dieu
les représente dans la parabole de notre évangile comme exclus, comme
excommuniés du troupeau, parce qu'étant

 

(_a_) _Var._ : Lui a préparée.

 

378

 

des membres pourris, ils ne participent point à la vie. C'est pourquoi
le pain de vie leur est refusé ; c'est pourquoi ils sont séparés du
banquet céleste, qui est la vie du peuple fidèle. D'où passant plus
outre, je dis qu'étant séparés de cette unité, ils commencent leur enfer
même sur la terre, et que leurs crimes les y font descendre. Car ne nous
imaginons, pas que l'enfer consiste dans ces épouvantables tourments,
dans ces étangs de feu et de soufre, dans ces flammes éternellement
dévorantes, dans cette rage, dans ce désespoir, dans cet horrible
grincement de dents. L'enfer, si nous l'entendons, c'est le péché même ;
l'enfer, c'est d'être éloigné de Dieu : et la preuve en est évidente par
les Écritures.

Job nous représente l'enfer en ces mots : « C'est un lieu, dit-il, où il
n'y a nul ordre, mais une horreur perpétuelle (1) . » De sorte que
l'enfer c'est le désordre et la confusion. Or le désordre n'est pas dans
la peine : au contraire, j'apprends de saint Augustin (2) que la peine,
c'est l'ordre du crime. Quand je dis _péché,_ —je dis le désordre, parce
que j'exprime la rébellion. Quand je dis péché puni, je dis une chose
très-bien ordonnée. Car c'est un ordre très-équitable que l'iniquité
soit punie. D'où il s'ensuit invinciblement que ce qui fait la confusion
dans l'enfer, ce n'est pas la peine, mais le péché. Que si le dernier
degré de misère, ce qui fait la damnation et l'enfer, c'est d'être
séparé de Dieu, qui est la véritable béatitude ; si d'ailleurs il est
plus clair que le jour que c'est le péché qui nous en sépare, comprends,
ô pécheur misérable, que tu portes ton enfer en toi-même, parce que tu y
portes ton crime , qui te fait descendre vivant en ces effroyables
cachots où sont tourmentées les âmes rebelles. Car comme l'apôtre saint
Paul, parlant des fidèles qui vivent en Dieu par la charité, assure «
que leur demeure est au ciel, et leur conversation avec les anges (3), »
ainsi nous pouvons dire très-certainement que les méchants sont abîmés
dans l'enfer, et que leur conversation est avec les diables. Etrange
séparation du pécheur, qui trouve son enfer même en cette vie ! et
n'est-il pas juste qu'il trouve l'enfer, puisqu'il est séparé du sacré
troupeau, que la charité fait vivre en Notre-Seigneur?

Mais peut-être vous répondrez que le pécheur se peut relever,

 

1 _Job_., X, 22. — 2 _Ad Honorat_., ep. CXL, n. 4. — 3 _Philipp_., III,
20.

 

379

 

et que l'enfer n'a point de ressource. Ah ! ne nous flattons point de
cette pensée : la blessure que fait le péché est éternelle et
irrémédiable. Mais Dieu, direz-vous, y peut remédier. Il le peut, à
cause qu'il est tout-puissant ; ce qui n'empêche pas que la maladie ne
soit incurable de sa nature. Concevons ceci, chrétiens. L'orgueilleux
Nabuchodonosor a fait jeter les trois saints enfants dans la fournaise
de flammes ardentes (1) : autant qu'il est en lui, il les a brûlés,
encore que Dieu les ait rafraîchis. Ainsi lorsque nous commettons un
péché mortel, nous donnons tellement la mort à notre âme, qu'encore que
Dieu nous puisse guérir, néanmoins de notre côté nous rendons et notre
péché, et notre damnation éternels, parce que nous éteignons la vie
jusqu'à la racine. Il faut regarder ce que fait le péché, non ce que
fait la Toute-Puissance. Qui renonce une fois à Dieu, y renonce
éternellement (_a_), parce que c'est la nature du péché de faire autant
qu'il le peut une séparation éternelle. C'est pourquoi le Prophète-Roi
se considérant dans le crime, se considère comme dans l'enfer, à cause
de cette effroyable séparation : _Aestimatus sum cum descendentibus in
lacum_ (2) : « Je suis, dit-il, compté parmi ceux qui descendent dans le
cachot ; » et après : « Ils m'ont mis dans le lac inférieur, dans les
ténèbres, et dans l'ombre de la mort : » _Posuerunt me in lacu
inferiori_ (3). Et de là vient qu'il s'écrie dans sa pénitence : _De
profundis clamavi ad te, Domine_ (4) : « Seigneur, je crie à vous des
lieux profonds ; » et rendant grâces de sa délivrance : « Vous avez,
dit-il, retiré mon âme de l'enfer inférieur (5). » C'est que ce saint
homme avait bien conçu que le péché est un abîme et une prison, un
gouffre, un cachot, un enfer.

Dans ce cachot et dans cet abîme où nos crimes nous précipitent, quelle
espérance aurions-nous, fidèles, si Dieu ne nous avait donné un
Libérateur, qui étant venu au monde pour notre salut, a bien voulu même
aller aux enfers pour achever un si grand ouvrage? C’est ce même
Libérateur qui est descendu aux enfers, qui daigne descendre encore tous
les jours dans l'enfer des consciences

 

1 _Dan_., III, 21. — 2 _Psal._ LXXXVII, 5. — 3 _Ibid.,_ 1. — 4 _Psal._
CXXIX, 1. — 5 _Psal._ LXXXV, 13.

 

(_a_) _Var._ : Pour jamais.

 

380

 

criminelles. Car certes vous y descendez, ô Sauveur, lorsque vous faites
luire en nos âmes, au milieu des ténèbres où elles languissent, les
belles et éclatantes lumières de vos divines inspirations. C'est ainsi,
ô Pasteur miséricordieux, que vous cherchez votre brebis égarée ; votre
amour vous transporte à un tel excès, que vous la cherchez jusque dans
l'enfer, parce que vous la cherchez jusque dans le crime. Figurez-vous
ici, chrétiens, quel fut le ravissement des saints Pères, lorsqu'ils
virent leurs limbes honorés de la glorieuse présence du Sauveur du monde
! Combien louèrent-ils la miséricorde de ce Dieu qui les visitait jusque
dans ces lieux souterrains, et qui allait pour l'amour d'eux jusqu'aux
enfers ! Or sa miséricorde est beaucoup plus grande, quand il va
chercher les pécheurs. Ils sont dans un enfer plus obscur et dans une
captivité bien plus déplorable. Nos pères, qui étaient réservés aux
limbes jusqu'à la venue du Sauveur, soupiraient continuellement après
lui, et pressaient son arrivée par leurs vœux. Au contraire les
misérables pécheurs, dans cet enfer de l'impiété où ils sont,
non-seulement ne cherchent pas le Sauveur, mais ils fuient sitôt qu'il
s'approche ; et c'est la seconde misère de l'âme.

Nous sommes infiniment éloignés de Dieu , et nous le fuyons quand il
vient à nous. Comprenons par un exemple sensible, combien est dangereuse
cette maladie. Voyez un pauvre malade, faible et languissant ; ses forces
se diminuent tous les jours, il faudrait qu'il prît quelque nourriture
pour soutenir son infirmité ; il ne peut. Je ne sais quelle humeur
froide (_a_) lui a causé un dégoût étrange ; si on lui présente quelque
nourriture, si exquise, si bien apprêtée qu'elle soit, aussitôt son cœur
se soulève ; de sorte que nous pouvons dire que sa maladie, c'est une
aversion du remède. Telle et encore beaucoup plus horrible est la
maladie d'un pécheur. Il a voulu goûter, aussi bien qu'Adam, cette pomme
qui lui paraissait agréable : il a voulu se rassasier des plaisirs
mortels ; et par un juste jugement de Dieu il a perdu tout le goût des
biens éternels. Vous les lui présentez, il en a horreur ; vous lui
montrez la terre promise, il retourne son cœur en Égypte ;

 

(_a_) _Var._ : Malfaisante.

 

381

 

vous lui donnez la manne, elle lui semble fade et sans goût. Ainsi nous
fuyons malheureusement le charitable Pasteur qui nous cherche.

Pécheur, ne le fuis-tu pas tous les jours? Maintenant que tu entends sa
sainte parole, peut-être que ce Pasteur miséricordieux te presse
intérieurement en ta conscience. Veux-tu pas restituer ce bien mal
acquis ? Veux-tu pas enfin mettre quelques bornes à cette vie débauchée
et licencieuse? Veux-tu pas bannir de ton cœur l'envie qui le ronge,
cette haine envenimée qui l'enflamme, ou cette amitié dangereuse qui ne
le flatte que pour le perdre? Ecoute, pécheur ; c'est Jésus qui te
cherche ; et ton cœur répond à ce doux Sauveur : Je ne puis encore. Tu
le remets de jour en jour, demain, dans huit jours, dans un mois.
N'est-ce pas fuir celui qui te cherche et mépriser sa miséricorde?
Insensé ! que t'a fait Jésus que tu fuis si opiniâtrement sa douce
présence? D'où vient que la brebis égarée ne reconnaît plus la voix du
pasteur qui l'appelle et lui tend les bras, et qu'elle court follement
au loup ravissant qui se prépare à la dévorer? Peut-être tu répondras :
Je ne puis, je ne puis marcher dans la voie étroite. Mais ne vois-tu
pas, misérable ! que Jésus te présente ses propres épaules pour soulager
ton infirmité et ton impuissance? Il descend à toi pour te relever ; en
prenant ton infirmité, il te communique sa force. C'est le dernier excès
de miséricorde.

Comme notre âme est faite pour Dieu, il faut qu'elle prenne sa force en
celui qui est l'auteur de son être. Que si se détournant du souverain
bien, elle tâche de se rassasier dans les créatures, elle devient
languissante et exténuée, à peu près comme un homme qui ne prendroit que
des viandes qui ne seraient pas nourrissantes. De là vient que l'enfant
prodigue sortant de la maison paternelle, ne trouve plus rien qui le
rassasie, parce que notre âme ne peut trouver qu'en Dieu seul cette
nourriture solide qui est capable de l'entretenir. De là ces rechutes
fréquentes, qui sont les marques les plus certaines que nos forces sont
épuisées. Que fera une âme impuissante, si Jésus ne supporte son
infirmité? Aussi présente-t-il ses épaules à cette pauvre brebis égarée,
« parce qu'errant deçà et delà, elle s'était extrêmement

 

382

 

fatiguée : » _Multùm enim errando laboraverat_ (1). Il la cherche quand
il l'invite par ses saintes inspirations, il la trouve quand il la
change par la vertu de sa grâce, il la porte sur ses épaules quand il
lui donne la persévérance.

O miséricorde ineffable et digne certainement d'être célébrée par la
joie de tous les esprits bienheureux ! La grandeur de Dieu, c'est son
abondance par laquelle étant infiniment plein, il trouve tout son bien
en lui-même. Ce qui montre la plénitude, c'est la munificence. C'est
pourquoi Dieu se réjouit en voyant ses œuvres, parce qu'il voit ses
propres richesses et son abondance dans la communication de sa bonté. Or
il y a deux sortes de bonté en Dieu : l'une ne rencontre (_a_) rien de
contraire à son action, et elle s'appelle libéralité ; l'autre trouve de
l'opposition, et elle prend le nom de miséricorde. Quand Dieu a fait le
ciel et la terre, rien ne s'est opposé à sa volonté. Quand Dieu
convertit les pécheurs , il faut qu'il surmonte leur résistance, et
qu'il combatte pour ainsi dire sa propre justice en lui arrachant ses
victimes. Or cette bonté qui se roidit contre tant d'obstacles, est sans
doute plus abondante que celle qui ne trouve point d'empêchements à ses
bienheureuses communications. C'est pourquoi les Écritures divines
disent que « Dieu est riche en miséricorde (2), » les richesses de sa
miséricorde, etc.

 


SECOND POINT.

 

Après vous avoir parlé, chrétiens, de la partie la plus douce de la
pénitence, la suite de mon évangile demande que je vous représente en
peu de paroles la partie difficile et laborieuse. Il paraît d'abord
incroyable que la justice divine doive avoir sa place dans la conversion
des pécheurs, puisqu'il semble qu'elle se relâche de tous ses droits
pour donner à la seule miséricorde toute la gloire de cette action.
Toutefois écoutons le Sauveur du monde, qui nous avertit dans notre
évangile : « Les anges se réjouissent, dit-il, sur un pécheur faisant
pénitence. » Qu'est-ce à dire _faire pénitence_? Si nous entendons faire
pénitence selon les maximes

 

1 Tertull., _de Paenit_., n. 8. — 2 _Ephes_., II, 4.

 

(_a_) _Var_. : Ne trouve.

 

383

 

de l'Évangile, certainement faire pénitence, c'est « faire ce que dit
saint Jean, des fruits dignes de pénitence (1). » Or ces fruits dignes
de pénitence selon le consentement de tous les docteurs, ce sont des
œuvres laborieuses par lesquelles nous vengeons nous-mêmes sur nos
propres corps la bonté de Dieu méprisée. C'est à quoi il nous exhorte
par son prophète : « Retournez à moi, dit-il, retournez à moi de tout
votre cœur, en pleurs, en jeûnes, en gémissements, dans le sac, dans la
cendre et dans le cilice (2). »

Et pour entendre cette doctrine, figurez-vous un pauvre pécheur, qui
reconnaissant l'horreur de son crime, considère la main de Dieu armée
contre lui, et regarde qu'il va supporter le poids de sa juste et
impitoyable vengeance. De là les craintes, de là les frayeurs, de là les
douleurs amères et inconsolables. Au milieu de ces effroyables
langueurs, la sainte pénitence se présente à lui pour soulager ses
infirmités par ses salutaires conseils ; elle lui fait voir dans les
Écritures que Dieu dit lui-même : « Je ne me vengerai pas deux fois
d'une même faute ; » et ailleurs : « Si nous nous jugions, nous ne
serions pas jugés (3). » Lui ayant remontré ces choses : Aie bon
courage, dit-elle, préviens la justice par la justice. Dieu se veut
venger, venge-le toi-même ; sa colère est armée contre toi, arme tes
propres mains contre tes propres iniquités ; Dieu recevra en pitié le
sacrifice d'un cœur contrit que tu lui offriras pour l'expiation de ton
crime ; et sans considérer que les peines que tu t'imposes ne sont pas
une vengeance proportionnée, il regardera seulement qu'elle est
volontaire. Là-dessus le pécheur s'éveille ; et regardant la justice
divine si fort enflammée contre nous, et que d'ailleurs il est
impossible de lui résister, il voit qu'il est impossible de faire autre
chose que de se joindre à elle pour en éviter la fureur, de prendre son
parti contre soi-même, et de venger par ses propres mains les mystères
de Jésus violés, son Saint-Esprit affligé et sa Majesté offensée. C'est
pourquoi il se transporte en esprit en cet épouvantable jugement, où
voyant que Dieu accuse les pécheurs, qu'il les condamne et qu'il les
punit, il se met en quelque sorte en sa place ;

 

1 _Luc.,_ III, 8. — 2 _Joel,_ II, 18. — 3 I _Cor.,_ XI, 31.

 

384

 

de criminel il devient le juge ; il s'accuse, confession ; il se condamne,
contrition ; et il se punit, satisfaction.

Et premièrement il s'accuse ; et voyant dans les Écritures que Dieu
menaçant les pécheurs, leur dit : « Je te mettrai contre toi-même (1) »
il prévient cette sentence très-équitable et il témoigne lui-même son
iniquité. Il dit hautement avec David : « J'ai péché au Seigneur (2) ; »
il dit encore avec Daniel : « Nous avons péché, nous avons mal fait,
nous avons transgressé vos commandements, nous avons laissé vos
préceptes et vos jugements ; à vous la gloire, à vous la justice, à nous
la confusion et l'ignominie (3). » Il dit avec le Publicain : « O Dieu,
ayez pitié de moi misérable pécheur (4). » Il va au tribunal de la
pénitence, il a recours aux clefs de l'Église. Une fausse honte l'arrête
: O honte, dit-il, qui m'étais donnée pour me retenir dans l'ardeur du
crime, et qui m'as abandonné si mal à propos, il est temps aussi que je
t'abandonne ; et t'ayant perdue malheureusement pour le péché, je te veux
perdre utilement pour la pénitence ! Là il découvre avec une sainte
confusion ses profondes et ignominieuses blessures, il se reproche
lui-même sa lâcheté devant Dieu et devant les hommes. Que demandez-vous,
justice divine? Qu'est-il nécessaire que vous l'accusiez? Il s'accuse
lui-même volontairement.

Mais il ne suffit pas qu'il s'accuse ; il faut encore qu'il se condamne.
Expliquez-le-nous, ô grand Augustin (5).... C'est ainsi que firent les
Ninivites : _Subvertitur planè Ninive, cum calcatis deterioribus studiis
ad meliora convertitur ; subvertitur, inquam, dùm purpura in cilicium,
affluentia in jejunium, lœtitia mutatur in fletum_ (6). O ville
heureusement renversée! Renversons Ninive en nous.

Mais écoutons encore : il ne suffit pas de nous condamner, il ne suffit
pas de changer nos mœurs. La bonté entreprenant sur la justice, la
justice fait quelques réserves. Parce que Jésus-Christ est bon, il ne
faut pas que nous soyons lâches. Au contraire nous devons être d'autant
plus rigoureux à nous-mêmes, que Jésus-Christ

 

1 _Psal._ XLIX, 21. — 2 II _Reg.,_ XII, 13. — 3 _Dan_., III, 29, 30.— 4
_Luc.,_ XVIII, 13. — 4 _In__ Psal._ XLIX, n. 28 ; in Psal. XXXVII, n. 24 ;
_in Psal._ LIX, n. 5. — 5 S. Eucher. Lugd., _Hom. de Pœnit. Niniv. ;
Biblioth. PP_., Lugd., tom. VI, p. 646.

 

385

 

est plus miséricordieux. _Panem meum cum cinere manducabam et potum meum
cum fletu miscebam, à facie irœ et indignationis tuœ_ (1)... _Ninivites,
tam manifestant judicantes afflictionis rente dium, ut sibi etiam
animalium crederent profuturum esse jejunium_ (2).

O spectacle digne de la joie des anges! parce que l'homme accuse Dieu
n'accuse plus ; l'homme se joignant avec la justice, lui fait tomber les
armes des mains ; il l'affaiblit pour ainsi dire en la fortifiant ; Dieu
lui pardonne, parce qu'il ne se pardonne pas ; Dieu prend son parti,
parce qu'il prend le parti de Dieu ; parce qu'il se joint à la justice
contre soi-même, la miséricorde se joint à lui contre la justice.
N'épargnons pas, mes Frères, des larmes si fructueuses ; frustrons
l'attente du diable par la persévérance de notre douleur ; plus nous
déplorons la misère où nous sommes tombés, plus nous nous rapprocherons
du bien que nous avons perdu.

 

1 _Psal._ CI, 10, 11. — 2 S. Eucher. Lugd., hom. _de Pœnit. Niniv_.

[Précédente]

[Accueil]

[Suivante]

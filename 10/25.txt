[SAINTE CROIX II]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Vendredi Saint
II Vendredi Saint
III Vendredi Saint
IV Vendredi Saint
I Pâques Jour
II Pâques Jour
III Pâques Jour
IV Pâques Jour
Pâques Jour abrg
Quasimodo
Pâques IIIe Dim.
Pâques IIIe Dim. abr.
Pâques Ve Dim.
Ascension
Pentecôte I
Pentecôte II
Pentecôte III
Pentecôte abr.
Trinité
III Pentecôte
V Pentecôte
IX Pentecôte
XXI Pentecôte abr.
Sainte Croix I
Sainte Croix II
Précis Ste Croix
Exh. Nvles Catholiques
Vie Xtienne
Ursulines Meaux I
Ursulines Meaux II
Ord. Ursul. Meaux
Ursulines Meaux III
Ursulines Meaux IV
Conf. Ursul. Meaux
Instr. Ursul. Meaux
Profess. Ursuline
Disc. Visitandines
Disc. Union J-C / Épouse
Pensées Xtiennes
Pensées Détachées

 



SECOND SERMON POUR LE JOUR DE L'EXALTATION DE LA SAINTE CROIX (_A_).


 

_Exaltari oportet Filium hominis_. 

Il faut que le Fils de l'homme soit exalté. _Joan.,_ III, 14.

 

_Christo confixus sum cruci._ 

Je suis attaché à la croix avec Jésus-Christ. _Galat.,_ II, 19.

 

Toute l'Écriture nous prêche que la gloire du Fils de Dieu est dans les
souffrances, et que c'est à la croix qu'il est exalté : il n'est rien de
plus véritable. Jésus est exalté à la croix par les peines qu'il a
endurées ; Jésus est exalté à la croix par les peines que nous endurons.
C'est, mes Frères, sur ce dernier point que je m'arrêterai aujourd'hui,
comme sur celui qui me semble le plus fructueux ; et je me propose de
vous faire voir combien le Fils de Dieu est glorifié dans les
souffrances qu'il nous envoie. Mais, chrétiens, ne nous trompons pas ;
dans la gloire qu'il tire de nos afflictions, il y est glorifié en deux
manières, dont l'une certainement n'est pas moins terrible que l'autre
est salutaire et glorieuse.

Voici une doctrine importante, voici un grand mystère que je vous
propose ; et afin de le bien entendre, venez le méditer au Calvaire, au
pied de la croix de notre Sauveur : vous y verrez deux actions opposées
que le Père y exerce dans le même temps. Il y exerce sa miséricorde et
sa justice ; il punit et remet les crimes ;

 

(_a_) Prêché a Paris ; aux nouveaux Catholiques, en 1661.

Le lecteur trouvera, dans le style et le ton du discours, les caractères
qui révèlent cette date ; et le lieu est clairement désigné par ces
paroles du second point : « Vous particulièrement, mes chers Frères,
sainte et bienheureuse conquête, nouveaux enfants de l'Église, qu'elle
se glorifie d'avoir retirés au centre de sou unité et au sein de sa
charité, » etc.

Les protestants ont dit, et des enfants de l'Église ont répété, que
jamais Bossuet n'a prononcé une parole en faveur des pauvres et des
malheureux. On comprendra tout à l'heure jusqu'où les hérétiques peuvent
pousser la calomnie, et certains catholiques la crédulité.

 

452

 

il se venge et se réconcilie tout ensemble : il frappe son Fils innocent
pour l'amour des hommes criminels, et en même temps il pardonne aux
hommes criminels pour l'amour de son Fils innocent. O justice ! ô
miséricorde ! qui vous a ainsi assemblées? C'est le mystère de
Jésus-Christ, c'est le fondement de sa gloire et de son exaltation à la
croix, d'avoir concilié en sa personne ces deux divins attributs, je
veux dire, la miséricorde et la justice.

Mais cette union admirable nous doit faire considérer que comme en la
croix de notre Sauveur la vengeance et le pardon se trouvent ensemble,
aussi pouvons-nous participer à la croix en ces deux manières
différentes, ou selon la rigueur qui s'y exerce, ou selon la grâce qui
s'y accorde. Et c'est ce qu'il a plu à Notre-Seigneur de nous faire voir
(_a_) au Calvaire. Nous y voyons, dit saint Augustin, « trois hommes en
croix, un qui donne le salut, un qui le reçoit (_b_), un qui le méprise
: » _Tres erant in cruce, unus Salvator, alius salvandus, alius
damnandus_ (1). Au milieu, l'auteur de la grâce ; d'un côté un qui en
profite, de l'autre côté un qui la rejette. Discernement terrible et
diversité surprenante ! Tous deux sont à la croix avec Jésus-Christ,
tous deux compagnons de son supplice ; mais hélas ! il n'y en a qu'un
qui soit compagnon de sa gloire. Ce que le Sauveur avait réuni, je veux
dire la miséricorde et la vengeance, ces deux hommes l'ont divisé.
Jésus-Christ est au milieu d'eux, et chacun a pris son partage de la
croix de Notre-Seigneur. L'un y a trouvé la miséricorde, l'autre les
rigueurs de la justice : l'un y a opéré son salut, l'autre y a commencé
sa damnation : la croix a élevé jusqu'au paradis la patience de l'un ;
la croix a précipité au fond de l'enfer l'impénitence de l'autre. Ils
ont donc participé à la croix en deux manières (_c_) bien différentes ;
mais cette diversité n'empêchera pas que Jésus ne soit exalté en l'un et
en l'autre, ou par sa miséricorde, ou par sa justice : _Exaltari oportet
Filium hominis_.

Apprenez de là, chrétiens, de quelle sorte et en quel esprit vous devez
recevoir la croix. Ce n'est pas assez de souffrir ; car qui ne souffre
pas dans la vie ? Ce n'est pas assez d'être sur la croix ; car

 

1 _Enar_. II _in Psal._ XXXIV, n. 1.

 

(_a_) _Var._ : Faire paraître. — (_b_) Un qui doit le recevoir. — (_c_)
D'une manière.

 

453

 

plusieurs y sont, comme ce voleur impénitent, qui sont bien éloignés du
Crucifié. La croix dans les uns est une grâce ; la croix dans les autres
est une vengeance ; et toute cette diversité dépend de l'usage que nous
en faisons. Avisez donc sérieusement, ô vous, âmes que Jésus afflige, ô
vous que ce divin Sauveur a mis sur la croix ; avisez sérieusement dans
lequel de ces deux états vous y voulez être attachés (_a_) ; et afin que
vous fassiez un bon choix (_b_), voyez ici en peu de paroles la peinture
de Fun et de l'autre, qui fera le partage de ce discours.

 


PREMIER POINT.

 

Pour parler solidement des afflictions, connaissons (_c_) premièrement
quelle est leur nature ; et disons (_d_), s'il vous plaît, Messieurs,
avant toutes choses, que la cause générale de toutes nos peines, c'est
le trouble qu'on nous apporte dans les choses que nous aimons. Or il me
semble que nous voyons par expérience que notre âme y peut être troublée
(_e_) en trois différentes façons : ou lorsqu'on lui refuse ce qu'elle
désire, ou lorsqu'on lui ôte ce qu'elle possède, ou lorsque lui en
laissant la possession, on l'empêche de le goûter.

Premièrement on nous inquiète quand on nous refuse ce que nous aimons.
Car il n'est rien de plus misérable que cette soif qui jamais n'est
rassasiée ; que ces désirs toujours suspendus, qui s'avancent (_f_)
éternellement sans rien prendre ; que cette fâcheuse agitation d'une âme
toujours frustrée de ce qu'elle espère : on ne peut assez exprimer
combien elle est travaillée par ce mouvement. Toutefois on l'afflige
beaucoup davantage quand on la trouble dans la possession du bien
qu'elle tient déjà entre ses mains, parce que, dit saint Augustin, «
quand elle possède ce qu'elle a aimé, comme les honneurs, les richesses
ou quelque autre chose semblable, elle se l'attache à elle-même par le
contentement (_g_) qu'elle a de l'avoir (1), » l'aise qu'elle sent d'en
jouir ; elle se l'incorpore en quelque façon, si je puis parler de la
sorte ; cela devient comme

 

1 S. August., _De lib. Arbit_., lib. I, n. 33.

 

(_a_) _Var._ : Lui appartenir. — (_b_) Pour faire ce choix avec
connaissance. —(_c_) Il faut connaître. — (_d_) Remarquez. — (_e_) Nous
pouvons y être troubles. (_f_) Courent. — (_g_) La joie.

 

454

 

une partie de nous-mêmes ou, pour dire le mot de saint Augustin, comme
un membre de notre cœur, » _velut membra animi_ : de sorte que si Ton
vient à nous l'arracher, aussitôt le cœur en gémit ; il est comme déchiré
et ensanglanté par la violence qu'il souffre.

La troisième espèce d'affliction qui est si ordinaire dans la vie
humaine, ne nous ôte pas entièrement le bien qui nous plaît ; mais elle
nous traverse de tant de côtés, elle nous presse tellement d'ailleurs,
qu'elle ne nous permet pas d'en jouir. Par exemple, vous avez acquis de
grands biens, il semble que vous devez être heureux ; mais vos
continuelles infirmités vous empêchent de goûter le fruit de votre bonne
fortune : est-il rien de plus importun? C'est être au milieu d'un jardin
sans avoir la liberté d'en goûter les fruits, non pas même d'en cueillir
les fleurs : c'est avoir pour ainsi dire la coupe à la main et n'en
pouvoir pas rafraîchir sa bouche, bien que vous soyez pressé d'une soif
ardente ; et cela vous cause un chagrin extrême. Voilà, Messieurs, comme
les trois sources qui produisent toutes nos plaintes ; voilà ce qui fait
murmurer les enfants des hommes.

Mais le fidèle serviteur de Dieu ne perd pas sa tranquillité parmi ces
disgrâces, de laquelle de ces trois sources que puissent naître ses
afflictions ; et quand même elles se joindraient toutes trois ensemble
pour remplir son âme d'amertume, il bénit toujours la bonté divine, et
il connaît que Dieu ne le frappe que pour exalter en lui sa miséricorde
: _Oportet exaltari Filium hominis_. En effet il est véritable ; et afin
de nous en convaincre, parcourons, je vous prie, en peu de paroles ces
trois sources d'afflictions ; sans doute nous y trouverons trois sources
de grâces.

Et premièrement, chrétiens, il n'est rien ordinairement de plus
salutaire que de nous refuser ce que nous désirons avec ardeur, et je
dis même dans les désirs les plus innocents : car pour les désirs
criminels, qui pourrait révoquer en doute que ce ne soit un effet de
miséricorde que d'en empêcher le succès ? Tu es enflammé de sales
désirs, et tu crois qu'on te favorise quand on te laisse le moyen de les
satisfaire. Malheureux, c'est une vengeance par laquelle Dieu punit tes
premiers désordres, en te livrant justement

 

455

 

au sens réprouvé. Car si tu étais si heureux qu'il s'élevât de toutes
parts des difficultés contre tes prétentions honteuses, peut-être qu'au
milieu de tant de traverses tes ardeurs insensées se ralentiraient ; au
lieu que ces ouvertures commodes, et cette malheureuse facilité que tu
trouves, précipitent ton intempérance aux derniers excès : tellement
qu'à force de t'abandonner à ces funestes appétits que la fièvre excite,
de fou tu deviens furieux , et une maladie dangereuse se tourne en une
maladie désespérée.

Reconnaissez donc, ô enfants de Dieu, avec quelle miséricorde Dieu nous
laisse dans la faiblesse et dans l'impuissance : c'est que ce souverain
Médecin sait guérir nos maladies de plus d'une sorte. Quelquefois il
nous laisse dans un grand pouvoir, qu'il réduit à ses justes bornes par
une droite volonté ; en sorte que celui qui a été maître de transgresser
le commandement ne l'a point transgressé : _Qui potuit transgredi, et
non est transgressus_ (1). Quelquefois il se sert d'une autre méthode,
et il réduit la volonté en restreignant le pouvoir : _Froenatur
potestas, ut sanetur voluntas,_ dit saint Augustin (2). Sa miséricorde,
qui nous veut guérir, oppose à nos désirs emportés des difficultés
insurmontables : ainsi il nous dompte par la résistance ; et fatiguant
notre esprit, il nous accoutume à ne vouloir plus ce que nous trouvons
impossible.

Mais, Messieurs, si vous trouvez juste qu'il s'oppose aux volontés
criminelles, peut-être aussi vous semble-t-il rude qu'il étende cette
rigueur jusqu'aux désirs innocents (_a_) : toutefois ne vous plaignez
pas de cette conduite. Un sage jardinier n'arrache pas seulement d'un
arbre les branches gâtées (_b_) ; mais il en retranche aussi quelquefois
les accroissements superflus. Ainsi Dieu n'arrache pas seulement en nous
les désirs qui sont corrompus, mais il coupe quelquefois jusqu'aux
inutiles ; et la raison de cette conduite est bien digne de sa bonté et
de sa sagesse : c'est que celui qui nous a formés, qui connaît les
secrets ressorts qui font mouvoir nos inclinations, sait qu'en nous
abandonnant sans réserve à toutes les choses qui nous sont permises,
nous nous laissons

 

1 _Eccli_., XXXI, 10. — 2 _Ad Maced_., ep. CLIII, n. 16.

 

(_a_) _Var._ : Qu'il refuse souvent les innocentes. — (_b_) Pourries.

 

456

 

aisément tomber à celles qui sont défendues. Et n'est-ce pas ce que
sentait saint Paulin, lorsqu'il se plaint familièrement au plus intime
de ses amis? « Je fais, dit-il, plus que je ne dois, pendant que je ne
prends aucun soin de me modérer en ce que je puis (_a_) : » _Quod non
expediebat admisi, dùm non tempero quod licebat_ (1). La vertu en
elle-même est infiniment éloignée du vice ; mais telle est la faiblesse
de notre nature, que les limites s'en touchent de près dans nos esprits,
et la chute en est bien aisée. Il importe que notre âme ne jouisse pas
de toute la liberté qui lui est permise, de peur qu'elle ne s'emporte
jusqu'à la licence ; et que s'étant épanchée à l'extrémité, elle ne passe
aisément au delà des bornes. C'est donc un effet de miséricorde de ne
contenter pas toujours nos désirs, non pas même les innocents : cette
croix nous est salutaire.

Mais notre Sauveur va beaucoup plus loin ; et cette même miséricorde qui
dénie (_b_) à notre âme ce qu'elle poursuit, lui arrache quelquefois ce
qu'elle possède. Chrétien, n'en murmure pas : il le fait par une bonté
paternelle ; et nous le comprendrions aisément, si nous nous savions
connaître nous-mêmes. Ne me dis pas, âme chrétienne : Pourquoi
m'ôte-t-on cet ami intime? pourquoi un fils, pourquoi un époux, qui
faisait toute la douceur de ma vie? Quel mal faisais-je en les aimant,
puisque cette amitié est si légitime? Non, je ne veux pas entendre ces
plaintes dans la bouche d'un chrétien, parce qu'un chrétien ne peut
ignorer combien la chair et le sang se mêlent dans les affections les
plus légitimes, combien les intérêts temporels, combien de sortes
d'inclinations qui naissent en nous de l'amour du monde. Et toutes ces
inclinations ne sont-ce pas, si nous l'entendons, comme autant de
petites parties de nous-mêmes qui se détachent du Créateur pour
s'attacher à la créature, et que la perte que nous faisons des personnes
chères nous apprend à réunir en Dieu seul, comme des lignes écartées du
centre? Mais les hommes n'entendent pas combien cette perte (_c_) leur
est salutaire, parce qu'ils n'entendent pas

 

1 _Ad Sever_., ep. XXX, n. 3.

 

(_a_) _Var._ : Et n'est-ce pas ce que sentait saint Paulin, lorsqu'il se
plaint familièrement au plus intime de ses amis, que son cœur s'est
laissé aller à ce qu'il ne fallait pas faire, pendant qu'il ne prenait
aucun soin de modérer ce qui était permis?— (_b_) Refuse. — (_c_) Cette
médecine.

 

457

 

combien ces attachements sont dangereux : ils ne se connaissent pas
eux-mêmes, ni la pente qu'ils ont aux biens périssables.

O cœur humain, si tu connaissais combien le monde te prend aisément,
avec quelle facilité tu t'y attaches, combien tu louerais la main
charitable qui vient rompre violemment ces liens, en te troublant dans
la possession des biens de la terre ! Il se fait en nous, en les
possédant, certains nœuds secrets qui nous engagent insensiblement dans
l'amour des choses présentes ; et cet engagement est plus dangereux, en
ce qu'il est ordinairement plus imperceptible. Oui, le désir se fait
mieux sentir, parce qu'il a de l'agitation et du mouvement ; mais la
possession assurée, c'est un repos, c'est comme un sommeil ; on s'y
endort, on ne le sent pas. C'est pourquoi le divin Apôtre dit que ceux
qui amassent de grandes richesses, « tombent dans de certains lacets
invisibles, _incidunt in laqueum_ (1), où le cœur se prend aisément. Il
se détache du Créateur par l'amour désordonné de la créature, et à peine
s'aperçoit-il de cet attachement excessif. Il faut, chrétiens, le mettre
à l'épreuve ; il faut que le feu des tribulations lui montre à se
connaître lui-même (_a_) ; « il faut, dit saint Augustin, qu'il apprenne
en perdant ces biens combien il péchait en les aimant : » _Quantum hœc
amando peccaverint, perdendo senserunt_ (2).

Et cela de quelle manière? Qu'on lui dise que cette maison est brûlée,
que cette somme est perdue sans ressource par la banqueroute de ce
marchand, aussitôt le cœur saignera, la douleur de la plaie lui fera
sentir par combien de fibres secrètes ces richesses tenaient au fond de
son cœur, et combien il s'écartait de la droite voie par cet engagement
vicieux : _Quantum hœc amando peccaverint, perdendo senserunt_. Il
connaîtra mieux par expérience la fragilité des biens de la terre, dont
il ne se voulait laisser convaincre par aucuns discours : dans le débris
des choses humaines il tournera les yeux vers les biens éternels, qu'il
commencent peut-être à oublier ; ainsi ce petit mal guérira les grands,
et sa blessure sera son salut.

Mais si Dieu laisse à ses serviteurs la jouissance des biens du

 

1 I _Timoth.,_ VI, 9. — 2 _De Civit. Dei,_ lib. I, cap. X.

 

(_a_) _Var._ : Il faut que le coup des afflictions lui vienne faire
sentir son mal.

 

458

 

siècle (_a_), ce qu'il peut faire de meilleur pour eux, c'est de leur en
donner du dégoût, de répandre mille amertumes sur tous leurs plaisirs,
de ne leur permettre pas de s'y reposer, de secouer et d'abattre cette
fleur du monde qui leur rit trop agréablement ; de leur faire naître des
difficultés, de peur que cet exil ne leur plaise et qu'ils ne le
prennent pour la patrie. Vous voyez donc, ô enfants de Dieu, qu'en
quelque partie de sa croix qu'il plaise au Sauveur de vous attacher,
soit qu'il vous refuse ce que vous aimiez, soit qu'il vous ôte ce que
vous possédiez, soit qu'il ne vous permette pas de goûter les biens dont
il vous laisse la jouissance, c'est toujours pour exercer en vous sa
miséricorde et exalter sa bonté dans vos afflictions.

O Dieu, si je pouvais vous faire comprendre combien elle est glorifiée
par vos souffrances, que ce discours serait fructueux, et ma peine
utilement employée ! Mais si mes paroles ne le peuvent pas, venez
l'apprendre de ce voleur pénitent, dont je vous ai d'abord proposé
l'exemple. Pendant que tout le monde trahit Jésus-Christ, pendant que
tous les siens l'abandonnent, il s'est réservé cet heureux larron pour
le glorifier à la croix : « Sa foi a commencé de fleurir, où la foi des
disciples a été flétrie : » _Tunc fides ejus de ligno floruit, quandù
discipulorum marcuit_ (1). Jésus déshonoré par tout le monde, n'est plus
exalté que par lui seul : venez profiter d'un si bel exemple ; voici un
modèle accompli.

        Il n'oublie rien, mes Frères, de ce qu'il faut faire dans
l'affliction ; il glorifie Jésus-Christ en autant de sortes qu'il veut
être glorifié sur la croix. Car voyez premièrement comme il s'humilie
par la confession de ses crimes. « Pour nous, dit-il, c'est avec
justice, puisque nous souffrons la peine que nos crimes ont méritée : »
_Et nos quidem justè, nam digna factis recipimus_ (2) : comme il baise
la main qui le frappe, comme il honore la justice qui le punit : c'est
là, mes Frères, l'unique moyen de la tourner en miséricorde. Mais ce
saint larron (_b_) ne finit pas là : après s'être considéré comme
criminel, il se tourne au Juste qui souffre avec lui : « Mais celui-ci,
ajoute-t-il, n'a fait aucun mal : » _Hic vero nihil_

 

1 S. August., lib. I _De Animâ et ejus orig_., n. 11. — 2 _Luc,_ XXIII,
41.

 

(_a_) _Var._ : Des biens temporels de ce monde. — (_b_) Cet heureux
criminel.

 

459

 

_mali gessit_ (1). Cette pensée adoucit ses maux : il s'estime heureux
dans ses peines de se voir uni avec l'innocent ; et cette société de
souffrances lui donnant avec Jésus-Christ une sainte familiarité, il lui
demande avec foi part en son royaume, comme il lui en a donné en sa
croix : _Domine, memento mei, cùm veneris in regnum tuum_ (2).

Je triomphe de joie, mes Frères, mon cœur est rempli de ravissement en
voyant la foi de ce saint voleur. Un mourant voit Jésus mourant, et il
lui demande la vie ; un crucifié voit Jésus crucifié, et il lui parle de
son royaume ; ses yeux n'aperçoivent que des croix, et sa foi ne se
représente qu'un trône. Quelle foi et quelle espérance ! Si nous
mourons, mes Frères, nous savons que Jésus-Christ est vivant, et notre
foi chancelante a peine toutefois à s'y confier : celui-ci voit mourir
Jésus avec lui, et il espère, et il se console, et il se réjouit même
dans un si cruel supplice. Imitons un si saint exemple ; et si nous ne
sommes animés par celui de tant de martyrs et de tant de saints,
rougissons du moins, chrétiens, de nous laisser surpasser par un voleur.
Confessons nos péchés avec lui, reconnaissons avec lui l'innocence de
Jésus-Christ : si nous imitons sa patience, la consolation ne manquera
pas. Aujourd'hui, aujourd'hui, dira le Sauveur, tu seras avec moi dans
mon paradis. Ne crains pas, ce sera bientôt ; cette vie se passe bien
vite, elle s'écoulera comme un jour d'hiver, le matin et le soir s'y
touchent de près : ce n'est qu'un jour, ce n'est qu'un moment, que la
seule infirmité fait paraître long : quand il sera écoulé, tu
t'apercevras combien il est court (3). Aie donc patience avec ce larron,
exalte cette rigueur salutaire qui te frappe par miséricorde. Mais si
cet exemple ne te touche pas, voici quelque chose de plus terrible qui
me reste maintenant à te proposer ; c'est la justice, c'est la vengeance
qui brise sur la croix les impénitents : c'est par où je m'en vais
conclure.

 


SECOND POINT.

 

Nous apprenons par les saintes Lettres que la prospérité des impies est
un effet de la vengeance de Dieu, et de sa colère qui les

 

1 _Luc.,_ XXIII, 41. — 2 _Ibid.,_ 42. — 3 S. August., tract, CI, _in
Joan_., n. 6.

 

460

 

poursuit. Oui, lorsqu'ils nagent dans les plaisirs, que tout leur rit,
que tout leur succède, cette paix que nous admirons, qui selon
l'expression du Prophète « fait sortir l'iniquité de leur graisse, »
_Prodiit quasi ex adipe iniquitas eorum_ (1), qui les enfle, qui les
enivre jusqu'à leur faire oublier la mort, c'est un commencement de
vengeance que Dieu exerce sur eux : cette impunité, c'est une peine qui
les livrant aux désirs de leur cœur, leur amasse un trésor de haine en
ce jour d'indignation et de fureur implacable.

Si nous voyons dans l'Écriture que Dieu sait quelquefois punir les
impies par une félicité apparente, cette même Écriture, qui ne ment
jamais, nous enseigne qu'il ne les punit pas toujours en cette manière,
et qu'il leur fait quelquefois sentir son bras par des misères
temporelles. Cet endurci Pharaon, cette prostituée Jézabel, ce maudit
meurtrier Achab ; et sans sortir de notre sujet, ce larron impénitent et
blasphémateur, rendent témoignage à ce que je dis et nous font bien
voir, chrétiens, que ce n'est pas assez d'être sur la croix pour être
uni au Crucifié. Ainsi cette croix, que vous avez vue comme une marque
de miséricorde, vous va maintenant être présentée comme un instrument de
vengeance : et afin que vous entendiez comme elle a pu sitôt changer de
nature, remarquez, s'il vous plaît, Messieurs, qu'encore que toutes les
peines soient nées du péché, il y en a néanmoins qui lui peuvent servir
de remède.

Je dis que toutes les peines sont nées du péché et en punissent les
dérèglements. Car sous un Dieu si bon que le nôtre, l'innocence n'a rien
à craindre et elle ne peut jamais espérer qu'un traitement favorable :
il est si naturel à Dieu d'être bienfaisant à ses créatures, qu'il ne
ferait jamais de mal à personne, s'il n'y était forcé par les crimes.
Toutefois il faut remarquer deux sortes de peines : il y a la peine
suprême, qui est la damnation éternelle ; il y a les peines de moindre
importance, comme les afflictions de cette vie : « Toutes deux, dit
saint Augustin, sont venues du crime, toutes deux en doivent venger les
excès. » Mais il y a cette différence, que la damnation éternelle est un
effet de pure vengeance, et ne peut jamais nous tourner à bien ; au lieu
que les afflictions

 

1 _Psal_. LXXII, 7.

 

461

 

temporelles sont mêlées de miséricorde, et peuvent être employées à
notre salut, suivant l'usage que nous en faisons: « C'est pourquoi dit
le même Saint, toutes les croix que Dieu nous envoie peuvent aisément
changer de nature, selon la manière dont on les reçoit : il faut
considérer, non ce que l'on souffre, mais dans quel esprit on le souffre
: » _Non qualia, sed qualis quisque patiatur_ (1). Ce qui était la peine
du péché, étant sanctifié par la patience, est tourné à l'usage de la
vertu ; « et le supplice du criminel devient le mérite de l'homme de bien
: » _Fit justi meritum etiam supplicium peccatoris_ (2).

S'il est ainsi, chrétiens, permettez que je m'adresse à l'impie qui
souffre sans se convertir, et que je lui fasse sentir, s'il se peut,
qu'il commence son enfer dès ce monde, afin qu'ayant horreur de
lui-même, il retourne à Dieu par la pénitence. Et afin de le presser par
de vives raisons (car il faut, si nous le pouvons, convaincre
aujourd'hui sa dureté), disons en peu de mots : Qu'est-ce que l'enfer ?
L'enfer, chrétiens, si nous l'entendons, c'est la peine sans la
pénitence. Ne vous imaginez pas, chrétiens, que l'enfer soit seulement
ces ardeurs brûlantes. Il y a deux feux dans l'Écriture : un feu qui
purge, _opus prubabit ignis_ (3) ; « un feu qui consume et qui dévore, »
_cum igne devorante, ignis non extinguetur_ (4). La peine avec la
pénitence, c'est un feu qui purge ; la peine sans la pénitence, c'est un
feu qui consume ; et tel est proprement le feu de l'enfer. C'est
pourquoi les afflictions de la vie sont un feu où se purgent les âmes
pénitentes : _Salvus erit, sic tamen quasi per ignem_ (5) : il en est
ainsi des âmes du purgatoire. Elles se nettoient dans ce feu, parce que
la peine est jointe aux sentiments de la pénitence qu'elles ont emportée
en sortant du monde, _quasi per ignem_. Par conséquent concluons que la
peine sanctifiée par la pénitence nous est un gage de miséricorde ; et
concluons aussi au contraire que le caractère propre de l'enfer, c'est
la peine sans la pénitence.

Si vous voulez voir, chrétiens, des peintures de ces gouffres éternels,
n'allez pas rechercher bien loin ni ces fourneaux ardents,

 

1 _De Civit. Dei,_ lib. I, cap. VIII. — 2 _Ibid_., lib. XIII, cap. IV. —
3 I _Cor.,_ III, 12. —  4 _Isa.,_ XXXIII, 14 ; LXVI, 24. — 5 I _Cor.,_
III, 15.

 

462

 

ni ces montagnes ensoufrées qui vomissent des tourbillons de flammes, et
qu'un ancien appelle « des cheminées de l'enfer, » _Ignis inferni
fumariola_ (1). Voulez-vous voir une vive image de l'enfer et d'une âme
damnée, regardez un pécheur qui souffre et qui ne se convertit pas. Tels
étaient ceux dont David parle comme d'un prodige, « que Dieu avait
dissipés, nous dit ce Prophète, et qui n'étaient pas touchés de
componction : » _Dissipati sunt, nec compuncti_ (2) : serviteurs
rebelles et opiniâtres, qui se révoltent même sous la verge ; abattus
(_a_) et non corrigés, atterrés et non humiliés, châtiés et non
convertis. Tel était le déloyal Pharaon, dont le cœur s'endurcissait
tous les jours sous les coups incessamment redoublés de la vengeance
divine. Tels sont ceux dont il est écrit dans l'_Apocalypse_ (3), que
Dieu les ayant frappés d'une plaie horrible, de rage ils mordaient leurs
langues, blasphémaient le Dieu du ciel, et ne faisaient point pénitence.
Tels hommes ne sont-ils pas des damnés qui commencent leur enfer dès ce
monde ?

Et il ne faut pas dire : Nous souffrons. Il y en a que la croix
précipite à la damnation avec ce larron endurci : au lieu de se corriger
par la pénitence et de s'irriter contre eux-mêmes, et de faire la guerre
à leurs crimes (_b_), ils s'irritent contre le Dieu du ciel ; ils se
privent des biens de l'autre vie, on leur arrache ceux de celle-ci : si
bien qu'étant frustrés de toutes parts, pleins de rage et de désespoir
et ne sachant à qui s'en prendre, ils élèvent contre Dieu leur langue
insolente par leurs murmures et par leurs blasphèmes ; « et il semble,
dit Salvien, que leurs fautes se multipliant avec leurs supplices, la
peine même de leurs péchés soit la mère de nouveaux crimes : » _Ut
putares pœnam ipsorum criminum quasi matrem esse vitiorum_ (4).

Ah! mes Frères, ils vous font horreur ces damnés vivants sur la terre ;
vous ne les pouvez supporter, vous détournez vos yeux de dessus leurs
crimes ; mais détournez-en plutôt votre cœur, et recourez à Dieu par la
pénitence. Eveillez-vous enfin, ô pécheurs, du moins quand Dieu vous
frappe par des maladies, par la perte

 

1 Tertull., _De Pœnit_., n. 12. — 2 _Psal_. XXXIV, 16. — 3 Apoc, XVI,
10, 11. — 4 _De Gubernat. Dei,_ lib. VI, n. 13.

 

(_a_) _Var._ ; Frappés. — (_b_) Et de s'irriter contre eux-mêmes et
contre leurs crimes.

 

463

 

de vos biens ou de vos amis : joignez aux peines que vous endurez la
conversion de vos âmes ; et cette croix que Dieu vous envoie qui
maintenant vous est un supplice, vous deviendra un salutaire
avertissement et un gage infaillible de miséricorde. Jusqu'à quand
fermerez-vous vos oreilles, jusqu'à quand endurcirez-vous vos cœurs
contre la voix de Dieu qui vous parle, et contre sa main qui vous
frappe? Abaissez-vous sous son bras puissant ; et portez la croix qu'il
vous met dessus les épaules (_a_), avec l'humilité et dans les
sentiments de la pénitence.

Vous particulièrement, mes chers Frères, sainte et bienheureuse
conquête, nouveaux enfants de l'Église, qu'elle se glorifie d'avoir
retirés au centre de son unité et au sein de sa charité : je n'ignore
pas les tourments que la haine irréconciliable de vos adversaires, que
le cruel abandonnement et l'injuste persécution de vos proches vous font
endurer. Mais soutenez tout par la patience : c'est une espèce de
martyre que vous souffrez pour la foi que vous avez embrassée. Dieu veut
épurer votre charité par l'épreuve des afflictions : ce ne lui est pas
assez, mes chers Frères, de vous avoir arrachés au diable par la foi,
s'il ne vous en faisait triompher (_b_) parla constance : il ne veut pas
seulement que vous échappiez, mais encore que vous surmontiez vos
ennemis. Non content de vous appeler au salut par la profession de la
foi, il vous invite encore à la gloire par le combat ; et il veut
apporter le comble au bonheur d'être délivrés, par l'honneur d'être
couronnés. C'est votre gloire devant Dieu, mes Frères, de sceller votre
foi par vos souffrances ; et la pauvreté où vous êtes, rend un
témoignage honorable à l'amour que vous avez pour l'Église.

Mais, chrétiens, ce qui fait leur gloire, c'est cela même qui fait notre
honte. Il leur est glorieux de souffrir ; mais il nous est honteux de le
permettre. Leur pauvreté rend témoignage pour eux et contre nous :
l'honneur de leur foi, c'est la conviction de notre dureté. Sera-t-il
dit, mes Frères, qu'ils seront venus à notre unité, y chercher leurs
véritables frères dans les véritables enfants de l'Église, pour être
abandonnés de leur secours ; et que nos adversaires nous reprocheront
qu'on a soin assez

 

(_a_) _Var. _: Qu'il vous impose. — (_b_) Les victorieux.

 

464

 

d'attirer les leurs, mais qu'on les laisse en proie à la misère? D'où
jugeant de la vérité de notre foi par notre charité, ô jugement injuste,
mais trop ordinaire parmi eux ! ils blasphémeront contre l'Église, et
notre insensibilité en sera la cause. Mes Frères, qu'il n'en soit pas de
la sorte : pendant qu'ils souffrent pour notre foi, soutenons-les par
nos charités.

Ceux qui ont souffert pour la foi, ce sont ceux que la sainte Église a
toujours recommandés avec plus de soin. Les martyrs étant dans les
prisons, les chrétiens y accouraient en foule : quelques gardes que l'on
posât devant les prisons, la charité des fidèles pénétrait partout.
Toute l'Église travaillait pour eux ; et croyait que leurs souffrances
honorant l'Église en sa foi, il n'y avait rien de plus nécessaire que
les autres qui étaient libres les honorassent par la charité. Ailleurs
on leur prêchait une discipline sévère ; il semblait qu'il n'y eût que
dans les prisons où il fût permis de les traiter délicatement, ou du
moins de relâcher quelque chose de l'austérité ordinaire. Il s'y coulait
même des païens, et nous en avons des exemples dans l'antiquité : ainsi
la charité des fidèles rendait les prisons délicieuses. Pourquoi tant de
zèle ? Ils croyaient par ce moyen professer la foi et participer au
martyre, « se ressouvenant de ceux qui étaient dans les chaînes, comme
s'ils eussent été eux-mêmes enchaînés : » _Vinctorum tanquam simul
vincti_ (1) ; ils croyaient s'enchaîner avec les martyrs.

C'est par la croix et par les souffrances que la confession de foi doit
être scellée. C'est ce qui fait dire à Tertullien que « la foi est
obligée au martyre, » _debitricem martyrii fidem_ (2) ; par où il veut
dire, si je ne me trompe, que cette grande soumission à croire les
choses incroyables ne peut être mieux confirmée qu'en se soumettant
aussi à en souffrir de pénibles et de difficiles, et qu'en captivant son
corps pour rendre un témoignage ferme et vigoureux à ces bienheureuses
chaînes par lesquelles la foi captive l'esprit. C'est pourquoi après
avoir fait faire aux nouveaux catholiques leur profession de foi, on les
met dans une maison dédiée à la croix.

Mes Frères, accourez donc en ce lieu ; ceux qui y sont retirés

 

1 _Hebr_., XIII, 3. — 2 _Scorp_., n. 8.

 

465

 

ne se comparent pas aux martyrs, mais néanmoins c'est pour la foi qu'ils
endurent. Ils ne sont pas liés dans des prisons ; mais néanmoins ils
portent leurs chaînes: _Vinctos in mendicitate et ferro_ (1) ; non
chargés de fer, mais bien par la pauvreté. Venez leur aider à porter
leur croix : car qu'attendez-vous, chrétiens? Quoi? que la misère et le
désespoir les contraignent à jeter les yeux du côté du lieu d'où ils
sont sortis, et à se souvenir de l'Égypte? O Dieu, détournez de nous un
si grand malheur. Ils ne le feront pas, chrétiens, ils sont trop fermes,
ils sont trop fidèles : mais combien toutefois sommes-nous coupables de
les exposer à ce péril!

Ouvrez donc vos cœurs, je vous en conjure par la croix que vous adorez ;
ouvrez vos cœurs, et ouvrez vos mains sur les nécessités de cette maison
et sur la pauvreté extrême de ceux qui l'habitent : abandonnés des leurs
qu'ils ont quittés pour le Fils de Dieu, ils n'ont plus de secours qu'en
vous. Recevez-les, mes Frères, avec des entrailles de miséricorde ;
honorez en eux la croix de Jésus : ils la portent avec patience, je leur
rends aujourd'hui ce témoignage ; mais ils ne la portent pas néanmoins
sans peine : rendez-la-leur du moins supportable par l'assistance de vos
charités ; et que j'apprenne en sortant d'ici que les paroles que je vous
adresse, ou plutôt que toute l'Église et Jésus-Christ même vous
adressent en leur faveur par mon ministère, n'auront pas été un son
inutile.

O joie, ô consolation de mon cœur î Si vous me donnez cette joie et
cette sensible consolation, je prierai ce divin Sauveur, qui souffre
avec eux et qui souffre en eux, qu'il répande sur vous les siennes,
qu'il vous aide à porter vos croix, comme vous aurez prêté vos mains
charitables pour aider ces nouveaux enfants de l'Église à porter la leur
plus facilement ; et enfin que pour les aumônes que vous aurez semées en
ce monde, il vous rende en la vie future la moisson abondante qu'il nous
a promise. _Amen_.

 

1  _Psal._ CVI, 10.

[Précédente]

[Accueil]

[Suivante]


CHAPITRE V.



*Le livre fermé de sept sceaux : l'Agneau devant le trône : lui seul
peut ouvrir le livre : les louanges qui lui sont données par toutes les
créatures.*


1. Je vis ensuite dans la main droite de celui qui était assis sur le
trône, un livre écrit dedans et dehors, scellé de sept sceaux.

2. Et je vis un ange fort, qui criait à haute voix : Qui est digne
d'ouvrir le livre et d'en lever les sceaux ?

3. Et nul ne pouvait, ni dans le ciel, ni sur la terre, ni sous la
terre, ouvrir le livre ni le regarder.

4. Je fondais en larmes de ce que personne ne s'était trouvé digne
d'ouvrir (a) le livre ni de le regarder.

5. Mais l'un des vieillards me dit : Ne pleure point ; voici le lion de
la tribu de Juda, le rejeton de David, qui a obtenu par sa victoire le
pouvoir d'ouvrir le livre et d'en lever les sept sceaux.

6. Je regardai : et je vis au milieu du trône et des quatre animaux, et
au milieu des vieillards, un Agneau debout comme égorgé, ayant sept
cornes et sept yeux, qui sont les sept Esprits de Dieu envoyés par toute
la terre.

7. Il vint, et il prit le livre de la main droite de celui qui était
assis sur le trône.

8. Et l'ayant ouvert (6), les quatre animaux et les vingt-quatre
vieillards se prosternèrent devant l'Agneau, ayant chacun des harpes et
des coupes d'or pleines de parfums, qui sont les prières des Saints.

9. Ils chantaient (c) un cantique nouveau, en disant : Vous êtes


(a) *Grec* : Ni de lire. — (b) Et l'ayant pria, les quatre, etc. — (c)
Chantent (Seigneur) n'est pas dans le grec.


397


digne, Seigneur, de prendre le livre et d'enlever les sceaux, parce que
vous avez été mis à mort et que vous nous avez rachetés pour Dieu par
votre sang, de toute tribu, de toute langue, de tout peuple et de toute
nation.

10. Et vous nous avez faits rois et sacrificateurs à notre Dieu ; et
nous régnerons sur la terre.

11. Je regardai encore, et j'entendis autour du trône, et des animaux,
et des vieillards, la voix de plusieurs anges, dont le nombre allait
jusqu'à des milliers de milliers (a) :

12. Qui disaient à haute voix : L'Agneau qui a été égorgé, est digne de
recevoir la vertu, la divinité (b), la sagesse, la force, l'honneur, la
gloire et la bénédiction.

13. Et j'entendis toutes les créatures qui sont dans le ciel, sur la
terre, sous la terre, et celles qui sont dans la mer, et tout ce qui y
est (c) : je les entendis toutes qui disaient : Bénédiction, honneur et
gloire, et puissance soient à celui qui est assis sur le trône, et à
l'Agneau, dans les siècles des siècles.

14. Et les quatre animaux disaient : Amen. Et les vingt-quatre
vieillards se prosternèrent sur le visage (d), et adorèrent celui qui
vit dans les siècles des siècles.



EXPLICATION DU CHAPITRE V.


*Le livre scellé, ce que c'est : le mystère du nombre de sept dans
l'Apocalypse.*


1. *Je vis ensuite... un livre écrit dedans et dehors, scellé de sept
sceaux* : c'était un rouleau à la manière des anciens. *Scellé de sept
sceaux* : ce sont les secrets jugements de Dieu. Saint Ambroise appelle
ce livre le *Livre prophétique* (1), le livre où étaient comprises les
destinées des hommes que Jésus-Christ va révéler à saint Jean. Le livre
est scellé, quand les jugements ne sont pas encore déclarés. « La vision
vous sera comme les paroles d'un livre scellé, où personne ne peut lire.
» *Is.,* XXIX, 11. *Écrit dedans*


1 Ambr., III *De Fid.,* VII.

(a) *Grec* : Des millions de millions , et des milliers de milliers. —
(b) Les richesses. — (c) *In eo,* dans notre Vulgate ; *in eis,* en eux,
dans le grec. — (d) Se prosternèrent (sans ajouter *sur leur visage*).


397


*et dehors* : on n'écrivait ordinairement que d'un côté, si ce n'est
quand il y avait beaucoup de choses à écrire. Ainsi dans *Ezéchiel,* ch.
II, 9, le livre présenté au prophète « est écrit dedans et dehors, et
contient les malédictions et les malheurs. »

*Sept sceaux* : les saints docteurs ont remarqué que le nombre de sept
était consacré dans ce livre pour signifier une certaine universalité et
perfection : c'est pourquoi on a vu d'abord les sept esprits qui sont
devant le trône, I, 4 ; sept chandeliers, sept étoiles, sept églises,
pour désigner toute l'unité catholique, comme il a été remarqué, là
même, vers. 4, 12, 16, 20, etc. On a vu ensuite les sept lampes
brûlantes, qui sont encore les sept esprits, IV, 5. Dans le chapitre que
nous expliquons, on signifie ces mêmes sept esprits par les « sept
cornes et les sept yeux de l'Agneau, » V, 6. C'est que dans le nombre de
sept on entend une certaine perfection, soit à cause des sept jours de
la semaine marqués dès la création, où la perfection est dans le
septième, soit pour quelqu'autre raison. Ici il y a sept sceaux ; on
entendra dans la suite sept anges avec leurs trompettes, et sept
tonnerres. Sept anges porteront les fioles, ou les coupes pleines de la
colère de Dieu. Le dragon et la bête qu'il animera, auront sept têtes ;
enfin tout ira par sept dans ce divin Livre, jusqu'à donner à l'Agneau,
en le bénissant, sept glorifications, V, 12, et autant à Dieu, chap.
VII, 12 ; ce qu'il faut observer d'abord, de peur qu'on ne croie que ce
soit partout un nombre préfix : mais qu'on remarque au contraire que
c'est un nombre mystique, pour signifier la perfection. On sait aussi
que c'est une façon de parler de la langue sainte, de signifier un grand
nombre et indéfini par le nombre défini de sept.

4. *Je fondais en larmes...* : il voit qu'on lui veut ouvrir le livre,
mais que personne n'est digne de l'ouvrir, 2, 3 ; et il déplore tout
ensemble la perte qu'il fait et l'indigne disposition du genre humain.

5. *Le lion de la tribu de Juda, le rejeton de David...* : selon ce qui
est écrit dans la prophétie de Jacob : Juda est un jeune lion etc.
*Gen.,* XLIX, 9. On entend bien que c'est Jésus-Christ, fils de David,
que saint Jean appelle un lion à cause de sa force invincible et qui va
paraître comme un agneau, à cause qu'il a été immolé.


399


C'est ainsi que le Saint-Esprit relève les idées de la faiblesse
volontaire de Jésus-Christ par celle de sa puissance.

*Qui a obtenu par sa victoire le pouvoir d'ouvrir le livre* :
Jésus-Christ, vainqueur du démon et de la mort, a mérité par cette
victoire d'entrer dans tous les secrets de Dieu.

6. *Et je vis un Agneau debout comme égorgé* : *ἑστηκὸς* : il est debout
et vivant ; mais il paraît comme mort et comme immolé, à cause de ses
plaies qu'il a portées dans le ciel. *Au milieu du trône* : cela marque
la médiation de Jésus-Christ, qui empêche les éclairs et les tonnerres
qui sortent du trône (*Apoc.,* IV, 5), de venir jusqu'à nous.

*Qui sont les sept esprits.* Voyez *Apoc.,* I, 4.

8. *Et l'ayant ouvert.* Le grec : *L'ayant pris* : ainsi ont lu André de Césarée, Ticonius, *Hom.* IV, Primase
l'interprète sous le nom de saint Ambroise, et Bède. Il semble naturel
qu'on prenne le livre devant que de l'ouvrir, et l'ouverture qui se fait
des sceaux l'un après l'autre est marquée au chap. VI. Mais il se peut
faire aussi que l'Écriture propose d'abord en gros ce qui s'explique
après dans le détail. On voit ici que c'est Jésus-Christ qui est le
dépositaire et l'interprète des desseins de Dieu.

*Les quatre animaux et les vingt-quatre vieillards se prosternèrent* : ...
ils adorent l'Agneau de la même sorte qu'ils avaient adoré Dieu et en sa
présence : marque de sa divinité.

*Des harpes et des coupes d'or* : ...les vieillards paraissent ici avec
des instruments de musique, dont on n'avait point parlé au chapitre IV.
Les harpes signifient la joie céleste, et le parfait accord des passions
avec la raison dans les Saints. Les coupes d'or pleines de parfums, qui
sont les prières des Saints, entre les mains des vieillards, signifient
qu'ils sont chargés de les présenter à Dieu.

11, 12. *J'entendis la voix de plusieurs anges... qui disaient : ...
L'Agneau... est digne de recevoir la vertu, la divinité...* Le grec,
comme aussi Primase et les autres anciens : *πλοῦτον,* *divitias* : d'où
il se peut qu'on ait fait *divinitas,* et puis *divinitatem,* quoiqu'on
peut dire dans un très-bon sens que le Fils reçoit la divinité, quand la
gloire en est manifestée en sa personne. Il faut ici observer


400


que les Saints disent que l'Agneau les a rachetés et qu'ils lui doivent
ce qu'ils sont, *Apoc.,* V, 9, 10 ; ce que les anges ne disent pas.

13, 14. *Et j'entendis toutes les créatures* : ...toutes les créatures
joignent leurs voix à celles des vieillards et des anges, et les quatre
animaux chantent *amen* ; il se fait un concert de tous les esprits pour
louer Dieu. Il faut aussi remarquer qu'après avoir loué Dieu le
Créateur, *Apoc.,* IV, 10, 11, et Jésus-Christ, V, 9, 11, tout le chœur
loue ensemble le Père et le Fils.


PREMIÈRE PARTIE DE LA PROPHÉTIE. LES AVERTISSEMENTS.

CHAPITRE II.

*Saint Jean reçoit ordre d'écrire aux évêques d'Éphèse, de Smyrne, de
Pergame et de Thyatire, les raisons du blâme ou des louanges que
méritent leurs églises.*

1. Écris à l'ange de l'église d'Éphèse : Voici ce que dit celui qui
tient les sept étoiles dans sa main droite , qui marche au milieu des
sept chandeliers d'or :

2. Je sais tes œuvres, et ton travail, et ta patience ; et que tu ne
peux supporter les méchants : tu as éprouvé ceux qui se disent apôtres,
et ne le sont point ; et les as trouvés menteurs :

3. Tu es patient (a), et tu as souffert pour mon nom, et tu ne t'es
point découragé.

4. Mais j'ai à te reprocher que tu es déchu de ta première charité.

5. Souviens-toi donc d'où tu es tombé, et fais pénitence, et reprends
tes premières œuvres : sinon je viendrai bientôt à toi ; et si tu ne
fais pénitence, j'ôterai ton chandelier de sa place.

6. Tu as toutefois cela de bon, que tu hais les actions des Nicolaïtes,
comme moi-même je les hais.

7. Que celui qui a des oreilles, écoute ce que l'Esprit dit aux églises
: Je donnerai au vainqueur à manger du fruit de l'arbre de vie, qui est
dans le paradis de mon Dieu (b).

8. Écris aussi à l'ange de l'église de Smyrne : Voici ce que dit celui
qui est le premier et le dernier, qui a été mort, et qui est vivant.

9. Je sais ton affliction et ta pauvreté (c) ; tu es toutefois riche,


(a) Et tu as été dans la peine. — (b) *Grec* : Qui est au milieu du
paradis. — (c) Tes œuvres.

359

et tu es calomnié par ceux qui se disent Juifs (a), et ne le sont pas,
mais qui sont la synagogue de Satan.

10. Ne crains rien de ce que tu auras à souffrir. Le diable mettra
bientôt quelques-uns de vous en prison, afin que vous soyez éprouvés, et
vous aurez à souffrir pendant dix jours. Sois fidèle jusqu'à la mort, et
je te donnerai la couronne de vie.

11. Que celui qui a des oreilles, écoute ce que l'Esprit dit aux églises
: Celui qui sera victorieux ne souffrira rien de la seconde mort.

12. Écris à l'ange de l'église de Pergame : Voici ce que dit celui qui
porte l'épée à deux tranchants (b).

13. (c) Je sais que tu habites où est le trône de Satan : tu as
conservé mon nom, et tu n'as point renoncé ma foi, lorsqu'Antipas, mon
témoin fidèle, a souffert la mort parmi vous, où Satan habite.

14. Mais j'ai quelque chose à te reprocher ; c'est que tu souffres
parmi vous qu'on enseigne la doctrine de Balaam, qui apprenait à Balac à
jeter des pierres de scandale devant les enfants d'Israël, afin qu'ils
mangeassent (d), et qu'ils tombassent dans la fornication.

15. Tu souffres aussi qu'on enseigne la doctrine des Nicolaïtes (e).

10. Fais pareillement pénitence ; sinon je viendrai bientôt à toi, et je
combattrai contre eux avec l'épée de ma bouche.

17. Que celui qui a des oreilles, écoute ce que l'Esprit dit aux
églises : Je donnerai au vainqueur (f) la manne cachée ; je lui donnerai
une pierre blanche, et un nom nouveau écrit sur la pierre, lequel nul ne
connaît que celui qui le reçoit.

18. Écris encore à l'ange de l'église de Thyatire : Voici ce que dit le
Fils de Dieu , qui a les yeux comme une flamme de feu et les pieds
semblables à l'airain fin :

19. Je sais tes œuvres, ta foi, ta charité , le soin que tu prends des
pauvres, ta patience et tes dernières œuvres plus abondantes que les
premières.


(a) *Grec* : Je connais les calomnies que tu souffres de ceux qui se
disent Juifs. — (b) Affilée. — (c) Je connais tes œuvres. — (d) Des
viandes immolées aux idoles. — (e) Ce que je hais. — (f) À manger la.


360


20. Mais j'ai quelque chose à te reprocher : Tu permets que Jézabel,
cette femme qui se dit prophétesse, enseigne et séduise mes serviteurs,
afin de les faire tomber dans la fornication et de leur faire manger des
viandes immolées aux idoles.

21. Je lui ai donné du temps pour faire pénitence (a), et elle ne veut
point se repentir de sa prostitution.

22. Je la jetterai dans le lit (6) ; et ceux qui commettent adultère
avec elle, seront dans une très-grande affliction, s'ils ne font
pénitence de leurs œuvres.

23. Je frapperai ses enfants de mort ; et toutes les églises connaîtront
que je suis celui qui sonde les reins et les cœurs ; et je rendrai à
chacun de vous selon ses œuvres : mais je vous dis,

24. Et aux autres qui sont à Thyatire, à tous ceux qui ne tiennent
point cette doctrine, et qui, comme ils disent, ne connaissent point
les profondeurs de Satan : je ne mettrai point d'autre poids sur vous.

25. Toutefois gardez fidèlement ce que vous avez, jusqu'à ce que je
vienne.

26. Celui qui sera victorieux et gardera mes œuvres jusqu'à la fin, je
lui donnerai puissance sur les nations.

27. Il les gouvernera avec un sceptre de fer, et elles seront brisées
comme un vase d'argile.

28. Tel est ce que j'ai reçu de mon Père : et je lui donnerai l'étoile
du matin.

29. Que celui qui a des oreilles, écoute ce que l'Esprit dit aux
églises.



EXPLICATION DU CHAPITRE II.


1. *Écris.* La fonction prophétique commence ici dans les admirables
avertissements que Jésus-Christ fait écrire aux églises par saint Jean.
Dans ces avertissements il fait voir qu'il sonde le secret des cœurs,
verset 23, qui est la plus excellente partie de la prophétie, selon ce
que dit saint Paul, I *Cor.,* XIV, 21, 25 : « Les secrets des cœurs sont
révélés » par ceux qui prophétisent dans les assemblées,


(a) *Grec* : De sa prostitution, et elle ne l'a pas voulu faire. — (b)
Et je jetterai dans une très-grande affliction ceux qui.


361


et celui qui les écoute « prosterné à terre, reconnaît que Dieu
est en vous. »

*À l'ange de l'église d'Éphèse* : à son évêque, selon la commune
interprétation de tous les Pères. Il ne faut pourtant pas croire que les
défauts qui sont marqués dans cet endroit et dans les autres semblables
soient les défauts de l'évêque ; mais c'est que le Saint-Esprit désigne
l'église par la personne de l'évêque qui y préside, et dans laquelle
pour cette raison elle est en quelque façon renfermée ; et aussi parce
qu'il veut que le pasteur qui voit des défauts dans son troupeau,
s'humilie et les impute à sa négligence.

*De l'église d'Éphèse.* On croit que c'était alors saint Timothée,
très-éloigné sans doute des défauts que saint Jean va reprendre dans les
fidèles d'Éphèse. D'autres disent que c'était saint Onésime, à qui je ne
voudrais non plus les attribuer, après le témoignage que lui rend saint
Paul dans l'Epître à Philémon : mais il y a plus d'apparence que c'était
saint Timothée, qui fut établi par saint Paul évêque d'Éphèse, et qui
gouverna cette église durant presque toute la vie de saint Jean.

*Celui qui tient les sept étoiles... qui marche au milieu des sept
chandeliers* : Tout cela signifie les sept églises, 20. Le Saint-Esprit
va reprendre toutes les diverses qualités qui viennent d'être attribuées
à Jésus-Christ les unes après les autres. Voyez ci-dessus, I, 13,16.

2. *Qui se disent apôtres, et ne le sont point* : le nombre de ces faux
apôtres était grand. Saint Paul en parle souvent, et principalement II
Cor. XI, 13, et saint Jean lui-même, IIIe *Epit.* 9, lorsqu'il parle de
Diotréphès qui ne voulait pas le reconnaître.

5. *J’ôterai ton chandelier de sa place* : Je t'ôterai le nom d'église,
et je transporterai ailleurs la lumière de l'Évangile. Lorsqu'elle cesse
quelque part, elle ne s'éteint pas pour cela, mais elle est transportée
ailleurs, et passe seulement d'un peuple à un autre.

6. *Des Nicolaïtes* : hérétiques très-impurs qui condamnaient le
mariage, et lâchaient la bride à l'intempérance, ci-dessous, 14, 15.

7. *À manger du fruit de l'arbre de vie qui est dans le paradis*


362


*de mon Dieu* : dont quiconque mangeait ne mourait point, dont Adam fut
éloigné, de peur qu'en mangeant de son fruit, il ne vécût éternellement,
Gen., II, 9 ; III, 22. Jésus-Christ nous le rend lorsqu'il dit : « Voici
le pain qui descend du ciel, afin que celui qui en mange ne meure point.
» Joan., VI, 50. C'est le fruit de l'arbre de vie, c'est-à-dire
Jésus-Christ attaché à la croix pour notre salut, *Prim. Amb*.

8. *À l'ange de l'église de Smyrne* : c'était alors saint Polycarpe, «
établi par les apôtres évêque de Smyrne, » comme le raconte saint Irénée
(1), et, selon Tertullien (2), par saint Jean même, homme apostolique,
dont le martyre arrivé très-longtemps après dans son âge décrépit, a
réjoui toutes les églises du monde.

*Qui est le premier et le dernier* : repris du chapitre I, 17, 18.

9. *Tu es calomnié par ceux qui se disent Juifs.* On voit ici la haine
des Juifs contre les églises, et en particulier contre l'église de
Smyrne, et on en vit les effets jusqu'au temps du martyre de saint
Polycarpe, contre lequel ils animèrent les gentils, comme il paraît par
la lettre de l'église de Smyrne à celle de Vienne (3). Voyez *Apoc.,* III,
9, et remarquez que les persécutions des églises chrétiennes étaient
suscitées par les Juifs, comme il sera dit ailleurs.

10. *Le diable mettra bientôt quelques-uns de vous en prison* : sur la
fin de Domitien, lorsque saint Jean écrivait, la persécution était
encore languissante : c'est pourquoi il ne parle ici que de «
quelques-uns mis en prison, » et d'une souffrance « de dix jours, »
c'est-à-dire courte, surtout en comparaison de celles qui devaient venir
bientôt après, comme on verra.

11. *De la seconde mort* : c'est l'enfer et la mort éternelle, comme il
sera expliqué, XX, 6, 14. C'est cette seconde mort qu'il faut craindre
seule, et qui l'aura évitée ne doit point appréhender la mort du corps :
ce que saint Jean remarque ici, afin qu'on ne craignit point de souffrir
la mort dans la persécution qui allait venir.

12. *Celui qui porte l'épée à deux tranchants,* repris du chapitre I,
16.


1 Iren., III, III. — 2 De Praesc., XXXII. — 3 Euseb., III, XIV.


363


13. *Antipas mon témoin fidèle.* Le supplice de ce saint martyr est
raconté dans les *Martyrologes,* et il y est dit qu'il fut jeté dans un
taureau d'airain brûlant ; ce que je laisse à examiner aux critiques.

14. 15. *La doctrine de Balaam.* Balaam après avoir béni les Israélites
malgré lui, donne des conseils pour les corrompre par des festins où ils
mangeaient des viandes immolées aux idoles, et par des femmes perdues.
L'histoire en est racontée, *Num.,* XXIV, 14 ; XXV, 1,2, etc. Ainsi les
Nicolaïtes enseignaient à participer aux fêtes et aux sacrifices des
gentils et à leurs débauches. Voyez aussi verset 20.

17. *La manne cachée* : dont le monde ne connaît point la douceur, et
que nul ne sait que celui qui la goûte. La manne, c'est la nourriture
dans le désert et la secrète consolation dont Dieu soutient ses enfants
dans le pèlerinage de cette vie. Ambr. Celui qui méprisera les appas des
sens, est digne d'être nourri de la céleste douceur du pain invisible.
Bed.

*Une pierre blanche* : une sentence favorable. *And. Cæsar.* Dans les
jugements on renvoyait absous, et dans les combats publics on adjugeait
la victoire avec une pierre blanche ; ainsi Dieu nous donnera dans le
fond du cœur, par la paix de la conscience, un témoignage secret de la
rémission de nos péchés et de la victoire remportée sur nos sens.

*Et un nom nouveau écrit sur la pierre,* c'est que « nous soyons
appelés, et que nous soyons en effet enfants de Dieu, » selon ce que dit
saint Jean, *I Joan.,* III, 1 ; et parce que, comme dit saint Paul, «
l'Esprit rend témoignage à notre esprit que nous sommes enfants de Dieu,
*Rom.,* VIII, 16.

*Un nom que nul ne connaît que celui qui le reçoit* : l'hypocrite ne
connaît pas combien Dieu est doux, et il faut l'avoir goûté pour le bien
savoir.

18. *À l'ange de l'église de Thyatire.* Cette église fut pervertie par
les Montanistes, au rapport de saint Épiphane1, qui semble avouer aux
Alogiens qu'il n'y a point eu d'église à Thyatire du temps de saint
Jean, et qui veut pour cette raison que la prophétie


1 *Hær.* LI, *Alog.,* n. 33.


364


des versets suivants regarde Montan et ses fausses prophétesses ; mais
le rapport paraît faible. On ne voit pas non pas pourquoi saint Jean
aurait adressé une lettre à une église qui ne fût pas, en la joignant
avec les autres si bien établies à qui il écrit. On pourrait attribuer
le commencement de l'église de Thyatire à Lydie qui était de cette
ville-là, et qui paraît si zélée pour l'Évangile à Philippes, où saint
Paul la convertit avec toute sa famille, *Act.,* XVI, 14, 40.

*Qui a les yeux comme une flamme* : repris du chapitre I, vers. 14, 15.

20. *Tu permets que Jézabel* : c'est, sous le nom de Jézabel, femme
d'Achab, quelque femme considérable, vaine et impie, qui appuyait les
Nicolaïtes, comme l'ancienne Jézabel appuyait les adorateurs de Baal. Le
rapport de ce verset avec les précédents, 14,15, ne permet pas de douter
qu'il ne s'agisse ici des Nicolaïtes. *Qui se dit prophétesse* : elle se
servait de ce nom pour autoriser les plus grandes impuretés. Tout ceci
ne revient guère aux prophétesses de Montan, et sent plutôt les
Nicolaïtes et les gnostiques que les Montanistes.

23. *Toutes les églises connaîtront que je sonde les reins.* Où sont
ceux qui disent que dans le gouvernement de l'Église, Jésus-Christ ne
doit pas agir comme scrutateur des cœurs ? Dans les reins sont marquées
les secrètes voluptés, et dans le cœur les secrètes pensées, *Bède*.

24. *Qui, comme ils disent, ne connaissent point les profondeurs de
Satan* : qui ne se laissent point séduire à sa profonde et impénétrable
malice, lorsqu'il tâche de tromper les hommes par une apparence de
piété, et qu'il couvre de ce bel extérieur les plus grossières erreurs.

*Je ne mettrai point d'autre poids sur vous.* Je ne vous donnerai point
d'autre combat à soutenir, et ce sera beaucoup si vous pouvez échapper
ce mystère d'iniquité et d'hypocrisie.

26. *Quiconque... gardera mes œuvres jusqu'à la fin* : il
marque ici clairement ceux qui auront reçu le don de persévérance.

*Je lui donnerai puissance sur les nations.* 27. *Il les gouvernera* :


365


... On voit ici le règne de Jésus-Christ avec ses Saints qu'il associe à
son empire : c'est pourquoi il les met sur son trône, III, 21, 2-2. Il
faut aussi comparer ce passage avec XIX, 15, où Jésus-Christ s'attribue
à lui-même ce qu'il donne ici à ses Saints. Ou voit encore les Saints
assesseurs de Jésus-Christ, XX, 4, et on a pu remarquer sur ce sujet un
beau passage de saint Denys d'Alexandrie chez Eusèbe, lib. VI, c. XLII.
Voyez la réflexion après la préface, n. 29.

28. *Et je lui donnerai l'étoile du matin.* Je lui ferai commencer un
jour éternel, où il n'y aura point de couchant et qui ne sera suivi
d'aucune nuit. Bède.

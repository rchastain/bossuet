[EXPLICATION]

[Précédente]

[Accueil]

[Remonter]
Bibliothèque

Accueil
Remonter
Remarques
Avertissement
Liber Sapientiae
Liber Ecclesiasticus
Isaïe
Psaume  XXI
Apocalypse
Apocalypse I
Apocalypse II
Apocalypse III
Prédictions
Apocalypse IV
Apocalypse V
Apocalypse VI
Apocalypse VII
Apocalypse VIII
Apocalypse IX
Apocalypse X
Apocalypse XI
Apocalypse XII
Apocalypse XIII
Apocalypse XIV
Apocalypse XV
Apocalypse XVI
Apocalypse XVII
Apocalypse XVIII
Apocalypse XIX
Apocalypse XX
Apocalypse XXI
Apocalypse XXII
Explication



ABRÉGÉ DE L'APOCALYPSE.


 


I. — Pourquoi cet abrégé : l'Apocalypse est une espèce d'histoire de l'Église, divisée en trois temps.

 

Comme nous nous sommes arrêtés à chaque partie de l'_Apocalypse,_ ou
pour prendre de temps en temps quelque repos dans cette espèce de
voyage, ou plutôt pour considérer, à mesure que nous avancions, le
progrès que nous avions fait, il faut encore nous arrêter à la fm de
toute la course, puisque c'est après avoir vu tout ce divin Livre que
nous pouvons nous en former une idée plus juste par une pleine
compréhension de tout l'ouvrage de Dieu qui nous y est représenté.

En voici donc l'abrégé. Jésus-Christ paraît : les églises sont averties
: c'est Jésus lui-même qui leur parle par saint Jean pour leur apprendre
leur devoir, et en même temps son Saint-Esprit leur fait des promesses
magnifiques. Jésus-Christ appelle saint Jean pour lui découvrir les
secrets de l'avenir, et ce qui devait arriver à son Église, depuis le
temps où il lui parlait jusqu'à la fin des siècles et à l'entier
accomplissement de tout le dessein de Dieu (1). Il y a trois temps de
l'Église bien marqués : celui de son commencement et de ses premières
souffrances, celui de son règne sur la terre , celui de sa dernière
tentation (2), lorsque Satan déchaîné pour la dernière fois, fera un
dernier effort pour la détruire (3) ; ce qui est suivi aussitôt par la
résurrection générale et le jugement dernier (4). Après quoi il ne reste
plus qu'à nous faire voir l'Église toute belle et toute parfaite dans le
recueillement de tous les Saints et le parfait assemblage de tout le
corps dont Jésus-Christ est le chef (5).

 

1 _Apoc.,_ I, II, III. — 2 Chap. IV, jusqu'au XX.— 3 Chap. XX.— 4 Chap.
XX, II, 12. — 5 Chap. XXI, XXII.

 

586

 


II.— Premier temps. Les commencements de l'Église. Deux ennemis abattus au milieu de ses souffrances, les Juifs et les gentils : ces deux ennemis marqués très-distinctement par saint Jean.

 

Dans le premier temps, qui est celui du commencement de l'Église et de
ses premières souffrances, toute faible qu'elle paraît dans une si
longue et si cruelle oppression , saint Jean nous en découvre la
puissance en ce que tous ses ennemis sont abattus, c'est-à-dire les
Juifs et les gentils : les Juifs au commencement, et les gentils dans la
suite de cette prédiction jusqu'au chapitre XX (1).

Ces deux ennemis sont marqués très-distinctement par saint Jean : les
Juifs, lorsqu'il nous fait voir le salut des douze mille de chaque tribu
d'Israël, pour l'amour desquels on épargnait tout le reste de la nation ;
d'où vient aussi qu'en tous ces endroits il n'est nulle mention
d'idoles, parce que les Juifs n'en connaissaient pas et ne péchaient en
aucune sorte de ce côté-là ; et les gentils aussitôt après, à l'endroit
où il fait venir avec des armées immenses les rois d'Orient et les
peuples d'au delà de l'Euphrate (2), qui est aussi celui où pour la
première fois il est parlé « d'idoles d'or et d'argent (3), » et où les
gentils sont repris, parmi les plaies que Dieu leur envoie, de ne s'être
pas corrigés d'adorer les œuvres de leurs mains et les démons, non plus
que des autres crimes que le Saint-Esprit nous représente partout comme
des suites inséparables de l'idolâtrie (4).

Voilà donc les deux sortes d'ennemis dont l'Église avait encore à
souffrir, bien distinctement marqués. Les Juifs, qui ne cessaient par
leurs calomnies d'irriter les persécuteurs, comme saint Jean l'avait
remarqué dès le commencement de son Livre , lorsqu'il écrivait aux
Églises (5), et les gentils ou les Romains, qui, ne songeant, qu'à
accabler l'Église naissante, allaient plus que jamais l'opprimer « par
toute la terre (6) » qui était soumise à son empire, comme le même saint
Jean l'avait dit aussi au même endroit.

 

1 Chap. VII, VIII ; chap. IX, 14, jusqu'au chapitre XX. — 2 Chap. IX, 14
et suiv. — 3 Chap. IX, 20, 21. — 4 Rom., I, 21, etc. — 5 _Apoc.,_ II, 9.
— 6 _Apoc.,_ III, 10.

 

587

 


III.  — Les sauterelles, ou les hérésies entre ce qui regarde les Juifs et ce qui regarde des gentils.

 

Entre ces deux ennemis, incontinent après les Juifs et avant que d'avoir
nommé les gentils et les idoles, nous trouvons dans les sauterelles
mystiques (1) une autre sorte d'ennemis d'une espèce particulière, où
nous avons entendu les hérésiarques placés à la suite des Juifs dont ils
ont imité les erreurs, et devant les gentils qu'à la vérité ils ne
semblaient pas attaquer directement, comme devaient faire ces rois
d'Orient qu'on voit paraître au même chapitre , mais qui ne laissaient
pas de leur nuire beaucoup en obscurcissant le soleil, c'est-à-dire avec
la gloire de Jésus-Christ, les lumières de son Évangile et de son Église
; par où s'augmentait l'endurcissement des gentils, qui, selon que l'a
remarqué saint Clément d'Alexandrie, disaient en parlant des chrétiens :
« Il ne faut pas les en croire, puisqu'ils s'accordent si mal entre eux,
et qu'ils sont partagés entant d'hérésies ; ce qui retarde, » poursuit ce
grand homme, « les progrès de la vérité, à cause des dogmes contraires
que les uns produisent à l'envi des autres (2). »

 


IV. — Saint Jean passe aux violences et aux punitions de l'empire persécuteur : les Perses montrés comme ceux d'où devait venir le premier coup.

 

Il était bon une fois de faire voir que l'Église triomphoit de cet
obstacle, comme de tous les autres. Saint Jean après l'avoir fait d'une
manière aussi vive que courte et tranchante, s'attache ensuite à
représenter les persécutions romaines comme l'objet dont les hommes
étaient le plus frappés, pour faire éclater davantage la force de
l'Église en montrant la violence de l'attaque , et afin aussi de faire
admirer les sévères jugements de Dieu sur Rome persécutrice, avec
l'invincible puissance de sa main qui abattait aux pieds de son Église
victorieuse une puissance redoutée de tout l'univers.

Tout le chapitre IX, depuis le verset 14 jusqu'au chapitre XX, est donné
à ce dessein. Pour préparer les esprits à la chute de ce grand Empire,
saint Jean nous montre de loin les Perses, d'où lui devait venir le
premier coup. Le caractère dont il se sert pour les

 

1 Chap. IX, depuis le vers. 1 jusqu'au vers. 14. — 2 _Strom_., lib. VII.

 

588

 

désigner n'est pas obscur, puisqu'il les appelle les rois d'Orient et
leur fait passer l'Euphrate (1), qui semblait fait pour séparer l'Empire
romain d'avec eux. C'est là que le saint apôtre commence à montrer
combien les Romains furent rebelles contre Dieu, qui les frappait pour
les corriger de leur idolâtrie ; ce qu'il continue à faire voir en
récitant les opiniâtres persécutions dont ils ne cessèrent d'affliger
l'Église.

 


V. — La persécution commence à paraître au chapitre XI avec la bête : la bête représentée aux chapitres XIII et XVII, montre la persécution en général, et plus particulièrement la persécution de Dioclétien.

 

Elles commencent à paraître au chapitre XI ; et comme jusqu'ici on nous
a donné des caractères bien marqués et bien sensibles des Juifs et des
gentils, on ne nous en a pas donné de moins clairs pour désigner la
persécution romaine. Le plus marqué de ces caractères a été celui de la
bête, qu'on ne nous représente parfaitement que dans les chapitres xiii
et XVII, mais que néanmoins on a commencé à nous faire voir dès le
chapitre XI comme celle qui mettait à mort les élus de Dieu et les
fidèles témoins de sa vérité. Il nous faut donc ici arrêter les yeux sur
les caractères de cette bête, que nous voyons beaucoup plus clairs et
mieux particularisés que tous les autres.

On est accoutumé par la prophétie de Daniel à reconnaître les grands
empires sous la figure de quelques fiers animaux : il ne faut donc pas
s'étonner si on nous représente l'Empire romain sous cette figure, qui
n'a plus rien d'étrange ni de surprenant pour ceux qui sont versés dans
les Écritures. Mais le dessein de saint Jean n'est pas de nous marquer
seulement un grand et redoutable empire : c'était aux saints
principalement et aux fidèles de Jésus-Christ qu'il était redoutable.
Saint Jean nous le montre donc comme persécuteur et avec son idolâtrie,
parce que c'était pour l'amour d'elle qu'il tourmentait les enfants de
Dieu.

Pour mieux entendre ce caractère d'idolâtrie et de persécution que saint
Jean a donné à la bête, il la faut considérer avec la prostituée qu'elle
porte sur son dos, au chapitre XVII, car la prostitution est dans
l'Écriture le caractère de l'idolâtrie et le symbole

 

1 Chap. IX, 14 ; XVI, 12.

 

589

 

d'une abandonnée à l'amour de plusieurs faux dieux, comme d'autant
d'amants impurs qui la corrompent. L'apôtre joint à ce caractère celui
de la cruauté et de la persécution, en faisant « la femme enivrée du
sang des Saints et des martyrs de Jésus (1) ; » en sorte qu'on ne peut
douter que ce qu'il veut nous représenter sous la figure de la bête, ne
soit d'abord et en général la puissance romaine idolâtre, ennemie et
persécutrice ; à quoi aussi convenaient parfaitement les noms de
blasphème sur les sept têtes de la bête, c'est-à-dire, comme saint Jean
l'explique lui-même, sur les sept montagnes de Rome (2) ; et ses fureurs
contre les Saints, et sa couleur de sang, et tout son air cruel et
sanguinaire. C'est aussi pour cela « que le dragon roux, » c'est-à-dire
le diable, « qui voulait engloutir l'Église (3), avait donné à la bête
sa grande puissance (4), » et lui avait inspiré sa haine contre les
fidèles. On avouera qu'il n'était pas possible de nous peindre la
persécution avec de plus vives couleurs. Mais outre la persécution en
général, que l'apôtre nous rend si sensible, nous avons vu qu'il se
réduit à des idées encore plus particulières, en s'attachant
spécialement à représenter la persécution de Dioclétien, qu'il a choisie
entre toutes les autres pour la décrire avec un soin si particulier,
parce qu'elle devait être la plus violente comme la dernière, et que
c'était au milieu de ses violences que l'Église devait commencer à être
élevée par Constantin au comble de la gloire.

Le caractère le plus spécifique de cette cruelle et dernière persécution
est d'avoir été exercée au nom de sept empereurs : c'est aussi pour
cette raison que saint Jean lui donne sept têtes, qui sont bien à la
vérité , comme on a vu qu'il l'explique, les sept montagnes de Rome ;
mais qui sont aussi, comme il ajoute, sept de ses rois. C'était la seule
persécution qui eût cette marque : les caractères particuliers des trois
empereurs qui furent les principaux auteurs de la persécution, nous ont
aussi été marqués fort historiquement, comme on a vu ; et parce qu'il y
en avait un des sept qui était aussi un de ces trois qui devait prendre
l'empire par deux fois, c'est-à-dire Maximien, surnommé Herculius ; il y
a

 

1 _Apoc.,_ XVII, 6. —2 _Apoc.,_ XIII, 1 ; XVII, 3, 9. — 3 _Apoc.,_ XII,
3, 4. — 4 _Apoc_. XIII, 1, 2 ; XVII, 3.

 

590

 

aussi un des sept rois qui nous est montré en même temps comme étant
tout ensemble un huitième roi et un des sept (1) : ce qui était
précisément choisir dans l'histoire ce qu'il y avait de plus précis ,
n'y ayant point dans toute la suite de l'Empire romain un caractère
semblable.

On voit donc ce que c'est que la bête : Rome comme persécutrice en
général et avec une désignation plus particulière , Rome exerçant la
dernière et la plus impitoyable persécution.

 


VI. — Désignation particulière de Dioclétien , et son nom dans l'Apocalypse : que ce n'est point par une rencontre fortuite qu'il s'y trouve : caractère de son premier collègue, Maximien Herculius.

 

On y a vu d'autres caractères de cette persécution que je ne répète pas ;
mais je ne puis oublier qu'elle portait le nom de Dioclétien, qui, comme
premier empereur, était aussi le premier en tête dans l'édit des
persécuteurs ; ce qui fait aussi que saint Jean en nous voulant marquer
le nom de la bête par ses lettres numérales, a marqué celui de
Dioclétien dans le nombre de 666, comme on a vu (1).

Saint Jean a tout ici caractérisé d'une manière admirable. Car il nous a
dit, non-seulement qu'il nous voulait donner le nom d'un homme, mais
aussi le nom d'une de ces bêtes mystiques, c'est-à-dire le nom d'un
empereur ; ce qui nous a conduit à un nom où nous est marqué Dioclétien,
et où le nom qu'il avait porté lorsqu'il était particulier, joint à
celui d'Auguste qui le faisait empereur, nous en donnait un caractère
incommunicable, non-seulement à tout autre prince , mais encore à tout
autre homme.

Mais parce que le nombre mystique de 666 que saint Jean attribue ici au
nom de la bête peut convenir à plusieurs noms, et qu'on en compte huit
ou dix peut-être où il se trouve, pour ne donner aucun lieu à ce jeu
frivole des esprits, nous avons vu qu'au même passage où saint Jean a
marqué le nom de Dioclétien (2), il y a joint d'autres caractères qui
sont aussi particuliers à ce prince que son nom même ; de sorte que
non-seulement on trouve dans l'_Apocalypse_ le nom de Dioclétien, mais
on y trouve

 

1 Chap. XVII, 11. — 2 Chap. XIII, 18. — 3 _Ibid.,_ 16.

 

591

 

que c'était le nom de cet empereur qu'il y fallait trouver, et que ce ne
peut être un autre nom que saint Jean ait voulu désigner, parce que ce
devait être le nom de celui dont la dernière persécution est intitulée ,
et de celui qui aurait fait l'action unique à laquelle le saint apôtre
fait une allusion manifeste dans ce passage (1) ; ce qu'on peut voir
aisément dans le commentaire. D'où aussi l'on peut conclure que si l'on
a trouvé en ce lieu le nom de Dioclétien, ce n'est pas l'effet d'une
rencontre fortuite, mais une chose qui devait être et qui était entrée
nécessairement dans le dessein de notre apôtre ; par où aussi les
protestants, qui ne veulent jamais rien voir que de confus et de vague,
se trouveront confondus.

Le premier collègue de Dioclétien et le second empereur, qui était
Maximien Herculius, ne nous a pas été moins bien désigné, puisqu'on lui
a donné le caractère qui lui était le plus propre, c'était-à-dire celui
de venir deux fois (2) : et c'est avec raison que ce prince a été appelé
la bête selon cette mystique signification, ce titre lui convenant plus
particulièrement qu'aux cinq autres empereurs sous qui la persécution
s'est exercée, parce que dans le caractère que saint Jean lui avait
donné, non-seulement il était une des sept têtes, c'est-à-dire un de ces
sept princes, mais encore le corps de la bête, comme on a vu (3).

 


VII. — La bête de saint Jean était une chose qui devait venir après lui : ce qu'elle devait faire et ce qui lui devait arriver.

 

Nous avons déjà observé que cette bête mystique était marquée par saint
Jean, non pas comme étant déjà de son temps, mais comme «devant» dans la
suite «s'élever de l'abîme (4) ; » ce qui maintenant s'entendra mieux, et
sur quoi il sera utile d'appuyer un peu. Car encore que l'Empire romain
idolâtre et persécuteur fût déjà au monde lorsque saint Jean écrivait
son _Apocalypse,_ dans l'application particulière qu'il faisait de la
bête à la persécution de Dioclétien, elle n'était pas encore. Les sept
têtes, c'était-à-dire les sept empereurs et tout le reste que saint Jean
nous y a marqué avec une désignation particulière, étaient encore à
venir ; et même la persécution, quoique déjà commencée quelques années

 

1 Chap. XIII, 16, 17. — 2 Ibid. — 3 Chap. XVII. — 4 Chap. XI ; XIII, 1 ;
XVII, 8.

 

592

 

auparavant sous Néron et sous Domitien, était encore future dans sa plus
longue durée et dans ses plus grandes fureurs ; ce qui donne lieu à
saint Jean de nous parler de la bête, comme « devant encore s'élever. »
Il la voit « sortir de l'abîme : » il assiste à sa naissance et ne la
fait paraître au monde que pour donner la mort aux Saints (1) ; ce qu'on
ne peut trop observer, parce qu'outre que ces caractères particuliers
sont la vraie clef de la prophétie, c'est aussi un des passages qui doit
porter un coup mortel au système des protestants (2), qui n'ont rien
voulu voir de particulier, parce que leurs fausses idées du Pape
antéchrist ne subsistent que dans la confusion.

Après avoir observé le fond et les caractères de la bête, tels que saint
Jean nous les a donnés, il faut voir encore ce qu'elle fera et ce qui
lui doit arriver. Ce qu'elle fera, c'est de tourmenter l'Église ; et ce
qui lui doit arriver, c'est après divers châtiments de périr à la fin à
cause de son idolâtrie et du sang qu'elle avait répandu : c'est ce que
saint Jean nous a déclaré par des marques aussi sensibles que toutes les
autres que nous avons vues.

 


VIII. — La persécution, comment décrite aux chapitres XI et XII de l'Apocalypse.

 

La persécution en général est exercée par la bête, lorsqu'elle dorme la
mort aux Saints et qu'elle tient dans l'oppression la sainte cité, qui
est l'Église, avec toutes les circonstances qu'on en a marquées (3).
Mais au milieu de ces caractères généraux, saint Jean a toujours mêlé
les caractères particuliers de la persécution de Dioclétien, à laquelle
le Saint-Esprit l'avait davantage attaché. C'est pourquoi dans le
chapitre XI, on voit les gentils se flatter delà pensée d'avoir éteint
le christianisme (4), comme on en flatta Dioclétien : on voit dans ce
même temps le christianisme élevé au comble de la gloire (5), comme il
arriva au milieu de cette sanglante persécution, par les ordres de
Constantin et par ses victoires.

Au chapitre XII, paraît le dragon qui donne sa force à la bête, et la
femme en travail, (6) c'est-à-dire l'Église souffrante. C'est la

 

1 Chap. XI ; XIII, 1 ; XVII, 8. — 2 Voyez ci-dessous dans
l'Avertissement sur l'Accompl. des proph., n. 20 et 21. — 3 Chap. XI,
2,7. — 4 Ibid., 9. — 5 _Ibid_., II, 12, etc. — 6 Chap. XI, 2, 3 ; XIII,
2.

 

593

 

persécution en général. Mais nous sommes bientôt conduits au particulier
de Dioclétien, lorsque la femme étant prête de mettre au monde un enfant
mâle et dominant (1), c'est-à-dire le christianisme vigoureux et
vainqueur sous Constantin, le diable redouble ses efforts pour le
détruire ; et comme là on voit le dragon faire trois divers efforts, on
voit aussi la persécution s'élever à trois reprises plus marquées sous
trois princes, plus frémissante sous Dioclétien et sous Maximin, plus
languissante sous Licinius, et en état de tomber bientôt après.

 


IX. — La bête blessée à mort, ressuscite sous Julien l'Apostat.

 

Voilà ce que fait la bête : tant qu'il lui reste quelque force. Mais
saint Jean nous la représente en un autre état où elle avait reçu un
coup mortel (2), où elle était morte, où pour vivre elle avait besoin
d'être ressuscitée : c'est ce qui est aussi arrivé à l'idolâtrie
détruite dans les sept têtes. Tous les persécuteurs étant à bas, et de
tous les empereurs, Constantin, un si zélé enfant de l'Église restant
seul, l'idolâtrie était morte par la défense de ses sacrifices et de son
culte ; et il n'y avait plus pour elle de ressource, si Julien l'Apostat
ne l'eût fait revivre. Saint Jean, comme on voit, continue toujours à
s'attacher aux grands événements. Il n'y a rien de plus marqué que la
mort de l'idolâtrie sous un prince qui l'abolit par ses édits, ni rien
aussi de plus sensible que d'appeler résurrection la force et l'autorité
que lui rend un autre prince. Voilà qui est grand en général, mais le
particulier est encore plus surprenant. Car on voit la bête aux abois,
comme saint Jean l'avait marqué par la blessure d'une de ses têtes (3),
qui était Maximin, le sixième persécuteur, et parce que la septième
tête, qui ne paraissait pas encore, devait périr sitôt après, comme il
arriva à Licinius. C'est ainsi que la bête mourut ; c'est ainsi que
l'idolâtrie fut abattue, et l'image est toute semblable à l'original.

Pour la bête ressuscitée (4), c'est-à-dire l'idolâtrie reprenant
l'autorité sous Julien, elle nous est clairement marquée par l'orgueil
de ce prince, par ses blasphèmes étudiés contre Jésus-Christ et

 

1 Chap. XII, 4, 13, 17. — 2 Chap. XIII, 3. — 3 _Ibid._ — 4 Chap. XIII,
3, 5, 7,8.

 

594

 

ses Saints ; par le concours de tout l'Empire réuni sous cet empereur
contre l'Église ; parla haine du christianisme qui le fit rentrer dans
les desseins de Dioclétien pour l'opprimer ; par l'imitation de l'Agneau
et de quelques vertus chrétiennes que ce faux sage affecta ; par les
prestiges de ces philosophes magiciens qui le gouvernaient absolument ;
par les illusions de sa fausse philosophie, et par la courte durée de
cette nouvelle vie de l'idolâtrie, où la femme ne se cacha point, comme
elle avait fait dans les autres persécutions, et où l'Église retint tout
son culte. Qu'on me donne d'autres caractères du règne de Julien
l'Apostat, et qu'on m'en fasse un tableau plus au naturel et plus vif.

 


X.— La seconde bête et l'esprit de rédaction dans l'idolâtrie persécutrice : le caractère particulier que saint Jean donne à la seconde bête, directement opposé à celui de l'homme de péché de saint Paul, avec lequel les protestants le veulent confondre.

 

Ce n'était pas assez de marquer la violence de la bête, c'est-à-dire de
l'idolâtrie persécutrice : nous n'en eussions pas vu la séduction et les
artifices, si saint Jean ne nous eût décrit la seconde bête mystique
(1), c'est-à-dire la philosophie pythagoricienne, qui soutenue de la
magie, faisait concourir à la défense de l'idolâtrie ses raisonnements
les plus spécieux avec ses prodiges les plus étonnants. C'est ce que
nous avons remarqué dans les figures de saint Jean : c'est ce que nous
avons vu accompli dans l'idolâtrie, en la regardant tant dans sa
première vigueur sous Dioclétien que dans sa vie réparée par Julien
l'Apostat.

On entendra mieux encore la seconde bête, en comprenant le caractère
qu'elle a dans saint Jean (2), qui est de faire adorer la première,
c'est-à-dire d'attacher les hommes à l'ancienne idolâtrie : de sorte que
la première bête paraît dans l'_Apocalypse_ comme le Dieu qu'on adorait,
et la seconde comme son prophète qui la faisait adorer ; d'où vient aussi
qu'elle est appelée le faux prophète. En quoi saint Jean nous a fait
voir le vrai caractère de cette philosophie magicienne, dont tous les
raisonnemens et tous les prestiges aboutissaient à faire adorer les
dieux que l'ancienne idolâtrie avait inventés.

 

1 Chap. XIII, 11 et suiv. — 2 _Ibid.,_ 12.

 

595

 

Telle est donc la seconde bête ; et c'est faute d'avoir bien compris ce
caractère que saint Jean lui donne, qu'on a voulu la confondre avec
l'homme de péché de saint Paul (1), encore que son caractère d'être le
prophète d'une divinité qu'elle annonçait, soit directement opposé à
celui qu'attribue saint Paul à son homme de péché, qui s'élève au-dessus
de tout ce qu'on nomme Dieu et qu'on adore.

On peut encore remarquer ici un caractère particulier de l'idolâtrie
romaine. C'est que partout elle fait adorer la bête et son image,
c'est-à-dire Rome et ses empereurs, dont les images , comme on a vu,
étaient proposées aux martyrs pour être l'objet de leur culte, autant ou
plus que celles des dieux immortels : caractère d'idolâtrie qu'on voit
répandu partout dans l'_Apocalypse_ (2), et que Julien y fait revivre
avec tous les autres.

 


XI. — La durée des persécutions, et ses trois ans et demi , que saint Jean dit expressément que ce temps est court : les deux marques, celle de Dieu et celle de la bête.

 

Ainsi la persécution a été caractérisée en toutes manières, parla
qualité de ses auteurs, par sa violence, par ses artifices, par la
nature du culte auquel on voulait forcer le genre humain. Mais un des
plus beaux et des plus particuliers caractères que nous en ayons dans
saint Jean (3), est celui qui marque les bornes que Dieu lui donnait par
une providence particulière et un secret ménagement de ses élus, comme
il avait fait autrefois à celle d'Antiochus. Nous avons vu en effet que
malgré la haine immortelle de Rome contre l'Église, il était ordonné de
Dieu que ses violences se relâcheraient de temps en temps et
reviendraient aussi à diverses reprises toutes courtes ; ce que saint
Jean a marqué dans ce temps mystique de trois ans et demi pour les
raisons et à la manière que nous avons vu.

Que dans ce temps toujours consacré aux persécutions et toujours le même
en quelque sorte qu'il soit expliqué, par jour , par mois ou par années
, le dessein du saint apôtre fut de nous marquer un temps court ; il le
déclare en termes formels, lorsque représentant le dragon irrité de
n'avoir plus « que peu de temps »

 

1 II _Thess.,_ II. — 2 _Apoc.,_ XIII, 4, 12, 11. — 3 _Apoc.,_ X , 2, 3 ;
XII, 6 ; XIII, 5.

 

596

 

à tyranniser les fidèles, il détermine aussitôt après, et dans le verset
suivant, ce « peu de temps » à ces « trois ans et demi (1), » qu'on voit
revenir si souvent : ce qui dans la suite nous découvrira la prodigieuse
illusion des protestants, qui veulent que ce peu de temps soit 1200 ans
entiers : et non-seulement un petit reste de temps que le démon déjà
terrassé voyait devant lui, mais encore son temps tout entier et toute
la durée de son empire.

Saint Jean nous fait voir encore que ce temps revenait souvent, comme
étant le commun caractère de toutes les reprises de persécution. C'est
pourquoi nous avons vu qu'il revient deux fois dans la persécution qui
précède la mort de la bête, et une troisième fois sous la bête
ressuscitée (2) ; ce qui montre plus clair que le jour que ce temps
n'est pas la mesure d'une seule et longue persécution qui dure près de
treize siècles, comme l'ont songé les protestants, mais la marque des
différentes reprises des persécutions romaines toutes courtes, et
bientôt suivies d'un adoucissement que Dieu procurait.

Durant ce temps, c'est un beau contraste et quelque chose de ravissant
dans le tableau de saint Jean, de voir d'un côté les fidèles, et de
l'autre les idolâtres, avec une double marque pour les distinguer les
uns des autres : d'un côté, la marque de Dieu sur les élus ; et de
l'autre, le caractère de la bête sur les impies ; c'est-à-dire pour les
fidèles, avec la foi au dedans, la profession du christianisme, et pour
les autres, l'attachement déclaré à l'idolâtrie : ceux qui portent la
marque de Dieu, ornés de toutes sortes de vertus et de grâces ; et ceux
qui portent celle de la bête, se plongeant eux-mêmes dans l'aveuglement
et dans le blasphème, pour ensuite être abandonnés à la justice divine.

 


XII. — La bête punie, et l'empire persécuteur démembré : la domination et la chute de Rome, dans une même vision de saint Jean comme dans un même tableau.

 

Ainsi nous avons l'idée des persécutions de la bête, c'est-à-dire de
Rome l'ancienne, par tous les moyens qu'on peut souhaiter. Mais pour ne
rien oublier de ce qui devait lui arriver, après nous l'avoir montrée
comme dominante et persécutrice, il fallait

 

1 Chap. XII, 13, 14. — 2 Chap. XII, 6, 14 ; XIII, 5.

 

597

 

encore la faire voir abattue et punie de ses attentats. Saint Jean ne
pouvait le faire d'une manière plus sensible, qu'en rappelant comme il
fait à notre mémoire dans le chapitre XVI le premier coup qu'elle reçut
du côté de l'Orient sous Valérien (1), et nous la montrant aussitôt
après dans le chapitre XVII, entre les mains des dix rois qui la
pillent, qui la désolent, qui la rongent, qui la consument, qui
l'abattent avec son empire, que nous voyons tomber dans saint Jean (2),
comme il est tombé en effet par une dissipation et par un démembrement
entre plusieurs rois ; en sorte qu'il ne reste plus qu'à déplorer sur la
terre son malheur (3), et à louer Dieu dans le ciel de la justice qu'il
a exercée sur elle (4) : ce que saint Jean a fait d'une manière si
claire et avec des caractères si précis des rois qui l'ont dépouillée,
qu'après avoir un peu démêlé les figures de son style mystique,
c'est-à-dire avoir entendu la langue que parlent les prophètes, nous
avons cru lire une histoire.

Dans ce grand tableau de saint Jean, la figure de la prostituée est une
des plus merveilleuses, puisqu'avec toute la parure et tous les autres
caractères qui lui sont donnés, elle marque aussi clairement qu'on le
pouvait souhaiter, une ville redoutée de tout l'univers, abandonnée à
l'idolâtrie, persécutrice des Saints (5) ; en sorte qu'il ne restait plus
qu'à nommer Rome. Afin de mieux ramasser toutes les idées, le saint
apôtre nous la montre dans une même vision comme dominante et comme
abattue, comme criminelle et comme punie, faisant éclater sa cruelle
domination dans les sept têtes de la bête qui la porte, et dans les dix
cornes de la même bête la cause de sa chute inévitable (6).

 


XIII. — Le règne de l'Église, combien vivement marqué : la dernière tentation de l'Église et le temps de l'Antéchrist : comparaison du XXe chapitre de l'Apocalypse avec celui de saint Paul, II Thess., II.

 

Voilà donc le premier temps de l'_Apocalypse_ qui exprime le
commencement de l'Église et ses premières souffrances. C'était !à le
grand objet de saint Jean, qui occupe aussi seize chapitres : les deux
autres temps, c'est-à-dire celui du règne de l'Église et

 

1 Chap. XVI, 12. — 2 Chap. XVII, 7 et suiv. — 3 Chap. XVIII. — 4
_Ibid.,_ 19. — 5 Chap. XVII. — 6 _Ibid_., vers. 1 et suiv. ; vers. 12 et
suiv.

 

598

 

celui de sa dernière persécution, sont tracés en deux ou trois coups de
pinceau, mais les plus vifs qu'on pût souhaiter et les plus
significatifs. Car déjà, pour ce qui regarde le règne de l'Église, elle
reçoit une assurance certaine qu'il sera long, ce qu'on nous figure par
les mille ans (1) ; qu'il sera tranquille, ce qu'on nous montre par
l'enchaînement de Satan, qui n'aura plus la liberté comme auparavant de
susciter des persécutions universelles ; enfin qu'il sera le règne de
Jésus-Christ et de ses martyrs, dont la gloire serait si grande par tout
l'univers, et la puissance si reconnue, à cause qu'ils auront vaincu la
bête et son caractère, Rome et son idolâtrie ; avec même une désignation
particulière du supplice usité parmi les Romains (2), afin que tout fût
marqué par les caractères des temps et par les circonstances les plus
précises.

La dernière tentation de l'Église n'est pas moins marquée, quoiqu'en
très-peu de paroles. Car saint Jean, qui n'ignorait pas ce qu'en avait
dit saint Paul plus expressément, s'est contenté d'en marquer en gros
les caractères, en nous faisant voir Satan déchaîné (3), comme saint
Paul nous avait montré toute sa puissance déployée ; en caractérisant
cette tentation par la séduction plutôt que par la violence, comme saint
Paul avait fait (4) ; en nous marquant comme lui la courte durée de cette
séduction ; et comme lui, qu'elle finirait par le dernier jugement et
l'éclatante arrivée de Jésus-Christ dans sa gloire : de sorte que ce
sera la fin de l'Église sur la terre et sa dernière tentation ; ce qui
suffit pour nous faire entendre qu'elle sera en même temps la plus
terrible, comme celle où le diable déchaîné fera son dernier effort, et
que Jésus-Christ aussi viendra détruire en personne par la plus grande
manifestation de sa puissance.

 


XIV. — Pourquoi des trois temps do l'Église, le premier dépeint plus au long, et les deux autres tracés si rapidement.

 

Voilà les trois temps de l'Église : le premier, qui est celui des
commencements représenté très-au long et sous une grande multiplicité de
belles images, comme celui qui allait venir et contre

 

1 Chap. XX. vers. 1 Jusqu'au 7. — 2 _Ibid.,_ 4, et la note dess.— 3
_Apoc.,_ XX, 7. — 4 II _Thess.,_ II.

 

599

 

lequel par conséquent les fidèles avaient besoin d'être le plus
prémunis ; et les deux autres tracés en deux mots, mais très-vivement et
pour ainsi dire de main de maître. C'était aussi la main d'un apôtre, ou
plutôt la divine main, dont il est dit « qu'elle écrivit vite (1), »
dont les traits ne sont pas moins forts ni moins marqués pour être tirés
rapidement ; qui sait donner toute la force qu'il faut à ses expressions,
en sorte que très-peu de mots ramassent, quand il lui plaît, le plus de
choses.

 


XV. — Satan vaincu et par quel degrés ; c'est le sujet de l'Apocalypse.

 

Au reste, je n'ai pas besoin de répéter que la défaite entière de Satan
est au fond le grand ouvrage que saint Jean célèbre. Ce vieux serpent
nous est montré dans l'_Apocalypse_ comme celui qu'il fallait abattre
avec son empire, et tout le progrès de sa défaite nous est marqué dans
ces trois temps qu'on vient de voir. Car à la fin du premier temps, qui
était celui de la première persécution, ses deux grands organes, la bête
et le faux prophète, sont jetés dans l'étang de feu et de soufre (2) :
là il paraît enchaîné, afin que l'Église règne plus tranquillement à
couvert des persécutions universelles, jusqu'aux environs des derniers
temps. A la fin de ce second temps Satan sera déchaîné et plus furieux
que jamais ; ce qui fera le troisième temps, court dans sa durée, mais
terrible par la profondeur de ses illusions : lequel étant écoulé, Satan
ne sera plus enchaîné comme auparavant pour un certain temps, mais à
jamais et sans rien avoir à entreprendre de nouveau, plongé dans l'abîme
où étaient déjà la bête et le faux prophète, autrefois ses deux suppôts
principaux et les deux premiers instruments des persécutions
universelles.

Que si l'on veut commencer l'enchaînement de Satan au temps où nous
avons vu que saint Jean nous a marqué en un certain sens le règne de
Jésus-Christ et celui de ses martyrs sur la terre, par la gloire qu'ils
y ont reçue dans toute l'Église, on le peut, et les temps peut-être
seront plus distinctement marqués : ce qui n'empêchera pas qu'en un
autre sens l'enchaînement de Satan ne

 

1 _Psal_. XLIV, 2. — 2 Chap. XIX, 20.

 

600

 

commence, selon la remarque de saint Augustin que j'ai suivie (1), dès
la prédication et dès la mort de Jésus-Christ, qui en effet est le
moment fatal à l'enfer, encore que toute la suite de ce premier coup ne
paroisse que longtemps après.

 


XVI. — La suite toujours visible de l'Église , très-clairement marquée dans l'Apocalypse.

 

Voilà donc toute l'histoire de l'Église tracée dans l'_Apocalypse,_ avec
ses trois temps ou ses trois états ; et ce que je trouve de plus
instructif, c'est que saint Jean a été soigneux de nous marquer la suite
toujours visihle de l'Église. Dans la première persécution, rien ne peut
faire taire ses deux témoins, c'est-à-dire ou son clergé et son peuple,
ou en quelque sorte qu'on le veuille entendre, le témoignage éclatant
qu'elle rend à la vérité : et lorsque le monde pense l'avoir fait périr
entièrement, loin d'avoir été détruite par les tourments, comme on
pensait, elle paraît un moment après plus forte et plus glorieuse que
jamais. Que si elle était contrainte de cacher son culte, ce qui
quelquefois la faisait paraître au monde qui la haïssait comme
entièrement opprimée, elle y avait ses pasteurs, comme autrefois les
Israélites durant leur pèlerinage avaient Moïse et Aaron, et comme sous
Antiochus les Juifs avaient Mathathias et ses enfants. Elle y allait
comme à un lieu préparé de Dieu pour sa retraite, qui lui était bien
connu et où les persécuteurs savaient bien eux-mêmes qu'elle était,
puisqu'ils l'y allaient chercher pour la tourmenter davantage (2). Après
cet état elle règne (3), et sa gloire est portée jusqu'au ciel durant
mille ans, c'est-à-dire durant tout le temps que le monde dure ; et si
elle est à la fin encore opprimée, elle n'en est pas moins visible,
puisque toujours attaquée, elle soutient toutes les attaques (4). Ce
n'est pas une troupe d'invisibles dissipés deçà et delà sans se
connaître ; c'est une _cité bien-aimée_ qui a son gouvernement, c'est un
camp bien ordonné qui a ses chefs (5) ; et lorsque ses ennemis
paraissent  en état de l'anéantir par leur grande et redoutable
puissance, ils sont eux-mêmes consumés par le feu venu du ciel, où la

 

1 Voyez chap. XX, vers. 2.— 2 Chap. XI. — 3 Chap. XII.— 4 Chap. XX, 2,
7.— 5 _Ibid.,_ 8.

 

601

 

cité enfin est transportée pour être éternellement hors de toute
atteinte.

 


XVll. — La Trinité annoncée dans l'Apocalypse.

 

J'ajouterai en finissant que le perpétuel objet de l'amour et de
l'adoration de l'Église, un seul Dieu en trois personnes, est célébré
dans l'_Apocalypse_. Le Père, qui est assis dans le trône, y reçoit les
hommages de toutes les créatures : le Fils, qui y porte aussi le nom du
_Verbe_ (1), sous lequel saint Jean a marqué sa divinité, reçoit les
mêmes honneurs, et il est, comme on a vu, traité d'égal avec le Père :
le Saint-Esprit est montré comme celui qui est l'auteur des sacrés
oracles, et qui parle dans tous les cœurs avec une autorité souveraine :
les Églises sont invitées par sept fois à entendre ce que dit l'Esprit
(2) ; l'Esprit prononce souverainement que les travaux de ceux qui
meurent au Seigneur sont finis (3) ; l'Esprit parle dans tous les cœurs
pour appeler Jésus-Christ (4) ; cet Esprit qui parle est toujours unique
en son rang et toujours incomparable ; un comme le Père et le Fils,
intime coopérateur de l'un et de l'autre, et consommateur de leur
ouvrage : ce qui confirme en passant que les sept Esprits au nom
desquels les églises sont saluées (5), ne sont pas cet Esprit égal au
Père et au Fils, à qui le caractère de l'unité est attribué partout,
mais des anges, à qui aussi le nombre de sept est attribué dans tout le
livre.

 


XVIII. — Economie de l'Apocalypse. Conclusion de l'explication. Passage à la suite.

 

On peut entendre maintenant toute l'économie de l'_Apocalypse_. Saint
Jean va d'abord à ce qui était le plus proche et le plus pressant, qui
était le commencement de l'Église et ses premières souffrances. Il s'y
attache partout aux événements les plus grands, aux caractères les plus
marqués, aux circonstances les plus importantes et les plus
particulières. Chaque chose a son caractère : ce qui est long est marqué
par un grand nombre ; ce qui est court est marqué comme court, et la
brièveté dans cet ouvrage se prend toujours à la lettre. Ce qui est
marqué comme devant arriver

 

1 Chap. XIX, 13. — 2 Chap. II, III. — 3 Chap. XIV, 13. — 4 Chap. XXII,
17. — 5 Chap. I, 4.

 

602

 

bientôt, commence en effet à se déployer incontinent après le temps de
saint Jean. Le livre n'est pas scellé, comme s'il devait demeurer
longtemps fermé, parce que l'accomplissement de ses prédictions devait
éclater bientôt. C'est ce que j'ai cru devoir ajouter à cette
explication de l'_Apocalypse_ pour la remettre tout entière, comme en un
moment, sous les yeux, et afin que le lecteur attentif en imprimant dans
sa mémoire tous les caractères marqués par saint Jean, commence à y
reconnaître les principes dont nous allons nous servir pour la
conviction des protestants.

 

FIN DE l'EXPLICATION DE l'APOCALYPSE.

[Précédente]

[Accueil]

[APOCALYPSE XVIII]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Avertissement
Liber Sapientiae
Liber Ecclesiasticus
Isaïe
Psaume  XXI
Apocalypse
Apocalypse I
Apocalypse II
Apocalypse III
Prédictions
Apocalypse IV
Apocalypse V
Apocalypse VI
Apocalypse VII
Apocalypse VIII
Apocalypse IX
Apocalypse X
Apocalypse XI
Apocalypse XII
Apocalypse XIII
Apocalypse XIV
Apocalypse XV
Apocalypse XVI
Apocalypse XVII
Apocalypse XVIII
Apocalypse XIX
Apocalypse XX
Apocalypse XXI
Apocalypse XXII
Explication



CHAPITRE XVIII.


 

_Chute de la grande Babylone : toute la terre dans l'effroi à la vue de
sa_

_désolation._

 

1.  Après cela je vis un autre ange qui descendait du ciel, ayant une
grande puissance ; et la terre fut éclairée de sa gloire.

2.  Il cria de toute sa force, en disant : Elle est tombée, elle est
tombée la grande Babylone ; et elle est devenue la demeure des démons, et
la retraite de tout esprit impur et de tout oiseau impur, et qui donne
de l'horreur.

3.  Parce que toutes les nations ont bu du vin de la colère de sa
prostitution ; et les rois de la terre se sont corrompus avec elle ; et
les marchands de la terre se sont enrichis de l'excès de son luxe.

4.  J'entendis aussi une autre voix du ciel, qui dit: Sortez de
Babylone, mon peuple, de peur que vous n'ayez part à ses péchés, et que
vous ne soyez enveloppés dans ses plaies.

5.  Parce que ses péchés sont montés jusqu'au ciel (a), et Dieu s'est
ressouvenu de ses iniquités.

6. Rendez-lui comme elle vous a rendu ; rendez-lui au double

 

(a) _Grec_ : L'ont suivie.

 

536

 

selon ses œuvres : faites-la boire deux fois autant dans le même calice
où elle vous a donné à boire.

7.  Multipliez ses tourments et ses douleurs à proportion de ce qu'elle
s'est élevée dans son orgueil, et de ce qu'elle s'est plongée dans les
délices ; car elle dit en son cœur : Je suis reine, je ne suis point
veuve, et je ne serai point dans le deuil.

8.  C'est pourquoi ses plaies, la mort, le deuil et la famine viendront
en un même jour, et elle sera brûlée par le feu, parce que c'est un Dieu
puissant (a) qui la jugera.

9.  Les rois de la terre qui se sont corrompus avec elle, et qui ont
vécu avec elle dans les délices, pleureront sur elle, et se frapperont
la poitrine en voyant la fumée de son embrasement.

10.  Ils se tiendront loin d'elle dans la crainte de ses tourments, en
disant : Malheur, malheur ! Babylone, grande ville, ville puissante, ta
condamnation est venue en un moment.

11.  Et les marchands de la terre pleureront et gémiront sur elle, parce
que personne n'achètera plus leurs marchandises ;

12.  Ces marchandises d'or et d'argent, de pierreries, de perles, de fin
lin, de pourpre, de soie, d'écarlate, de toute sorte de bois
odoriférant, et de meubles d'ivoire, de pierres précieuses, d'airain, de
fer et de marbre,

13.  De cinnamome, de senteurs, de parfums, d'encens, de vin, d'huile,
de fleurs de farine, de bled, de bêtes de charge, de brebis, de chevaux,
de chariots, d'esclaves (6) et d'âmes d'hommes.

14.  Les fruits qui faisaient tes délices t'ont quittée ; toute
délicatesse et toute magnificence est perdue pour toi, et on ne les
trouvera (c) plus jamais.

15.  Ceux qui lui vendaient ces marchandises et qui s'en sont enrichis,
s'éloigneront d'elle dans la crainte de ses tourments ; ils en
pleureront, et ils en gémiront :

16.  Ils diront : Malheur, malheur! cette grande ville, qui était vêtue
de fin lin, de pourpre et d'écarlate, parée d'or, de pierreries de
perles :

17.  Elle a perdu en un moment ces grandes richesses : et tous les
pilotes, ceux qui font voyage sur la mer, les mariniers et tous

 

(a) _Grec_ : Le Seigneur. — (b) De corps et. — (c) Tu ne les trouveras.

 

537

 

ceux qui sont employés sur les vaisseaux, se sont arrêtés loin d'elle.

18.  Et se sont écriés en voyant le lieu (a) de son embrasement, et ils
ont dit -..Quelle ville a jamais égalé cette grande ville?

19.  Ils se sont couvert la tête de poussière, et ils ont jeté des cris
mêlés de larmes et de sanglots, en disant : Malheur, malheur! Cette
grande ville, qui a enrichi de son abondance tous ceux qui avaient des
vaisseaux sur la mer, a été ruinée en un moment.

20.  Ciel, réjouissez-vous sur elle, et vous saints apôtres et
prophètes, parce que Dieu vous a vengés d'elle.

21.  Alors un ange fort leva en haut une pierre comme une grande meule,
et la jeta dans la mer, en disant : Babylone, cette grande ville, sera
ainsi précipitée, et elle ne se trouvera plus.

22.  Et la voix des joueurs de harpes, des musiciens, des joueurs de
flûtes et de trompettes, ne s'entendra plus en toi : nul artisan, nul
métier ne se trouvera plus en toi ; et le bruit de la meule ne s'y
entendra plus.

23.  Et la lumière des lampes ne luira plus en toi, et la voix de
l'époux et de l'épouse ne s'y entendra plus : car tes marchands étaient
des princes de la terre, et toutes les nations ont été séduites par tes
enchantements.

24.  Et on a trouvé dans cette ville le sang des prophètes et des
Saints, et de tous ceux qui ont été tués sur la terre.

 


EXPLICATION DU  CHAPITRE  XVIII.

 

_Chute et  désolation  de  Rome sous  Alaric._

 

1.  _Je vis un ange... ayant une grande puissance ; et la terre fut
éclairée_... C’est celui qui va annoncer le grand ouvrage de la
vengeance prochaine, qui éclatera comme le soleil par toute la terre.

2.  _Elle est tombée la grande Babylone_ ; tiré d'Isaïe, XXI, 9, et de
Jérémie, LI, 8.

_Elle est devenue la demeure des démons_. Dans le style de l'Écriture,
les lieux désolés sont représentés comme abandonnés, non-seulement aux
oiseaux de mauvais augure, mais encore aux

 

(a) _Grec_ : La fumée.

 

538

 

spectres et aux démons, _Jerem_., LI, 37 ; _Isa_., XIII, 21,22 ; XXXIV,
14 ;

qui sont façons de parler tirées du langage populaire.

On dira que Rome ne fut pas si entièrement désolée par Alaric, qu'on ne
la voie réparée bientôt après : mais Babylone elle-même, qui est choisie
par le Saint-Esprit pour nous représenter la chute de Rome, aussi bien
que son impiété et son orgueil, n'a pas été détruite d'une autre sorte.
Après sa prise et son pillage sous Cyrus, on la voit encore subsister
jusqu'au temps d'Alexandre avec quelque sorte de gloire, mais qui
n'était pas comparable avec celle qu'elle avait eue auparavant. Ce qui
fait que les prophètes la regardent comme détruite, c'est à cause
qu'elle fut en effet saccagée, et qu'il n'y eut jamais aucune ressource
à la perte qu'elle fit de son empire. Rome a été poussée bien plus loin,
puisqu'en perdant son empire, elle est devenue le jouet des nations
qu'elle avait vaincues, le rebut de ses propres princes et la proie du
premier venu, comme on a vu, _Hist. abr_., 11, 16.

Il est bon aussi de se souvenir combien grand fut le désastre de Rome
ravagée par Alaric. Outre tout ce qu'on a rapporté de saint Augustin, de
Paul Orose et de saint Jérôme, ce dernier nous la représente « comme
devenue le sépulcre de ses enfants ; comme réduite par la famine à des
aliments abominables, et ravagée par la faim avant que de l'être par
l'épée ; de sorte qu'il ne lui restait qu'un petit nombre de ses
citoyens, et que les plus riches, réduits à la mendicité, ne trouvèrent
de soulagement que bien loin de leur patrie dans la charité de leurs
frères. » _Epist_. XVI, _ad Princip_. ; Proœm. _Comm. in Ezech_., lib.
III, vu, etc. Voyez _Préf_., n. 10, et sur le chap. XVII, 16.

3.   _Toutes les nations ont bu du vin de la colère de sa prostitution_.
Hébraïsme : c'est-à-dire, du vin de sa prostitution digne d'un châtiment
rigoureux.

_Ont bu du vin_ : ci-dessus, XVII, 2. Le vice et l'erreur enivrent comme
un vin fumeux qui fait perdre la raison.

_Les marchands de la terre se sont enrichis de l'excès de son luxe_. Ce
n'est pas seulement l'idolâtrie de Rome que Dieu punit, c'est son luxe
et son orgueil.

4.  _Sortez de Babylone, mon peuple_. Ainsi dans Jérémie : « Fuyez

 

539

 

du milieu de Babylone, et que chacun sauve son âme, » _Jerem_., LI, 6.
Tout cela ne signifie autre chose, sinon qu'il fallait sortir de Rome,
ainsi qu'autrefois de Babylone, comme d'une ville pleine d'impiété et
qui enfin allait périr. _De peur que vous n'ayez part à ses péchés_ ;
c'est-à-dire à la peine de ses péchés ou, si l'on veut, à sa corruption,
à son luxe, à ses idolâtries, où elle tâchait d'attirer tous ses
habitants, comme on a vu, _Hist. abr_., n. 13, 14.

_Et que vous ne soyez enveloppés dans ses plaies_. Il fallait que les
anciens Juifs sortissent de Babylone pour n'être pas enveloppés dans son
supplice. Saint Jean applique à Rome cette parole, comme les autres qui
ont été dites pour Babylone.

Dieu en fit sortir son peuple en plusieurs manières. Premièrement, en
retirant de cette vie ceux à qui il voulait épargner la douleur de voir
périr une telle ville. Ainsi saint Jérôme a dit du pape saint Anastase,
que « Rome ne le put posséder longtemps, et que Dieu l'avait enlevé du
monde, de peur que la capitale de l'univers ne fût abattue sous un si
grand évêque, » Hier., _Epist_. XVI.

Secondement, dans les approches de la prise de Rome, une secrète
providence en éloigna plusieurs gens de bien, et entre autres le pape
Innocent, « qu'elle fit sortir comme autrefois le juste Lot de Sodome,
de peur qu'il ne vît la ruine d'un peuple livré au péché, » Aug., _de
Excid. Urb_., VII ; Oros., VII, XXXIX. Nous avons vu aussi sainte
Mélanie avec plusieurs grands de Rome, en sortir dans le même temps par
une espèce de pressentiment de la ruine de cette grande ville, _Hist.
Lausiac,_ c. 118 ; Préface, n. 8. Longtemps auparavant, Dieu avait mis
dans le cœur à sainte Paule et à beaucoup d'illustres Romains, de se
retirer à Bethléem avec leur famille. Hieron., _Epist_. VII. Et en
général nous apprenons de Paul Orose qu'un grand nombre de chrétiens se
retirèrent de Rome, suivant ce précepte de l'Évangile : « Quand ils vous
poursuivront dans une ville, fuyez en une autre : ce que ceux qui ne
croyaient pas à l'Évangile ne firent point, et se trouvèrent accablés, »
Paul. Oros., VII, XLI, XLIX.

Troisièmement dans le sac de la ville, Dieu prépara un asile hors de
Rome, dans l'église de Saint Pierre, à tous ceux qui s'y réfugièrent,
car Alaric l'avait ainsi ordonné ; et encore que les

 

540

 

païens aient profité de cette ordonnance, on ne doute point qu'elle ne
fût faite principalement pour les chrétiens. Aug., _de Civit.,_ lib. I,
IV ; Oros., VII, XXX. L'Égypte, l'Afrique, tout l'Orient et
principalement la Palestine, tout l'univers enfin fat rempli de
chrétiens sortis de Rome, qui trouvèrent un refuge assuré dans la
charité de leurs frères, comme le raconte saint Jérôme, Epist. XII, _ad
Gaud_ ; Proœm. in Ezech., lib. III, VII.

Quatrièmement pour ce qui est des élus de Dieu qui moururent dans cette
guerre, ce fut ceux-là, dit saint Augustin, qui sortirent le plus
glorieusement et le plus sûrement de Babylone, puisqu'ils furent
éternellement délivrés du règne de l'iniquité, et furent mis en un lieu
où ils n'eurent plus à craindre aucun ennemi, ni parmi les démons, ni
parmi les hommes. Aug., _de Urb. excid_., VI.

5. _Ses péchés sont montés jusqu'au ciel_. Le grec : _L'ont suivie
jusqu'au ciel_. Ils l'ont poursuivie jusqu'au jugement de Dieu, comme
nous lisons de Babylone : « Son jugement est monté aux cieux, » Jerem.,
LI, 9.

6. _Rendez-lui comme elle vous a rendu_. Claudius II tailla en pièces
trois cent vingt mille Goths, et coula à fond deux mille de leurs
vaisseaux. Toutes les provinces furent remplies d'esclaves de cette
nation, Trebell. Pol. _in Claud_. ; et du temps même de la prise de Rome,
après la défaite de Radagaise, le nombre des esclaves goths fut infini :
« On les vendait comme des bêtes, et on en avait, dit Orose, des
troupeaux entiers pour un écu, » Oros., VII, XXXVII. Ainsi c'est avec
raison qu'on dit aux Goths : Faites à Rome comme elle vous a fait, outre
qu'il les faut ici regarder comme les vengeurs de l'injure commune de
toutes les nations.

7. _Elle dit en son cœur : Je suis reine_... C'est ce qu'Isaïe faisait
dire à Babylone : « Je dominerai éternellement : il n'y a que moi sur la
terre, et je ne saurai jamais ce que c'est qu'affliction, » Isa. XLVII,
7, 8. Rome vantait à son exemple l'éternité de son empire ; et un des
blasphèmes que les saints Pères lui reprochent, était de s'être appelée
la ville éternelle (1) : titre qu'on trouve encore dans une inscription
qui fut faite six ou sept ans avant sa prise, à l'occasion de ses
murailles rebâties (2).

 

1 Hier. ; Ep. LI, _ad_ _Alg.,_ _qu_. XI. — 2 Ap. Bar. t. V, an 403.

 

541

 

8. _Elle sera brûlée par le feu_ : sous Alaric même, ci-dessus,

XVII, 10.

9. _Les rois de la terre... pleureront sur elle_ : la chute d'une si
grande ville, qu'on regardent comme la maîtresse de l'univers, étonnera
tout le genre humain.

_Qui se sont corrompus avec elle, pleureront_. Tout ce qui restait de
rois alliés de Rome et de grands qui avaient pris part ou à ses
idolâtries, ou à son ambition et à son luxe, s'affligera de sa perte.

10. _Malheur ! malheur ! Vœ !_ Le voilà, ce troisième et dernier _Vœ_
que nous attendions depuis si longtemps, et depuis le verset li du
chapitre XI. Il retentit encore ci-dessous, verset 10 : _Vœ ! vœ !_
Malheur, Malheur ! Hélas, hélas ! la grande ville de Babylone ! Et
encore au verset 19 : _Vœ,_ _Vœ_ ! Malheur, malheur! Ne le cherchons
plus ce terrible _Vœ_ ; le voilà, sans qu'il soit besoin de nous le
faire remarquer. Un cri si perçant et si souvent répété se fait assez
remarquer lui-même.

13_. D'esclaves et d'âmes d'hommes_. Le grec : _somaton,_ qui veut dire
corps : ce que la Vulgate a très-bien rendu par le mot de _mancipia,_
comme le prouve très-clairement Drusius, savant protestant, sur ce
passage. On le trouve dans les critiques d'Angleterre. _D'âmes
d'hommes_ ; c'est-à-dire d'hommes en général, selon la façon de parler
usitée dans toutes les langues, et en particulier dans la langue sainte,
Gen., XLVI , 26. Mais ici, comme saint Jean oppose les hommes aux
esclaves, il faut entendre par hommes les hommes libres, car on vend
tout, esclaves et libres dans une ville d'un si grand abord. D'autres au
contraire veulent par les âmes entendre les esclaves qui n'ont que leur
âme en leur puissance, Grot. _sur ce verset_ ; mais le premier sens
paraît meilleur.

18. _Le lieu de son embrasement_ : grec. _La fumée de son embrasement_ ;
ce qui convient mieux à ce qui est dit, XIX, 3. _Et la fumée s'élève aux
siècles des siècles_.

20. _Réjouissez-vous sur elle_. La voix dont il est parlé, _sup_. 4, est
ici adressée aux apôtres et aux prophètes ; et Dieu montre par là qu'il
découvre aux âmes saintes les jugements qu'il exerce sur la terre. D'où
vient qu'il en est loué par ces âmes bienheureuses, XIX, 1, 2.

 

542

 

21.  _Un ange fort leva,_ imité de Jérémie, LI, 63, 64.

22.  _Et la voix... des musiciens... et le bruit de la meule... Et la
lumière_... Imité de Jérémie, XXV, 10. Tout est triste, tout est
ténébreux , tout est mort dans une ville saccagée. _Tes marchands
étaient des princes de la terre_ : imité d'Ézéchiel, XXVII, 25, au sujet
de la ruine de Tyr. En général, il faut conférer tout ce chapitre avec
le chapitre XXVII d'Ezéchiel.

24. _Et on a trouvé dans cette ville le sang des prophètes, et le sang
des Saints, et le sang de tous ceux qui ont été tués sur la terre_. Tout
ce qu'il y a eu de martyrs dans les provinces ont péri par les décrets
et par les exemples de Rome ; et pour étendre encore plus loin cette
pensée, ceux qui répandent le sang innocent portent la peine de tout le
sang innocent répandu depuis le sang d'Abel, _Matth.,_ XXIII, 35.

[Précédente]

[Accueil]

[Suivante]

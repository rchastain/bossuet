

AVERTISSEMENT.


 

 LETTRES DE H. DE VALINCOUR

 A l'ABBE LE DIEU.

 

Pendant que je m'occupais à découvrir les erreurs des critiques
judaïsants, je sentais mon esprit ému en soi-même, en voyant des
chrétiens, et des chrétiens savants, qui semblaient même zélés pour la
religion , au lieu de travailler, comme ils le devaient, à l'édification
de la foi, employer toute leur subtilité à éluder les prophéties sur
lesquelles elle est appuyée , et plus dangereux que les rabbins , leur
fournir des armes pour combattre les apôtres et Jésus-Christ même. Les
sociniens avaient ouvert cette dispute, et la licence augmentait tous
les jours. Il me paraissait qu'une courte interprétation de quelques
anciennes prophéties pouvait être un remède aussi abrégé qu'efficace
contre un si grand mal ; et alors il arriva qu'un de mes amis m'ayant
proposé ses difficultés sur la prédiction d'Isaïe, où l'enfantement
d'une vierge était expliqué, j'avais tâché d'y répondre avec toute la
netteté et toute la précision possible, et néanmoins en faisant sentir
la force des preuves de la mission de Jésus-Christ et un caractère
certain de sa divinité.

En même temps je me souvenais d'avoir prêché, il y a deux ans, une
_Explication du Psaume_ XXI, où j'avais démontré d'une manière sensible
à toute âme fidèle, la passion, le crucifiement , la résurrection de
notre Sauveur, et sa gloire qui devait paraître dans la conversion des
Gentils.

Je me sentais aussi sollicité durant une convalescence qui ne me
permettait pas tout à fait l'usage de mes réflexions,

 

234

 

d'entretenir mon esprit de saintes pensées, capables de le soutenir : et
c'est ce qui a produit ces petits écrits.

Dieu ayant mis dans le cœur de plusieurs personnes pieuses d'en demander
des copies, on a eu plus tôt fait de les imprimer : et les voilà tels
qu'ils sont sortis d'une étude qui n'a rien eu de pénible. Qui sait si
Dieu ne voudra pas se servir de cet exemple pour exciter des mains plus
habiles à donner de pareils ouvrages à l'édification publique : et
apprendre aux chrétiens, non pas à disputer contre les Juifs , ce qui ne
produit que de sèches altercations, mais à poser solidement les
principes de la foi, afin que la tentation venant peut-être dans la
suite à s'élever par les discours des libertins aussi remplis
d'ignorance que d'inconsidération, elle se trouve heureusement prévenue
par une doctrine établie sur la pierre, qui empêche non-seulement les
orages et les tempêtes, mais encore qui déracine jusqu'aux moindres
doutes ; et que nous marchions d'un pas ferme, comme ont fait nos pères,
sur le fondement des apôtres et des prophètes?

 

 


LETTRES DE H. DE VALINCOUR

A BOSSUET ET A L'ABBÉ LEDIEU

SUR L'EXPLICATION DE LA PROPHÉTIE D'ISAÏE.

(inédites. )

 

A BOSSUET.

A Toulon, le 25 novembre 1703.

 

Monseigneur,

 

Quoique l'embarras où je me trouve ici, et surtout depuis quelques
jours, ne soit guère propre pour méditer sur des choses aussi sérieuses
et aussi importantes que celles que vous m'avez fait l'honneur de
m'écrire, cela ne m'a pas empêché de sentir toute la reconnaissance que
je dois à la bonté avec laquelle vous avez bien voulu prendre la peine
d'éclaircir la difficulté que j'avais eu l'honneur de vous proposer.
J'ai eu même beaucoup de joie d'apprendre par votre lettre que c'était
une chose fort en usage, dans la primitive Église, que de voir les
laïques et les femmes mêmes consulter les évêques et les docteurs sur
les difficultés qui se trouvent dans l'Écriture. C'est en effet un
secours qui leur est absolument nécessaire ; et nous en avons un bel
exemple , dès le temps même des apôtres, dans la personne de celui à qui
saint Philippe demanda s'il entendait Isaïe, et qui répondit : _Et
quomodo possum, nisi aliquis ostenderit mihi?_

Cela a lieu principalement à l'égard des prophéties qui, étant la preuve
et le fondement de la religion, sont cependant aujourd'hui ce qu'elle a
de plus obscur et de plus difficile à entendre, soit qu'elles aient
perdu de leur évidence par l'éloignement des temps où elles ont été
faites et par le peu de lumière qui nous reste sur l'histoire de ces
temps avec laquelle elles ont un rapport nécessaire ; soit que les
chrétiens étant élevés, dès l'enfance, dans la véritable religion et la
connaissant par son intérieur et d'une manière qui

 

236

 

nous en fait sentir la vérité sans que nous ayons besoin de preuves, ils
aient négligé cette manière de la démontrer par les prophéties, qui ne
laisse pas d'être absolument nécessaire pour ceux qui, n'ayant pas eu le
même bonheur que nous, cherchent de bonne foi les marques auxquelles on
peut la reconnaître. Or, Monseigneur, je suis persuadé qu'il n'y en a
point de plus sûre ni de plus efficace que les prophéties, lorsqu'on se
sera appliqué à leur donner, par une explication exacte et fidèle, toute
l'évidence qu'elles peuvent avoir et dont elles ont besoin pour servir
de preuves. Car il semble qu'il n'y a que deux moyens pour obliger un
homme à croire une vérité qu'on lui propose pour l'objet de sa créance,
ou l'évidence qui ne manque jamais d'entraîner le consentement par sa
propre lumière, ou la certitude que l'on a que cette vérité nous est
proposée par un être qui ne peut ni se tromper lui-même, ni vouloir
tromper les autres, parce qu'alors l'autorité tient lieu d'évidence et
nous fait croire fermement les choses les plus obscures et même les plus
contraires à notre raison. Et pour donner un exemple de ceci, dans la
géométrie qui est la seule de toutes les connaissances humaines où l'on
puisse s'assurer de trouver la vérité, si je propose à un homme de
croire que le tout est plus grand que sa partie, ou qu'en ajoutant
choses égales à choses égales, les touts demeureront égaux, il ne me
demandera aucune preuve de ces vérités, parce qu'il en sera convaincu
par leur propre évidence ; mais si je lui dis que deux lignes asymptotes
prolongées à l'infini, s'approcheront toujours et ne se rencontreront
jamais ; si j'ajoute que l'espace compris entre ces deux lignes
prolongées à l'infini, et qui par conséquent est infini lui-même, est
pourtant égal à un espace fini et déterminé que je lui trace sur un
papier, alors il pourra me dire que cela lui paraît impossible et
inconcevable ; mais que si je lui montre que cela est conforme aux
démonstrations de la géométrie dont il connaît la certitude, il ne
laissera pas de croire cette proposition sans pouvoir la comprendre.

Il en est de même de la religion. Jamais homme raisonnable ne sera
surpris qu'elle propose à croire des choses obscures et au-dessus de
notre raison ; mais tout homme raisonnable, avant que de les croire,
voudra être assuré que cette religion est en effet

 

237

 

celle qui conserve la parole de Dieu, et qui par conséquent ne peul
jamais être dans l'erreur ni y jeter ceux qui la suivent. Il faut donc
une évidence qui marque tellement la vérité de la religion à tous ceux
qui la cherchent, que personne n'en puisse douter et ne puisse avoir
d'excuse légitime pour refuser de croire les choses qu'elle nous offre
pour objet de notre créance.

Car supposant un homme dans l'état où M. Pascal supposait celui qu'il
voulait instruire, c'est-à-dire qui, dans un âge de raison, cherche de
bonne foi la véritable religion dont il n'a aucune connaissance : si cet
homme commence par interroger la nature, le soleil, les deux et tous les
éléments, ils lui diront qu'ils ne se sont pas faits eux-mêmes, mais
qu'ils ont été faits par un être supérieur, à qui ils doivent tout ce
qu'ils ont d'admirable, et surtout l'ordre dans lequel il les maintient
depuis tant d'années. Mais s'il continue à leur demander : Quel est donc
cet être, quels sentiments on en doit avoir, quel culte il exige des
hommes, ce qu'ils ont à craindre ou à espérer de lui? alors ils ne lui
répondront que par un affreux silence. Il faut donc qu'il regarde autour
de lui parmi les hommes qui sont sur la terre comme lui ; il faut qu'il
interroge tous ceux des siècles passés, et qu'il tâche de trouver dans
leurs écrits et dans leurs enseignements la vérité qu'il cherche et
qu'il a envie de connaître. C'est alors qu'au milieu de cette multitude
infinie de sectes et de religions différentes, qui ont partagé les
hommes dans tous les temps, il découvre un livre très-ancien, qu'on lui
dit être la parole de Dieu et la loi véritable hors de laquelle il n'y a
point de salut. Cela attire son attention ; il examine ce livre ; il y
trouve des choses au-dessus de sa raison, quelques-unes mêmes qui lui
paraissent y être entièrement contraires. Cependant comme il y voit
aussi des choses admirables, il déclare à ceux qui en sont les
dépositaires qu'il est prêt à croire tout ce qu'il enseigne et à
exécuter tout ce qu'il ordonne, pourvu que l'on lui montre, à n'en
pouvoir douter, que ce livre contient en effet la véritable loi de Dieu.
Jusque-là on ne saurait douter qu'il ne soit dans la disposition la plus
raisonnable où un homme en cet état puisse être : car comme il y aurait
de la folie à refuser sa créance aux choses que Dieu nous ordonne de
croire, puisque dès là qu'il

 

238

 

est Dieu, nous savons qu'il ne saurait nous tromper, il n'y en aurait
pas moins à recevoir légèrement de la main des hommes sujets à se
tromper et à tromper les autres, un livre qu'ils disent être la loi de
Dieu, si nous ne sommes assurés d'ailleurs que ce livre est en effet ce
que l'on nous assure qu'il est. Il faut donc trouver des preuves de la
divinité de ce livre, et il paraît qu'on ne saurait les trouver que dans
les prophéties. Car un homme qui n'a encore aucune connaissance de notre
religion, ne saurait encore être touché, ni par les martyrs, ni par les
miracles, voyant que tant d'autres religions manifestement impies et
extravagantes se vantent d'avoir les leurs. Il faut donc lui dire :
Cette religion que je vous annonce et que je vous propose comme la seule
véritable , a été annoncée par des prophètes qui, pour preuve de leur
mission, ont prédit des choses surprenantes et extraordinaires, qui sont
arrivées précisément dans les temps et dans la manière qu'ils l'avaient
prédit. Or cet être, quel qu'il soit, qui donne aux hommes le pouvoir de
faire des prédictions de cette nature et qui se vérifient par les
événements, doit non-seulement connaître l'avenir, mais encore en être
le maître absolu, pour pouvoir disposer toutes choses selon sa parole,
et par conséquent ce ne peut être que le véritable Dieu.

Celui qui est conduit jusqu'à ce point-là, paraît n'avoir plus rien à
faire que ce que faisaient les Juifs à qui les apôtres annoncèrent
l'Évangile : _Scrutantes quotidie Scripturas, si hœc ita essent_. Et
cela ne peut pas être regardé comme l'effet de la défiance d'un homme
qui doute de la vérité delà parole de Dieu, mais comme la sage
précaution d'un homme qui veut n'être point trompé, en prenant pour la
parole de Dieu ce qui pourrait n'être que l'imagination des hommes. Il
examine donc les prophéties : si elles lui sont clairement expliquées,
le voilà convaincu ; si elles ne le sont pas, il demeure dans son
obscurité et dans le même état que celui à qui on veut faire croire une
proposition de géométrie très-difficile, sans lui en donner la
démonstration. Car puisque les prophéties sont les preuves de la
religion, elles doivent avoir 1 évidence qui sert à convaincre ; et
puisqu'elles doivent nous convaincre de la vérité d'une religion qui
nous propose à croire tant

 

239

 

de choses obscures, elles doivent avoir assez de certitude et de clarté
pour nous montrer que cette religion est en effet la véritable. Il faut
donc qu'elles aient cette évidence et cette clarté par elles-mêmes et,
si l'on peut parler ainsi, indépendamment de la religion qu'elles
prouvent. Car si je dis à un homme : Vous devez croire cette religion
qui est prouvée par tant de prophéties, et que je lui donne pour preuve
de la certitude de ces prophéties l'autorité qu'elles ont dans la
religion et le respect avec lequel on les y a toujours regardées, je
tombe dans un cercle en prenant pour preuve la chose même que je veux
prouver.

C'est pourquoi, Monseigneur, après toutes les choses admirables que vous
avez eu la bonté de m'écrire, il ne laisse pas de me paraître toujours
difficile de pouvoir donner _l'Ecce virgo_ d'Isaïe comme une prophétie
qui ait pu servir à faire reconnaître le Messie : puisque, comme j'ai eu
l'honneur de vous le dire, il était absolument impossible aux Juifs les
plus éclairés et les mieux intentionnés, de reconnaître pour fils d'une
vierge celui qui paraissait né dans un légitime mariage. Il est vrai,
comme vous le remarquez , que parmi les Juifs mêmes le mariage de la
sainte Vierge ne devait pas faire croire qu'il fût impossible qu'elle
eût conservé sa virginité. Mais cette virginité cachée et inconnue à
tout le monde ne leur servait donc de rien pour les instruire sur ce qui
leur était annoncé? Peut-être que ceux qui furent touchés par la
doctrine et par les miracles de Jésus-Christ, et qui jugèrent de là
qu'il était le Messie, en conclurent, suivant le passage d'Isaïe, qu'il
devait être né d'une vierge, et par conséquent que sa mère, quoique
mariée, avait conservé sa virginité. Mais alors vous voyez que cette
prophétie, au lieu de servir de preuve pour reconnaître le Messie, a
besoin elle-même du Messie et de ses miracles pour être prouvée. On peut
dire la même chose de toutes les prophéties qui ne sont pas claires et
évidentes : ce sont des preuves qui ont besoin d'être prouvées. Si l'on
se sert pour cela de faits historiques et pour ainsi dire étrangers à la
religion, comme le sceptre ôté de la maison de Juda, la ruine de
Jérusalem prédite et accomplie, et autres semblables, on fera des
démonstrations régulières. Mais si l'on donne ces prophéties comme un
objet de foi et comme devant

 

240

 

être crues par ceux à qui l'on annonce la religion, parce qu'elles l'ont
toujours été par ceux qui sont élevés dans cette même religion , il
semble que l'on affaiblit toute leur force et que l'on dépouille la
religion du plus sur moyen que l'on a pour la démontrer, et c'est à quoi
il ne paraît pas que jusqu'à présent l'on ait assez fait d'attention.
Car une chose qui n'est pas évidente par elle-même ne peut être douteuse
ou certaine par rapport à nous qu'à proportion de l'obscurité ou de la
certitude que nous trouvons dans les moyens dont on se sert pour la
prouver ; et il ne faut point dire que la religion est faite pour être
crue avec soumission, et non pas prouvée par démonstration comme une
proposition de géométrie. Car il y a une grande différence entre
demander les preuves des mystères de la religion, ce qui serait une
extravagance et une impiété ; ou demander des preuves qui fassent voir
que cette religion est la véritable, ce qui n'est que l'effet d'une sage
précaution que doit prendre tout homme qui, ayant été assez malheureux
pour n'y avoir pas été élevé, est pourtant assez sage pour la chercher
de bonne foi et pour craindre de se tromper en la cherchant.

Voilà, Monseigneur, une longue lettre. Il me doit être permis, dans le
lieu où je suis, de vous dire comme a fait autrefois un plus habile
homme que moi, qu'elle n'est si longue que parce que je n'ai pas eu le
loisir de la faire plus courte ; mais j'espère que vous aurez la bonté
d'en excuser la longueur et les défauts. Je suis avec toute la
vénération que je dois,

 

Monseigneur ,

 

Votre très-humble et très-obéissant serviteur,

De Valincour.

 

N'ayant pas, Monseigneur, le temps de relire cette lettre avant que de
vous l'envoyer, permettez-moi d'y ajouter ce mot qui ne sera peut-être
qu'une répétition, mais qui expliquera du moins ce que je puis n'avoir
pas assez expliqué. Quand je dis : Voilà ce que la véritable religion me
propose à croire, alors il n'y a qu'à croire aveuglément sans preuve,
sans raisonnement : _Captivantes intellectum ;_ le mérite est dans la
soumission. Mais quand je dis : Entre

 

241

 

toutes les religions du monde, voilà la seule bonne et la seule
véritable, alors il faut de la certitude, il faut des preuves pour me
déterminer ; je puis me tromper dans le choix, et je dois le craindre :
_Videte ne quis vos seducat_. Or il paraît que rien ne peut m'assurer
que l'évidence des prophéties.

 

 


A l'ABBE LE DIEU.

 

A bord du Foudroyant, le 11 octobre 1703.

 

J'ai reçu, Monsieur, la réponse de Monseigneur l'évêque de Meaux. Je me
donne l'honneur de l'en remercier par cet ordinaire ; mais je vous dois
aussi un remerciement de me l'avoir procuré , et de l'attention que vous
avez bien voulu avoir à lui parler de ma question pour l'engager à y
répondre. On ne peut être plus sensible que je suis à la bonté avec
laquelle il le fait, ni plus pénétré des lumières avec lesquelles il a
bien voulu m'instruire. Si par hasard , dans les promenades des
Tuileries ou les autres qu'il fera cet automne, la conversation
retombait encore sur la même question, et que vous puissiez lui pouvoir
faire une nouvelle difficulté sans crainte d'altérer sa santé, à
laquelle rien n'est plus contraire que le trop d'application, je vous
prierais de vouloir bien lui dire que le fond de l'objection consiste à
dire que la prophétie d'Isaïe, dont il s'agit, non-seulement n'éclaircit
point les Juifs et ne leur montre point que Jésus-Christ était le
Messie, et c'est à quoi notre admirable et respectable prélat a
divinement répondu, mais que cette même prophétie les aveugle et leur
fournit un argument auquel il leur était impossible de trouver la
réponse, et c'est sur quoi je désirerais qu'il eût la bonté de
m'instruire. Car Isaïe disant positivement que le Messie doit naître
d'une vierge, et Jésus-Christ paraissant aux yeux de tout le monde être
né d'une femme mariée, il s'ensuivrait qu'ils pouvaient conclure que
Jésus-Christ n'était pas le Messie et qu'ils ne pouvaient le reconnaître
pour tel sans démentir la prophétie. Il est même si vrai que cela leur
pouvait passer par l'esprit, qu'ils en font l'objection eux-mêmes

 

242

 

en disant : « Lorsque le Messie viendra , on ne s'aura d'où il est venu
; mais pour celui-là nous savons parfaitement d'où il vient. » C'est sur
cela, Monsieur, que je désirerais fort être instruit ; mais je désire
avant toutes choses et préférablement à tout, que l'on ne propose rien à
Monseigneur l'évêque de Meaux qui puisse le moins du monde, comme j'ai
déjà eu l'honneur de vous dire, altérer sa santé par quelque application
que ce puisse être. Ainsi je vous laisse le maître de supprimer ma
difficulté qui ne vaut assurément pas la moindre peine que cela pourrait
lui donner. Je suis de tout mon cœur,

 

Monsieur,

 

Voire très humble et très-obéissant serviteur,

 

De Valincour.

[LETTRES LXII-LXX]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Lettres diverses
Sur la Comédie
Lettres III-VIII
Lettres IX-XVII
Lettres XVIII-XXXVII
Lettres XXXVIII-XL
Lettres XLI-XLIV
Lettres XLV-LV
Lettres LVI-LXI
Lettres LXII-LXX
Lettres LXXI-LXXXII
Lettres LXXXIII-XCV
Lettres Dlle de Metz
Lettre Mme de Maisonfort I
Lettre Mme de Maisonfort II
Lettre Mme de Maisonfort III
Lettres Mme de Maisonfort IX
Lettres Mme de Maisonfort XV
Lettres Mme de Cornuau I
Lettres Mme de Cornuau IX
Lettres Mme de Cornuau XXXVIII
Lettres Mme de Cornuau CXXXI

 

 

LETTRE LXII.  BOSSUET A M. BRISAClER, SUPÉRIEUR DU  SÉMINAIRE DES  MISSIONS ÉTRANGÈRES.  De Meaux, ce 13 septembre 1701.

 

Une fausse miséricorde et une fausse sagesse inspirent à certains
savants l'inclination d'étendre la vraie religion sur plusieurs peuples,
autres que celui que Dieu lui-même a choisi. Ils s'imaginent qu'ils
dégraderaient la Divinité, s'ils la réduisaient à ce seul peuple ; et au
lieu d'adorer en tremblant les secrets et impénétrables jugements de
Dieu, qui livre toutes les nations à l'idolâtrie, à la réserve de celle
qu'il a séparée des autres par tant de

1 Sozom., lib. II, c. XII. — 2 Lib. I, _de Divin_.

 

242

 

prodiges, ils cherchent à obscurcir la sainte rigueur qui veut
convaincre l'homme par expérience de son aveuglement, afin qu'il soit
plus capable de comprendre d'où lui venait la lumière. C'est ce que ces
savants curieux et vains ne veulent pas entendre. A quelque prix que ce
soit, ils entreprennent de sauver les Perses, les Ethiopiens, les
Indiens et plusieurs autres nations. Les Chinois, qu'on a voulu
épargner, ont animé les esprits à cette dispute. La censure de la
Faculté contre leurs défenseurs, a donné occasion de publier un vœu qui
a été prononcé par un docteur de Sorbonne, dans les délibérations où eUe
a été résolue. L'auteur s'attache principalement à justifier par
l'Écriture la religion des anciens Perses ; et quoiqu'il ait désavoué
l'impression de son vœu et se soit soumis d'ailleurs à la censure qui en
rejette la doctrine, il est bon de joindre la raison à l'autorité d'une
Faculté si célèbre, pour ne pas laisser subsister des preuves qui
pourraient induire les gens mal instruits à des erreurs où toute
l'économie de la religion est renversée. Mais avant que d'entrer à fond
dans cette réfutation, et dans la discussion des autres matières qui
regardent la religion de quelques anciens peuples, je proposerai en
abrégé la doctrine de saint Athanase sur les causes et l'étendue de
l'idolâtrie, ainsi qu'elle est contenue dans les deux discours de même
dessein et de même suite, qui sont à la tête de ses ouvrages, dont l'un
a pour titre : _Contre les Gentils_ ; et l'autre : _De l'Incarnation du
Verbe_.

Il enseigne donc que la cause de l'idolâtrie, c'est que l'homme ayant
quitté par le péché la contemplation de la nature divine invisible et
intellectuelle, s'est plongé entièrement dans les sens ; en sorte qu'il
est incapable d'être frappé d'autres objets que des objets sensibles :
d'où il est venu à l'oubli de Dieu, à adorer le soleil, les astres, les
éléments, les animaux, les images même, les passions et les vices, et
enfin toute autre chose que Dieu (1).

Cette erreur s'est répandue par toute la terre, mais en telle sorte
qu'encore que tous les peuples aient été plongés dans l'idolâtrie, ils
ne sont pas pour cela convenus des mêmes dieux, chaque nation s'étant
fait le sien comme elle a voulu (2). Ainsi

 

1 _Oratio contra gentes,_ n. 9, 11-13, etc. — 2 _Ibid.,_ n. 23.

 

243

 

autant qu'il y a eu de peuples divers, autant on a imaginé de dieux. Les
pays et les villes se sont partagés. Les Phéniciens ignorent les dieux
que l'Égypte adore : les Scythes ne connaissent pas les divinités des
Perses, ni les Perses celles des Syriens, ni les Indiens celles des
Arabes, ni les Arabes celles des Ethiopiens, ni les Grecs celles des
Thraces, ni ceux-ci celles des Arméniens ; et ainsi des autres, dont
saint Athanase fait un grand dénombrement, pour nous faire voir que tous
les peuples conviennent dans l'idolâtrie, sans pour cela convenir des
mêmes dieux. Au contraire ceux qui sont en exécration aux uns, sont en
honneur chez les autres : les uns immolent comme victimes ce que les
autres honorent comme dieux : on en est même venu jusqu'à immoler son
semblable par une inhumanité dont ce Père allègue beaucoup d'exemples
(1) ; et il serait aisé de montrer cet usage barbare parmi presque tous
les peuples de l'univers.

Voilà donc parmi les idolâtres tous les peuples du monde, sans exception
aucune. Les Perses, les Ethiopiens, les Indiens y sont compris comme les
autres, et les Grecs avec les Barbares (2).

Il ne réserve que le peuple qui a reçu la loi de Dieu (3). Il fait voir
que l'âme s'oublie elle-même, et qu'elle ne conçoit plus que Dieu l'a
faite à son image, par où elle eût dû être amenée à la connaissance du
Verbe ; et il ne connaît pour vrais adorateurs que ceux qui en sont ornés
(4).

n donne pour principe assuré qu'avoir plusieurs dieux, c'est n'en avoir
point ; et qu'ainsi l'idolâtrie étant partout, conséquemment il y a
partout une espèce d'athéisme (5).

Dans cette inondation de l'idolâtrie, il observe toujours avec soin
l'exception qu'il faut faire en faveur des Juifs, comme de ceux à qui
les idoles sont expressément défendues, et à qui la connaissance de Dieu
et de son Verbe Jésus-Christ Notre-Seigneur a été donnée, tenant pour
des insensés ceux qui ne connaissent ni l'un ni l'autre (6).

Je passe au second discours, _de l'Incarnation du Verbe,_ où

1 _Oratio contra gentes,_ n. 24, 25, p. 23 et seq. —  2 N.9, 24 ;
_ibid_., n. 9,24. — 3 _Ibid._ n. 27, 30 ; _ibid_., p. 26 et 29. — 4  N.
33, 34. — 5 N. 38, _ibid_., p. 36 et seq. — 6 N. 30, 45-47 ; _ibid_., p.
44 et seq.

 

244

 

saint Athanase pose pour fondement que ce n'est pas connaitre Dieu que
de ne pas connaitre la création, et d'assujettir la Divinité à ne rien
faire que d'une manière (1) (c'était l'erreur universelle, on croyait 
que les astres et les corps célestes donnaient l'être à tout). Il
continue à prouver qu'il n'y a point de véritable religion sans la
connaissance de Dieu et de son Verbe : « Tout, dit-il, était dans
l'impiété, tout était plein de malice ; et le seul Dieu et son Verbe
étaient ignorés (2). »

Les hommes n'ayant pas profité de la beauté des ouvrages de Dieu, il
leur a envoyé la loi et les prophètes (3). Car ni la loi ni les
prophètes n'avaient point été donnés aux Juifs pour eux seuls, mais
encore pour éclairer tout l'univers de la connaissance de Dieu et des
bonnes mœurs. Mais au lieu de profiter de cette instruction céleste, ils
s'enfonçaient tous les jours de plus en plus dans l'erreur ; en sorte
qu'ils semblaient avoir entièrement perdu la raison, et n'être plus que
des bêtes brutes.

On pourrait étendre ici ce que saint Athanase ne dit qu'en un mot, qui
est que la loi et les prophètes étaient envoyés à tout le monde. Les
enseignements admirables que Dieu donnait à son peuple, et les prodiges
éclatants qu'il faisait pour le maintenir et l'instruire, rayonnaient
bien loin aux environs, et auraient pu de proche en proche se répandre
par toute la terre. Mais loin que les peuples voisins et les autres
successivement en aient profité, les Juifs eux-mêmes ont persécuté les
prophètes : « Ils étaient, dit-il, envoyés aux Juifs, et en même temps
persécutés par les Juifs (4) ; » ce qui achève de démontrer que la
corruption était universelle, et la pente à l'erreur si prodigieuse, que
ceux-là même à qui les prophètes étaient adressés se déclaraient leurs
ennemis.

Il n'y avait point d'autre remède à un si grand mal que la venue du
Verbe, qui, ayant tout fait, devait aussi tout refaire et tout réparer
(5).

L'idolâtrie et l'impiété avaient rempli tout le monde : les ouvrages de
Dieu n'avaient servi de rien pour le faire connaitre : tous les hommes
avaient les yeux attachés en bas sans les

 

1 _De Incarn. Verbi,_ n. 2 et 3. — 2 N. 11, 12, _ibid_., p. 56 et seq. —
3 N. 12, ibid., p. 57. — 4 _Ibid.,_ n. 12. —  5 N. 12, 13, _ibid_., p.
57, etc.

 

245

 

pouvoir élever au ciel, et il n'y avait que le Verbe qui les pût
redresser en prenant un corps (1).

Il montre ici que le Verbe s'est répandu par toute la terre et, comme
disait saint Paul, s'est dilaté en longueur et en largeur, en hauteur et
en profondeur, tant par la prédication de l'Évangile que par le nombre
infini de ses martyrs. Il étend beaucoup cette preuve ; et c'est ici que
se trouve ce passage si net et si précis, qui a été traduit ainsi par M.
Dupin, à qui rien n'a échappé : « Autrefois il y avait des idoles par
toute la terre ; l'idolâtrie tenait les hommes captifs, et ils ne
connaissaient point d'autres dieux que les idoles (2). »

Saint Athanase distingue partout soigneusement les deux peuples,
l'ancien qui était les Juifs, et les gentils (3). Il remarque que les
gentils n'ont jamais commencé à connaitre Dieu et le Verbe, que quand
Jésus-Christ a paru. Quoiqu'il y eût une infinité de religions, nul
peuple n'a attiré son voisin à reconnaître son Dieu. Les sages des
gentils avec leurs discours magnifiques et la sublimité de leur
éloquence, n'ont pu par tant de volumes attirer personne dans leur
voisinage à la doctrine des bonnes mœurs et de l'immortalité des âmes
(4). Il n'a été donné qu'à Jésus-Christ de se faire connaitre seul par
toutes les nations, dont les sentiments étaient si contraires. Il y a eu
parmi les gentils, Chaldéens, Egyptiens, Indiens, des rois et des sages
: les philosophes de la Grèce ont écrit plusieurs livres avec beaucoup
d'art : mais ni vivants ni morts, ils n'ont rien avancé (5) :
Jésus-Christ seul a pu persuader sa doctrine aux enfants mêmes. « Quel
autre, dit-il, a étendu son empire sur les Scythes, les Ethiopiens, les
Perses, les Arméniens, les Goths, et ainsi des autres ; et leur a pu
persuader par une illumination cachée et intérieure, de ne plus adorer
les dieux de leurs pères et de leur pays, et d'adorer le Père par son
Verbe (6)? » Enfin tout le discours de ce saint docteur tend à faire
voir que tous les peuples du monde, sans en excepter ceux qu'on veut
croire les plus privilégiés, comme les Perses, les Ethiopiens,

1 _De Incarn_., n. 15. 16 ; _ibid_., p. 60. — 2 N. 46, p. 88. — 3 N. 25,
36, 38, 40, 41, 43. 46, 50, 51. — 4 N. 47, p. 88. — 5 N. 50, _ibid_., p.
91. — 6 N. 51, ibid.,

 

246

 

les Indiens, étaient livrés à l'idolâtrie ; que les Juifs étaient
éclairés par Moïse et par les prophètes ; que les autres n'ont commencé
à ouvrir les yeux que quand Jésus-Christ est venu (1) ; que c'a été
l'effet du sacrifice qu'il a offert à la croix pour tous les hommes ; et
qu'auparavant ils étaient tous dans les ténèbres, et que toute la nature
humaine était aveugle (2).

Voilà les principes sur lesquels a raisonné ce grand homme. Tout ce qui
était gentil, c'est-à-dire tout ce qui n'était pas juif, était idolâtre.
Tous les autres Pères ont enseigné la même doctrine. M. Dupin l'a
démontré d'une manière à ne laisser aucun doute ni aucune réplique (3).
Il n'a eu garde d'oublier saint Athanase ; et outre le passage que nous
venons de remarquer, il a encore cité celui où ce grand défenseur de la
divinité du Verbe a dit, conformément au Psalmiste, que « Dieu n'était
connu que dans la seule _Judée_ (4). » Tout est déjà démontré dans le
fond, et j'ai voulu seulement donner ici le principe général sur lequel
saint Athanase s'est fondé. C'est, en un mot, que par le péché l'homme
entièrement asservi aux sens oubliait Dieu, et ne faisait que s'enfoncer
de plus en plus dans l'idolâtrie. Le principe est évident, la
conséquence est certaine, la démonstration est parfaite : elle convainc
également tous les peuples de l'univers ; et il ne faut pas s'étonner si
tous les Pères sans exception ont tenu le même langage.

Il ne reste plus qu'à répondre à certains exemples particuliers que
l'auteur du Vœu a proposés, dont le premier est celui de Cyrus et des
anciens Perses.

 

LETTRE LXIII.  A MILORD PERTH. A Meaux, ce 20 septembre 1701.

 

Mon cœur me presse de vous témoigner la part que je prends à votre juste
douleur (_a_), et en même temps de vous supplier

 

1 _Cont. gent_., n. 30, 45, 46, etc. ; _De Incarn_., n. 12, 34, 35, 39,
40, etc. — 2 N. 20, 37, 43. — 3  _Déf. de la Censure,_ etc. — 4 _Oratio
I contra Arian_., a. 59. (a) Sur la mort de Jacques II, décédé le 6
septembre 1701.

 

247

 

humblement de prendre quelque temps propre à présenter au jeune roi et à
la reine mes très-profonds et très-fidèles respects ; me confiant que
par la bonté de Leurs Majestés et par votre entremise, elles les auront
pour agréables.

Dieu est le Seigneur ; il sait les moments : il a des couronnes à
donner, dont rien ne peut approcher sur la terre. Tout ce qui passe
n'est rien : tout ce qui finit, comme dit saint Paul, doit presque être
compté comme n'étant pas. On fait des vœux, on offre des sacrifices, on
espère, on attend les temps que Dieu a réservés à sa puissance. Dieu
seul sait ce qui est bon ; et c'est là, Milord, ce que vous ferez sentir
au roi. Je suis avec un sincère respect, etc.

 

LETTRE, LXIV.  BOSSUET A MILORD PERTH. A Versailles, ce 29 janvier 1702.

 

Je prends la liberté de vous envoyer le petit ouvrage sur les promesses
de Jésus-Christ à l'Église.

Sans quelque incommodité, qui ne me permet pas d'aller à Saint-Germain,
j'aurais été avec un profond respect le présenter à Leurs Majestés. Je
vous conjure, Milord, de prendre le temps de m'acquitter de ce devoir,
et de vouloir bien les assurer du désir extrême que j'aurais d'y
satisfaire en personne. Je suis, avec un respect sincère, etc.

PISTOLA LXV.  RECTOR ET UNIVERSITAS LOVANIENSIS AD BOSSUETUM.

 

Tam notus est orbi catholico tuus in Ecclesiam et sacras Litteras amor,
ut quoties earum causa agitur, opem patrociniumque tuum magnâ cum
fiduciâ omnes implorent. Tuis pro Ecclesià triumphis ex animo
gratulamur, Prœsul sapientissime, et hoc unum oramus ut eorum particeps
esse, et in eamdem tecum pro Ecclesià arenam descendere queat Facultas
nostra theologica Lovaniensis, illa utique, teste Leone X, _agri
Dominici piissima_

 

248

 

249

_religiosissimaque cultrix,_ ac. non ita pridem in hàc inferiori
Germaniâ fidei columen.

At nota sunt dissidiorum zizania, quœ in illà seminavit inimicus homo,
quae nisi quantociùs evellantur, verendum est ne celeberrima illa
Facultas ipsa se consumât, nec tantùm Academiœ nostrœ, sed toti etiam
Belgio gravem perniciem afferat. Dùm horum malorum originem studiosè
indagamus, hanc unam esse comperimus, quôd optimi quique hujus Academiœ
theologi vagis accusationibus obruantur, ac eo prœtextu à Facultatis suœ
muniis excludantur : dùm autem innocentiam suam tueri volunt, per
interdicta à ministris regiis extorta, omnis eis justitiœ via
occludatur. Nostras eâ de re querelas, scriptis ad Regem
christianissimum litteris, déferre hodiè ausi fuimus : apud quem ut suo
nos patrocinio dignetur illustrissima Gratia Vestra humillimè
supplicamus. Hoc unum votum nostrum est, ut infortunatis hisce dissidiis
finis tandem imponatur, _regibusque nos__tris sanguine animoque
junctis,_ jungamur et nos, unum dicamus omnes, Ecclesiœque fidem
unanimiter tueamur. Deus vota nostra secundet, patrocinante nobis
pietate vestrâ, cui causam hanc summo affectu ac veneratione
commendamus, illustrissime, etc.

RECTOR ET UINIVERSITAS LOVANIENSIS.

 

Lovanii, die 22 feb. 1702.

 

EPISTOLA LXVI.  BOSSUETUS AD REVERENDUM RECTOREM,  ET CLARISSIMOS VIROS ACADEMIAE LOVANIENSIS.

 

Pergratum et per honestum quod vester amplissimus Cœtus de me tam
prœclarè senserit, ut res quoque suas commendatas vellet : cui equidem
officio, data opportunitate quàvis, spondeo me nunquàm defuturum, etiam
non rogatum. Quis enim aut catholicus episcopus non suspiciat
Universitatem Lovaniensem doctissimam, facundissimam ac de re catholicà
optimè meritam ; aut theologus Parisiensis non impensè diligat eamdem
Academiam,

 

249

 

Parisiensis nostrœ fœtum egregium, suœ originis memorem institutisque
dignam? Rogo autem et obsecro ut ea de quibus agitis vestrœ theologicœ
Facultatis dissidia, quantum fieri poterit, componatis, ne suis manibus
se ipsa conflciat, rem dolendam omnibus sœculis, et tantum Ecclesiœ
lumen extinguat. Quod malum avertat Deus auctor pacis, Deoque aspirante
summa ac beata illa Sedes, quœ sapientiâ, œquitate, paternà auctoritate
res Ecclesiœ tempérât, ac dissociata membra recolligit. Ita voveo,
Reverende Domine Rector, Viri Academici, etc.

 

Datum Meldis, die 28 martii, anno Domini 1702.

 

LETTRE LXVII.  BOSSUET A MILORD PERTH.  A Meaux, ce 12 avril 1702.

 

Tout ce qui dépend de moi est absolument dans la dépendance de la reine.
Je vous supplie seulement de faire considérer à Sa Majesté que l'affaire
dont vous me faites l'honneur de m'écrire de sa part (a), est de la
nature de celles qui ne sont en aucune sorte de ma connaissance, et dont
aussi je me fais une loi inviolable de laisser la disposition à
Messieurs du collège de Navarre. C'est, Milord, ce que je vous dirai
être pour moi une règle dont je ne me suis jamais départi. Je vous ai
toujours présent au saint autel et, si j'ose le dire, j'y offre toujours
à Dieu Leurs Majestés britanniques et leurs royaumes. Je suis avec un
respect sincère et cordial, etc.

(a) Nous ignorons absolument de quelle affaire il pou voit être
question. (Les édit.)

 

250

 

LETTRE LXVIII.  A DOM MABILLON. A Meaux, ce 26 avril 1702.

 

Vous avez bien fait, mon cher et révérend Père, de donner la _Mort
chrétienne_ : je l'ai reçu et je le lis avec agrément. J'ai aussi reçu
le livre de mon compatriote, à qui je vous prie de faire mes
remerciements. Je suis bien aise que vous alliez commencer à imprimer
les Annales ; trois volumes, c'est déjà une grande avance. Je suis bien
obligé à dom Thierry de son cher souvenir ; je vous embrasse tous deux
de tout mon cœur.

 

LETTRE LXIX.  M. PIROT A BOSSUET. En Sorbonne, 29 avril 1702.

 

J'eus l'honneur de vous répondre il y a deux jours sur le _C'est là mon
corps_ de M. Simon, dont vous me donniez ordre de vous mander ce que je
pensais. J'oubliai de vous toucher dans ma réponse un autre endroit de
cette version, où je crois que l'auteur doit s'expliquer dans sa note
autrement qu'il ne fait ; c'est sur le verset 7 du chapitre V de la
_Première Epître de saint Jean_. Vous savez qu'il avait fort mal écrit
sur ce verset dans son Histoire critique du texte du Nouveau Testament
et dans celle des Versions, qui ne sont l'une et l'autre imprimées que
de contrebande, et que je n'ai jamais voulu approuver, quoique
monseigneur l'archevêque de Paris en eût fort envie. M. Arnauld a écrit
sur cela contre lui dans ses _Steyaertes_ (_a_). Il ne s'étend pas ici
comme il avait fait dans cette histoire critique du texte et des
versions :

 

(a) Cet ouvrage est principalement dirigé contre M. Steyaert, docteur de
la Faculté de Louvain ; et à la tête de la neuvième partie de ses
_Difficultés à ce Docteur,_ M. Arnauld a mis une longue Dissertation
contre Richard Simon, touchant les exemplaires sur lesquels cet écrivain
prétendait que l'ancienne Vulgate avait été faite. (_Les Edit_.)

 

251

 

mais la note qu'il y fait, après s'être rendu si suspect auparavant, ne
peut satisfaire : il aurait été mieux de n'en point faire. Il semble
qu'il n'en ait voulu faire que pour donner atteinte à ce verset autorisé
par saint Cyprien, comme l'évêque d'Oxford l'a remarqué dans l'édition
de ce Père, au livre de _l'Unité de l'Église_. Je ne sais si ce qu'il
dit des censeurs de Rome sous Urbain VIII, que tous leurs manuscrits
grecs étaient sans ce septième verset, est bien vrai : mais il semble ne
le remarquer que pour faire entendre qu'ils ont eu tort de l'avoir voulu
retenir dans le plan d'une nouvelle édition grecque qu'ils ont dressée.
Je ne dis rien du prologue de saint Jérôme sur les sept épîtres
canoniques, parce que l'auteur n'en parle pas ici, comme il en avait
parlé dans sa critique contre ce qu'en dit l'évêque d'Oxford. Je suis
avec un très-profond respect, etc.

Pirot.

 

LETTRE LXX.  BOSSUET A M. LE CARDINAL DE NOAILLES,  ARCHEVÊQUE DE PARIS (_a_). Ce 19 mai 1702.

 

J'envoie enfin mes remarques à Votre Eminence : je la supplie de les
vouloir bien communiquer à M. Pirot ; et quand il lui en aura rendu
compte, et que Votre Eminence elle-même en aura pris la connaissance que
ses grandes et continuelles occupations lui pourront permettre, qu'elle
veuille bien me prescrire l'usage que j'en dois faire. Nous devons tout
à la vérité et à l'Évangile ; et dès que l'affaire est devant vous,
Monseigneur, je tiens pour certain que, non-seulement vous y ferez par
vous-même ce qu'il faudra, mais encore que vous ferez voir à moi et aux
autres ce

(_a_) Cette lettre et les suivantes furent écrites par Bossuet, en
envoyant à ceux à qui elles sont adressées ses remarques sur la version
du Nouveau Testament de M. Simon. M. de Meaux fondit depuis toutes ces
remarques dans ses deux _Instructions pastorales sur la version de
Trévoux_. Au reste, les trois lettres qui suivent sont sans date dans
les originaux ; mais on voit par le _Journal de M. Ledieu,_ secrétaire
de Bossuet, qu'elles furent envoyées de Meaux le 19 mai 1702. (Les
édit.)

 

252

 

qu'il convient à chacun. J'ose seulement vous dire qu'il y faut regarder
de près, et qu'un verset échappé peut causer un embrasement universel.
Je trouve presque partout des erreurs, des vérités affaiblies, des
commentaires, et encore des commentaires mauvais mis à la place du
texte, et enfin les pensées des hommes au lieu de celles de Dieu, un
mépris étonnant des locutions consacrées par l'usage de l'Église ; et
enfin de tels obscurcissements, qu'on ne peut les dissimuler sans
prévarication. Aucune des fautes de cette nature ne peut passer pour peu
importante, puisqu'il s'agit de l'Évangile, qui ne doit perdre ni un
iota ni un de ses traits.

Je supplie Votre Eminence de croire qu'en appuyant mes remarques avec un
peu plus de loisir, je puis, par la grâce de Dieu, les tourner en
démonstrations. On peut bien remédier au mal à force de cartons : mais
il faudra que le public en ait connaissance, puisque sans cela le débit
qui se fait du livre porterait l'erreur par tout l'univers, et qu'il ne
faut pour cela qu'un seul exemplaire. Je m'expliquerai davantage,
Monseigneur, sur les desseins que l'amour de la vérité me met dans le
cœur, quand j'aurai appris sur ceci les sentiments de Votre Eminence.

Post-scriptum de la main de M. de Meaux. Le prier pendant les
occupations de l'assemblée, de faire examiner mes remarques
non-seulement par M. Pirot, mais encore par MM. de Beaufort et Boileau,
et de me donner communication de ces remarques, qui donneront lieu à de
nouvelles réflexions.

[Précédente]

[Accueil]

[Suivante]

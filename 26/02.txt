[MÉLANGES II]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Mélanges I
Mélanges II
Mélanges III
Mélanges IV
Lettres I-XL
Lettres XLI-LXIII
Lettres LXIV-XCIII
Lettres XCIV-CXXIX
Lettres CXXX-CLXXI
Lettres CLXXII-CLXXX
Lettres CLXXXI-CLXXXII
Lettres CLXXXIII-CXCIII
Lettres CXCIV-CCX
Lettres CCXI-CCXVII
Lettres CCXVIII-CCXXXVIII
Lettres CCXXIX-CCXLIII
Lettres CCXLIV-CCXLIX

 

 



EXTRAITS DE LA MORALE D'ARISTOTE (A).


 


DEUX MOTS D'ARISTOTE.

[Description : File0013] .
 


DE MORIBUS AD NICOMACHUM.  I.

 

LIVRE I, CHAPITRE 6.

 

Il semble que la perfection de chaque chose consiste dans son action.
Car chaque chose a son action. La perfection et le bien

 

(a) Bossuet dit dans le rapport au souverain Pontife sur l'éducation du
Dauphin : » Pour la doctrine des mœurs, nous avons cru qu'elle ne se
devait pas tirer d'une autre source que de l'écriture et des maximes de
l'Évangile ; et qu'il ne fallait pas, quand on peut puiser au milieu d'un
fleuve, aller chercher des ruisseaux bourbeux. Nous n'avons pas
néanmoins laissé d'expliquer la Morale

(b) Comme on lui demandait (à Aristote ) quelle est la chose qui
vieillit : « La reconnaissance, » répondit-il.

A cette autre question : « Qu'est-ce que l'espérance? » il répondit : «
Le rêve d'un homme éveillé. »

A l'exemple de saint Basile et d'autres auteurs, Bossuet rapporte
ailleurs ce

 

24

 

d'un architecte c'est de bâtir, et du peintre comme tel de faire un
tableau, et ainsi des autres. Quoi donc? les artisans, ceux même qui
font profession des arts les plus mécaniques, ont leurs actions, les
cordonniers, les maçons, les charpentiers. L'homme seul se trouverait-il
sans action? la nature l'aura-t-elle destiné à une oisiveté éternelle?
l'aura-t-elle formé si beau, si adroit, si désireux de savoir pour le
laisser toujours inutile? ou bien ne faut-il pas dire plutôt que si les
yeux, les oreilles, le cœur, le cerveau et généralement toutes les
parties qui composent l'homme ont leur action, l'homme aura outre
celles-là quelque action, quelque ouvrage, quelque fonction principale?
Quelle donc pourra être cette fonction ? Car certes la faculté de
croître lui est commune avec les plantes. Or il est besoin ici de
quelque chose qui lui soit propre, parce que nous trouvons que la
perfection de chaque chose est d'exercer l'action que Dieu et la nature
lui ont donnée pour la distinguer des autres. Par exemple, la perfection
du joueur de luth, en tant qu'il est tel, ne consiste pas en ce qu'il
peut avoir de commun entre l'arithméticien et le peintre, comme peuvent
être la subtilité de la main et la science des nombres, mais en ce qui
lui est propre. Par cette même raison, il est clair que l'homme ne peut
pas trouver la perfection dans les fonctions animales. Car les bêtes
brutes l'égalent et le surpassent même quelquefois dans cette partie :
que si nous trouvons après

 

d'Aristote ; à quoi nous avons ajouté cette doctrine admirable de
Socrate, vraiment sublime pour son temps, qui peut servir à donner de la
foi aux incrédules et à faire rougir les plus endurcis. »

Ces paroles expliquent suffisamment l'origine et le but des Extraits
qu'on va lire. Ces extraits furent faits, nous le pensons du moins, vers
1678, lorsque l'éducation du Dauphin approchait de sa fin. Ils
paraissent ici pour la première fois. Le manuscrit, tout entier de la
main de Bossuet, se trouve à la bibliothèque du séminaire de Meaux.

 

dernier mot : « L'espérance dont le monde parle, dit-il, n'est autre
chose, à le bien entendre, qu'une illusion agréable ; et ce philosophe
l'avait bien compris lorsque ses amis le priant de définir l'espérance,
il leur répondit en un mot : « C'est un songe de personnes qui veillent:
_Somnium vigilantium_ (Apud S. Basil, Epist. XIV, n. 4). » Un peu plus
loin, parlant de l'homme revenu à son serrs rassis après des rêves
enivrants, Bossuet ajoute : « Que peut-il juger de lui-même, sinon
qu'une espérance trompeuse le faisait jouir pour un temps de la douceur
d'un songe agréable ? Et ensuite ne doit-il pas dire, selon la pensée de
ce philosophe, que l'espérance peut être appelée « la rêverie d'un homme
qui veille, _» somnium vigilantium_? (Panégyrique de sainte Thérèse, Ier
point.)

 

25

 

une exacte recherche de tout ce qui est dans l'homme, que la raison est
tout ensemble et ce qu'il a de plus propre et de plus divin, ne
faudra-t-il pas décider que la perfection de l'homme est de vivre selon
la raison? Et de là il résulte que c'est dans cet exercice que consiste
la félicité. Car il est certain que chaque chose est heureuse quand elle
est parvenue à la perfection pour laquelle elle est née, et le bonheur
du joueur de luth comme tel est de toucher délicatement de cet
instrument si harmonieux. Car comme le propre du joueur de luth c'est
déjouer du luth, aussi est-ce d'un bon joueur de luth d'en jouer selon
les règles de l'art. Que si l'homme n'avait d'autre qualité que celle
déjouer du luth, il serait parfaitement heureux, quand il aurait atteint
la perfection de cette science. Il en est de même de la raison. Et
encore qu'il y ait en l'homme autre chose que la raison, si est-ce
néanmoins qu'elle est la partie dominante, et l'autre est née pour lui
obéir. Par où il paraît que la félicité de l'homme est à vivre selon la
raison. En quoi il ne faut pas prendre garde aux sentiments des
particuliers. Car l'esprit de l'homme est capable d'errer non moins dans
le choix des choses qu'il faut faire pour être heureux, que dans la
connaissance de, toutes les autres vérités. De sorte qu'il ne faut pas
avoir égard à ceux qui se sont figuré une fausse idée de bonheur, et
ainsi leur imagination étant abusée, semblent jouir de quelque ombre de
félicité, semblables aux hypocondriaques, dont la fantaisie blessée se
repaît du simulacre et du songe d'un plaisir vain et chimérique, et d'
un fantôme léger, d'un spectacle sans corps.

 

II. LIVRE I, CHAPITRE 10.

 

[Description : File0014] 

 

(a) S'il est quelque autre don que les dieux aient fait aux hommes, il
s'ensuit que le bonheur aussi est un présent divin.

 

20

 

III. LIVRE I, CHAPITRE 12.

 

[Description : File0015] 

 

IV. LIVRE II, CHAPITRE 9.

 

[Description : File0015] 

 

V. LIVRE IV, CHAPITRE 5.

 

[Description : File0015]

VI. livre IV, chapitre 7.

 

[Description : File0015]

 

(a) Il demande si l'on doit placer le bonheur parmi les choses louables
ou parmi les choses estimables, et pose en principe qu'on loue les
choses relativement à d'autres. Cela étant, il est clair que les choses
les plus excellentes ne sont pas l'objet de la louange, mais quelque
chose de meilleur et de plus élevé. Car nous disons les dieux heureux,
au comble de la félicité... Et personne ne loue le bonheur comme la
justice, mais on l'élève comme quelque chose de plus divin et de
meilleur. C'est pourquoi Eudoxe semble avoir parfaitement montré
l'excellence du plaisir ; car de ce qu'il n'est pas loué, bien qu'il se
trouve au nombre des biens, il en conclut qu'il les surpasse tous, et
c'est ainsi qu'il en est de Dieu et du souverain bien.

(b) Celui qui veut atteindre le milieu, doit avant tout s'écarter des
contraires car un extrême pèche par excès, et l'autre par défaut. — Ce
principe est le fondement de toute la morale de saint Thomas.

(c) Ce n'est pas pour lui que l'homme magnifique est prodigue, mais pour
la chose publique. — Il est, dit Bossuet, « comme une fontaine publique
qu'on élève pour la répandre. » (_Oraison funèbre de Louis de Bourbon_.)

 

27

 

[Description : File0015] 

 

VII. LIVRE IV, CHAPITRE 8.

 

[Description : File0015]

 

(a) De la grandeur d'âme. — ... S'il se croit digne, l'étant en effet,
de grandes choses et surtout des plus grandes, il n'aura plus qu'une
pensée. .. c'est que l'homme magnanime ne fait injure à personne. Car
pourquoi commettrait-il des actions honteuses, lui pour qui rien n'est
grand ? Et si l'on considère attentivement les choses, ne serait-il pas
complètement ridicule de paraître magnanime sans être homme de bien ?

(b) C'est pourquoi l'homme qui a de la grandeur d'âme semble dédaigneux.

Il n'a pas de petites intrigues et ne court point les hasards, parce
qu'il n'estime que peu de chose.

On voit qu'il se souvient de ceux à qui il a rendu des bienfaits ; niais
de ceux qui lui en ont rendu, non: car l'obligé est au-dessous du
bienfaiteur, et il veut avoir le dessus. On remarque en lui de la
nonchalance et de la lenteur, à moins que l'honneur ne le réclame ou une
grande action ; il agit peu, mais il fait des choses grandes et qui
donnent de la renommée.

Il prend plus souci de la vérité que de l'opinion ; il agit et parle
ouvertement, parce qu'il est fier.

Il oublie les injures ; car il n'est pas d'un grand cœur de se souvenir
même du mal qu'on lui a fait.

Il n'est point admirateur, parce qu'il n'y a rien de grand pour lui.

Il préfère les choses belles, alors même qu'elles sont stériles, aux
choses utiles et profitables, car il se suffit à lui-même. Enfin sa
démarche et ses mouvements n'ont rien de précipité.

 

28

 

VIII. LIVRE V, CHAPITRE 8.

 

Gratiarum templum in propatulo orbis loco collocari solet, ut
remuneratio commendetur.

 

IX. livre VI, chapitre 14.

 

[Description : File0016]

X. livre VII, chapitre 1.

 

[Description : File0016]

 

XI. livre VIII, chapitre 14.

 

[Description : File0016] 

 

XII. livre IX, chapitre 12.

 

[Description : File0016]

 

(a) Tout a par la nature quelque chose de divin.

(o) Les hommes s'aimant, la justice n'est pas nécessaire ; mais les
hommes étant justes, il faut l'amitié.

(c) Les hommes vivent en société, non-seulement pour la procréation des
enfants, mais encore pour les choses de la vie. Car aussitôt les offices
leur sont distribués, et ceux du mari sont autres que ceux de la
femme... Les enfants sont d'ailleurs un hen d'union, et voilà pourquoi
ceux qui n'en ont pas se séparent aisément ; car les enfants sont un bien
commun entre les époux, et ce qui est commun réunit.

(d) Ce qu'il y a de plus désirable pour les amis, c'est de vivre
ensemble ; car les affections que l'on a pour soi-même, on les a pour
son ami ; et c'est une chose agréable de sentir qu'on est.

 

29

 

XIII. LIVRE X, CHAPITRE 7.

 

[Description : File0016]

XIV. DE VIRTUTIBUS ET VITIIS, CAPUT ULTIMUM.

 

[Description : File0016]

XV. LIVRE X, CHAPITRE 9.

 

[Description : File0016]

(a) Cette vie (bienheureuse) surpassera la condition humaine : car
l'homme ne vivra pas comme tel, mais ce qu'il y a de divin en lui
dominera. Malgré donc les conseils qu'on nous donne, gardons-nous, tout
hommes et tout mortels que noirs sommes, nourrir des pensées humaines et
mortelles ; mais tâchons de nous détacher autant que possible de la
mortalité, et faisons toutes choses pour vivre conformément à la partie
supérieure de nous-mêmes.

(b) Il est aussi de l'homme vertueux de faire du bien à ceux qui le
méritent et d'aimer les gens de bien ; de n'être ni rancuneux ni
vindicatif, mais miséricordieux, clément et prêt à pardonner. La vertu a
pour compagnes la probité, la droiture, la franchise et l'espérance.

(c) Il est probable que le sage est chéri de la divinité (et heureux).
Car si les dieux s'occupent, selon toute apparence (*), des affaires
humaines, comme il est

 

(*) Les expressions: _Il semble, il paraît, selon les apparences,_ etc.,
ne sont point dubitatives dans le chef des péripatéticiens, non plus que
dans le prince de l'Ecole. Aristote ne pouvait

 

30

 

A MONSEIGNEUR LE DAUPHIN.

 

XVI. livre VII, chapitre 15.

 

[Description : File0017]

XVII. livre VII, chapitre 11.

 

[Description : File0017] 

 

Itaque animal multo labore onustum ; laborat enim omnium sensuum
exercens facultates, quanquàm assuetudo vetat quominùs id sentiat, in
voluptatem tanquàm in quietem ac relaxationem suspirat (b).

 

raisonnable de croire qu'ils mettent leur complaisance dans les choses
les meilleures et les plus semblables à eux, telles que l'esprit, ils
récompensent ceux qui l'honorent et le cultivent, comme s'occupant de ce
qui leur est cher et se livrant à des exercices nobles et élevés. Or
toutes ces occupations, qui ne le voit? se trouvent surtout chez le
sage ; il est donc clair qu'il jouit de l'amitié divine tout ensemble et
du bonheur. Le sage donc, sous ce rapport encore, est très-heureux.

(a) La même chose ne nous est pas toujours agréable, parce que notre
nature n'est pas simple ; mais qu'elle renferme une diversité qui la
soumet à la corruption... Celui dont la nature est simple trouve
toujours de l'agrément dans la même action ; voilà pourquoi Dieu jouit
toujours d'un plaisir un et simple. Et l'action consiste non-seulement
dans le mouvement, mais aussi dans l'absence du mouvement ; et le plaisir
se trouve plus dans le repos que dans le mouvement. Si le changement de
toutes choses a le plus grand attrait pour l'homme, cela vient d'un
désordre et d'un défaut ; et comme l'homme est changeant par dépravation
et par défaut, ainsi la nature est mobile par dérèglement ; car elle
manque de droiture par cela qu'elle manque de simplicité..

(b) Ceux qui n'ont pas d'autre soulagement recherchent, parce qu'elles
sont violentes, les voluptés du corps ; ils allument par là dans leur
sein une soif qui

 

heurter de front les croyances de son époque  et le ton tranchant ne
convient pas au vrai philosophe. Le Stagyrite ne met pas en doute, quoi
qu'un en dise, le dogme de la Providence.

 

31

 

[Description : File0017]

 

XVIII. AD EUDEMUM, LIVRE IV, CHAPITRE 8.

 

La société consiste dans les services mutuels que se rendent les
particuliers. C'est pourquoi elle se lie par la communication et
permutation. Et tout cela est né du besoin, parce qu'il n'est pas
possible qu'un seul homme puisse suffire à tout. Ainsi la société
demande la diversité des ouvrages. Car s'il n'y en avait que d'une
sorte, chacun serait suffisant à lui-même. De là vient que deux médecins
ne composeraient jamais une société, mais le médecin par exemple et le
laboureur. Ils se donnent donc l'un à l'autre les choses dont ils ont
besoin. Mais d'autant qu'il y en a dont l'ouvrage vaut mieux que celui
des autres, afin d'obliger le meilleur à donner au moindre, i! a fallu
faire une mesure commune,

 

les dévore, car ils n'ont pas d'autres jouissances. Car l'animal est
toujours dans un travail fatigant, comme l'attestent les écrits sur la
physique ; ils disent que voir et entendre sont des actes pénibles, mais
que l'habitude en cache la peine. Ainsi l'animal accablé de fonctions
laborieuses, car il travaille en exerçant les facultés de tous ses sens,
encore que l'accoutumance l'empêche de le sentir, cherche dans le
plaisir le relâche et le repos.

(a) Semblablement dans l'adolescence, les hommes, pour favoriser
l'accroissement, sont comme endormis dans le vin, et le jeune âge leur
est une douce chose. Ceux qui sont d'un naturel mélancolique ont
toujours besoin de remède ; car leur corps est rongé par la tristesse de
leur tempérament et se trouve continuellement dans une excitation
violente. Or les plaisirs sont comme un remède qui calme un peu les
douleurs, et la tristesse sombre et les mouvements impétueux de cette
noire humeur.

(b) En voyant ainsi paraître comme vraies les choses qui ne le sont pas,
on découvre une nouvelle raison de croire le vrai ; et l'on comprend
pourquoi les plus violents plaisirs du corps sont avant tout recherchés
comme des remèdes à la douleur, pour en tempérer les atteintes.

 

32

 

et cela les hommes l'ont fait par l'estimation. Or, afin que cela fût
plus commode, d'autant qu'il semblait extrêmement difficile d'égaler des
choses de si différente nature, comme une maison et du blé, on a
introduit l'usage de l'argent. Je vous donne mon blé par exemple, mais
j'aurai besoin d'un logement dans quelque temps : je fais un échange
avec Paul, afin de me loger. Mais Paul n'a pas de quoi m'accommoder ; il
substitue de l'argent en la place du logement que je lui demande, ainsi
l'argent m'est comme caution que je pourrai avoir une maison quand la
nécessité me pressera. Sans quoi il est évident que je ne livrerais pas
mon blé, que je n'eusse la maison en mes mains. C'est pourquoi Aristote
appelle l'argent, _nummus sponsor,_ _tò nomisma oion enguetes estin
emon_. L'argent n'est pas une chose que la nature désire pour lui-même.
Car les métaux par eux-mêmes n'ont aucun usage utile au service de
l'homme. Aussi dans l'origine des choses, les richesses consistaient
dans la possession des biens dont la nature avait besoin et dont le
désir nous est naturel, tels qu'on les trouve dans le vin, dans les
troupeaux. Nous le voyons dans les patriarches. Que si l'argent ne nous
est nécessaire que comme substitué en la place de ces choses, le désir
n'en doit pas être plus grand qu'il ne serait de ces choses-là mêmes. Le
désir maintenant va à proportion du besoin. Or les bornes du besoin sont
étroites. La nature est sobre et se contente de peu. Mais la cupidité
est venue, qui ne s'est plus voulu contenter du nécessaire, mais par le
désir du commode, du plaisant, du bienséant, est montée au délicieux, au
mal, au superflu, au somptueux. Nous nous sommes fait certaines idées
d'une bienséance incommode ; d'où il est arrivé qu'un homme peut être
pauvre, et néanmoins ne manquer de rien de ce que la nature désire ; et
cela est absolument ne manquer de rien, parce qu'il faut contenter la
nature, non l'opinion. La pauvreté n'est plus opposée à la nécessité,
mais au luxe ; et ainsi ce que dit Aristote se vérifie en cette
rencontre : _dipsas tinas paraskeuadzotsi._

 

[Précédente]

[Accueil]

[Suivante]

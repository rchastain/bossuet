[LETTRES CCXXIX-CCXLIII]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Mélanges I
Mélanges II
Mélanges III
Mélanges IV
Lettres I-XL
Lettres XLI-LXIII
Lettres LXIV-XCIII
Lettres XCIV-CXXIX
Lettres CXXX-CLXXI
Lettres CLXXII-CLXXX
Lettres CLXXXI-CLXXXII
Lettres CLXXXIII-CXCIII
Lettres CXCIV-CCX
Lettres CCXI-CCXVII
Lettres CCXVIII-CCXXXVIII
Lettres CCXXIX-CCXLIII
Lettres CCXLIV-CCXLIX


 


LETTRE CCXXIX. MILORD PERTH A BOSSUET. A Rome, ce 14 novembre 1695.

 

Je prends la liberté de vous présenter le gentilhomme qui aura l'honneur
de vous porter cette lettre, M. de Menize, un de mes amis, qui ne m'a
jamais abandonné, et qui a toujours adhéré au roi par principe d'honneur
et de justice. Je serais très-aise d'y ajouter de religion aussi ; mais
c'est de vous, Monseigneur, que j'espère que, Dieu se servira pour lui
donner des principes si au-dessus de la raison humaine. Pour ce qui est
des raisonnements sur les matières qui touchent les affaires de ce
monde, vous le trouverez, comme je l'espère, au moins en quelque façon
digne de votre illustre protection : et j'espère que si vous voulez
avoir la bonté pour moi de discourir avec lui sur la religion catholique
et même la chrétienne, car j'ai peur qu'il ne soit pas trop persuadé de
ce premier principe, il en sera convaincu, et se rendra avec gloire au
plus habile et plus digne prélat qui soit sur la terre.

Pour moi, Monseigneur, c'est à vous que je dois mes espérances après
Dieu : et si par mon expérience je vous adresse un autre malheureux
comme j'étais, c'est par charité pour lui, et pour donner aussi à mon
illustre Père spirituel l'occasion d'exercer sa charité. Et je prie le
Seigneur, qui est la charité essentielle, de vouloir bénir ce dessein,
afin que ce gentilhomme, qui m'est fort cher, puisse participer au
bonheur dont je jouis par la grâce de

 

(a) Préjugé contraire à la conviction des hommes qui connaissent à fond,
non superficiellement, la Ville des Pontifes, des martyrs et des saints.

 

507

 

Dieu, en espérant de parvenir dans le ciel à la joie et à la
tranquillité dont je suis si injustement privé en ce monde par les
ennemis du plus saint roi qui soit sur la terre. J'espère, Monseigneur,
que vous pardonnerez ma présomption, et que vous continuerez de
m'honorer de votre bienveillance, comme étant, etc.

Je vous supplie de m'accorder votre sainte bénédiction paternelle et
épiscopale.

 


LETTRE CCXXX. M. DE MENIZE, GENTILHOMME ECOSSAIS. AMI DE MILORD PERTH, A BOSSUET.

 

La lettre que je prends la liberté de vous envoyer est d'un de vos
admirateurs et mon cher patron, le comte de Perth, milord chancelier
d'Ecosse. C'est, Monseigneur, une des plus grandes marques de son amour
et de l'amitié dont il m'a toujours honoré, que de me vouloir présenter
à une personne que tout le monde admire, et qui semble être faite tout
exprès pour honorer notre siècle.

Une indisposition m'a fait garder la lettre quelques jours, et m'a
empêché d'avoir l'honneur de la porter moi-même. Mais pour vous dire
franchement la vérité, Monseigneur, je n'osais pas me produire à un si
grand jour, et je n'ose pas encore, sans vous demander pardon par avance
de vous présenter une personne si indigne de votre connaissance, qui ne
sait pas encore parler de suite six mots de français, et encore moins de
bon sens, et qui ne vous apportera rien que des occasions d'exercer
votre patience et votre humilité. Tout le mérite que je pourrais avoir
auprès de vous, Monseigneur, c'est d'admirer de plus près cette profonde
érudition, cette candeur, cette justesse de pensées, et toutes ces
grandes qualités qui vous ont tant fait renommer dans la république des
lettres et dans toutes les religions. A la vérité, Monseigneur, j'ai
commencé de vous admirer au même âge que j'ai commencé de déjuger ; car
quelque scythique que soit notre pays, votre réputation et vos écrits se
trouvent en grande vénération

 

508

 

dans les montagnes et les neiges de cette _ultima tellus_ ; et mon cher
ami et patron est témoin que vous avez poussé vos victoires où les
Romains mêmes autrefois ne pouvaient pas porter leurs armes.

Je vous demande mille pardons, Monseigneur, pour la liberté que j'ai
prise de vous écrire : sitôt que ma santé me permettra, je viendrai pour
avoir l'honneur de vous voir, et pour être _auditor tantùm_ : c'est par
nécessité, parce, que je ne saurais pas parler. J'ai honte de n'avoir
pas encore appris le français : et les agitations continuelles de cette
violente usurpation m'ont fait oublier le peu que je savais de quelque
chose que ce soit, aussi bien que mon latin. Mais vous, Monseigneur, que
le Dieu de la nature a fait d'un limon bien différent de celui du reste
des hommes, vous aurez, s'il vous plaît, la bonté de m'excuser, et de
pardonner la faiblesse en considération du respect et de la vénération
avec laquelle je vous assure, en fort mauvais français, mais de fort bon
cœur, que je suis, etc.

 

DE MENIZE.

 

 


EPISTOLA CCXXXI. BOSSUETUS AD CARDINALEM DE AGUIRRE. Parisiis, 13 martii 1696.

 

Nihil mihi unquàm fuit optatius, eminentissime Cardinalis, quàm ut in
urbem profecturus fratris mei filius tuo conspectu frueretur, meque et
se totum tuum in sinum effunderet ; sin, quod nolim, abes, quoad fieri
poterit, quocumque loco versabere, votis saltem ac desideriis
sequeretur. Te enim, eminentissime Cardinalis, ut Ecclesiae lumen,
morumque ac pietatis exemplar in pectore gerere, in ore habere non
cesso, summoque te honore, ac, si liberae vocis simplicitatem admittis,
amore prosequi certum quoad vita supererit. Quarè etiam atque rogo, ut
etiam me tibi addictissimum solità benevolentiâ cohonestatum velis.
Vale.

 

 

509

 


EPISTOLA CCXXXII. CARDINALIS DE AGUIRRE AD BOSSUETUM. Romae, die 10 julii 1690.

 

Pergratum mihi fuit, illustrissime Praesul, legere litteras tuas amoris
et honoris plenas, quas exhibuit domnus nepos tuus ex fratre, semel et
iterùm à me admissus libenter admodùm, et cum eo affectu quo par erat.
lntereà prosequebar lectionem aurei tui libri, quo gallicè tueris
_Historiam Variationum_  jam antè editam adversùs heterodoxos quosdam et
praesertim Jurieum. Has lucubrationes tuas, et quasdam alias ejusdem
ferè argumenti, legere, aut saltem audire mihi jam pridem in deliciis
fuit. Gaudeo enim, non solùm olim, sed etiam modo gravissimam Ecclesiam
Gallicanam tam insignes praesules simul et scriptores habere, qui fidem
catholicam adeò fortiter et eruditè tueantur adversùs quaslibet
novatorum calumnias, imò et deliramenta.

Prœtereà mihi admodùm placet tam in scriptis tuis, quàm in aliis
recentibus modernorum Galliae praesulum ac doctorum legere plura ad
disciplinam ecclesiasticam, et doctrinam morum tutiorem spectantia, quae
quotidièm omnibus ferè regnis et nationibus magis ac magis vigent inter
scriptores magni nominis. Oportet certè in hàc parte exerere ampliùs et
uberiùs sacrae eruditionis vires, quibus abundas, simul cum
illustrissimis antistitibus Rhemensi et Abrincensi, quos jam diù impensè
diligo ac veneror. Idipsum spero de illustrissimis prasulibus Parisiensi
(a) et Aure-Banensi (b), dudùm mihi ex famà et communi aestimatione
notis, quosvelim, si occasio fuerit, salutes nomine meo.

lntereà jam à multis mensibus, Deo favente, fïuor et frui spero optatà
salute, cujus defectum prostremis hisce annis passus fui Romae ac
Neapoli. Epilepsia illa, quà excruciabar interdùm cum magno vitae
discrimine, cessavit jam à multis mensibus, et censetur, juxta
dispositionem prœsentem quotidiè ampliùs confirmatam, minime reditura.
Vale, Praesul doctissime ac piissime, meque

 

(a) Ludovicus Antonius de Noailles. — (b) Cœsar du Camboust de Coislin.

 

510

 

inter veneratores tuos et amicos recense, et in sacrificiis ac
orationibus tuis ac tuorum mei apud Deum memento.

 


LETTRE CCXXXIII. BOSSUET A M. L'ABBÉ RENAUDOT. A Meaux, ce 23 juin 1898.

 

Je vous rends grâces, Monsieur, de la copie des sentences des
Inquisitions (_a_). Le dépôt de la foi est-il pas bien en de telles
mains (b) ? Dieu veillera sur son Église, qui a bien besoin de ses
bontés. C'est encore une autre merveille que l'empereur ne trouve rien à
dire à ces censures, sinon qu'elles sont contre les jésuites.
Mandez-moi, Monsieur, je vous prie, à votre loisir, comment notre ami
est content de la Trappe. Je suis à vous, Monsieur, comme vous savez.

 


LETTRE CCXXXIV. BOSSUET A M. PASTEL, DOCTEUR DE SORBONNE. A Meaux, ce 3 août 1696.

 

J'ai reçu, Monsieur, avec une sincère reconnaissance le témoignage de
l'amitié de votre famille, dans votre lettre qui m'a été rendue par M.
votre frère (_c_). Il continue toujours à honorer son ministère, et
c'est l'exemple de notre église.

 

(a) L'année précédente 1693, le 17 septembre, la congrégation de
l'Inquisition avait condamné un livre de M. Baillet, de la Dévotion à la
sainte Vierge, et du culte gui lui est dû ; imprimé à Paris, eu 1693.
Elle proscrivit par le même jugement l'Année chrétienne de M. le
Tourneux. L'Inquisition d'Espagne rendit la même année, le 14 novembre,
un décret contre les dictes des Saints de Bollandus, des mois de mars et
d'avril, publiés par les jésuites d'Anvers. Le motif de la censure était
qu'ils révoquaient en doute les visions et révélations de Simon Stock,
grand promoteur de la confrérie du Scapulaire de la sainte Vierge.
L'empereur Léopold écrivit au Roi Catholique pour se plaindre de cette
censure, précisément, comme le dit Bossuet, parce qu'elle était contre
les jésuites ; mais la défense qui excluait leur ouvrage d'Espagne, ne
fut levée qu'en 1715. (Les édit.) — (_b_) Serait-il plus en sûreté dans
les mains d'un docteur particulier que sous la garde d'un corps chargé
par l'Église universelle de veiller au maintien de la vraie doctrine?—
(_c_) Chanoine de Meaux, et grand vicaire du prélat.

 

511

 

Il est vrai que le malheureux Faydit, après avoir si longtemps souillé
sa plume impie et licencieuse dans toutes sortes d'emportements et
d'erreurs, s'est fait prendre enfin pour avoir osé publier un livre
abominable sur la Trinité (_a_), où il a poussé le blasphème jusqu'à
dire qu'il y a trois dieux. J'ai ce livre, et il ne faut pas vous
fatiguer à m'en envoyer des extraits : il est monstrueux en toutes ses
parties. On a vu que, pour le bien de l'auteur et pour celui de toute
l'Église, il était bon de l'enfermer ; et M. de Paris a remis entre les
mains de Desgrets un ordre du roi pour le mettre à Saint-Lazare. M. de
La Reynie l'avait déjà fait arrêter, l'ayant trouvé débitant lui-même
ses ouvrages. Il serait digne sans doute d'un plus rigoureux châtiment,
s'il n'y avait autant de folie que d'erreur et d'impiété dans ses
écrits. Je suis avec l'estime que vous savez, etc.

 


LETTRE CCXXXV. BOSSUET A M. PAÏEN, LIEUTENANT GÉNÉRAL. PRÉSIDENT AU PRÉSIDIAL DE MEAUX. A Germigny, au mois d'août 1696.

 

M. de Thémines vient de me mander, Monsieur, qu'il acceptait la
proposition. J'en suis très-aise pour le bien de la paix, et afin que
tout le monde concoure à la splendeur et à l'unité du culte de Dieu. Il
ne faut pas que, M. le Prévôt trouble notre concert. Il a donné sa
parole : la considération de ses officiers ne doit plus le peiner,
puisque les principaux ont leur place plus honorable dans le présidial,
et que les autres dans une occasion de concert public ne sont nullement
à considérer. C'est l'ordre de M. le chancelier, de M. de Pontchartrain
et de M. l'intendant. J'ai tout concerté avec eux, et ne prendrais pas
plaisir de me voir dédit : cela aussi bien serait inutile. Il est bon,
Monsieur, et je vous en prie, de faire parler

 

(a) Cet ouvrage a pour titre : _Fausses Idées des Scholastiques sur
toutes les matières de la Théologie_. Le P. Hugo, chanoine régulier de
l'ordre des Prémontrés, le réfuta ; et Faydit, après sa sortie de
Saint-Lazare, lui répliqua par un écrit qui parut eu 1704, et dans
lequel il adoucit les propositions qui avaient révolte dans son premier
ouvrage.

 

512

 

à M. Le Prévôt. Je lui parlerai après, et ce sera d'une manière à lui
faire voir qu'il ne doit ni ne peut nous troubler. Après tout il ne
s'agit que d'une provision et pour un seul jour. L'intention du roi est
que tous les corps honorent la sainte Vierge protectrice de son royaume,
qui vient de lui obtenir de si grandes grâces. On trouverait
très-mauvais que le concours manquât ; et celui par qui il serait rompu
avant, à en rendre raison, je puis assurer qu'il n'en rendra jamais une
qui soit agréable. Je serai mardi de bonne heure à Meaux (_a_), et en
état, s'il plaît à Dieu, de tout terminer d'un commun consentement. Je
suis avec L'estime que vous savez, Monsieur, très-parfaitement à vous.

 


LETTRE CCXXXVI. BOSSUET A M. L'ABBÉ RENAUDOT. 1696.

 

C'est vous, Monsieur, qui m'avez donné l'agréable avis de milord grand
chancelier d'Ecosse. Depuis ce temps-là nous nous cherchons l'un l'autre
avec un égal empressement. J'ai été à Saint-Germain ; j'ai été en un
autre lieu où l'on m'avait assuré qu'il était ; j'ai été au collège des
Ecossais, où l'on m'avait dit qu'il devait dîner. Joignez-nous.
Monsieur, je vous en supplie, dès aujourd'hui, s'il se peut: j'attendrai
ici vos ordres toute la journée. Vous savez ce que je vous suis.

(a) Le prélat se rendit en effet à Meaux, avant la fête ; et parvint
tellement à concilier les esprits, que d'un commun accord on dressa la
veille de la Notre-Dame d'août un acte sous seing privé, dont la minute
fut déposée entre mains, et par lequel on détermina provisionnellement
le rang que chacun devait occuper tant à la procession qu'aux autres
cérémonies publiques. (Les édit.)

 

513

 


LETTRE CCXXXVII. BOSSUET A MILORD PERTH. A Meaux, ce 16 août 1696.

 

Ce n'est pas avec vous, Milord ; c'est avec Leurs Majestés Britanniques
et avec Monseigneur le prince de Galles (_a_) qu'il se faut réjouir de
ce que vous êtes choisi pour son gouverneur. Dieu vous préparait à cette
grande charge par les souffrances qui vous ont rendu en quelque façon le
martyr de la religion et de la royauté, où Dieu veut que Sa Majesté
reluise. Conservez donc à l'Église, Milord, ce grand et précieux dépôt ;
et gardez en la personne de ce jeune prince un instrument dont je crois
que Dieu se veut servir pour l'exécution de ses grands desseins. Il
fallait un homme comme vous pour les seconder. J'aurai bientôt l'honneur
de vous embrasser, et je suis avec un respect sincère, etc.

 


LETTRE CCXXXVIII. BOSSUET A M. DE NOAILLES, ARCHEVÊQUE DE PARIS (6). A Meaux, 26 décembre 1695.

 

Quoique vous sachiez, Monsieur, l'intérêt sincère que je prends en ce
qui regarde votre famille, je me fais un trop grand plaisir de vous le
dire pour être capable d'y manquer. Je suis très-aise de voir un saint
succéder à un saint, et s'il est permis de le regarder un peu, un ami
qui m'est très-cher à un autre qui me l'est au dernier point. Je suis de
tout mon cœur, Monsieur, votre très-humble et très-obéissant serviteur.

 

J. Bénigne, év. de Meaux.

 

(a) Fils de Jacques II, connu depuis en France sous le nom de chevalier
de Saint-Georges. Il se retira dans la suite à Rome, où il fut reconnu
roi d'Angleterre.

(b) Cette lettre et celles qui vont suivre, à M. de Noailles archevêque
de Paris, sont médites. Les autographes de Bossuet se trouvent à la
bibliothèque du Louvre, Manuscrits Noailles, vol. VI et IX.

 

514

 


LETTRE CCXXXIX. BOSSUET A M. DE NOAILLES, ARCHEVÊQUE DE PARIS. A Meaux, 1er novembre 1696.

 

Mon neveu est très-éloigné, mon cher Seigneur, de parler mollement à
Rome sur votre ordonnance. Pour moi, je me suis trop expliqué par les
lettres que j'ai écrites en ce pays-là pour y laisser aucun doute de mon
sentiment. Après tout il nous revient de tous côtés que Rome n'a plus
besoin d'être excitée. Je ne sais si le cardinal Noris fera encore
longtemps le mystérieux, mais enfin le torrent l'emporte. L'oserais-je
dire? Vous donnez à l'Église de France l'avantage d'avoir à cette fois
instruit sa mère l'Église romaine, et peut-être que vous avez sans rien
hasarder les approbations de ce côté-là.

Je ne suis plus du tout en peine de rien sur le sujet de M. ***, après
ce que vous m'en écrivez. Il faut toujours dire ce qu'on pense à ses
amis et après se reposer, quand ce sont des amis comme vous, sur leur
prudence et leurs saintes intentions.

Je vais demain à la Fortelles-les-Rosoy, d'où le trajet est si petit
pour Fontainebleau, que je compte d'y être le 3. Si vous avez, mon cher
Seigneur, quelque ordre à m'y donner, vous savez mon obéissance.

 

J. Bénigne, év. de Meaux.

 

Je viens de recevoir une lettre de M. de Mirepoix, où il est comme nous
tous en admiration sans réserve de l'ordonnance ; mais je vois qu'il
n'avait point reçu le paquet où je la lui avais envoyée de votre part,
quoique je l'eusse confiée en mains qui paraissaient sûres.

 

515

 


LETTRE CCXL. BOSSUET A M. DE NOAILLES, ARCHEVÊQUE DE PARIS.  Dimanche.

 

Je vous renvoie, mon cher Seigneur, l'écrit que vous m'avez confié :
plus tard que je ne vous l'avais promis ; mais assez tôt, puisqu'on a
ordre de vous le porter à Conflans. Avec deux heures de réflexion, je me
mettrai en état d'y dire ce qu'il faut, s'il plaît à Dieu. Tout consiste
maintenant à la diligence. Je serai prêt à tout moment. Donnez ordre, je
vous en conjure, que tous ces Messieurs se trouvent avec nous. C'est à
la conclusion qu'on a besoin de ramasser tout le bon conseil. Je prie
Dieu qu'il nous inspire une paix qui ne blesse ni n'affaiblisse la
vérité.

 


LETTRE CCXLI. BOSSUET A M. DE NOAILLES, ARCHEVÊQUE DE PARIS. A Meaux, 15 août 1697.

 

Voilà, mon cher Seigneur, les deux articles à considérer dans la
quatrième thèse qui est celle du 1er août. Le 38^(e) article a ces mots
: _Amor Dei propter se, sine ullo ad nos respectu, et possibile est et
prœcipitur_.

Dans la même thèse, article 40, est aussi la suffisance de l'attrition.
Dans toutes les quatre thèses on affecte le _sine ullo respectu_ ; il est
dans celle du 26 juin, article 21 ; dans celle du 3 juillet, article 12 ;
et dans celle du 16 juillet, article 23.

Dans la thèse du 3 juillet se trouve encore, article 33, la suffisance
de l'attrition, comme dans l'article 40 de la thèse du 1er août.

Dans la même thèse du 3 juillet, article 40, tout à la fin, se trouve en
des termes formels l'infaillibilité du Pape d'une manière aussi odieuse
que dans la censure de Strigonie. _Ecclesia romana sola infallibiliter
definit_  : l'Église romaine ne définit que par son

 

516

 

Pontife ; il s'agit dans tout cet article des prérogatives du Pape ; le
_sola_ est exclusif du Concile et de l'Église catholique.

Tout cela est attentatoire à notre autorité.

Les deux articles où est enseignée la suffisance de l'attrition, sont
directement opposés à la décision de notre grande ordonnance sur la
grâce, où vous mettez expressément l'amour commencé comme nécessaire à
la justification. Je me suis fait relire l'article.

Le _sine ullo respectu ad nos_ dans ce temps est d'une visible
affectation pour favoriser M. de Cambray ; et il ne fallait point
dissimuler que cela s'entend seulement de l'objet spécificatif, sans
exclusion des autres motifs qui ont rapport à nous, et qui dans la
pratique sont absolument nécessaires.

Dans la thèse du 10 juillet, article 23, il est porté formellement :
_Caritas est virtus theologica ex objecto, tàm materiali, tùm formali,
quod utrumque est Dei perfectio quœlibet_. Il y a contradiction que la
charité ait pour objet formel toute perfection de Dieu, et qu'en même
temps elle soit sans rapport à nous, puisqu'il y a des attributs qui
emportent nécessairement ce rapport, comme la bonté et la miséricorde.

Quant à la thèse de l'infaillibilité, surtout d'une manière si odieuse,
elle nous attaque avec toute l'Église de France, et même aux termes où
elle est couchée, avec la plupart des auteurs même ultramontains, le
_sola_ n'étant approuvé que d'un petit nombre.

Ainsi il me paraît évident que vous pouvez user de votre autorité pour
faire supprimer ces thèses, et pour établir votre droit de faire
examiner toutes les thèses des religieux (a) en sorte qu'il y ait
quelqu'un qui en réponde au roi, au public et à vous.

C'est le seul moyen d'empêcher les divisions que ces thèses feront
naître infailliblement avec le Pape ; il croira que c'est lui faire une
querelle que de supprimer ces thèses, où on mettra son infaillibilité :
il faudra donc demeurer exposé à les laisser passer, ou établir un moyen
pour les prévenir en les assujettissant à votre examen, qui après tout
est de droit commun, puisque vous êtes

 

(a) Les thèses dont Bossuet vient de parler, étaient avancées par les
jésuites.

 

517

 

naturellement le juge de la doctrine. La conjoncture est heureuse pour
établir votre autorité.

Souvenez-vous, je vous en supplie, du P. Augustin, barnabite, qui
n'attend que son rétablissement pour s'aller jeter à la Trappe. Je vous
en parle pourtant, mon cher Seigneur, sans rien savoir et en supposant
que les raisons de lui retirer les pouvoirs ne peuvent être que bonnes.
Tout à vous avec respect...

 

 


EPISTOLA CCXLII. BOSSUETUS AD CARDINALEM NORIS. 3 Septembris 1696.

 

Redit ad te nepos meus, eminentissime Cardinalis, non jam à me, sed ab
illustrissimo archiepiscopo Parisiensi, amico meo singulari jussus, qui
in doctas manus tuas ejusdem praesulis Constitutionem (_a_) deferat, te
sanè dignissimam. Et ille quidem christianam commendat gratiam ; tu
ejusdem gratiae defensor intrepidus, nomen tuum posteris commendasti.
Ille Augustinum meritis extollit laudibus, tu parentem tuum ab
adversariorum intemperiis pari facundiâ ac doctrinae gloriâ vindicasti ;
ejus discipulos ac fortissimos gratiae defensores, Joannem Maxentium
(_b_) sociosque ab Eutychianismi labe purgatos, orbi christiano puros et
integros reddidisti. Quid verò est postremà Apologiâ tuâ, quam tuo
munere accepi ;quid, inquam, est, eminentissime Cardinalis, et elegantiâ
jucundius, et eruditione praestantius, et omni litterarum genere
ornatius? Quidquid ex antiquâ historiâ tangis, mirum in modum illustras.
Patribus inserendus, Patrum locos excutis reconditissimos ; omnia
circumspicis, retegis, ornas, lectoremque tuî cupientissimum facis.
Tuere, doctissime Cardinalis, episcopos Gallicanos pro verâ Augustini
theologiâ, pro morali disciplinâ, pro antiquitatis honore tuis jam
auspiciis acriter certaturos ; meque tuâ benevolentiâ honestatum velis,
Eminentiae tuae addictissimum et obsequentissimum.

 

(a) Agitur de constitutione editâ occasione libri cui titulus :
_Problème ecclésiastique_. (b) De Maxentio, Scythiœ monacho.

 

518

 


EPISTOLA CCXLIII. BOSSUETUS ABBATI GRAVINAE (a).  In Germiniaco nostro, XIV kal. decemb. 1696.

 

Accepi, mi illustrissime, litteras tuas humanitatis officiique plenas ;
tantae verò venustatis, ut statim persentiscerem Tullianae eloquentiae
gustum. Itaque arripui libellum, quo me munere cumulatum voluisti :
nihil aut sermone elegantius, aut  sententiarum gravitate majus ac
sapientius visum est, seu Juris scrutaris origines, seu luctui modum
ponis (b), seu latinae linguae fontes reseras, et Graecorum opibus nos
ditas. Caetera omnia, paris licet eloquentiae, commemorare non vacat.
Nec desunt vernaculi sermonis gratiae, quibus si Apocalyptica nostra vel
pondus accesserit, tuas inter manus, quidquid contigerint continuò
exsplendescet. Rem sanè non indignam ingenio tuo et eloquentiâ, ut Romam
christianam, et Ecclesiae caput ab impiorum calumniis vindicandum putes
ex ipsâ historiae fide, et certis verbi divini testimoniis. Quà de re
tibi gratias refero, quantas possum maximas : nec minores quôd abbatem
Bossuetum tantâ benevolentiâ prosequare. Phelipucium verò nostrum, tuî
assiduum laudatorem, etiam atque etiam tibi commendatum volo. Me verò,
mi illustrissime, scito perpetuum, quâcumque ratione licuerit, studiorum
tuorum fautorem futurum, atque omnia praestiturum quae ab amicissimo
atque addictissimo, tuarumque laudum studiosissimo exspectare possis.
Vale.

 

(a) Joanuus Vincentius Gravina, Rouiae falo functus 6 januarii 1718,
annos natus 54. Inter hujus œvi scriptores claruit ; multaque opera
edidit, quorum praecipua sunt : Origines Juris civilis : De Romano
Imperio liber singularis. (b) Alludit ad epistolam Gravinae de modo
luctui ponendo.

 

[Précédente]

[Accueil]

[Suivante]

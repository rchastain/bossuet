[HENRI DE GORNAY]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Saint André
Saint Jean
S. Th de Cantorbéry
Saint Sulpice
S. François de Sales
S. Pierre de Nolasque
Saint Joseph I
Saint Joseph II
Saint Benoît
S. François de Paule I
S. François de Paule II
Saint Pierre
Saint Paul
Saint Paul précis
Saint Victor
Saint Jacques précis
Saint Bernard
Saint Gorgon I
Saint Gorgon II
Saints Anges Gardiens
Saint François d'Assise
Sainte Thérèse
Sainte Catherine
Henriette de France
Henriette d'Angleterre
Anne d'Autriche
Anne de Gonzague
Michel Le Tellier
Louis de Bourbon
R.P. Bourgoing
Nicolas Cornet
Yolande de Monterby
Henri de Gornay
Académie Française

 



ORAISON FUNÈBRE DE MESSIRE  HENRI  DE GORNAY.


 

Oraison Funèbre


REMARQUES HISTORIQUES.

 

Henri de Gornay n'a pas laissé de traces dans l'histoire du XVIIe siècle
: les éditeurs de son Oraison funèbre n'ont trouvé jusqu'à ce jour aucun
renseignement sur sa vie, et nous n'avons été guère plus heureux dans
nos recherches. Voici tout ce que nous avons pu découvrir.

Henri de Gornay ou de Gournay descendait d'une famille très-ancienne
dans le pays de Metz. Son père fut Henri de Gournay, comte de Talange ;
et sa mère, Madeleine de Gommay.

Il épousa Marie-Agathe Raidsessel de Calembourg. Son fils aine porta le
nom de Jean-Christophe (1).

l'_Oraison funèbre_ contient quelques détails sur sa famille et sur sa
vie ; nous y renvoyons le lecteur.

Ce discours fut prononcé probablement à Metz, vers 1662. Probablement à
Metz, car il semble annoncer un auditoire moins imposant que ceux de la
capitale ; et l'histoire en aurait sans doute conservé le souvenir, s'il
n'avait pas été prononcé dans la province. Vers 1662, comme le révèlent
des indices certains : l'écriture et le papier du manuscrit, les
passages simplement esquissés, le style du discours qui ne renferme pas
de termes surannés. Il est vrai que Bossuet n'habitait pas Metz à cette
époque ; mais l'on sait qu'il y faisait de fréquents voyages pendant les
premières années de son séjour à Paris.

Le discours n'est pas complet, tant s'en faut : les deux derniers points
sont à peine effleurés.

 

1 Bibliothèque imp., Cabinet des titres, Collection des mémoires, Cahier
_Gournay_. Les indications données plus haut se trouvent dans une liste
généalogique, qui établit les seize quartiers de la famille.

 

690

 

Le manuscrit se trouve au collège de Juilly (1). L'imprimé le reproduit
d'une manière fort imparfaite. Nous avons comparé pour ainsi dire sous
les yeux du lecteur, avec les manuscrits originaux, plusieurs passages
du premier volume des sermons ; nous allons lui soumettre, au même point
de vue, quelques pages du dernier, renfermant l'éloge de Henri de
Gornay. Nous passons un grand nombre de fautes pour abréger.

 

 

+-----------------------------------+-----------------------------------+
| _Les éditions_.                   | _Le manuscrit original_.          |
|                                   |                                   |
|                                   |                                   |
|                                   |                                   |
| Il se voit peu d'hommes assez     | Il se voit peu d'hommes assez     |
| insensés pour se consoler de leur | insensés pour se consoler de la   |
| mort par l'espérance d'un superbe | mort par l'espérance d'un superbe |
| tombeau... Tout ce que peuvent    | tombeau... Tout ce que peuvent    |
| faire ces misérables amoureux des | faire ces misérables amoureux des |
| grandeurs humaines, c'est de      | grandeurs humaines, c'est de      |
| goûter tellement la vie, qu'ils   | goûter tellement la vie qu'ils ne |
| ne songent point à la mort. La    | songent point à la mort ; c'est le |
| mort jette divers traits [qui     | seul moyen qui leur reste de      |
| préparent son triomphe. Elle se   | secouer en quelque façon le joug  |
| fait sentir] dans toute la vie    | insupportable de sa tyrannie,     |
| par la crainte, [les maladies,    | lorsqu'en détournant leur esprit, |
| les accidents de toute espèce ;]  | ils n'en sentent pas l'amertume   |
| et son dernier coup est           | (_a_).                            |
| inévitable. Les hommes superbes   |                                   |
| croient faire beaucoup d'éviter   |                                   |
| les autres : c'est le seul moyen  |                                   |
| qui leur reste de secouer, en     | (_a_) _Note marg_. : La mort      |
| quelque façon, le joug            | jette divers traits dans la vie   |
| insupportable de sa tyrannie,     | par la crainte, et le dernier est |
| lorsqu'en détournant leur esprit, | inévitable ; ils croient faire    |
| ils n'en sentent pas l'amertume   | beaucoup d'éviter les autres.     |
| (2).                              |                                   |
|                                   |                                   |
|                                   |                                   |
+-----------------------------------+-----------------------------------+

 

On a sans doute remarqué que les éditeurs n'ont pas renfermé, dans les
crochets sacramentels, tous les commentaires qu'ils prêtent à l'auteur.
Une édition qui se publie dans ce moment fait mieux encore : elle

 

1 M. l'abbé Hautain avait prêché un sermon de charité dans une réunion
de l'Œuvre de Saint-François-Régis ; le président de la société, M.
Gossin, lui envoya le manuscrit du panégyrique funèbre de Henri de
Gornay, avec une lettre datée du 15 mai 1813 ; voici deux passages de
cette lettre : ..... « Il était difficile aussi de faire connaître notre
pauvre Œuvre qui, participant presque à la honte des pécheurs dont elle
excite le repentir et la conversion, a ce désavantage unique et
cependant réel, de n'oser en quelque sorte se nommer, et d'être obligée
de recourir à des périphrases pour faire connaître l'espèce d'infirmité
spirituelle à laquelle elle s'efforce d'apporter remède. Vous avez tout
dit, Monsieur, avec une chasteté de langage égale à la susceptibilité de
nos oreilles parisiennes ; et si vous n'eussiez pas improvisé, nous
aurions demandé la grâce de copier votre discours, afin de montrer aux
prédicateurs que la langue française, dans la bouche d'un orateur zélé
et charitable autant qu'habile, peut plaider la cause des Thaïs et des
Marie Egyptienne en respectant jusqu'au scrupule toutes les bienséances.
Comme nous aimerons toujours, Monsieur, à nous rappeler et votre nom et
le service que vous nous avez rendu, nous souhaitons que, de votre côté,
il reste autour de vous quelque chose qui de temps à autre attire votre
pensée et vos prières sur notre Œuvre. Nous chargeons de ce soin les
quelques pages autographes de Bossuet que nous joignons ici. C'est
l'Oraison funèbre de Henri de Gornay. Un prêtre tel que vous, Monsieur,
ne voudra pas mal accueillir un évêque, et un évêque comme Bossuet.—
Agréez, etc.»— 2 _Edition de Versailles,_ vol. XVII, p. 606 et 607. Les
fautes que nous signalons dans l'édition de Lebel se trouvent dans
toutes les autres, sans en accepter aucune.

 

 

691

 

supprime tous les crochets, si bien que la prose de Déforis se présente
sur la même ligne que la parole de Bossuet.

 

+-----------------------------------+-----------------------------------+
| _Les éditions_.                   | _Le manuscrit original._          |
|                                   |                                   |
|                                   |                                   |
|                                   |                                   |
| Il a plu à notre Sauveur de       | Il a plu à notre Sauveur de       |
| naître d'une race illustre... Et  | naître d'une race illustre...     |
| pour quelle raison, loi qui a     | Pour quelle raison, lui qui a     |
| méprisé toutes les grandeurs      | méprisé toutes les autres         |
| humaines, qui n'a appelé, « ni    | grandeurs humaines? _Non multi    |
| beaucoup de sages, ni beaucoup de | sapientes, non multi nobiles_ ;    |
| nobles ; » _Non multi sapientes,   | Jésus-Christ l'a voulu être.      |
| non multi nobiles_ (1).           |                                   |
|                                   |                                   |
|                                   |                                   |
+-----------------------------------+-----------------------------------+

 

 

 

Les rigides éditeurs voulaient absolument que Jésus-Christ ait méprisé
la noblesse de la naissance.

 

+-----------------------------------+-----------------------------------+
| _Les éditions_.                   | _Le manuscrit original_.          |
|                                   |                                   |
|                                   |                                   |
|                                   |                                   |
| Louer dans un gentilhomme         | Louer en un gentilhomme chrétien  |
| chrétien ce que Jésus-Christ même | ce que Jésus-Christ même a voulu  |
| a voulu avoir, [n'aurait rien ,   | avoir (_a_)... Je ne dirai point  |
| ce semble, que de conforme aux    | ni les grandes charges ...        |
| règles de la foi. Mais cette      |                                   |
| noblesse temporelle est en soi]   |                                   |
| trop peu de chose pour qu'on      |                                   |
| doive s'y arrêter ; c'est un      | (_a_) _Note marg_. : Peu de chose |
| sujet trop profane [ pour mériter | : sujet trop profane. Néanmoins,  |
| les éloges des prédicateurs.]     | d'autant plus volontiers, qu'il y |
| Néanmoins nous louerons ici       | a quelque chose de saint à        |
| d'autant plus volontiers la       | traiter.                          |
| noblesse de la famille du défunt, |                                   |
| qu'il y a quelque chose de saint  |                                   |
| à traiter. Je ne dirai point ni   |                                   |
| les grandes charges (2)...        |                                   |
|                                   |                                   |
|                                   |                                   |
+-----------------------------------+-----------------------------------+

 

Les mots que nous mettons sous la rubrique de note marginale, sont
manifestement des remarques que Bossuet faisait pour lui seul.

 

+-----------------------------------+-----------------------------------+
| _Les éditions_.                   | _Le manuscrit original_.          |
|                                   |                                   |
|                                   |                                   |
|                                   |                                   |
| Le sang qu'a répandu ce généreux  | Le sang qu'a répandu ce généreux  |
| martyr,... vous donne plus de     | martyr,... vous donne plus de     |
| gloire que celle que vous avez    | gloire que celui que vous avez    |
| reçue de tant d'illustres         | reçu de tant d'illustres ancêtres |
| ancêtres. [Vous pouvez dire à     | : « Nous sommes de la race des    |
| juste titre avec Tobie :] « Nous  | saints : » _Filii sanctorum       |
| sommes de la race des saints : »  | sumus_. L'histoire remarque qu'il |
| _Filii sanatorum sumus_.          | était _claris parentibus_... Mais |
| L'histoire remarque que saint     | tous ces titres glorieux ne lui   |
| Livier était issu de parents      | ont jamais donné de l'orgueil. Il |
| illustres : _Claris               | a toujours...                     |
| parentibus_... Mais tous ces      |                                   |
| titres glorieux n'ont jamais      |                                   |
| donné l'orgueil [ au respectable  |                                   |
| défunt que nous regrettons.] Il a |                                   |
| toujours (3)...                   |                                   |
|                                   |                                   |
|                                   |                                   |
+-----------------------------------+-----------------------------------+

 

1 _Edit. de Vers_., vol. XVII, p. 608. — 2 _Ibid.,_ p. 609. — 3 _Ibid.,_
p. 610.

 

692

 

Que dites-vous du _respectable défunt que nous regrettons?_

 

+-----------------------------------+-----------------------------------+
| _Les éditions_.                   | _Le manuscrit original_.          |
|                                   |                                   |
|                                   |                                   |
|                                   |                                   |
| Il devient premier capitaine et   | Premier capitaine et major dans   |
| major dans Falzbourg, corps       | Falzbourg, corps célèbre et       |
| célèbre et renommé. Les belles    | renommé, les belles actions qu'il |
| actions qu'il y fit l'ayant fait  | y fit le firent connaître par le  |
| connaître par le cardinal de      | cardinal de Richelieu, auquel la  |
| Richelieu, auquel la vertu ne     | vertu ne pouvait être cachée.     |
| pouvait pas être cachée, [il s'en | Négociations d'Allemagne.         |
| servit avantageusement dans les]  | Ordinairement ceux qui sont dans  |
| négociations d'Allemagne. [Mais   | les emplois de la guerre, croient |
| partout il porta une vertu digne  | que c'est une prééminence de      |
| de sa naissance.] Ordinairement   | l'épée de ne s'assujettir à       |
| ceux qui sont dans les emplois de | aucunes lois. Il a révéré celles  |
| la guerre croient que c'est une   | de l'Église. Les abstinences      |
| prééminence de l'épée de ne       | jamais violées : comment          |
| s'assujettir à aucunes lois. Pour | n'aurait-il pas respecté celles   |
| lui, il a révéré celles de        | qu'il recevait de toute l'Église, |
| l'Église jusque dans les points   | puisqu'il observait si            |
| qui paraissaient les plus         | soigneusement et avec tant de     |
| incompatibles avec sou état.      | religion celle que sa dévotion    |
| Jamais on ne l'a vu violer les    | particulière lui avait imposée ?  |
| abstinences prescrites, sans une  | Jeûne des samedis. Déshonorant la |
| raison capable de lui procurer    | profession des armes par cette    |
| une dispense légitime. Comment    | honte de bien faire les exercices |
| n'aurait-il pas respecté la loi   | de piété, on croit assez faire,   |
| qu'il recevait de toute l'Église, | pourvu qu'on observe les ordres   |
| puisqu'il observait si            | du général.                       |
| soigneusement, et avec tant de    |                                   |
| religion, celles que sa dévotion  |                                   |
| particulière lui avait imposées ? |                                   |
| Il jeûnait régulièrement tous les |                                   |
| samedis ; gardait avec la plus    |                                   |
| scrupuleuse exactitude et le plus |                                   |
| grand respect, toutes les         |                                   |
| pratiques que la religion lui     |                                   |
| imposait. Bien différent de ces   |                                   |
| militaires qui déshonorent la     |                                   |
| profession des armes par cette    |                                   |
| honte trop commune de bien faire  |                                   |
| les exercices de la piété. On     |                                   |
| croit assez faire, pourvu qu'on   |                                   |
| observe les ordres du général     |                                   |
| (1).                              |                                   |
|                                   |                                   |
|                                   |                                   |
+-----------------------------------+-----------------------------------+

 

 

Voilà les principales altérations que renferment les quatre dernières
pages des œuvres oratoires de Bossuet. Déforis a continué l'impression
de ces œuvres comme il l'avait commencée, ajoutant, retranchant selon
ses caprices. Et le texte ainsi remanié, arrangé de cette façon, tous
les éditeurs l'ont reproduit aveuglément, sans aucune correction.

 

Nous sommes arrivés, pour les notices historiques, à la fin des

 

1 _Edit. de Vers_., p. 611.

 

693

 

_Oraisons funèbres_ ; toutefois, avant de passer outre, encore quelques
mots. Bossuet prononça le 20 janvier 1667, dans l'église des Carmélites,
rue du Bouloy, l'éloge de sa bienfaitrice la plus vénérée, d'Anne
d'Autriche, mère de Louis XIV. L'archevêque de Paris officia
pontificalement dans la cérémonie religieuse, et plusieurs prélats se
pressaient attentifs autour de la chaire sacrée. L'orateur avait choisi
pour texte ces paroles : « _Timor Domini, ipso est thesaurus ejus_ (1) ;
et « son discours fut d'autant plus touchant, dit l'abbé Ledieu, qu'il
était lui-même plus pénétré de douleur de la perte qu'il avait faite
(2). » Ce discours n'a pas été livré à l'impression, et l'on a fait de
vaines recherches pour en retrouver le manuscrit. Il est perdu sans
retour.

On lit dans le _Siècle de Louis XIV_ : « L'_Oraison funèbre_ de la
reine-mère, que Bossuet prêcha en 1667, lui valut l'évêché de Condom.
Mais ce discours n'était pas encore digne de lui ; il ne fut pas
imprimé, non plus que ses _Sermons_. » Voilà des paroles étranges.
L'Oraison funèbre d'Anne d'Autriche précéda de trois ans la nomination
de Bossuet à l'évêché de Condom : comment donc lui valut-elle son
élévation à l'épiscopat? D'un autre côté, ce panégyrique n'a pas été
imprimé, et personne ne l'a jamais lu : comment Voltaire savait-il qu'il
n'était pas digne de Bossuet?

 

1 _Isa_., XXXIII, 6.— 2 _Mémoires,_ 1667.

 

694

 



ORAISON FUNÈBRE DE MESSIRE  HENRI  DE  GORNAY.


 

_Non privabit bonis cos qui ambulant in innocentiâ : Domine virtutum,
beatus homo qui sperat in te_. _Psal._ LXXXIII. 13.

 

C'est, Messieurs, dans ce dessein salutaire que j'espère aujourd'hui
vous entretenir de la vie et des actions de messire _Henri de Gornay,_
chevalier, seigneur de Talange, de Louyn sur Seille, que la mort nous a
ravi depuis peu de jours ; où rejetant loin de mon esprit toutes les
considérations profanes, et les bassesses honteuses de la flatterie
indignes de la majesté du lieu où je parle et du ministère sacré que
j'exerce, je m'arrêterai à vous proposer trois ou quatre réflexions
tirées des principes du christianisme, qui serviront, si Dieu le permet,
pour l'instruction de tout ce peuple et pour la consolation particulière
de ses parents et de ses amis.

Quoique Dieu et la nature aient fait tous les hommes égaux en les
formant d'une même boue, la vanité humaine ne peut souffrir cette
égalité, ni s'accommoder à la loi qui nous a été imposée, de les
regarder tous comme nos semblables. De là naissent ces grands efforts
que nous faisons tous pour nous séparer du commun, et nous mettre en un
rang plus haut par les charges ou par les emplois, par le crédit ou par
les richesses. Que si nous pouvons obtenir ces avantages extérieurs, que
la folle ambition des hommes a mis à un si grand prix, notre cœur
s'enfle tellement que nous regardons tous les autres comme étant d'un
ordre inférieur à nous ; et à peine nous reste-t-il quelque souvenir de
ce qui nous est commun avec eux.

Cette vérité importante et connue si certainement par l'expérience,
entrera plus utilement dans nos esprits, si nous considérons avec
attention trois états où nous passons tous successivement :

 

695

 

la naissance, le cours de la vie, sa conclusion par la mort. Plus je
remarque de près la condition de ces trois états, plus mon esprit se
sent convaincu que quelque apparente inégalité que la fortune ait mise
entre nous, la nature n'a pas voulu qu'il y eût grande différence d'un
homme à un autre.

Et premièrement, la naissance a des marques indubitables de notre
commune faiblesse. Nous commençons tous notre vie par les mêmes
infirmités de l'enfance ; nous saluons tous, en entrant au monde, la
lumière du jour par nos pleurs (1) ; et le premier air que nous
respirons, nous sert à tous indifféremment à former (_a_) des cris. Ces
faiblesses de la naissance attirent sur nous tous généralement une même
suite d'infirmités dans tout le progrès de la vie, puisque les grands,
les petits et les médiocres vivent également assujettis aux mêmes
nécessités naturelles, exposés aux mêmes périls, livrés en proie aux
mêmes maladies. Enfin, après tout arrive la mort, qui foulant aux pieds
l'arrogance humaine et abattant sans ressource toutes ses grandeurs
imaginaires, égale pour jamais toutes les conditions différentes, par
lesquelles les ambitieux croyaient s'être mis au-dessus des autres : de
sorte qu'il y a beaucoup de raison de nous comparer à des eaux
courantes, comme fait l'Écriture sainte. Car de même que quelque
inégalité qui paroisse dans le cours des rivières qui arrosent la
surface de la terre, elles ont toutes cela de commun, qu'elles viennent
d'une petite origine ; que dans le progrès de leur course, elles roulent
leurs flots en bas par une chute continuelle ; et qu'elles vont enfin
perdre leurs noms avec leurs eaux dans le sein immense de l'Océan, où
l'on ne distingue point le Rhin, ni le Danube, ni ces autres fleuves
renommés d'avec les rivières les plus inconnues. Ainsi tous les hommes
commencent par les mêmes infirmités : dans le progrès de leur âge, les
années se poussent les unes les autres comme des flots : leur vie roule
et descend sans cesse à la mort par sa pesanteur naturelle ; et enfin
après avoir fait, ainsi que des fleuves, un peu plus de bruit les uns
que les autres, ils vont tous se confondre dans ce gouffre infini du

 

1 _Sapient_., VII, 3.

(_a_) _Var._ ; Pousser.

 

696

 

néant, où l'on ne trouve plus ni rois, ni princes, ni capitaines, ni
tous ces autres augustes noms qui nous séparent les uns des autres ;
mais la corruption et les vers, la cendre et la pourriture qui nous
égalent. Telle est la loi de la nature, et l'égalité nécessaire à
laquelle elle soumet tous les hommes dans ces trois états remarquables,
la naissance, la durée, la mort.

Que pourront inventer les enfants d'Adam pour combattre (_a_) cette
égalité, qui est gravée si profondément dans toute la suite de notre
vie? Voici, mes Frères, les inventions par lesquelles ils s'imaginent
forcer la nature et se rendre différents des autres, malgré l'égalité
qu'elle a ordonnée. Premièrement, pour mettre à couvert la faiblesse
commune de la naissance, chacun tâche d'attirer sur elle toute la gloire
de ses ancêtres, et la rendre plus éclatante par cette lumière
empruntée. Ainsi l'on a trouvé le moyen de distinguer les naissances
illustres d'avec les naissances viles et vulgaires, et démettre une
différence infinie entre le sang noble et le roturier, comme s'il
n'avait pas les mêmes qualités et n'était pas composé des mêmes éléments
; et par là vous voyez déjà la naissance magnifiquement relevée. Dans le
progrès de la vie, on se distingue plus aisément par les grands emplois,
par les dignités éminentes, par les richesses et par l'abondance. Ainsi
on s'élève et on s'agrandit, et on laisse les autres dans la lie du
peuple. Il n'y a donc plus que la mort où l'arrogance humaine est bien
empêchée. Car c'est là que l'égalité est inévitable : et encore que la
vanité tâche en quelque sorte d'en couvrir la honte par les honneurs de
la sépulture, il se voit peu d'hommes assez insensés pour se consoler de
la mort par l'espérance d'un superbe tombeau, ou par la magnificence de
ses funérailles. Tout ce que peuvent faire ces misérables amoureux des
grandeurs humaines, c'est de goûter tellement la vie qu'ils ne songent
point à la mort ; c'est le seul moyen qui leur reste de secouer en
quelque façon le joug insupportable de sa tyrannie, lorsqu'en détournant
leur esprit ils n'en sentent pas l'amertume. (_b_)

 

(_a_) _Var_. : Pour couvrir ou pour effacer. — (_b_) _Note marg_. : La
mort jette divers traits dans la vie par la crainte, et le dernier est
inévitable ; ils croient faire beaucoup d'éviter les autres.

 

697

 

C'est ainsi qu'ils se conduisent à l'égard de ces trois états ; et de là
naissent trois vices énormes qui rendent ordinairement leur vie
criminelle. Car cette superbe grandeur dont ils se flattent dans leur
naissance, les fait vains et audacieux. Le désir démesuré dont ils sont
poussés de se rendre considérables (_a_) au-dessus des autres dans tout
le progrès de leur âge, fait qu'ils s'avancent à la grandeur par toutes
sortes de voies, sans épargner les plus criminelles ; et l'amour
désordonné des douceurs qu'ils goûtent dans une vie pleine de délices,
détournant leurs yeux de dessus la mort, fait qu'ils tombent entre ses
mains sans l'avoir prévue : au lieu que l'illustre gentilhomme, dont je
vous dois aujourd'hui proposer l'exemple, a tellement ménagé toute sa
conduite, que la grandeur de sa naissance n'a rien diminué de la
modération de son esprit ; que ses emplois glorieux, dans la ville et
dans les armées, n'ont point corrompu son innocence ; et que bien loin
d'éviter l'aspect de la mort, il l'a tellement méditée, qu'elle n'a pas
pu le surprendre, même en arrivant tout à coup, et qu'elle a été
soudaine sans être imprévue.

 

Si autrefois le grand saint Paulin, digne prélat de l'église de Nole, en
faisant le panégyrique de sa parente sainte Mélanie (1), a commencé les
louanges de cette veuve si renommée par la noblesse de son extraction,
je puis bien suivre un si grand exemple, et vous dire un mot en passant
de l'illustre maison de Gornay, si célèbre et si ancienne. Mais pour ne
pas traiter ce sujet d'une manière profane, comme fait la rhétorique
mondaine, recherchons par les Écritures de quelle sorte la noblesse est
recommandable, et l'estime qu'on en doit faire selon les maximes du
christianisme.

Et premièrement, chrétiens, c'est déjà un grand avantage qu'il ait plu à
notre Sauveur de naître d'une race illustre par la glorieuse union du
sang royal et sacerdotal dans la famille d'où il est sorti : _Regum et
sacerdotum clara progenies_ (2). Pour quelle raison, lui qui a méprisé
toutes les autres grandeurs humaines?

 

1 _Ad Sever_., ep. XXIX, n. 7. — 2 _Ibid.,_ p. 179.

(_a_) _Var._ : Recommandables.

 

698

 

_Non multi sapientes, non multi nobiles_ (1) ; Jésus-Christ l'a voulu
être. Ce n'était pas pour en recevoir de l'éclat, mais plutôt pour en
donner à tous ses ancêtres. Il fallait qu'il sortît des patriarches,
pour accomplir en sa personne toutes les bénédictions qui leur avoient
été annoncées. Il fallait qu'il naquît des rois de Juda, pour conserver
à David la perpétuité de son trône, que tant d'oracles divins lui
avoient promise.

Louer en un gentilhomme chrétien ce que Jésus-Christ même a voulu
avoir... (_a_) Je ne dirai point ni les grandes charges qu'elle a
possédées, ni avec quelle gloire elle a étendu ses branches dans les
nations étrangères, ni ses alliances illustres avec les Maisons royales
de France et d'Angleterre, ni son antiquité, qui est telle que nos
chroniques n'en marquent point l'origine. Cette antiquité a donné lieu à
plusieurs inventions fabuleuses, par lesquelles la simplicité de nos
pères a cru donner du lustre à toutes les maisons anciennes, à cause que
leur antiquité, en remontant plus loin aux siècles passés dont la
mémoire est toute effacée, a donné aux hommes une plus grande liberté de
feindre. La hardiesse humaine n'aime pas à demeurer court ; où elle ne
trouve rien de certain, elle invente. Je laisse toutes ces
considérations profanes, pour m'arrêter à des choses saintes.

Saint Livier, environ l'an 400, selon la supputation la plus exacte, est
la gloire de la maison de Gornay. Le sang qu'a répandu ce généreux
martyr, l'honneur de la ville de Metz, pour la cause de Jésus-Christ,
vous donne plus de gloire que celui que vous avez reçu de tant
d'illustres ancêtres : « Nous sommes la race des saints : » Filii
sanctorum sumus (1). L'histoire remarque qu'il était _claris parentibus_
: ce qui est une conviction manifeste qu'il faut reprendre la grandeur
de cette Maison d'une origine plus haute.

Mais tous ces titres glorieux ne lui ont jamais donné de l'orgueil. Il a
toujours méprisé les vanteries ridicules dont il arrive assez
ordinairement que la noblesse étourdit le monde. Il a cru

 

1 I _Cor.,_ I, 23. — 2 _Tob_., II, 18.

(_a_) _Note marg._ : Peu de chose : sujet trop profane. Néanmoins
d'autant plus volontiers, qu'il y a quelque chose de saint à traiter.

 

699

 

que ces vanteries étaient plutôt dignes des races nouvelles, éblouies de
l'éclat non accoutumé d'une noblesse de peu d'années ; mais que la
véritable marque des Maisons illustres, auxquelles la grandeur et
l'éclat étaient depuis plusieurs siècles passés en nature, ce devait
être la modération. Ce n'est pas qu'il ne jetât les yeux sur l'antiquité
de sa race, dont il possédait parfaitement l'histoire : mais comme il y
avait des saints dans sa race, il avait raison de la contempler pour
s'animer par ces grands exemples. Il n'était pas de ceux qui semblent
être persuadés que leurs ancêtres n'ont travaillé que pour leur donner
sujet de parler de leurs actions et de leurs emplois. Quand il regardait
les siens, il croyait que tous ses aïeux illustres lui criaient
continuellement jusque des siècles les plus reculés : Imite nos actions,
ou ne te glorifie pas d'être notre fils. Il se jeta dans les exercices
de sa profession à l'imitation de saint Livier : il commença à faire la
guerre contre les hérétiques rebelles. Premier capitaine et major dans
Falzbourg, corps célèbre et renommé, les belles actions qu'il y fit le
firent connaître par le cardinal de Richelieu, auquel la vertu ne
pouvait être cachée. Négociations d'Allemagne. Ordinairement ceux qui
sont dans les emplois de la guerre, croient que c'est une prééminence de
l'épée de ne s'assujettir à aucunes lois. Il a révéré celles de
l'Église. Les abstinences jamais violées : comment n'aurait-il pas
respecté celles qu'il recevait de toute l'Église, puisqu'il observait si
soigneusement et avec tant de religion celle que sa dévotion
particulière lui avait imposée ? Jeûne des samedis. — Déshonorant la
profession des armes par cette honte de bien faire les exercices de la
piété, on croit assez faire, pourvu qu'on observe les ordres du général.

Sa vieillesse, quoique pesante, n'était pas sans action : son exemple et
ses paroles animaient les autres. Il est mort trop tôt : non ; car la
mort ne vient jamais trop soudainement quand on s'y prépare par la bonne
vie.

 

FIN  DES  ORAISONS  FUNÈBRES.

 

[Précédente]

[Accueil]

[Suivante]

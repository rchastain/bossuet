
A SA GRANDEUR
MONSEIGNEUR SERGENT,
ÉVÊQUE DE QUIMPER.

Monseigneur,

Il y a trois ans à peine, quand Votre Grandeur daignait bénir la
première pierre de mon humble maison, mon cœur attendri se rappelait
avec une profonde reconnaissance que la main Épiscopale qui répandait
l'eau sainte s'était constamment rencontrée sur les divers chemins de ma
laborieuse existence pour guider mes pas et encourager mes efforts. Dès
lors j'eus la pensée de placer mes principales publications sous le
patronage du nom illustre qui avait protégé le modeste éditeur.

Déjà Votre Grandeur m'a permis de lui dédier l'édition des Œuvres
complètes de Suarez, le plus grand théologien qu'ait eu l'Église depuis
l'ange de l'École, saint Thomas d'Aquin. Le succès de cette entreprise a
dépassé mes espérances, et m'a prouvé une fois de plus que la
bénédiction du Ciel est attachée à votre nom.

Aujourd'hui je viens offrir à Votre Grandeur les œuvres immortelles du
plus beau génie catholique de notre France, et du plus illustre de nos
évêques ; je viens déposer à vos pieds la première édition authentique de
Bossuet.

II

C'est une lamentable histoire, Monseigneur, que celle des mutilations et
des surcharges, des suppressions calculées et des perfides additions
infligées jusqu'ici à l'œuvre et à la mémoire de l'Aigle de Meaux. Les
manuscrits de Bossuet devinrent, après sa mort, la propriété d'un neveu
dont le caractère sacré, en retenant sous ma plume toute expression
flétrissante, ne saurait pourtant m'empêcher de signaler l'incroyable
audace. C'est sa main qui donna à l'Europe, contre des vœux qui devaient
être sacrés pour lui, la fameuse _Defensio Cleri Gallicani_. Depuis 1743
jusqu'à la révolution, la secte de Port-Royal, survivant comme à ses
propres ruines, édita seule une portion considérable des manuscrits du
grand homme. Alors disparurent pour jamais des cahiers écrits en entier
de la main de Bossuet ; un janséniste ardent, Lequeux, abbé de
Saint-Yves, les livra aux flammes, en ajoutant à ce crime l'impudence de
s'en vanter. Une _Lettre aux Religieuses de Port-Royal,_ échappée par
miracle à ce vandalisme de sectaire, et que j'ai le bonheur de publier
dans le volume des Œuvres inédites de Bossuet, permettra enfin de venger
sa mémoire d'un reproche trop accrédité jusqu'ici, celui d'avoir
secrètement favorisé le jansénisme. Les Lettres du grand Évêque ne
furent pas mieux traitées. Au moyen d'habiles retranchements et
d'additions insidieuses, on défigura la physionomie de l'auteur, et on
supprima la _partie intime_ qui aurait révélé dans Bossuet un cœur aussi
noble que son génie. Après l'avoir ainsi outragé dans sa doctrine comme
évêque, dans sa foi comme chrétien, dans sa correspondance comme homme,
il ne restait plus à rabaisser dans Bossuet que sa réputation d'orateur,
et à dénaturer cette éloquence qui tint sous le charme les plus grands
esprits du siècle de Louis XIV. Cette dernière injure ne lui fut pas
épargnée. En 1772, un petit-neveu de Bossuet donnait les sermons de
l'illustre orateur aux

III

Bénédictins des Blancs-Manteaux de Paris. Ce don précieux tombait encore
aux mains du jansénisme, dont ces religieux étaient alors les plus
fougueux partisans. Ils trouvèrent dans ce legs une occasion favorable
pour annoncer au public une édition complète des Œuvres de Bossuet : dom
Déforis, avec d'autres de ces religieux, fut chargé de la préparer.
Jamais on n'associa aux ailes du génie un esprit plus lourd, plus étroit
et plus présomptueux. Les _Sermons_ en particulier subirent sa fatale
empreinte. On est stupéfait en présence des insipides commentaires, des
corrections et des surcharges dans lesquelles le véritable texte de
Bossuet se trouve noyé. Substituer une telle plume au burin puissant du
maître, cela ressemble à un sacrilège. Ce fut pourtant le travail de dom
Déforis qui resta définitif ; il survécut aux révolutions, et s'imposa
désormais aux éditions successives des Œuvres de Bossuet, si nombreuses
dans toutes les langues.

Jamais peut-être une telle obstination dans l'abaissement n'a poursuivi
une mémoire. Combien de fois, Monseigneur, ne l'avez-vous pas déploré
vous-même devant moi ! Mais, pour faire revivre dans sa splendeur
première l'immortelle figure de l'Évêque de Meaux, que de sacrifices,
que de recherches et de labeurs ! Les sacrifices, tout considérables
qu'ils dussent être, se trouvèrent pour moi la partie la plus facile de
ma tâche. Je commençai par briser le cliché d'une édition complète dont
j'étais propriétaire (c'était une fortune renversée à mes pieds), trop
heureux de mériter à ce prix l'honneur de ressusciter une des plus
belles gloires de notre patrie ! La Providence me fit ensuite rencontrer
un homme dont le consciencieux respect pour la mémoire de l'Évêque de
Meaux est un véritable culte, dont les investigations laborieuses et
patientes n'ont reculé devant aucune fatigue, et qui estime un texte de
Bossuet rétabli

IV

dans son intégrité à l'égal d'une victoire. Votre Grandeur a bien voulu
reconnaître, dans un écrit rendu public, les qualités du traducteur de
saint Thomas d'Aquin, et qui va joindre désormais à ce titre celui
d'éditeur littéraire de Bossuet. L'Europe entière s'est intéressée à
cette grande œuvre, les bibliothèques publiques et particulières se sont
ouvertes pour la compléter ; que Votre Grandeur daigne en accepter
l'hommage, et attirer sur elle les faveurs et les bénédictions célestes.

Je suis avec le plus profond respect, Monseigneur, De Votre Grandeur

Le très-humble et très-obéissant serviteur.

L. VIVÈS.

Paris, 17 octobre 1862.

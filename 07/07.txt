
VE SEMAINE. SUITE DES SINGULARITÉS DE LA CRÉATION DE L'HOMME.

 Ve SEMAINE. SUITE DES SINGULARITÉS  DE LA CRÉATION DE L'HOMME.

 PREMIÈRE ÉLÉVATION. Dieu met l'homme dans le paradis, et lui amène
tous les animaux pour les nommer.

 IIe ÉLÉVATION. La création du second sexe.

 IIIe ÉLÉVATION. Dieu donne à L'homme un commandement, et l'avertit de
son franc arbitre, et tout ensemble de sa sujétion.

 IVe ÉLÉVATION. Sur l'arbre de la science du bien et du mal, et sur
l'arbre de vie.

 Ve ÉLÉVATION. Dernière singularité de la création de l'homme dans son
immortalité.

PREMIÈRE ÉLÉVATION. Dieu met l'homme dans le paradis, et lui amène tous les animaux pour les nommer.

Après avoir formé l'homme, Dieu commence à lui faire sentir ce qu'il est
dans le monde par deux mémorables circonstances : l'une en lui plantant
de sa propre main un jardin délicieux qu'on appelle paradis, où il avait
ramassé toutes les beautés de la nature pour servir au plaisir de
l'homme, et par là l'élever à Dieu qui le comblait de tant de biens :
l'autre en lui amenant tous les animaux comme à celui qui en était le
maître , afin de lui faire voir que non-seulement toutes les plantes et
tous les fruits de la terre étaient à lui, mais encore tous les animaux
qui par la nature de leurs mouvements semblaient moins sujets à son
empire.

Pour le paradis, Dieu ordonna deux choses à l'homme : l'une « de le
cultiver, » et l'autre « de le garder : » c'est-à-dire d'en conserver la
beauté ; ce qui revient encore à la culture. Car au reste il n'y avait
pas d'ennemi qui put envahir ce lieu tranquille et saint : _Ut
operaretur, et custodiret illum_ (1). Dieu apprenait à l'homme, par
cette figure, à se garder soi-même et à garder à la fois la place qu'il
avait dans le paradis. Pour la culture, ce n'était pas cette culture
laborieuse qui a été la peine de notre péché, lorsqu'il a fallu comme
arracher dans la sueur de notre front, du sein de la terre, le fruit
nécessaire à la conservation de notre vie : la culture donnée à l'homme
pour son exercice, était cette culture

 

1 _Genes.,_ II, 15.

 

85

 

comme curieuse, qui fait cultiver les fruits et les fleurs, plus pour le
plaisir que pour la nécessité. Par ce moyen l'homme devait être instruit
de la nature des terres et du génie des plantes, de leurs fruits ou de
leurs semences : et il y trouvait la figure de la culture des vertus.

En amenant les animaux à l'homme (1), Dieu lui fait voir qu'il en est le
maître, comme un maître dans sa famille, qui nomme ses serviteurs pour
la facilité du commandement. L'Écriture, substantielle et courte dans
ses expressions, nous indique en même temps les belles connaissances
données à l'homme, puisqu'il n'aurait pas pu nommer les animaux, sans en
connaître la nature et les différences, pour ensuite leur donner des
noms convenables selon les racines primitives de la langue que Dieu lui
avait apprise.

C'est donc alors qu'il connut les merveilles de la sagesse de Dieu, dans
cette apparence et cette ombre de sagesse qui paraît dans les industries
naturelles des animaux. Louons Dieu avec Adam, et considérons comme
devant nous toute la nature animale, comme l'objet de notre raison. Qui
a formé tant de genres d'animaux, et tant d'espèces subordonnées à ces
genres, toutes ces propriétés, tous ces mouvements, toutes ces adresses,
tous ces aliments, toutes ces forces diverses, toutes ces images de
vertu. de pénétration, de sagacité et de violence? Qui a fait marcher,
ramper, glisser les animaux? Qui a donné aux oiseaux et aux poissons ces
rames naturelles qui leur font fendre les eaux et les airs? Ce qui
peut-être a donné lieu à leur Créateur de les produire ensemble, comme
animaux d'un dessein à peu près semblable ; le vol des oiseaux semblant
être une espèce de faculté de nager dans une liqueur plus subtile, comme
la faculté de nager dans les poissons est une espèce de vol dans une
liqueur plus épaisse. Le même Auteur a fait ces convenances et ces
différences : celui qui a donné aux poissons leur triste et pour ainsi
dire leur morne silence, a donné aux oiseaux leurs chants si divers, et
leur a mis dans l'estomac et dans le gosier une espèce de lyre et de
guitare, pour annoncer chacun à leur mode les beautés de leur Créateur.
Qui

 

1 _Genes.,_ II, 19.

 

87

 

n'admirerait les richesses de sa providence, qui fait trouver à chaque
animal jusqu'à une mouche, jusqu'à un ver, sa nourriture convenable? En
sorte que la disette ne se trouve dans aucune partie de sa famille ; mais
au contraire que l'abondance y règne partout, excepté maintenant parmi
les hommes, depuis que le péché a introduit la cupidité et l'avarice.

Par la considération tous les animaux sont à l'usage de l'homme,
puisqu'ils lui servent à connaître et à louer Dieu. Mais outre cet usage
plus universel, Adam connut dans les animaux des propriétés
particulières, qui leur donnaient le moyen d'aider par leur ministère
celui que Dieu faisait leur seigneur. O Dieu, j'ai considéré vos
ouvrages, et j'en ai été effrayé ! Qu'est devenu cet empire que vous
nous aviez donné sur les animaux? On n'en voit plus parmi nous qu'un
petit reste, comme un faible mémorial de notre ancienne puissance et un
débris malheureux de notre fortune passée.

Rendons grâces à Dieu de tous les biens qu'il nous a laissés dans le
secours des animaux ; accoutumons-nous à le louer en tout. Louons-le
dans le cheval qui nous porte ou qui nous traîne : dans la brebis qui
nous habille et qui nous nourrit : dans le chien qui est notre garde et
notre chasseur : dans le bœuf qui fait avec nous notre labourage.
N'oublions pas les oiseaux, puisque Dieu les a amenés à Adam comme les
autres animaux ; et qu'encore aujourd'hui apprivoisés par notre
industrie, ils viennent flatter nos oreilles par leur aimable musique ;
et chantres infatigables et perpétuels, ils semblent vouloir mériter la
nourriture que nous leur donnons. Si nous louons les animaux dans leur
travail et pour ainsi dire dans leurs occupations, ne demeurons pas
inutiles : travaillons : gagnons notre pain chacun dans son exercice,
puisque Dieu l'a mis à ce prix depuis le péché.

 

87

 


IIe ÉLÉVATION. La création du second sexe.

 

En produisant les autres animaux, Dieu a créé ensemble les deux sexes ;
et la formation du second est une singularité de la création de l'homme.

Que servait à l'homme d'être introduit dans ce paradis de délices, dans
tout un vaste pays que Dieu avait mis en son pouvoir, et au milieu de
quatre grands fleuves dont les riches eaux traînaient des trésors : au
reste sous un ciel si pur, que sans être encore obscurci par ces nuages
épais qui couvrent le nôtre et produisent les orages, il s'élevait de la
terre par une bénigne chaleur une vapeur douce qui se distillait en
rosée et qui arrosait la terre et toutes ses plantes? L'homme était
seul, et le plus seul de tous les animaux ; car il voyait tous les autres
partagés et appareillés en deux sexes ; et, dit l'Écriture, il n'y avait
que l'homme « à qui on ne trouvait point d'aide semblable à lui (1). »
Solitaire, sans compagnie, sans conversation, sans douceur, sans
espérance de postérité, et ne sachant à qui laisser ou avec qui partager
ce grand héritage et tant de biens que Dieu lui avait donnés, il vivait
tranquille, abandonné à sa providence, sans rien demander. Et Dieu aussi
de lui-même, ne voulant laisser aucun défaut dans son ouvrage, dit ces
paroles : « Il n'est pas bon que l'homme soit seul : donnons-lui une
aide semblable à lui (2). »

Peut-être donc va-t-il former le second sexe, comme il avait formé le
premier ? Non : il veut donner au monde dans les deux sexes l'image de
l'unité la plus parfaite et le symbole futur du grand mystère de
Jésus-Christ. C'est pourquoi il tire la femme de l'homme même, et la
forme d'une côte superflue qu'il lui avait mise exprès dans le côté.
Mais pour montrer que c'était là un grand mystère et qu'il fallait
regarder avec des yeux plus épurés

1 _Genes.,_ II, 20.— 2 _Ibid.,_ 18.

88

que les corporels, la femme est produite dans une extase d'Adam ; et
c'est par un esprit de prophétie qu'il connut tout le dessein d'un si
bel ouvrage. « Le Seigneur Dieu envoya un sommeil à Adam : » un sommeil,
disent tous les saints, qui fut un ravissement et la plus parfaite de
toutes les extases : Dieu « prend une côte d'Adam et il en remplit de
chair la place (1). » Ne demandez donc point à Dieu pourquoi voulant
tirer de l'homme la compagne qu'il lui donnait, il prit un os plutôt que
de la chair : car s'il avait pris de la chair, on aurait pu demander de
même pourquoi il aurait pris de la chair plutôt qu'un os. Ne lui
demandons non plus ce qu'il ajouta à la côte d'Adam pour en former un
corps parfait : la matière ne lui manque pas : et quoi qu'il en soit,
cet os se ramollit entre ses mains. C'est de cette dureté qu'il voulut
former ces délicats et tendres membres, où dans la nature innocente il
ne faut rien imaginer qui ne fût aussi pur qu'il était beau. Les femmes
n'ont qu'à se souvenir de leur origine ; et sans trop vanter leur
délicatesse, songer après tout qu'elles viennent d'un os surnuméraire,
où il n'y avait de beauté que celle que Dieu y voulut mettre.

Mon Dieu, que de vains discours je prévois dans les lecteurs au récit de
ce mystère ! Mais pendant que je leur raconte un grand et mystérieux
ouvrage de Dieu, qu'ils entrent dans un esprit sérieux et, s'il se peut,
dans quelque sentiment de cette admirable extase d'Adam, pendant
laquelle il édifia, « il bâtit en femme la côte d'Adam (2) : » grave
expression de l'Écriture, pour nous faire voir dans la femme quelque
chose de grand et de magnifique, et comme un admirable édifice où il y
avait de la grâce, de la majesté , des proportions admirables et autant
d'utilité que d'ornement.

La femme ainsi formée est présentée « de la main de Dieu » au premier
homme, qui ayant vu dans son extase ce que Dieu faisait : « C'est ici,
dit-il d'abord, l'os de mes os, et la chair de ma chair : elle
s'appellera Virago, parce qu'elle est formée de l'homme ; et l'homme
quittera son père et sa mère, et il s'unira à sa femme (3). » On peut
croire par cette parole que Dieu avait formé la femme d'un os revêtu de
chair, et que l'os seul est nommé

1 _Genes.,_ II, 21. — 2 _Ibid.,_ 22. — 3 _Ibid.,_ 23, 24.

89

comme prévalant dans cette formation. Quoi qu'il en soit, encore une
fois sans nous arrêter davantage à des questions curieuses, et
remarquant seulement en un mot ce qui paraît dans le texte sacré,
considérons en esprit cette épouse mystérieuse ; c'est-à-dire la sainte
Église tirée et comme arrachée du sacré côté du nouvel Adam pendant son
extase, et formée pour ainsi parler par cette plaie dont toute la
consistance est dans les os et dans les chairs de Jésus-Christ, qui se
l'incorpore parle mystère de l'incarnation et par celui de l'Eucharistie
qui en est une extension admirable. Il quitte tout pour s'unir à elle :
il quitte en quelque façon son Père qu'il avait dans le ciel, et sa mère
la Synagogue, d'où il était issu selon la chair, pour s'attacher à son
épouse ramassée parmi les Gentils. C'est nous qui sommes cette épouse :
c'est nous qui vivons des os et des chairs de Jésus-Christ, par les deux
grands mystères qu'on vient de voir : « C'est nous qui sommes, comme dit
saint Pierre , cet édifice spirituel et le temple vivant du Seigneur
(1), » bâti en esprit dès le temps de la formation d'Eve notre mère, et
dès l'origine du monde. Considérons dans le nom d'Eve, qui signifie mère
des vivants, et l'Église mère des véritables vivants, et la bienheureuse
Marie la vraie mère des vivants, qui nous a tous enfantés avec
Jésus-Christ qu'elle a conçu par la foi. O homme ! voilà ce qui t'est
montré dans la création de la femme, pour prévenir par ce sérieux toutes
les frivoles pensées qui passent dans l'esprit des hommes au souvenir
des deux sexes, depuis seulement que le péché en a corrompu
l'institution. Revenons à notre origine : respectons l'ouvrage de Dieu
et son dessein primitif : éloignons les pensées de la chair et du sang ;
et ne nous plongeons point dans cette boue, pendant que dans le récit
qu'on vient d'entendre Dieu prend tant de soins de nous en tirer.

1 I _Petr.,_ II, 5.

90

IIIe ÉLÉVATION. Dieu donne à L'homme un commandement, et l'avertit de son franc arbitre, et tout ensemble de sa sujétion.

« Vous mangerez de tous les fruits du paradis : mais vous ne mangerez
point de l'arbre de la science du bien et du mal : car au jour que vous
en mangerez, vous mourrez de mort (1) : » la mort vous sera inévitable.

Eve fut présente à ce commandement, quoique par anticipation il soit
rapporté avant sa production ; ou en tout cas il fut répété en sa
présence, puisqu'elle dit au serpent : « Le Seigneur nous a commandé de
ne point manger ce fruit (2) : » si ce n'est qu'on aime mieux croire
qu'elle apprit d'Adam la défense de Dieu ; et que dès lors il ait plu à
Dieu de nous enseigner que c'est un devoir des femmes « d'interroger,
comme dit saint Paul, dans la maison et en particulier leurs maris (3), »
et d'attendre d'eux les ordres de Dieu.

Quoi qu'il en soit, Dieu fait deux choses par ce commandement, et il
enseigne à l'homme premièrement son libre arbitre, et secondement sa
sujétion.

Le libre arbitre est un des endroits de l'homme où l'image de Dieu
paraît davantage. Dieu est libre à faire ou ne faire pas au dehors tout
ce qui lui plaît, parce qu'il n'a besoin de rien et qu'il est supérieur
à tout son ouvrage : qu'il fasse cent mille mondes, il n'en est pas plus
grand : qu'il n'en fasse aucun, il ne l'est pas moins. Au dehors le
néant ou l'être lui est égal ; et il est maître ou de ne rien faire, ou
de faire tout ce qui lui plaît. Que l'âme raisonnable puisse aussi faire
d'elle-même ou du corps qui lui est uni, ce qui lui plaît, c'est
assurément un trait admirable et une admirable participation de l'être
divin. Je ne suis rien ; mais parce qu'il a plu à Dieu de me faire à son
image et d'imprimer dans

1 _Genes.,_ II, 16, 17.— 2 _Ibid.,_ III, 3. — 3 I _Cor.,_ XIV, 35.

91

mon fond une ressemblance quoique faible de son libre arbitre, je veux
que ma main se lève, que mon bras s'étende, que ma tête. que mon corps
se tourne ; cela se fait : je cesse de le vouloir, et je veux que tout
se tourne d'un autre côté ; cela se fait de même. Tout cela m'est
indifférent : je suis aussi bien d'un côté que d'un autre : et de tout
cela il n'y en a aucune raison que ma volonté : cela est, parce que je
le veux : et je le veux, parce que je le veux : et c'est là une dernière
raison, parce que Dieu m'a voulu donner cette faculté : et quand même il
y a quelque raison de me déterminer à l'un plutôt qu'à l'autre, si cette
raison n'est pas pressante, et qu'il ne s'agisse pour moi que de quelque
commodité plus ou moins grande, je puis aisément ou me la donner, ou ne
me la donner pas : et je puis ou nie donner ou m'ôter de grandes
commodités, et si je veux des incommodités et des peines aussi grandes,
et tout cela parce que je le veux, et Dieu a soumis cela à ma volonté :
et je puis même user de ma liberté jusqu'à me procurer à moi-même de
grandes souffrances, jusqu'à m'exposer à la mort, jusqu'à me la donner ;
tant je suis maître de moi-même par ce trait de la divine ressemblance
qu'on appelle le libre arbitre. Et si je rentre au dedans de moi, je
puis appliquer mon intelligence à une infinité d'objets divers, et à
l'un plutôt qu'à l'autre, et à tout successivement, à commencer par où
je veux : et je puis cesser de le vouloir, ou même vouloir le contraire
; et d'une infinité d'actes de ma volonté, je puis faire ou celui-ci ou
celui-là, sans qu'il y en ait d'autre raison, sinon que je le veux ; ou
s'il y en a d'autre raison, je suis le maître de cette raison pour m'en
servir ou ne m'en servir pas, ainsi que je le veux. Et par ce principe
de libre arbitre, je suis capable de vertus et de mérites, et on
m'impute à moi-même le bien que je fais ; et la gloire m'en appartient.

Il est vrai que je puis aussi me détourner vers le mal, et mon œuvre
m'est imputée à moi-même. Et je commets une faute dont je puis aussi me
repentir ou ne me repentir pas ; et ce repentir est une douleur bien
différente des autres que je puis souffrir. Car je puis bien être fâché
d'avoir la fièvre ou d'être aveugle, mais non pas me repentir de ces
maux, lorsqu'ils me viennent malgré moi. Mais si je mens, si je suis
injuste ou médisant, et que j'en sois

92

fâché, cette douleur est un repentir que je puis avoir et n'avoir pas ;
heureux si je me repens du mal, et que volontairement je persévère dans
le bien.

Voilà dans ma liberté un trait défectueux, qui est de pouvoir mal faire :
ce trait ne me vient pas de Dieu, mais il me vient du néant dont je
suis tiré. Dans ce défaut je dégénère de Dieu qui m'a fait : car Dieu ne
peut vouloir le mal ; et le Psalmiste lui chante : « Vous êtes un Dieu
qui ne voulez pas l'iniquité (1). » Mon Dieu, voilà le défaut et le
caractère de la créature : je ne suis pas une image et ressemblance
parfaite de Dieu : je suis seulement fait à l'image : j'en ai quelque
trait : mais parce que je suis, je n'ai pas tout : et on m'a tourné à la
ressemblance ; mais je ne suis pas une ressemblance, puisqu'enfin je puis
pécher. Je tombe dans le défaut par mille endroits : par l'imperfection,
par la multiplicité, par la variabilité de mes actes ; tout cela n'est
pas en Dieu, et je dégénère par tous ces endroits ; mais l'endroit où je
dégénère le plus, le faible et pour ainsi dire la honte de ma nature,
c'est que je puisse pécher.

Dieu dans l'origine m'a donné un précepte ; car il était juste que je
sentisse que j'étais sujet. Je suis une créature à qui il convient
d'être soumise : je suis né libre, Dieu l'a voulu ; mais ma liberté n'est
pas une indépendance : il me fallait une liberté sujette, ou si l'on
aime mieux parler ainsi avec un Père de l'Église, une servitude libre
sous un seigneur souverain : _Libera servitus_ : et c'est pourquoi il me
fallait un précepte pour me faire sentir que j'avais un maître. O Dieu,
le précepte aisé que vous m'avez donné d'abord. Parmi tant d'arbres et
de fruits, était-ce une chose si difficile de m'abstenir d'un seul ? Mais
vous vouliez seulement me faire sentir par un joug aisé et avec une main
légère que j'étais sous votre empire. O Dieu, après avoir secoué le
joug, il est juste que je subisse celui des travaux, de la pénitence et
de la mort que vous m'avez imposé. O Dieu, vous êtes mon roi :
faites-moi ce que vous voudrez par votre justice, mais n'oubliez pas vos
miséricordes.

1 _Psal_. V, 5.

93

IVe ÉLÉVATION. Sur l'arbre de la science du bien et du mal, et sur l'arbre de vie.

 

On peut entendre que « Dieu avait produit de la terre tout arbre beau à
voir et agréable au goût : et il avait mis aussi dans le milieu du
paradis l'arbre de vie et l'arbre de la science du bien et du mal (1). »
Dieu pouvait annexer aux plantes certaines vertus naturelles par rapport
à nos corps ; et il est aisé à croire que le fruit de l'arbre de vie
avait la vertu de réparer le corps par un aliment si proportionné et si
efficace, que jamais on ne serait mort en s'en servant. Mais pour
l'arbre de la science du bien et du mal, comme c'était là un effet qui
passait la vertu naturelle d'un arbre, on pourrait dire que cet arbre a
été ainsi appelé par l'événement, à cause que l'homme en usant de cet
arbre contre le commandement de Dieu, a appris la malheureuse science
qui lui fait discerner par expérience le mal que son infidélité lui
attirait, d'avec le bien où il avait été créé, et qu'il devait savoir
uniquement s'il eût persévéré dans l'innocence.

On peut encore penser que la vertu de donner à l'homme la science du
bien et du mal, était dans cet arbre une vertu surnaturelle, semblable à
celle que Dieu a mise dans les sacrements, comme dans l'eau la vertu de
régénérer l'intérieur de l'homme et d'y répandre la vie de la grâce.

Quoi qu'il en soit, sans rechercher curieusement le secret de l'œuvre de
Dieu, il me suffit de savoir que Dieu avait défendu absolument et dès
l'origine l'usage de l'arbre de la science du bien et du mal, et non pas
l'usage de l'arbre de vie. Voici ses paroles : « Mangez du fruit de tous
les arbres du paradis, mais ne mangez point de celui de l'arbre de la
science du bien et du mal (2). » Il n'y avait donc que ce seul fruit qui
fût défendu, et celui de l'arbre de vie ne le fut qu'après le péché,
conformément à cette

 

1 _Genes.,_ II, 9. — 3 _Ibid.,_ 16, 17.

 

94

 

parole : « Prenons garde qu'il ne mette encore la main sur l'arbre de
vie, et qu'il ne vive éternellement (1). »

O Dieu, je me soumets à vos défenses : je renonce à toute science
curieuse, puisque vous m'en défendez l'usage : je ne devais savoir par
expérience que le bien : je me suis trop mal trouvé d'avoir voulu savoir
ce que vous n'aviez pas voulu m'apprendre ; et je me contente de la
science que vous me voulez donner. Pour l'arbre de vie, vous m'en aviez
permis l'usage, et je pouvais être immortel avec ce secours, et
maintenant vous me le rendez par la croix de mon Sauveur. Le vrai fruit
de vie pend à cet arbre mystérieux, et je le mange dans l'Eucharistie de
dessus la croix, en célébrant ce mystère selon le précepte de
Jésus-Christ en mémoire de sa mort, conformément à cette parole : «
Faites ceci en mémoire de moi (2) ; » et celle-ci de saint Paul : a
Toutes les fois que vous mangerez de ce pain » céleste « et que vous
boirez de cette coupe » bénite, « vous annoncerez, » vous publierez,
vous célébrerez « la mort du Seigneur (3). » C'est donc ici un fruit de
mort et un fruit de vie : un fruit de vie, puisque Jésus-Christ a dit :
« Vos pères ont mangé la manne, et ils sont morts : mais quiconque
mangera du pain que je vous donnerai, ne mourra jamais (4). »
L'Eucharistie est donc un fruit et un pain de vie : mais en même temps
c'est un fruit de mort, puisqu'il fallait pour nous vivifier que Jésus «
goûtât la mort pour nous tous (5) ; » et que rappelés à la vie par cette
mort, « nous portassions continuellement en nos corps la mortification
de Jésus (6), » par la mort de nos passions et en mourant à nous-mêmes
et à nos propres désirs, «pour ne vivre plus qu'à celui qui est mort et
ressuscité pour nous (7). » Pesons ces paroles et vivons avec
Jésus-Christ, comme lui « mortifiés selon la chair et vivifiés selon
l'esprit (8), » ainsi que disait saint Pierre.

 

1 _Genes_., III, 22. — 2 _Luc.,_ XXII, 19.— 3 I _Cor.,_ XI, 26. — 4
_Jooan_., IV, 49, 50. — 5 _Hebr.,_ il, 9. — 6 II _Cor.,_ IV, 10. — 7
_Ibid.,_ V, 15. — 8 I Petr., III, 18.

 

95

 


Ve ÉLÉVATION. Dernière singularité de la création de l'homme dans son immortalité.

 

Nous ne comptons plus les admirables singularités de la création de
l'homme, tant le nombre en est grand ; mais la dernière est
l'immortalité. O Dieu, quelle merveille ! tout ce que je vois d'animaux
autour de moi sont sujets à la mort ; moi seul avec un corps composé des
mêmes éléments, je suis immortel par mon origine.

Je pouvais mourir cependant, puisque je pouvais pécher : j'ai péché, et
je suis mort : mais je pouvais ne pas mourir, parce que je pouvais ne
pas pécher, et que c'est le péché seul qui m'a privé de l'usage de
l'arbre de vie.

Quel bonheur ! quelle perfection de l'homme ! Fait à l'image de Dieu par
un dessein particulier de sa sagesse ; établi dans un paradis, dans un
jardin délicieux où tous les biens abondaient , sous un ciel toujours
pur et toujours bénin ; au milieu des riches eaux de quatre fleuves ;
sans avoir à craindre la mort, libre, heureux, tranquille, sans aucune
difformité ou infirmité, ni du côté de l'esprit, ni du côté du corps ;
sans aucun besoin d'habits, avec une pure et innocente nudité ; ayant
mon salut et mon bonheur en ma main ; le ciel ouvert devant moi pour y
être transporté quand Dieu voudrait, sans passer par les ombres
affreuses de la mort! Pleure sans fin , homme misérable qui as perdu
tous ces biens et ne te console qu'en Jésus-Christ qui te les a rendus,
et encore dans une plus grande abondance!

[ELÉV. SEMAINE XIV]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Elévations
Elév. Semaine I
Elév. Semaine II
Elév. Semaine III
Elév. Semaine IV
Elév. Semaine V
Elév. Semaine VI
Elév. Semaine VII
Élév. Semaine VIII
Elév. Semaine IX
Elév. Semaine X
Elév. Semaine XI
Elév. Semaine XII
Elév. Semaine XIII
Elév. Semaine XIV
Elév. Semaine XV
Elév. Semaine XVI
Elév. Semaine XVII
Elév. Semaine XVIII
Elév. Semaine XIX
Elév. Semaine XX
Elév. Semaine XXI
Elév. Semaine XXII
Elév. Semaine XXIII
Elév. Semaine XXIV
Elév. Semaine XXV
Vie cachée
Concupiscence
Opuscules I
Opuscules II
Opuscules III

 



XIVE SEMAINE. LES EFFETS QUE PRODUIT SUR LES HOMMES LE VERBE INCARNÉ INCONTINENT  APRÈS   SON   INCARNATION.


 

 XIVe SEMAINE.  LES EFFETS QUE PRODUIT SUR LES HOMMES LE VERBE INCARNÉ
INCONTINENT  APRÈS   SON   INCARNATION.

 PREMIÈRE ÉLÉVATION.  Marie va visiter sainte  Elisabeth.

 IIe ELEVATION.  Jésus-Christ moteur secret des cœurs : divers mouvement
qu'il excite dans les âmes dont il s'approche.

 IIIe ÉLÉVATION.  Le cri de sainte Elisabeth et son humble étonnement.

 IVe ÉLÉVATION.  Le tressaillement de saint Jean.

 Ve ÉLÉVATION.  Le cantique de Marie : première partie.

 VIe ÉLÉVATION.  Seconde partie du cantique à ces paroles : Le
Tout-Puissant m'a fait de grandes choses.

 VIIe ÉLÉVATION.  Suite du cantique, où sont expliqués les effets
particuliers de l'enfantement de Marie et de l'incarnation du Fils de
Dieu.

 VIIIe ÉLÉVATION.  Effets particuliers de l'enfantement de Marie dans
les deux derniers versets de son cantique.

 IXe ÉLÉVATION.  Demeure de Marie avec Elisabeth.

 

 


PREMIÈRE ÉLÉVATION. Marie va visiter sainte  Elisabeth.

 

« Aussitôt après » que Marie eut conçu le Verbe dans son sein, « elle
part et marche avec promptitude dans le pays des montagnes de Judée (1),
» pour visiter sa cousine sainte Elisabeth. Ne sentons-nous point la
cause de cette promptitude, de cette élévation, de cette visite? Quand
on est plein de Jésus-Christ, on l'est en même temps de charité, d'une
sainte vivacité, de grands sentiments ; et l'exécution ne souffre rien
de languissant. Marie, qui porte la grâce avec Jésus-Christ dans son
sein , est sollicitée par un divin instinct à l'aller répandre dans la
maison de Zacharie, où Jean-Baptiste vient d'être conçu.

C'est aux supérieurs à descendre, à prévenir. Marie, qui se voyait
prévenue par le Verbe descendu en son sein, pouvait-elle n'être pas
touchée du désir de s'humilier et de descendre à son exemple? Jésus doit
être précédé par saint Jean au dehors ; mais au dedans c'est Jésus qui
le devait prévenir, qui le devait sanctifier. Il fallait que Jean reçût
de Jésus la première touche de la grâce.

Si vous sortez, âmes saintes et cachées, que ce soit pour chercher les
saintes, les Elisabeths qui se cachent elles-mêmes : allez vous cacher
avec elles : cette sainte société honorera Dieu et fera paraître ses
grâces.

Dans toutes les visites que nous rendons, imitons Marie : rendons-les en
charité : alors sous une simple civilité, il se cachera

 

1 _Luc.,_ I, 39.

 

238

 

de grands mystères : la grâce s'augmentera ou se déclarera par

l'humilité et par l'exercice d'une amitié sainte.

Cultivez , âmes pieuses, les devoirs de la parenté : soyez amies, femmes
chrétiennes, comme Marie et Elisabeth : que votre amitié s'exerce par la
piété : que vos conversations soient pleines de Dieu. Jésus sera au
milieu de vous, et vous sentirez sa présence.

Hommes, imitez aussi ces saintes et humbles femmes. O Dieu, sanctifiez
les visites: ôtez-en la curiosité, l'inutilité, la dissipation ,
l'inquiétude, la dissimulation et la tromperie : faites-y régner la
cordialité et le bon exemple.

 


IIe ELEVATION. Jésus-Christ moteur secret des cœurs : divers mouvement qu'il excite dans les âmes dont il s'approche.

 

Merveille de cette journée ! Jésus-Christ est caché, et c'est lui qui
opère tout : il ne paraît en lui aucun mouvement, il meut tout :
non-seulement Marie et Elisabeth , mais encore l'enfant qui est au sein
de sa mère, agissent sensiblement. Jésus , qui est en effet le moteur de
tout, est le seul qui paraît sans action , et son action ne se produit
que par celle qu'il inspire aux autres.

Nous voyons ici dans ces trois personnes sur lesquelles Jésus-Christ
agit, trois dispositions différentes des âmes dont il approche : « D'où
me vient ceci (1), » dit Elisabeth ? Elle s'étonne de l'approche de Dieu
; et n'en pouvant découvrir la cause dans ses mérites, elle demeure dans
l'étonnement des bontés de Dieu. En d'autres âmes Dieu opère le
transport et de saints efforts pour les faire venir à lui : c'est ce qui
paroitdans le tressaillement de saint Jean-Baptiste. Sa dernière
opération est la paix dans la glorification de la puissance divine ; et
c'est ce qui paraît dans la sainte Vierge. Voyons donc dans ces trois
personnes si diversement émues, ces trois divines opérations de
Jésus-Christ dans les âmes : dans Elisabeth l'humble étonnement d'une
âme de qui il

 

1 _Luc.,_ I, 43.

 

239

 

approche : dans Jean-Baptiste le saint transport d'une âme qu'il attire
: et dans Marie l'ineffable paix d'une aine qui le possède.

 


IIIe ÉLÉVATION. Le cri de sainte Elisabeth et son humble étonnement.

 

A la voix de Marie et à sa salutation , « l'enfant tressaillit dans son
sein ; et remplie du Saint-Esprit elle s'écria : » Ce grand cri de
sainte Elisabeth marque tout ensemble et sa surprise et sa joie ; « Vous
êtes bénie entre toutes les femmes, et le fruit de vos entrailles est
béni (1). » Celui que vous y portez est celui en qui toutes les nations
seront bénies : il commence par vous à répandre sa bénédiction : « D'où
me vient ceci, que la Mère de mon Seigneur vienne à moi (2) ? » Les âmes
que Dieu aborde, étonnées de sa présence inespérée, le premier mouvement
qu'elles font est de s'éloigner en quelque sorte comme indignes de cette
grâce : « Retirez-vous de moi, Seigneur, disait saint Pierre, parce que
je suis un pécheur (3). » Et le Centenier : « Seigneur, je ne suis pas
digne que vous entriez dans ma maison (4). » Dans un semblable
sentiment, mais plus doux, Elisabeth , quoique consommée dans la vertu,
ne laisse pas d'être surprise de se voir approchée par le Seigneur d'une
façon si admirable. « D'où me vient ceci, que la Mère de mon Seigneur, »
et qui le porte dans son sein, » vienne à moi? » Elle sent que c'est le
Seigneur qui vient lui-même, mais qui vient et qui agit par sa sainte
Mère : « A votre voix, dit-elle, l'enfant que je porte a tressailli dans
mon sein (5) : » il sent la présence du maître, et commence à faire
l'office de son précurseur, si ce n'est encore par la voix, c'est par ce
soudain tressaillement : la voix môme ne lui manque pas, puisque c'est
lui qui secrètement anime celle de sa mère. Jésus vient à lui par sa
mère , et Jean le reconnaît par la sienne.

Dans cette dispensation des grâces de Jésus-Christ sur Elisabeth et sur
sou fils à la Visitation de la sainte Vierge, l'avantage

 

2 _Luc.,_ I, 41, 42. — 2 _Ibid.,_ 13. — 3 Ibid., V, 8. — 4 _Matth.,_
VIII, 8— 5 _Luc.,_ I, 44.

 

240

 

est tout entier du côté de l'enfant. C'est ce qui fait dire à un saint
Père (1) : « Elisabeth a la première écouté la voix , mais Jean a le
premier senti la grâce : Elisabeth, » poursuit saint Ambroise, « a la
première aperçu l'arrivée de Marie : mais Jean a le premier senti
l'avènement de Jésus : » _Illa Mariœ, iste Domini sensit adventum_.

Elisabeth, comme revenue de son étonnement, s'étend sur la louange de la
sainte Vierge : « Vous êtes heureuse d'avoir cru : ce qui vous a été dit
par le Seigneur sera accompli (2) : » vous avez conçu vierge, vous
enfanterez vierge : votre Fils remplira le trône de David, et son règne
n'aura point de fin.

Croyons donc, et nous serons bienheureux comme Marie : croyons comme
elle au règne de Jésus et aux promesses de Dieu : Disons avec foi : «
Que votre règne arrive (3) : » Crions avec tout le peuple : « Déni soit
celui qui est venu au nom du Seigneur, et béni soit le règne de notre
père David (4) ! »

La béatitude est attachée à la foi : « Vous êtes bienheureuse d'avoir
cru : Vous êtes bienheureux, Simon, parce que ce n'est point la chair et
le sang qui vous ont révélé » la foi que vous devez annoncer, « mais que
c'est mon Père céleste (5). » Et où est cette béatitude de la foi ? «
Bienheureuse d'avoir cru : ce qui vous a été dit s'accomplira (6). »
Vous avez cru, vous verrez : vous vous êtes fiée aux promesses, vous
recevrez les récompenses : vous avez cherché Dieu par la foi , vous le
trouverez par la jouissance.

Mettons donc tout notre bonheur dans la foi : ne soyons point
insensibles à cette béatitude : c'est Jésus-Christ lui-même qui nous la
propose : la gloire de Dieu et sa volonté se trouvent dans notre
béatitude : ce qui est bienheureux est excellent en même temps : il est
plus heureux de donner que de recevoir, c'est-à-dire il est meilleur. On
est bienheureux de croire : il n'est rien de plus excellent ni de
meilleur que la foi, qui appuyée sur les promesses, s'abandonne aux
bontés de Dieu et ne songe qu'à lui plaire : _Beata quœ credidisti_.

 

1 _Ambr_., 1. II, _in Luc_., n. 23. — 2 _Luc.,_ I, 45. — 3 _Matth.,_ VI,
10. — 4 _Marc_., XI, 9, 10. — 5 _Matth.,_ XVI, 17. — 6 _Luc.,_ I, 45.

 

241

 


IVe ÉLÉVATION. Le tressaillement de saint Jean.

 

Quand l'âme dans son ignorance et ses ténèbres ressent les premières
touches de la divine présence, après ce premier étonnement par lequel
elle semble s'éloigner, rassurée par sa bonté, elle se livre à la
confiance et à l'amour. Elle sent je ne sais quels mouvements, souvent
encore confus et peu expliqués : ce sont des transports vers Dieu et des
efforts pour sortir de l'obscurité où l'on est, et rompre tous les liens
qui nous y retiennent. C'est ce que veut faire saint Jean, saisi d'une
sainte joie il voudrait parler ; mais il ne sait comment expliquer son
transport. Jésus-Christ qui en est l'auteur, en connaît la force ; et
quoiqu'en apparence il ne fasse rien, il se fait sentir au dedans par un
subit ravissement qu'il inspire à l'âme. Ame qui te sens saisie d'un si
doux sentiment, s'il ne t'est pas encore permis de parler, il t'est
permis de tressaillir!

Venez, Seigneur, venez me toucher d'un saint et inopiné désir d'aller à
vous. Que ce désir s'élève en moi aujourd'hui à la voix de votre mère :
faites-moi dire avec Elisabeth : « D'où me vient ceci? » Faites-moi dire
: Elle est « heureuse d'avoir cru, » et je veux imiter sa foi.
Faites-moi tressaillir comme Jean-Baptiste ; et enfant encore dans la
piété, recevez mes innocents transports. Je ne suis pas un Jean, en qui
votre grâce avance l'usage de la raison ; je suis un vrai enfant dans
mon ignorance : agréez mon bégaiement, et l'a, a, a de ma langue (1),
qui n'est pas encore dénouée : c'est vous du moins que je veux ; c'est à
vous seul que j'aspire ; et je ne puis exprimer ce que votre grâce
inspire à mon cœur.

 

1 _Jerem.,_ I, 2.

 

242

 


Ve ÉLÉVATION. Le cantique de Marie : première partie.

 

Ces premiers transports d'une âme qui sort d'elle-même et qui déjà ne se
connaît plus , sont suivis d'un calme ineffable , d'une paix qui passe
les sens et d'un cantique céleste.

« Mon âme glorifie le Seigneur, et mon esprit est ravi de joie en Dieu
mon Sauveur (1). » Que dirai-je sur ce divin cantique ? Sa simplicité,
sa hauteur qui passe mon intelligence m'invite plutôt au silence qu'à
parler. Si vous voulez que je parle, ô Dieu, formez vous-même mes
paroles.

Quand l'âme entièrement sortie d'elle, ne glorifie plus que Dieu et met
en lui toute sa joie, elle est en paix, puisque rien ne lui peut ôter
celui qu'elle chante.

« Mon âme glorifie : mon âme exalte le Seigneur : » après qu'elle s'est
épuisée à célébrer ses grandeurs, quoi qu'elle ait pensé, elle l'exalte
toujours le perdant de vue, et s'élevant de plus en plus au-dessus de
tout.

« Mon esprit est ravi de joie en Dieu mon Sauveur. » Au seul nom de
Sauveur, mes sens sont ravis ; et ce que je ne puis trouver en moi, je
le trouve en lui avec une inébranlable fermeté.

« Parce qu'il a regardé la bassesse de sa servante. » Si je croyais de
moi-même pouvoir attirer ses regards, ma bassesse et mon néant m'ôterait
le repos avec l'espérance. Mais puisque de lui-même , par pure bonté il
a tourné vers moi ses regards, j'ai un appui que je ne puis perdre, qui
est sa miséricorde par laquelle il m'a regardée, à cause qu'il est bon
et libéral.

Elle ne craint point après cela de reconnaître ses avantages, dont elle
a vu la source en Dieu et qu'elle ne peut plus voir que dans ce principe
: « Et voilà, dit-elle, que tous les siècles me reconnaîtront
bienheureuse. »

Ici étant élevée à une plus haute contemplation, elle commence

 

1 _Luc.,_ I, 46 et seq.

 

243

 

à joindre son bonheur à celui de tous les peuples rachetés, et c'est
comme la seconde partie de son cantique.

 


VIe ÉLÉVATION. Seconde partie du cantique à ces paroles : Le Tout-Puissant m'a fait de grandes choses.

 

« Celui qui seul est puissant a fait en moi de grandes choses, et son
nom est saint, et sa miséricorde s'étend d'âge en âge et de race en race
sur ceux qui le craignent (1) » Elle commence à voir que son bonheur est
le bonheur de toute la terre, et qu'elle porte celui en qui toutes les
nations seront bénies. Elle s'élève donc à la puissance et à la sainteté
de Dieu, qui est la cause de ces merveilles.

Celui qui est seul puissant a fait en moi un ouvrage seul digne de sa
puissance : un Dieu homme, une mère vierge, un enfant qui peut tout ; un
pauvre dépouillé de tout et néanmoins sauveur du monde, dompteur des
nations et destructeur des superbes.

« Et son nom est saint : » Dieu est la sainteté même : il est saint et
sanctifiant : et quand est-ce qu'il le paraît davantage, que lorsque son
Fils qui est aussi celui de Marie, répand la miséricorde . la grâce et
la sainteté d'âge en âge sur ceux qui le craignent?

Si nous voulons participer à cette grâce, soyons saints ; et publions en
même temps avec toutes les nations, que Marie est bienheureuse.

 


VIIe ÉLÉVATION. Suite du cantique, où sont expliqués les effets particuliers de l'enfantement de Marie et de l'incarnation du Fils de Dieu.

 

Pour expliquer de si grands effets, Marie en revient à la puissance de
Dieu : «  Il a, dit-elle, déployé la puissance de son bras : il a
dissipé ceux qui étaient enflés d'orgueil dans les pensées de leur cœur.
Il a renversé les puissants de dessus le trône , et il a élevé les
humbles (2). » Quand est-ce qu'il a fait toutes ces

 

1 _Luc.,_ I, 49, 50. — 2 _Ibid_., 51, 52.

 

244

 

merveilles, si ce n'est quand il a envoyé son Fils au monde, qui a
confondu les rois et les superbes empires par la prédication de son
Évangile ? Ouvrage où sa puissance a paru d'autant plus admirable , «
qu'il s'est servi de la faiblesse pour anéantir la force, et de ce qui
n'était pas pour détruire ce qui était, afin que ne parois-sant rien du
côté de l'homme (1), » on attribuât tout à la seule puissance de son
bras. C'est pourquoi il a paru au milieu des hommes comme n'étant rien.
Et lorsqu'il a dit : « Je vous loue, mon Père, Dieu du ciel et de la
terre, de ce que vous avez caché ces mystères aux sages et aux prudents,
et que vous les avez révélés aux petits (2) : » n'a-t-il pas
véritablement confondu les superbes et élevé ceux qui étaient vils à
leurs yeux et à ceux des autres ?

Marie elle-même en est un exemple : il l'a élevée au-dessus de tout,
parce qu'elle s'est déclarée la plus basse des créatures. Quand il s'est
fait une demeure sur la terre, ce n'a point été dans les palais des rois
: il a choisi de pauvres, mais d'humbles parents et tout ce que le monde
méprisait le plus pour en abattre la pompe. C'est donc là le propre
caractère de la puissance divine dans la nouvelle alliance, qu'elle y
fait sentir sa vertu par la faiblesse même.

« Il a rassasié les affamés, et il a renvoyé les riches avec les mains
vides (3). » Et quand ? Si ce n'est lorsqu'il a dit : « Heureux ceux qui
ont faim, car ils seront rassasiés (4) : Malheur à vous qui êtes
rassasiés, car vous aurez faim (5). » C'est ici qu'il faut dire avec
Marie : Mon âme glorifie le Seigneur et n'exalte que sa puissance, qui
va paraître par l'infirmité et par la bassesse.

C'est là que l'âme trouve sa paix, lorsqu'elle voit tomber toute la
gloire du monde, et Dieu seul demeurer grand.

 


VIIIe ÉLÉVATION. Effets particuliers de l'enfantement de Marie dans les deux derniers versets de son cantique.

 

Les palais et les trônes sont à bas : les cabanes sont relevées :

 

1 I _Cor.,_ I, 27-29. — 2 _Matth.,_ XI, 25. — 3 _Luc.,_  I, 53. — 4
_Matth.,_ V,  6. — 5 _Luc.,_ VI, 25.

 

245

 

toute fausse grandeur est anéantie : c'est un effet général de
l'enfantement de Marie dans toute la terre. Mais ne dira-t-elle rien de
la rédemption d'Israël et de ces brebis perdues de la maison de Jacob,
pour lesquelles son Fils a dit qu'il était venu? Ecoutons la fin du
divin cantique : « Il a pris en sa protection Israël son serviteur (1).
» Ce n'est point à cause des mérites dont se vantaient les présomptueux
: au contraire il a abattu le faste pharisaïque et les superbes pensées
des docteurs de la loi : il a reçu un Nathanaël, vrai Israélite, simple,
sans présomption comme sans fard et sans fraude : et voilà les
Israélites qu'il a protégés, à cause qu'ils mettaient leur confiance,
non point en eux-mêmes, mais en sa grande miséricorde : « Il s'est
souvenu des promesses qu'il a faites à Abraham et à sa postérité, » qui
doit subsister « aux siècles des siècles (2). »

Heureux que Dieu ait daigné s'engager avec nous par des promesses. Il
pouvait nous donner ce qu'il eut voulu : mais quelle nécessité de nous
le promettre, si ce n'est qu'il voulait, comme dit Marie, faire passer
d'âge en âge sa miséricorde, en nous sauvant par le don et nos pères par
l'attente. Attachons-nous donc avec Marie aux immuables promesses de
Dieu qui nous a donné Jésus-Christ. Disons avec Elisabeth : Nous sommes
heureux d'avoir cru : ce qui nous a été promis s'accomplira. Si la
promesse du Christ s'est accomplie tant de siècles après, doutons-nous
qu'à la fin des siècles tout le reste ne s'accomplisse? Si nos pères
avant le Messie ont cru en lui, combien maintenant devons-nous croire,
que nous avons Jésus-Christ pour garant de ces promesses?
Abandonnons-nous à ces promesses de grâce, à ces bienheureuses
espérances et noyons dedans toutes les trompeuses espérances dont le
monde nous amuse.

« Nous sommes les vrais enfants de la promesse : enfants selon la foi,
et non pas selon la chair (3) ; » qui ont été montrés à Abraham, non
point en la personne d'Ismaël, ni dans les autres enfants sortis
d'Abraham selon les lois de la chair et du sang, mais en la personne
d'Isaac qui est venu selon la promesse par grâce et par miracles.
Abraham a cru à cette promesse, « pleinement persuadé

 

1 _Luc.,_ I, 54. — 2 _Ibid.,_ 54, 55. — 3 _Galat_., IV, 28 ; _Rom.,_ IX,
7, 8.

 

246

 

et sachant très-bien que Dieu est puissant pour faire ce qu'il a promis
(1). » Il ne dit pas seulement qu'il prévoit ce qui doit arriver, mais
qu'il fait ce qu'il a promis : il a promis à Abraham des enfants selon
la foi : il les fait donc. Nous sommes ses enfants selon la foi : il
nous a donc faits enfants de foi et de grâce, et nous lui devons cette
nouvelle naissance. Si Dieu nous a faits par grâce selon sa promesse, ce
n'a point été par nos œuvres, mais par sa miséricorde qu'il nous a
produits et régénérés. Nous sommes ceux que voyait Marie, quand elle
voyait la postérité d'Abraham : nous sommes ceux au salut de qui elle a
consenti, quand elle a dit : « Qu'il me soit fait selon votre parole
(2). » Elle nous a tous portés dans son sein avec Jésus-Christ, en qui
nous étions.

Chantons donc sa béatitude avec la nôtre : publions qu'elle est
bienheureuse, et agrégeons-nous à ceux qui la regardent comme leur mère.
Prions cette nouvelle Eve qui a guéri la plaie de la première, au lieu
du fruit défendu dont nous sommes morts, de nous montrer le fruit béni
de ses entrailles. Unissons-nous au saint cantique, où Marie a chanté
notre délivrance future. Disons avec saint Ambroise : « Que l'âme de
Marie soit en nous pour glorifier le Seigneur : que l'esprit de Marie
soit en nous pour être ravis de joie en Dieu notre Sauveur (3). » Comme
Marie, mettons notre paix à voir tomber toute la gloire du monde, et le
seul règne de Dieu exalté, et sa volonté accomplie.

 


IXe ÉLÉVATION. Demeure de Marie avec Elisabeth.

 

« Marie demeura environ trois mois dans la maison d'Elisabeth, et elle
retourna en sa maison (4). » La charité ne doit pas être passagère :
Marie demeure trois mois avec Elisabeth : quiconque porte la grâce ne
doit point aller en courant, mais lui donner le temps d'achever son
œuvre. Ce n'est pas assez que l'enfant ait tressailli une fois, ni
qu'Elisabeth ait crié : « Vous êtes heureuse, » il faut fortifier
l'attrait de la grâce, et c'est ce qu'a fait Marie, ou plutôt

 

1 _Rom.,_ IV, 20, 21.— 3 _Luc.,_ I, 38. — 3 _Ambr_., _in Luc.,_ l. II,
n. 26.— 4 _Luc.,_ I, 58.

 

247

 

ce qu'a fait Jésus en demeurant trois mois avec son précurseur.

Regardons ce saint précurseur sanctifié dès le ventre de sa mère. Gomme
les autres il était conçu dans le péché : mais Jésus-Christ a voulu
prévenir sa naissance, et la rendre sainte. Il a voulu qu'il fît son
office de précurseur jusque dans le ventre de sa mère. Il ne faut pas
s'étonner si dès le commencement de l'évangile de l'apôtre saint Jean,
on voit Jean-Baptiste si étroitement uni à Jésus. Jean-Baptiste, qui «
n'était pas la lumière , » devait pourtant et devait avant sa naissance,
et dès le sein de sa mère, « rendre témoignage à la lumière (1) » encore
cachée. Il n'était pas la lumière, puisque conçu dans le péché, il
attendait pour en sortir la présence du Sauveur. « Il y avait une
véritable lumière qui illumine tout homme venant au monde (2) ; » et
c'est par cette lumière que Jean a été illuminé, afin que nous
entendions que s'il montre Jésus-Christ au monde, c'est par la lumière
qu'il reçoit de Jésus-Christ même. O Marie, ô Elisabeth, ô Jean, que
vous nous montrez aujourd'hui de grandes choses! Mais, ô Jésus, Dieu
caché, qui sans paraître faites tout dans cette sainte journée, je vous
adore dans ce mystère et dans toutes les œuvres cachées de votre grâce.

Savoir si la sainte Vierge vit la naissance de saint Jean, l'Évangile
n'a pas voulu nous le découvrir. Elisabeth était dans son sixième mois,
quand Marie la vint visiter : elle fut environ trois mois avec elle :
elle était donc ou à terme ou bien près de son terme : et l'Évangile
ajoute aussi que « le temps d'Elisabeth s'accomplit (3), » insinuant
selon quelques-uns qu'il s'accomplit pendant que Marie était avec elle.
Mais qui osera l'assurer, puisque l'Évangile semble avoir évité de le
dire? Quoi qu'il en soit, ou Marie attachée à sa solitude, et prévoyant
l'abord de tout le monde au temps de l'enfantement d'Elisabeth, le
prévint par sa retraite : ou si elle est demeurée avec tous les autres,
elle y a été humble et cachée, inconnue, sans s'être fait remarquer dans
une si grande assemblée et contente d'avoir agi envers ceux à qui Dieu
l'avait envoyée. O humilité! ô silence, qui n'a été interrompu que par
un cantique inspiré de Dieu, puissé-je vous imiter toute ma vie !

 

1 _Joan.,_ I, 8. — 2 _Ibid_., 9. —3 _Luc.,_ I, 57.

 

 

[Précédente]

[Accueil]

[Suivante]

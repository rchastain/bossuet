
SERMON POUR LE SAMEDI APRÈS LES CENDRES, SUR L'ÉGLISE (_A_).


_Erat navis in medio mari_.

Le navire était au milieu de la mer. _Marc_., VI, 47.

Le mystère de l'Évangile, c'est l'infirmité et la force unies, la
grandeur et la bassesse assemblées. Ce grand mystère (_b_), Messieurs ,
a paru premièrement en notre Sauveur, où la puissance ; divine et la
faiblesse humaine s'étant alliées, composent ensemble ce tout admirable
que nous appelons Jésus-Christ ; mais ce qui paraît en sa personne, il a
voulu aussi le faire éclater dans l'Église qui est son corps, « où une
partie triomphe par les miracles, l'autre succombe sous les outrages
qu'elle reçoit : » Unum

(_a_) _Premier point_. — Dans l'homme, un esprit de contrariété à
l'Évangile.

Église victorieuse dans les persécutions. _Sœpe expugnaverunt me_
(_Psal._ CVIII, 3).

_Second point_.—Curiosité. Ses tempêtes. _Ascendant usque ad coelos_
(_Psal._ CVI, 26). Ses bornes comme à la mer.

Autorité, infaillibilité de l'Église.

_Troisième point_. — Église diminuée en sa foi par la multiplication de
ses enfants. Salvien. _Multiplicati sunt super numerum_ (_Psal._ XXXIX,
6).

Pourquoi les bons parmi les méchants ?

Nulle impatience de ce mélange. 

Ce   sermon a été prêché le 21 février 1660, dans la maison des
_Nouveaux Catholiques_.

La maison des Nouveaux Catholiques rendait aux hommes qui embrassaient
la foi les mêmes services que la maison des _Nouvelles Catholiques_
rendait aux femmes dans la même circonstance. Ces deux établissements
avaient été dotés par le marquis de Morangis, et le premier se trouvait
rue de Seine-Saint-Victor. La liste des prédicateurs qui devaient
prêcher le Carême de 1660, après avoir nommé Bossuet pour l'église des
Minimes, ajoute: « Aux filles Nouvelles Catholiques le premier vendredi
de Carême, M. Bossuet ; aux hommes _Nouveaux Catholiques,_ le premier
samedi, M. Bossuet. » Voir la note du sermon précédent.

(_b_) _Var._ : Ce mystère admirable.

532

_horum coruscat miraculis, aliud succumbit injuriis_ (1). C'est pourquoi
nous voyons dans son Écriture (2) que tantôt cette Église est
représentée comme une maison bâtie sur une pierre immobile, et tantôt
comme un navire qui flotte au milieu des ondes au gré des vents et des
tempêtes ; si bien qu'il paraît, chrétiens, qu'il n'est rien de plus
faible que cette Église, puisqu'elle est ainsi agitée ; et qu'il n'est
rien aussi de plus fort, puisqu'on ne la peut jamais renverser et
qu'elle demeure toujours immuable malgré les efforts de l'enfer.
L'évangile de cette journée nous la représente « parmi les flots : »
_Erat navis in medio mari,_ « portée deçà et delà par un vent contraire
: » _Erat enim ventus contrarius_ (3). Et ce qui est de plus surprenant,
c'est que Jésus, qui est son appui, semble l'abandonner à la tempête ;
il s'approche « et il veut passer, » comme si son péril ne le touchait
pas : _Et volebat prœterire eos_ (4). Toutefois ne croyez pas qu'il
l'oublie ; il permettra bien que les flots l'agitent, mais non pas
qu'ils la submergent ni qu'ils l'engloutissent. Il commande aux vents,
et « ils s'apaisent ; il entre dans le navire, et il arrive sûrement au
port : » _Ascendit in navim, et cessavit ventus et applicuerunt_ (5),
afin, Messieurs, que nous entendions qu'il n'y a rien à craindre pour
l'Église, parce que le Fils de Dieu la protège. J'entreprends
aujourd'hui de vous faire voir cette vérité importante ; et afin que
vous en soyez convaincus plus facilement, je laisse les raisonnements
recherchés pour l'établir solidement par expérience.

Considérez en effet, Messieurs, les trois furieuses tempêtes qui ont
troublé l'état de l'Église. Aussitôt qu'elle a paru sur la terre,
l'infidélité s'est élevée, et elle a excité les persécutions ; après, la
curiosité s'est émue, et elle a fait naître les hérésies ; enfin la
corruption des mœurs a suivi, qui a si étrangement soulevé les flots, «
que la nacelle y a paru (_a_) presque enveloppée : » _Ita ut navicula
operiretur fluctibus_ (6). Voilà, mes frères, les trois tempêtes qui ont
successivement tourmenté l'Église (_b_). Les infidèles se sont assemblés
pour la détruire par les fondements ; les hérétiques en sont

1 S. Léo, serm. III _De Pass. Domin_., cap. II. — 2 _Luc.,_ VI, 48. — 3
_Marc_., vi, 48. — 4 _Ibid._ — 5 _Ibid_. 51, 53. — 6 _Matth.,_ VIII, 24.

(_a_) Par. : Y a été. — (_b_) Dont l'Église a été tourmentée.

533

sortis pour lui arracher ses enfants et lui déchirer les entrailles ; et
si enfin les mauvais chrétiens sont demeurés dans son sein, ce d'est que
pour lui porter le venin jusque dans le cœur (_a_). Il faut donc bien,
mes frères, que cette Église soit bien appuyée et bien fortement
établie, puisqu'au milieu de tant de traverses, malgré l'effort des
persécutions, elle s'est soutenue par sa fermeté ; malgré les attaques de
l'hérésie, elle a été la colonne de la vérité ; malgré la licence des
mœurs dépravées, elle demeure le centre de la charité. Voilà le sujet de
cet entretien et les trois points de cette méditation.


PREMIER POINT.

Comme l'Église n'a plus à souffrir la tempête des persécutions, je
passerai légèrement sur cette matière ; et néanmoins je ne laisserai
pas, si Dieu le permet, de toucher des vérités assez importantes. La
première sera, chrétiens, qu'il ne faut pas s'étonner si l'Église a eu à
souffrir quand elle a paru sur la terre, ni si le monde l'a combattue de
toute sa force. Il était impossible qu'il ne fût ainsi ; et vous en
serez convaincus, si vous savez connaître ce que c'est que l'homme. Je
dis donc que nous avons tous dans le fond du cœur un principe
d'opposition et de répugnance à toutes les vérités divines ; en telle
sorte que l'homme laissé à lui-même, non-seulement ne peut les entendre,
mais qu'ensuite il ne les peut souffrir ; et qu'en étant choqué au
dernier point, il est comme forcé de les combattre. Ce principe de
répugnance s'appelle dans l'Écriture « infidélité (1), » ailleurs «
esprit de défiance (2), » ailleurs «esprit d'incrédulité (3). » Il est
dans tous les hommes ; et s'il ne produit pas en nous tous ses effets,
c'est la grâce de Dieu qui l'empêche.

Si vous remontez jusqu'à l'origine, vous trouverez, Messieurs, que deux
choses produisent en nous cette répugnance : la première, c'est
l'aveuglement ; la seconde, la présomption. L'aveuglement, Messieurs,
nous est représenté dans les Écritures par une façon de parler
admirable. Elles disent que « les pécheurs ont

1 _Luc.,_ IX, 41, etc. — 2 _Ephes_., II, 2. — 3 _Coloss_., III, 6.

(_a_) _Var._ : Et enfin les mauvais chrétiens ne sont demeurés dans son
spin qu'afin de porter le venin jusque dans son cœur.

534

oublié Dieu : _» Omnes gentes quœ obliviscimtur Deum_.— _Obliti sunt
verba tua inimici mei :—Intelligite hœc, qui obliviscimini Deum_ (1).
Que veut dire cet oubli, mes frères? Il est bien aisé de le comprendre :
c'est que Dieu, à la vérité, avait éclairé l'homme de sa connaissance,
mais l'homme a fermé les yeux à cette lumière ; il s'est laissé mener
par ses sens, peu à peu il n'a plus pensé à ce qu'il ne voyait pas, il a
oublié aisément ce à quoi il ne pensait pas. Voilà Dieu dans l'oubli,
voilà ses vérités effacées ; ne lui en parlez pas, c'est un langage
qu'il ne connaît plus : _Obliti sunt verba tua inimici mei_. C'est
pourquoi la même Écriture voulant aussi nous représenter de quelle sorte
les hommes retournent à Dieu : _Reminiscentur_ : « Ils se souviendront ;
» et ensuite qu'arrivera-t—il ? _Et convertentur ad Dominum_ (2) . — «
Ah ! ils se convertiront au Seigneur. » Quoi ! ils l'avaient donc
oublié, leur Dieu, leur Créateur, leur Époux, leur Père? Oui, mes
frères, il est ainsi ; ils en ont perdu le souvenir. Cela va bien loin,
si vous l'entendez ; toute la connaissance de Dieu, toutes les idées de
ses vérités, l'oubli comme une éponge a passé dessus et les a
entièrement effacées ; ou s'il en reste encore quelques traces, elles
sont si obscures qu'on n'y connaît rien. Voyez durant le règne de
l'idolâtrie , durant qu'elle régnait sur toute la terre.

Ce serait peu que ce long oubli pour nous exciter à la résistance, si
l'orgueil ne s'y était joint ; mais il est arrivé pour notre malheur
que, quoique l'homme soit aveugle à l'extrémité, il est encore plus
présomptueux. En quittant la sagesse de Dieu, il s'est fait une sagesse
à sa mode ; il ne sait rien, et croit tout entendre : si bien que tout ce
qu'on lui dit qu'il ne conçoit pas, il le prend pour un reproche de son
ignorance, il ne le peut souffrir, il s'irrite ; si la raison lui
manque, il emploie la force, il emprunte les armes de la fureur pour se
maintenir en possession de sa profonde et superbe ignorance. Jugez où
les vérités évangéliques, si hautes, si majestueuses, si impénétrables,
si contraires au sens humain et à la raison préoccupée, ont dû pousser
cet aveugle présomptueux , je veux dire l'homme, et quelle résistance il
fallait attendre d'une indocilité si opiniâtre. Voyez-la par expérience
en la personne

1 _Psal._ IX, 18 ; CXVIIl, 139 ; XLIX, 22. — 2 _Psal._ XXI, 28.

535

de notre Sauveur. Qu'aviez-vous fait, ô divin Jésus, pour exciter contre
vous ce scandale horrible? Pourquoi les peuples se troublent-ils (1) ?
pourquoi frémissent-ils contre vous avec une rage si désespérée ?
Chrétiens, voici le crime du Sauveur Jésus. Il a enseigné les vérités de
son Père (2) ; ce qu'il a vu dans le sein de Dieu, il est venu
l'annoncer aux hommes (3). Ces aveugles ne l'ont pas compris, et ils
n'ont pas pu le comprendre : _Animalis homo non potest intelligere_ (4).
Ecoutez comme il leur reproche : « Pourquoi ne connaissez-vous pas mon
langage ? Parce que vous ne pouvez pas prêter l'oreille à mon discours :
» _Quare loquelam meam non rognoscitis ? Quia non potestis audire
sermonem meum,_(5).

Mais peut-être ne l'entendant pas, ils se contenteront de le mépriser.
Non, mes frères ; ce sont des superbes ; tout ce qu'ils n'entendent pas
ils le combattent, « tout ce qu'ils ignorent ils le blasphèment (6). »
C'est pourquoi Jésus-Christ leur dit : Vous me voulez tuer, méchants que
vous êtes, parce que mon discours ne prend point en vous : » _Quœritis
me interficere, quia sermo meus non capit in vobis_ (7). Quelle fureur,
mes frères, d'entreprendre de tuer un homme, parce qu'on n'entend pas
son discours ! Mais il n'y a pas sujet de s'en étonner : il parlait des
vérités de son Père à des ignorants opiniâtres. Comme ils n'entendaient
pas ce divin langage, car il n'y a que les humbles qui l'entendent , ils
ne pouvaient qu'être étourdis de la voix de Dieu ; et c'est ce qui les
excitait à la résistance. Plus les vérités étaient hautes, et plus leur
raison superbe était étourdie, et plus leur folle résistance était
enflammée. Il ne faut donc pas trouver étrange si Jésus leur prêchant,
comme il dit lui-même, « ce qu'il avait appris au sein de son Père (8),
» ils se portent à la dernière fureur et se résolvent de le mettre à
mort par un infâme supplice : _Quia sermo meus non capit in vobis._

Après cela pouvez-vous douter de ce principe d'opposition, qu'une
ignorance altière et présomptueuse a gravé dans le cœur des hommes
contre Dieu et ses vérités? Jésus-Christ l'a éprouvé le premier ; son
Église paraissant au monde pour soutenir la

1 _Psal._ II, 1. — 2 _Joan.,_ VIII, 28. — 3 _Ibid.,_ 18. — 4 I _Cor.,_
II, 14. — 5 _Joan.,_ VIII, 43. — 6 _Jud_., 10. — 7 _Joan.,_ VIII, 37. —
8 _Ibid.,_ 38.

536

même doctrine par laquelle ce divin Maître avait scandalisé les
superbes, pouvait-elle manquer d'ennemis ? Non, mes frères, il n'est pas
possible ; puisque la foi qu'elle professe vient étonner le monde par sa
nouveauté, troubler les esprits par sa hauteur, effrayer les sens par sa
sévérité, qu'elle se prépare à souffrir. Il faut qu'elle soit en haine à
tout le monde ; et vous le savez, chrétiens, c'est une chose
incompréhensible, ce qu'a souffert l'Église de Dieu durant près de
quatre cents ans sous les empereurs infidèles. Il serait infini de le
raconter ; concevez seulement ceci, qu'elle était tellement chargée et
de la haine publique et des imprécations de toute la terre, qu'on
l'accusait hautement de tous les désordres du monde. Si la pluie
manquait aux biens de la terre, si les Barbares faisaient quelques
courses et ravageaient, si le Tibre se débordait, les chrétiens en
étaient la cause ; et tout le monde disait qu'il n'y avait point de
meilleure victime pour apaiser la colère des dieux, que de leur immoler
les chrétiens « par tout ce que la rage et le désespoir pouvaient
inventer de plus cruel : » _Per atrociora ingenia pœnarum_ (1).
Qu'aviez-vous fait, Église, pour être traitée de la sorte? J'en pourrais
rapporter plusieurs causes ; mais celle-ci est la principale : elle
faisait profession de la vérité, et de la vérité divine ; de là ces cris
de la haine, de là ces injustes persécutions. Si l'Église en a été
agitée, elle n'en a pas été surprise. Elle sait bien connaître la main
qui l'appuie, et elle se sent à l'épreuve de toutes sortes d'attaques.

Et à ce propos, chrétiens, saint Augustin se représente que les fidèles,
étonnés de voir durer si longtemps la persécution, s'adressent à
l'Église leur mère et lui en demandent la cause. Il y a longtemps, ô
Église, que l'on frappe sur vos pasteurs, et les troupeaux sont
dispersés. Dieu vous a-t-il oubliée ? (_a_) Les vents grondent, les
flots se soulèvent ; vous flottez deçà et delà battue des ondes et de la
tempête ; ne craignez-vous pas d'être abîmée ? La réponse de l'Église
est dans le psaume CXXVIII. — Mes enfants, je ne m'étonne pas de tant de
traverses ; j'y suis accoutumée dès mon enfance : _Sœpe expugnaverunt me
à juventute meâ_ (2) : « Ces

1 Tertull., _De Resurr. carn_., n. 8. — 2 _Psal._ CXXVIII, 1.

(_a_) _Note marg._ : Si ce n'eût été qu'en passant..... Tant de
siècles.....

537

mêmes ennemis qui m'attaquent m'ont déjà persécutée dès ma jeunesse. »
L'Église a toujours été sur la terre ; dès sa plus tendre enfance elle
était représentée en Abel, et il a été tué par Caïn son frère. Elle a
été représentée en Enoch, et il a fallu le tirer du milieu des impies :
_Translatus est ab iniquis_ (1), sans doute parce qu'ils ne pouvaient
souffrir son innocence. La famille de Noé, il a fallu la délivrer du
déluge. Abraham, que n'a-t-il pas souffert des impies, son fils Isaac
d'Ismaël, Jacob d'Esau? Celui qui était selon la chair, n'a-t-il pas
persécuté celui qui était selon l'esprit (2)? Moïse, Elie, les
prophètes, Jésus-Christ et les apôtres? Par conséquent, mon fils, dit
l'Église, ne t'étonne pas de ces violences : _Sœpe expugnaverunt me à
juventute meâ : numquid ideo non perveni ad senectutem_ (3) ? Regarde
mon antiquité, considère mes cheveux gris ; « ces cruelles persécutions
dont on a tourmenté mon enfance, m'ont-elles empêchée de parvenir à
cette vénérable vieillesse? » Si c'était la première fois, j'en serais
peut-être troublée ; maintenant la longue habitude fait que mon cœur ne
s'en émeut pas. Je laisse faire aux pécheurs : _Supra dorsum meum
fabricaverunt peccatores_ (4) : je ne tourne pas ma face contre eux,
pour m'opposer à leur violence ; je ne fais que tendre le dos ; ils
frappent cruellement, et je souffre sans murmurer. C'est pourquoi ils ne
donnent point de bornes à leur furie : _Prolongaverunt iniquitatem
suam_. Ma patience sert de jouet à leur injustice ; mais je ne me lasse
point de souffrir, et je me souviens de celui qui a abandonné ses joues
aux soufflets et n'a pas détourné sa face des crachats : » _Faciem meam
non averti ab increpantibus et conspuentibus in me_ (5). Quoique je
semble toujours flottante, ne t'étonne pas ; la main toute-puissante qui
me sert d'appui saura bien m'empêcher d'être submergée. — Que si Dieu la
soutient avec tant de force contre la violence, pourrez-vous croire,
Messieurs, qu'il la laisse accabler par les hérésies? Non, Messieurs ;
ne le croyez pas : c'est ma seconde partie.

1 _Hebr_., XI, 5. — 2 _Galat.,_ IV, 29. — 3 _In__ Psal._ CXXVIII, n. 2,
3.— 4 _Psal._ CXXVIII, 3. — 5 _Isa_., I, 6.

538


SECOND POINT.

La seconde tempête de l'Église, c'est la curiosité qui l'excite :
curiosité, chrétiens, qui est la peste des esprits, la ruine de la piété
et la mère des hérésies. Pour bien entendre cette vérité, il faut
remarquer avant toutes choses que la sagesse divine a donné des bornes à
nos connaissances. Car comme cette Providence infinie voyant que les
eaux de la mer se répandraient par toute la terre et en couvriraient
toute la surface, lui a prescrit un terme qu'il ne lui permet pas de
passer : ainsi sachant que l'intempérance des esprits s'étendrait
jusqu'à l'infini par une curiosité démesurée, il lui a marqué des
limites auxquelles il lui ordonne d'arrêter son cours. « Tu iras,
dit-il, jusque-là, et tu ne passeras pas plus outre : » _Usque hue
gradieris, et non procèdes ampliùs, et hic confringes tumentes fluctus
tuos_ (1). C'est pourquoi Tertullien a dit sagement que le chrétien ne
veut savoir que fort peu de choses, parce que, poursuit ce grand homme,
les choses certaines sont en petit nombre : _Christiano paucis ad
scientiam veritatis opus est, nam et certa semper in paucis_ (2). Il ne
se veut pas égarer dans les questions infinies qui sont défendues par
l'Apôtre : _Infinitas quœstiones devita_ (3) ; il se resserre humblement
dans les points que Dieu a révélés à son Église ; et ce qu'il n'a pas
révélé, il trouve de la sûreté à ne le savoir pas ; il déteste la vaine
science que l'esprit humain usurpe, et il aime la docte ignorance que la
loi divine prescrit : « C'est tout savoir, dit-il, que de n'en pas
savoir davantage : » _Nihil ultra scire, omnia scire est_ (4).

Quiconque se tient dans ces bornes et sait régler sa foi par ce qu'il
apprend de Dieu par l'Église, ne doit pas appréhender la tempête ; mais
la curiosité des esprits superbes ne peut souffrir cette modestie : «Ses
flots s'élèvent, dit l'Écriture, ils montent jusqu'aux deux, ils
descendent jusqu'aux abîmes : » _Exaltati sunt fluctus ejus, ascendunt
usque ad cœlos et descendunt usque ad abyssos_ (5). Voilà une agitation
bien violente ; c'est une vive image des esprits curieux. Leurs pensées
vagues et agitées se poussent

1 Job, XXXVIII, 11. — 2 _De Animâ,_ n. 2. — 3 Tit., III, 9. — 4
Tertull., _De Prœscr. advers. Hœres_., n. 14. — 5 _Psal._ CVI, 25, 26.

539

comme des flots les unes les autres ; elles s'enflent, elles s'élèvent
démesurément : il n'y a rien de si élevé dans le ciel, ni rien de si
caché dans les profondeurs de l'enfer où ils ne s'imaginent de pouvoir
atteindre : _Ascendunt usque ad cœlos_ ; et les conseils de sa
Providence, et les causes de ses miracles, et la suite impénétrable de
ses mystères, ils veulent tout soumettre à leur jugement : _Ascendunt_.
Malheureux, qui s'agitant de la sorte, ne voient pas qu'il leur arrive
comme à ceux qui sont tourmentés par la tempête : _Turbati sunt, et moti
sunt sicut ebrius_ : « Ils sont troublés comme des ivrognes ; » la tête
leur tourne dans ce mouvement : _Et omnis sapientia eorum devorata est_
(1) : « Là toute leur sagesse se dissipe ; » et ayant malheureusement
perdu la route, ils se heurtent contre des écueils, ils se jettent dans
les abîmes, ils s'égarent dans des hérésies. Àrius, Nestorius, votre
curiosité vous a perdus. Voilà la tempête élevée par la curiosité des
hérétiques : c'est par là qu'ils séduisent les simples, parce que, dit
saint Augustin (2) « toute âme ignorante est curieuse : » _Omnis anima
indocta curiosa est_ : — Cela est nouveau, écoutons. — Arius, Nestorius,
etc., pourquoi cherchez-vous ce qui ne se peut pas trouver? _Ampliùs
quœrere non licet, quàm quod inveniri licet_ (3).

Pour empêcher les égarements de cette curiosité pernicieuse, le seul
remède, mes frères, c'est d'écouter la voix de l'Église et de soumettre
son jugement à ses décisions infaillibles. Je parle à vous, enfants
nouveaux nés que l'Église a engendrés : c'est sur la fermeté de cette
Église qu'il faut appuyer vos esprits, qui seraient flottants sans ce
soutien. Êtes-vous curieux de la vérité? voulez-vous voir? voulez-vous
entendre? Voyez et écoutez dans l'Église : _Sicut audivimus, sic
vidimus_ : « Nous avons ouï et nous avons vu, » dit David ; et où? _In
civilate Domini virtutum_ (4) : « En la cité de notre Dieu, »
c'est-à-dire en sa sainte Église. « Celui qui est hors de l'Église, dit
saint Augustin, quelque curieux qu'il soit, de quelque science qu'il se
vante, il ne voit ni n'entend ; quiconque est dans l'Église, il n'est ni
sourd ni aveugle : » _Extra illam qui est, nec audit nec videt ; in illà
qui est, nec surdus nec cœcus est_ (5).

1 _Psal._ CVI, 27. — 2 _De Agon. Christ_., n. 4. — 3 Tertull., _De
Animà,_ n. 2. — 4 _Psal._ XLVII, 9. — 5 _In__ Psal_. XLVII, n. 7.

540

Donc s'il est ainsi, chrétiens, que notre curiosité n'aille pas plus
loin. L'Église a parlé, c'est assez : cet homme est sorti de l'Église ;
il prêche, il dogmatise, il enseigne : — Que dit-il? que prêche-t-il?
quelle est sa doctrine? — O homme vainement curieux! je ne m'informe pas
de sa doctrine ; il est impossible qu'il enseigne bien, puisqu'il
n'enseigne pas dans l'Église. Un martyr illustre, un docteur
très-éclairé, saint Cyprien... Antonianus, un de ses collègues, lui
avait écrit au sujet de Novatien, schismatique, pour savoir de lui par
quelle hérésie il avait mérité la censure ; le saint docteur lui fait
cette belle réponse : _Desiderasti ut rescriberem tibi quam hœresim
Novatianus introduxisset.....Quisque ille fuerit, multùm de se licet
jactans et sibi plurimùm vindicans, profanus est, alienus est, foris
est_ (1) : « Pour ce qui regarde Novatien, duquel vous désirez que je
vous écrive quelle hérésie il a introduite, sachez premièrement que nous
ne devons pas même être curieux de ce qu'il enseigne, puisqu'il enseigne
hors de l'Église ; quel qu'il soit et de quoi qu'il se vante, il n'est
pas chrétien, n'étant pas en l'Église de Jésus-Christ. »

L'orgueil des hérétiques s'élève : Quoi! je croirai sur la foi d'autrui!
Je veux voir, je veux entendre moi-même. — Langage superbe!
Reconnaissez-le, mes chers frères ; c'est celui que vous parliez
autrefois. L'Église l'a dit : n'est-ce pas assez? — Mais elle se peut
tromper ? — Enfant qui déshonores ta mère, en quelle Écriture as-tu lu
que l'Église puisse tromper ses enfants? Tu reconnais qu'elle est mère ;
elle seule peut engendrer les enfants de Dieu ; si elle peut les
engendrer, qui doute qu'elle puisse les nourrir? Certes la terre, qui
produit les plantes, leur donne aussi leur nourriture ; la nature ne fait
jamais une mère, qu'elle ne fasse en même temps une nourrice. L'Église
sera-t-elle seule qui engendrera des enfants et n'aura point de lait à
leur donner? Ce lait des fidèles, c'est la vérité, c'est la parole de
vie. Enfants dénaturés, si j'ai des entrailles qui vous ont portés, j'ai
des mamelles pour vous allaiter (_a_) : voyez, voyez le lait qui en
coule, la parole de vérité qui en distille ; approchez-vous, sucez et
vivez, et ne portez pas

1 Cypr., Epist. LII _ad Anton_., p. 66, 68.

(_a_) _Var._ : Qui sortez des entrailles et rejetez les mamelles,
voyez...

541

votre bouche à des sources empoisonnées. — Mais il faut connaître quelle
est cette Église.— Ah! qu'il est bien aisé d'exclure la vôtre dressée de
nouveau, ô Église bâtie sur le sable! Vous croyez, ô divin Jésus, avoir
bâti sur la pierre ; c'est sur un sable mouvant : c'est la confession de
foi. Donc votre édifice est tombé par terre, il a fallu que Luther et
Calvin vinssent le dresser de nouveau. Mes enfants, respectez mes
cheveux gris ; voyez cette antiquité vénérable : je ne vieillis pas,
parce que je ne meurs jamais ; mais je suis ancienne. Pourquoi vous
vantez-vous de m'avoir rétablie? Quoi ! vous avez fait votre mère ! Mais
si vous l'avez faite, d'où êtes-vous nés? Et vous dites que je suis
tombée! Je suis sortie de tant de périls.

Laissons-les errer, mes frères ; Dieu n'a perdu .pour cela pas un des
siens. Ils étaient de la paille, et non du bon grain : le vent a
soufflé, et la paille s'en est allée ; « ils s'en sont allés en leur
lieu (1) : ils étaient parmi nous, mais ils n'étaient point des nôtres
(2). » Pour nous, enfants de l'Église, et vous que l'on avait exposés
dehors comme des avortons, et qui êtes enfin rentrés dans son sein,
apprenez à n'être curieux qu'avec l'Église, à ne chercher la vérité
qu'avec l'Église, et retenez cette doctrine. Dieu aurait pu sans doute,
car que peut-on dénier à sa puissance? il aurait pu nous conduire à la
vérité par nos connaissances particulières ; mais il a établi une autre
conduite ; il a voulu que chaque particulier fit discernement de la
vérité, non point seul, mais avec tout le corps et toute la communion
catholique, à laquelle son jugement doit être soumis. Cette excellente
police est née de l'ordre de la charité, qui est la vraie loi de
l'Église. Car si quelqu'un cherchait en particulier, et si les
sentiments se divisaient, les cœurs pourraient enfin être partagés. Mais
pour nous unir tous ensemble par le lien d'une charité indissoluble,
pour nous faire chérir davantage la communion et la paix, il a établi
cette loi. Voulez-vous entendre la vérité, allez au sein de l'unité, au
centre de la charité ; c'est l'imité catholique qui sera la chaste
mamelle d'où coulera sur vous le lait

(_a_) de la doctrine évangélique, tellement que l'amour de la

1 Act., I, 25. — 2 I Joan., II, 19.

(_a_) _Var._ : D'où vous prendrez le lait.

342

vérité est un nœud qui nous lie à l'imité et à la société fraternelle.
Nous sommes membres d'un même corps : cherchons tous ensemble, laissons
faire les fonctions à chaque membre, laissons voir les yeux, laissons
parler la bouche. Il y a des pasteurs à qui le Saint-Esprit même a
appris à dire sur toutes les contestations qui sont nées : « Il a plu au
Saint-Esprit et à nous (1). » Arrêtons-nous là, chrétiens, et « ne
soyons pas plus sages qu'il ne faut ; mais soyons sages avec retenue (2)
» et selon la mesure qui nous est donnée.


TROISIÈME POINT.

Jusqu'ici, mes frères, tout ce que j'ai dit est glorieux à l'Église :
j'ai publié sa constance dans les tourments, sa victoire sur les
hérésies ; tout cela est grand et auguste ; mais que ne puis-je maintenant
vous cacher sa honte, je veux dire les mœurs dépravées de ceux qu'elle
porte en son sein ? Mais puisqu'à ma grande douleur cette corruption est
si visible et que je suis contraint d'en parler, je commencerai à la
déplorer par les éloquentes paroles d'un saint et illustre écrivain.
C'est Salvien, prêtre de Marseille, qui dans le premier livre qu'il a
adressé à la sainte Église catholique , lui parle en ces termes : « Je
ne sais, dit-il, ô Église, de quelle sorte il est arrivé que ta propre
félicité combat tant contre toi-même, tu as presque autant amassé de
vices que tu as conquis de nouveaux peuples : » _Nescio quomodo pugnante
contra temetipsam tua felicitate, quantum tibi auctum est populorum,
tantùm penè vitiorum_ (3). « La prospérité a attiré les pertes ; la
grandeur est venue, et la discipline s'est relâchée. Pendant que le
nombre des fidèles s'est augmenté, l'ardeur de la foi s'est ralentie ;
et l'on t'a vue, ô Église, affaiblie par ta fécondité, diminuée par ton
accroissement et presque abattue par tes propres forces : » _Quantum
tibi copiœ accessit, tantùm disciplinœ recessit... Multiplicatis fidei
populis, fides imminuta est...., factaque es, Ecclesia, profectu tuœ
fœcunditatis infirmier atque accessu relabens, et quasi viribus minus
valida_ (4). Voilà une plainte bien éloquente ; mais, mes frères, à
notre honte elle n'est que trop véritable. L'Église

1 Act., XV, 28. — 2 _Rom_., XII, 3. — 3 _Adver._ _Avarit_., lib. I, n.
1.— 4 _Ibid_.

543

n'est faite que pour les saints : il est vrai, les enfants de Dieu y
sont appelés de toutes parts, tous ceux qui sont du nombre y sont
entrés ; mais plusieurs y sont entrés par-dessus le nombre : »
_Multiplicati sunt super numerum_ (1). L'ivraie est crue avec le bon
grain ; et la charité s'étant refroidie, le scandale s'est élevé jusque
dans la maison de Dieu. Voilà ce qui scandalise les faibles, voilà la
tentation des infirmes. Quand vous verrez, mes frères, l'iniquité qui
lève la tête au milieu même du temple de Dieu, Satan vous dira : Est-ce
là l'Église ? sont-ce là les successeurs des apôtres ? et il tâchera de
vous ébranler, imposant à la simplicité de votre foi.

Il faudrait peut-être un plus long discours pour vous fortifier contre
ces pensées ; mais étant pressé par le temps, je dirai seulement ce petit
mot, plein de consolation et de vérité. Ne croyez pas, mes frères, que
l'homme ennemi qui va semer la nuit dans le champ (2), puisse empêcher
de croître le bon grain du père de famille, ni lui ôter sa moisson. Il
peut bien la mêler, remarquez ceci, il peut bien semer par-dessus ; mais
il ne peut pas ni arracher le froment, ni corrompre la bonne semence. Il
y en a qui profanent les sacrements ; mais il y en a toujours qu'ils
sanctifient. Il y a des terres sèches et pierreuses où la parole tombe
inutilement ; mais il y a des champs fertiles où elle fructifie au
centuple. Il y a des gens de bien, il y a des saints : le bras de
Jésus-Christ n'est pas affaibli ; l'Église n'est pas devenue stérile ;
le sang de Jésus-Christ n'est pas inutile ; la parole de son Évangile
n'est pas infructueuse à l'égard de tous. Déplorez donc, quand il vous
plaira, la prodigieuse corruption de mœurs qui se voit même dans
l'Église ; je me joindrai à vous dans cette plainte ; je confesserai ,
avec saint Bernard (3), « qu'une maladie puante infecte quasi tout son
corps. » Non, non, le temple de Dieu n'en est pas exempt : Jésus-Christ
en enrichit qui le déshonorent ; Jésus-Christ en élève qui servent à
l'Antéchrist ; l'iniquité est entrée comme un torrent ; on ne peut plus
noter les impies, on ne peut plus les fuir, on ne peut plus les
retrancher ; tant ils sont forts, tant ils sont puissants, tant le nombre
en est infini ; la maison de Dieu n'en est pas exempte. Mais au milieu de
tous ces désordres, sachez

1 _Psal_. XXXIX, 6.— 2 _Matth.,_ XIII, 24 et seq.— 3 Serm., XXXIII _in
Cant_., n. 15.

544

que «Dieu connaît ceux qui sont à lui (1). » Jetez les yeux dans ces
séminaires ; combien de prêtres très-charitables! dans les cloîtres,
combien de saints pénitents ! dans le monde, combien de magistrats
!..... combien qui « possèdent comme ne possédant pas, qui usent du
monde comme n'en usant pas, sachant bien que la figure de ce monde passe
(2) ! » Les uns paraissent, les autres sont cachés, selon qu'il plaît au
Père céleste ou de les sanctifier par l'obscurité, ou de les produire
parle bon exemple.

— Mais il y a aussi des méchants, le nombre en est infini, je ne puis
vivre en leur compagnie.— Mon frère, où irez-vous ? Vous en trouverez
par toute la terre ; ils sont partout mêlés avec les bons. Ils seront
séparés un jour, mais l'heure n'en est pas encore arrivée. Que faut-il
faire en attendant? Se séparer de cœur, les reprendre avec liberté afin
qu'ils se corrigent ; et s'ils ne le font, les supporter en charité afin
de les confondre. Mes frères, nous ne savons pas les conseils de Dieu.
Il y a des méchants qui s'amenderont, et il les faut attendre en
patience ; il y en a qui persévéreront dans leur malice, et puisque Dieu
les supporte, ne devons-nous pas les supporter? Il y en a qui sont
destinés pour exercer la vertu des uns, venger le crime des autres ; on
les ôtera du milieu quand ils auront accompli leur ouvrage : laissez
accoucher cette criminelle avant que de la faire mourir. Dieu sait le
jour de tous ; il a marqué dans ses décrets éternels le jour de la
conversion des uns, le jour de la damnation des autres ; ne précipitez
pas le discernement. «Aimez vos frères, dit saint Jean (3), et vous ne
souffrirez point de scandale ; » pourquoi? Parce que, dit saint Augustin
(4) « celui qui aime son frère, il souffre tout pour l'unité : » _Qui
diligit fratrem, tolerat omnia propter unitatem_.

Aimons donc, mes frères, cette unité sainte ; aimons la fraternité
chrétienne, et croyons qu'il n'y a aucune raison pour laquelle elle
puisse être violée. Que les scandales s'élèvent, que l'impiété règne
dans l'Église, qu'elle paraisse, si vous voulez, jusque sur l'autel ;
c'est là le triomphe de la charité, d'aimer l'unité catholique malgré
les troubles, malgré les scandales, malgré les

1 II _Timoth_., II, 19. — 2 I _Cor.,_ VII, 30, 31. — 3 I _Joan.,_ II,
10. — 4 Tract. I _in Epist. Joan.,_ n. 12.

545

dérèglements de la discipline, Gémissons-en devant Dieu ; reprenons-les
devant les hommes, si notre vocation le permet ; mais si nous avons un
bon zèle, ne crions pas vainement contre les abus, mettons la main à
l'œuvre sérieusement et commençons chacun par nous-mêmes la réformation
de l'Église. Mes enfants, nous dit-elle, regardez l'état où je suis ;
voyez mes plaies, voyez mes ruinas. Ne croyez pas que je veuille me
plaindre des anciennes persécutions que j'ai souffertes, ni de celle
dont je suis menacée à la fin des siècles : je jouis maintenant d'une
pleine paix sous la protection de vos princes, qui sont devenus mes
enfants, aussi bien que vous. Mais c'est cette paix qui m'a désolée :
_Ecce, ecce in pace amaritudo mea amarissima_ (1). Il m'était
certainement bien amer, lorsque je voyais mes enfants si cruellement
massacrés ; il me l'a été beaucoup davantage, lorsque les hérétiques se
sont élevés et ont arraché avec, eux, en se retirant avec violence, mie
grande partie de mes entrailles : mais les blessures des uns m'ont
honorée , et quoique touchée au dernier point de la retraite des autres,
enfui ils sont sortis de mon sein comme des humeurs qui me
surchargeaient. Maintenant, « maintenant mon amertume très-amère est
dans la paix : » _Ecce in pace amaritudo mea amarissima_. C'est vous,
enfants de ma paix, c'est vous, mes enfants et mes domestiques, qui me
donnez les blessures les plus sensibles par vos mœurs dépravées ; c'est
vous qui ternissez ma gloire, qui me portez le venin au cœur, qui
couvrez de honte ce front auguste sur lequel il ne devait paraître ni
tache, ni ride (2). Guérissez-moi, etc.

Que reste-t-il après cela, sinon qu'elle vous parle des intérêts de ces
nouveaux frères que sa charité vous a donnés? elle vous les recommande.
Le schisme lui a enlevé tout l'Orient ; l'hérésie a gâté tout le Nord. O
France, qui étais autrefois exempte de monstres, elle t'a cruellement
partagée ! Parmi des ruines si épouvantables, l'Église, qui est toujours
mère, tâche d'élever un petit asile pour recueillir les restes d'un si
grand naufrage, et ses enfants dénaturés l'abandonnent dans ce besoin.
Le jeu engloutit tout ; ils jettent dans ce gouffre des sommes immenses ;
pour cette

1 _Isa_., XXXVIII, 17. — 2 _Ephes_., V, 27.

546

œuvre de piété si nécessaire, il ne se trouve rien dans la bourse. Les
prédicateurs élèvent leur voix avec toute l'autorité que leur donne leur
ministère, avec toute la charité que leur inspire la compassion de ces
misérables ; et ils ne peuvent arracher un demi-écu, et il faut les aller
presser les uns après les autres, et ils donnent quelque aumône chétive,
faible et inutile secours, et encore ils s'estiment heureux d'échapper,
au lieu qu'ils devraient courir d'eux-mêmes pour apporter du moins
quelque petit soulagement à une nécessité si pressante. O dureté des
cœurs ! ô inhumanité sans exemple! mes chers frères, Dieu vous en
préserve! Ah ! si vous aimez cette Église dont je vous ai dit de si
grandes choses, laissez aujourd'hui, en ce lieu où elle rappelle ses
enfants dévoyés, quelque charité considérable. Ainsi soit-il.

FIN DU HUITIÈME VOLUME. (PREMIER DES SERMONS.)


EXORDE D'UN SERMON SUR LA CHARITÉ ENVERS LES PAUVRES.

_Beati misericordes_! Matth., V, 7.

        Le Prophète-Roi, chrétiens, était entré bien profondément dans
la méditation de la dureté et de l'insensibilité des hommes, lorsqu'il
adresse à Dieu ces beaux mots : _Tibi derelictus est_ pauper (2) : « O
Seigneur, on vous abandonne le pauvre. » En effet il est véritable qu'on
fait peu d'état des malheureux. Chacun s'empresse avec grand concours
autour des fortunés de la terre ; les pauvres cependant sont délaissés,
leur présence même donne du chagrin (_a_), et il n'y a que Dieu seul à
qui leurs plaintes ne soient point à charge. Puisque tout le monde les
lui abandonne, il était digne de sa bonté de les recevoir sous ses ailes
et de prendre en main leur défense. Aussi s'est-il déclaré leur
protecteur. Parce qu'on méprise leur condition, il relève leur dignité ;
parce qu'on croit ne leur rien devoir, il impose la nécessité de les
soulager ; et afin de nous y engager par notre intérêt, il ordonne que
les aumônes nous soient une source infinie de grâces. Dans cette maison
des pauvres, dans cette assemblée qui se fait pour eux, on ne peut rien
méditer de plus convenable que ces vérités chrétiennes ; et comme les
prédicateurs de l'Évangile sont les véritables avocats des pauvres, je
m'estimerai bienheureux de parler aujourd'hui en leur faveur.

2 _Psal. H_. IX, 14.

(a) _Var_., : Ou fait peu d'état des misérables ; chacun s'empresse à
servir les grands ; les pauvres sont abandonnés, leur seule présence
donne du chagrin.

71

Tout le ciel s'intéresse dans cette cause, et je ne doute pas, chrétiens
que je n'obtienne facilement son secours par l'intercession de la sainte
Vierge.


SECOND EXORDE DU SERMON POUR LE IIE DIMANCHE DE L'AVENT.

Si nous apprenons des Écritures divines que notre Seigneur Jésus-Christ
a toujours été l'unique espérance du monde, la consolation et la joie de
tous ceux qui attendaient la rédemption d’Israël, à plus forte raison,
chrétiens, devons-nous être persuadés

1 Act., IV, 12. — 2 _Tit.,_ II, 12, 13.

176

que Jean-Baptiste son bienheureux précurseur n'avait point de plus chère
occupation que celle d'entretenir son esprit de ce doux objet. C'est
pourquoi je me le représente aujourd'hui dans les prisons du cruel
Hérode comme un homme qui n'a de contentement que d'apprendre ce que son
Maître fait parmi les hommes, et comme par ses prédications et par ses
miracles il se fait reconnaître à ses vrais fidèles pour le Fils du Dieu
tout-puissant. C'est ce qu'il me semble que saint Matthieu nous fait
conjecturer en ces mots de notre évangile : « Jean entendant dans les
liens les grandes œuvres de Jésus-Christ, il lui envoie deux de ses
disciples pour lui faire cette demande (_a_) : Êtes-vous celui qui devez
venir, ou si nous en attendons quelque autre (1) ? » Pour moi je
m'imagine, fidèles, que le fruit qu'il espérait de cette ambassade,
c'est que ses disciples lui rapportant la réponse de son bon Maître, il
ne doutait nullement que sa parole ne dût être pleine d'une si ineffable
douceur, que seule elle serait capable non-seulement de chasser les maux
d'une dure captivité, mais encore d'adoucir les amertumes de cette vie.
Chères Sœurs, dans cette prison volontaire où vous vous êtes jetées pour
l'amour de Dieu, dites-moi, que pourriez-vous faire sans la douce
méditation des mystères du Sauveur Jésus ? Et n'est-ce pas cette seule
pensée qui fait triompher en vos cœurs une sainte joie dans une vie si
laborieuse? Oui certes, il le faut avouer, Dieu a répandu une certaine
grâce sur toutes les paroles et sur toutes les actions du Seigneur Jésus
; y penser, c'est la vie éternelle. Oui, son nom est un miel à nos
bouches, et une lumière à nos yeux, et une flamme à nos cœurs ; et
lorsque remplis de l'Esprit de Dieu, nous concevons en nos âmes le
Sauveur Jésus, nous ressentons une joie à peu près semblable à celle que
sentit l'heureuse Marie, lorsque couverte de la vertu du Très-Haut ,
elle conçut en ses chastes entrailles le Fils unique du Père éternel,
après que l'ange l'eut saluée par ces célestes paroles : _Ave, Maria,_
etc.

1 Matth., XI, 2, 3.

(_a_) _Var._ : Pour lui demander.


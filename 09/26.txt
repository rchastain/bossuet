[BRIÈVETÉ VIE FRG]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Carême I
I Carême II
I Carême III
I Carême IV
I Carême Plan
I Carême Ld
I Carême Vd
II Carême I
II Carême II
II Carême Ld
Honneur
II Carême Jd I
II Carême Jd II
III Carême I
III Carême II
III Carême Mardi
III Carême Vdi
III Carême Sdi
III Carême Sdi abr.
IV Carême I
IV Carême II
IV Carême III
IV Carême Mardi abr.
IV Carême Mcdi Plan
IV Carême Vdi
Brièveté Vie frg
Passion I
Passion II
Passion III
Passion Mardi
Sem. Passion (3) I
Sem. Passion (3) II
Sem. Passion (3) III
Vendr. Passion I
Vendr. Passion II
Sem. Passion abrg.
Rameaux I
Rameaux II
Rameaux III
Rameaux IV

 



FRAGMENT SUR  LA BRIÈVETÉ  DE  LA  VIE ET LE NÉANT DE L'HOMME.


 

C'est bien peu de chose que l'homme, et tout ce qui a fin est bien peu
de chose. Le temps viendra où cet homme qui vous semblait si grand ne
sera plus, où il sera comme l'enfant qui est encore à naître, où il ne
sera rien. Si longtemps qu'on soit au monde, y serait-on mille ans, il
en faut venir là. Il n'y a que

 

373

 

le temps de ma vie qui me fait différent de ce qui ne fut jamais. Cette
différence est bien petite, puisqu'à la fin je serai encore confondu
avec ce qui n'est point ; ce qui arrivera le jour où il ne paraîtra pas
seulement que j'aie été, et où peu m'importera combien de temps j'ai
été, puisque je ne serai plus. J'entre dans la vie avec la loi d'en
sortir. Je viens faire mon personnage, je viens me montrer comme les
autres ; après il faudra disparaître. J'en vois passer devant moi,
d'autres me verront passer ; ceux-là même donneront à leurs successeurs
le même spectacle ; tous enfin viendront se confondre dans le néant. Ma
vie est de quatre-vingts ans tout au plus, prenons-en cent : qu'il y a
eu de temps où je n'étois pas ! qu'il y en a où je ne serai point ! et
que j'occupe peu de place dans ce grand abîme des ans! Je ne suis rien ;
ce petit intervalle n'est pas capable de me distinguer du néant où il
faut que j'aille. Je ne suis venu que pour faire nombre, encore
n'avait-on que faire de moi ; et la comédie ne se serait pas moins bien
jouée, quand je serais demeuré derrière le théâtre. Ma partie est bien
petite en ce monde et si peu considérable, que quand je regarde de près,
il me semble que c'est un songe de me voir ici, et que tout ce que je
vois ne sont que de vains simulacres : _Prœterit figura hujus mundi_
(1).

Ma carrière est de quatre-vingts ans tout au plus ; et pour aller là, par
combien de périls faut-il passer! par combien de maladies, etc.! A quoi
tient-il que le cours ne s'en arrête à chaque moment? Ne l'ai-je pas
reconnu quantité de fois? J'ai échappé la mort à telle, et telle
rencontre ; c'est mal parler : J'ai échappé la mort. J'ai évité ce
péril, mais non pas la mort. La mort nous dresse diverses embûches ; si
nous échappons l'une, nous tombons en une autre ; à la fin il faut venir
entre ses mains. Il me semble que je vois un arbre battu des vents ; il y
a des feuilles qui tombent à chaque moment ; les unes résistent plus, les
autres moins. Que s'il y en a qui échappent de l'orage, toujours l'hiver
viendra, qui les flétrira et les fera tomber. Ou comme dans une grande
tempête, les uns sont soudainement suffoqués, les autres flottent sur un
ais abandonné aux vagues ; et lorsqu'il croit avoir

 

1 I _Cor.,_ VII, 31.

 

374

 

évité tous les périls, après avoir duré longtemps, un flot le pousse
contre un écueil et le brise. Il en est de même : le grand nombre
d'hommes qui courent la même carrière fait que quelques-uns passent
jusqu'au bout ; mais après avoir évité les attaques diverses de la mort,
arrivant au bout de la carrière où ils tendaient parmi tant de périls,
ils la vont trouver eux-mêmes et tombent à la fin de leur course : leur
vie s'éteint d'elle-même, comme une chandelle qui a consumé sa matière.

Ma carrière est de quatre-vingts ans tout au plus, et de ces
quatre-vingts ans, combien y en a-t-il que je compte pendant ma vie ? Le
sommeil est plus semblable à la mort : l'enfance est la vie d'une bête.
Combien de temps voudrais-je avoir effacé de mon adolescence? Et quand
je serai plus âgé, combien encore? Voyons à quoi tout cela se réduit.
Qu'est-ce que je compterai donc ? Car tout cela n'en est déjà pas. Le
temps où j'ai eu quelque contentement, où j'ai acquis quelque honneur?
Mais combien ce temps est-il clairsemé dans ma vie ! C'est comme des
clous attachés à une longue muraille dans quelques distances ; vous
diriez que cela occupe bien de la place ; amassez-les, il n'y en a pas
pour emplir la main. Si j'ôte le sommeil, les maladies, les inquiétudes,
etc., de ma vie ; que je prenne maintenant tout le temps où j'ai eu
quelques contentements ou quelque honneur, à quoi cela va-t-il? Mais ces
contentements, les ai-je eus tous ensemble? les ai-je eus autrement que
par parcelles? mais les ai-je eus sans inquiétude? Et s'il y a de
l'inquiétude, les donnerai-je au temps que j'estime, ou à celui que je
ne compte pas? Et ne l'ayant pas eu à la fois, l'ai-je du moins eu tout
de suite? L'inquiétude n'a-t-elle pas toujours divisé deux
contentements? Ne s'est-elle pas toujours jetée à la traverse pour les
empêcher de se toucher ? Mais que m'en reste-t-il des plaisirs licites?
Un souvenir inutile. Des illicites? En regret, une obligation à l'enfer
ou à la pénitence, etc.

Ah ! que nous avons bien raison de dire que nous passons notre temps!
Nous le passons véritablement, et nous passons avec lui. Tout mon être
tient à un moment, voilà ce qui me sépare du rien ; celui-là s'écoule,
j'en prends un autre ; ils se passent les uns après les autres ; les uns
après les autres je les joins, tâchant de m'assurer ;

 

375

 

et je ne m'aperçois pas qu'ils m'entraînent insensiblement avec eux, et
que je manquerai au temps, non pas le temps à moi. Voilà ce que c'est
que de ma vie ; et ce qui est épouvantable, c'est que cela passe à mon
égard ; devant Dieu, cela demeure, ces choses me regardent. Ce qui est à
moi, la possession en dépend du temps, parce que j'en dépends moi-même ;
mais elles sont à Dieu devant moi, elles dépendent de Dieu devant que du
temps ; le temps ne les peut retirer de son empire, il est au-dessus du
temps : à son égard cela demeure, cela entre dans ses trésors. Ce que
j'y aurai mis, je le trouverai : ce que je fais dans le temps, passe par
le temps à l'éternité, d'autant que le temps est compris et est sous
l'éternité, et aboutit à l'éternité. Je ne jouis des moments de ce
plaisir que durant le passage ; quand ils passent, il faut que j'en
réponde comme s'ils demeuraient. Ce n'est pas assez dire ; ils sont
passés, je n'y songerai plus. Ils sont passés? Oui pour moi, mais à
Dieu, non ; il m'en demandera compte.

Eh bien, mon âme, est-ce donc si grande chose que cette vie ? Et si
cette vie est si peu de chose, parce qu'elle passe, qu'est-ce que les
plaisirs qui ne tiennent pas toute la vie et qui passent en un moment?
Cela vaut-il bien la peine de se damner? cela vaut-il bien la peine de
se donner tant de peines, d'avoir tant de vanité? Mon Dieu, je me résous
de tout mon cœur en votre présence de penser tous les jours, au moins en
me couchant et en me levant, à la mort. En cette pensée, j'ai peu de
temps, j'ai beaucoup de chemin à faire, peut-être en ai-je encore moins
que je ne pense ; je louerai Dieu de m'avoir retiré ici pour songer à la
pénitence. Je mettrai ordre à mes affaires, à ma confession, à mes
exercices avec grande exactitude, grand courage, grande diligence,
pensant non pas à ce qui passe, mais à ce qui demeure.

 

[Précédente]

[Accueil]

[Suivante]

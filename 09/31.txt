[SEM. PASSION (3) I]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Carême I
I Carême II
I Carême III
I Carême IV
I Carême Plan
I Carême Ld
I Carême Vd
II Carême I
II Carême II
II Carême Ld
Honneur
II Carême Jd I
II Carême Jd II
III Carême I
III Carême II
III Carême Mardi
III Carême Vdi
III Carême Sdi
III Carême Sdi abr.
IV Carême I
IV Carême II
IV Carême III
IV Carême Mardi abr.
IV Carême Mcdi Plan
IV Carême Vdi
Brièveté Vie frg
Passion I
Passion II
Passion III
Passion Mardi
Sem. Passion (3) I
Sem. Passion (3) II
Sem. Passion (3) III
Vendr. Passion I
Vendr. Passion II
Sem. Passion abrg.
Rameaux I
Rameaux II
Rameaux III
Rameaux IV

 



PREMIER SERMON POUR LES TROIS DERNIERS JOURS DE  LA   SEMAINE  DE  LA  PASSION, SUR L'EFFICACE  DE LA PÉNITENCE (_A_).


 

_Vides hanc mulierem?_

 

Voyez-vous cette femme? _Luc,_ VII, 44.

 

Madeleine, le parfait modèle de toutes les âmes réconciliées, se
présente à nous dans cette semaine, et on ne peut la contempler

 

(_a_) Prêché en 1602, dans le Carême du Louvre, devant la Cour. Nous
lisons dès le commencement de ce sermon : « Madeleine, le parfait modèle
de toutes les âmes réconciliées, se présente à nous dans cette semaine,
et on ne peut la contempler aux pieds de Jésus sans penser en même temps
à la pénitence. C'est donc à la pénitence que ces trois discours seront
consacrés. » On voit que l'auteur, suivant l'histoire de Madeleine dans
l'évangile, va faire trois discours sur la pénitence ; et comme l'Église
nous propose, dans la célébration des mystères, cette histoire le jeudi
de la semaine de la Passion, c'est ce jour-là que le prédicateur
prononcera le premier.

Il ajoute un peu plus loin : « Ces trois considérations m'engagent à
vous faire voir par trois discours l'efficace de la pénitence, qui peut
surmonter les plus grands obstacles ; l'ardeur de la pénitence, qui doit
vaincre tous les délais ; l'intégrité de la pénitence, qui doit anéantir
tous les crimes et n'en laisser aucun reste. Je commencerai aujourd'hui
à établir l'espérance des pécheurs par la possibilité de la conversion.
» Voilà donc le sujet de nos trois discouru : l'efficace de la
pénitence, l'ardeur de la pénitence, l'intégrité de la pénitence. Ces
discours ont été prêchés devant le même auditoire, car autrement ils
n'auraient pas été complets ; mais ils n'ont pas été prêchés le même
jour, puisque l'auteur dit: « Je commencerai aujourd'hui à établir
l'espérance des pécheurs par la possibilité de leur conversion ; »
d'ailleurs il répète souvent dans le deuxième et dans le troisième : «
Comme je l'ai dit hier, comme nous l'avons vu hier. » Cependant les
éditeurs, pensant qu'ils ont été prêches le même jour, les ont publiés
tous les trois sous le titre de Sermons pour le jeudi de la semaine de
la Passion. Ce qui les a trompés, c'est que l'évangile de sainte
Madeleine se lit ce jour-là.

Mais où nos discours ont-ils été prononcés? L'auteur dit à la fin du
premier : « Renversez Ninive, renversez la Cour. O Cour vraiment auguste
et vraiment royale, que je puisse voir tomber par terre l'ambition qui
t'emporte, les jalousies qui te partagent, les médisances qui te
déchirent... » Nos discours ont été manifestement prêches devant la
Cour.

A quelle époque ? Voici un passage qui nous l'apprendra. L'auteur dit
dans le troisième, vers la fin du dernier point : « Ces excès sont
Criminels eu tout temps... ;  mais les peut-on maintenant souffrir dans
ces extrêmes misères où le ciel et la terre fermant leurs trésors, ceux
qui subsistaient par leur travail sont réduits à la honte de mendier
leur vie ; où ne trouvant plus de secours dans les aumônes
particulières, ils cherchent un vain refuge dans les asiles publics de
la pauvreté , je veux dire les hôpitaux, où par la dureté de nos cœurs
ils trouvent encore la faim et le désespoir. Dans ces états déplorables
peut-on songer à orner son corps, et ne tremble-t-on pas de porter sur
soi la subsistance, la vie, le patrimoine des pauvres? » — « O ambition,
dit Tertullien, que tu es forte, de pouvoir porter sur toi ce qui
pourrait faire subsister tant d'hommes mourants ! » Ces artisans
contraints de « mendier leur vie, » ces malheureux qui trouvent la faim
jusque dans « les asiles de la pauvreté, » ces « hommes mourants, » ces
« extrêmes misères » nous mènent droit à cette année malheureuse où « le
ciel et la terre avaient fermé leurs trésors, » à 1662. Mais qu'il me
soit permis de le demander en passant , est-il vrai que Bossuet n'a
jamais prononcé une parole en faveur de la souffrance?

Ainsi trois sermons traitant la même matière, prêches dans le Carême de
1662, les trois derniers jours de la semaine de la Passion.

 

451

 

aux pieds de Jésus sans penser en même temps à la pénitence. C'est donc
à la pénitence que ces trois discours seront consacrés ; et je suis bien
aise, Messieurs, d'en proposer le sujet pour y préparer les esprits.

Je remarque trois sortes d'hommes qui négligent la pénitence : les uns
n'y pensent jamais, d'autres diffèrent toujours, d'autres n'y
travaillent que faiblement ; et voilà trois obstacles à leur conversion
(_a_). Plusieurs, endurcis dans leurs crimes, regardent leur conversion
comme une chose impossible, et dédaignent s'y appliquer (_b_) ;
plusieurs se la figurent trop facile, et ils la diffèrent de jour en
jour comme un ouvrage qui est en leurs mains, qu'ils feront quand il
leur plaira ; plusieurs étant convaincus du péril qui suit les remises
(_c_), commencent ; mais la commençant mollement (_d_), ils la laissent
toujours imparfaite. Voilà les trois défauts qu'il nous faut combattre
par l'exemple de Madeleine, qui enseigne à tous les pécheurs que leur
conversion est possible et qu'ils doivent l'entreprendre, que leur
conversion est pressée et qu'ils ne doivent point la remettre, enfin que
leur conversion est un grand ouvrage et qu'il ne faut point le faire à
demi, mais s'y donner d'un cœur tout entier.

Ces trois considérations m'engagent à vous faire voir par trois

 

(_a_) _Var._ : Et voilà trois empêchements de la conversion véritable ;
— tous trois méprisent la conversion véritable. — (_b_) Plusieurs
veulent croire qu'elle est impossible, et ne daignent s'y appliquer.—
(_c_) Le délai.— (_d_) Mais l'entreprenant mollement ; — mais s'y
appliquant mollement.

 

452

 

discours l'efficace de la pénitence, qui peut surmonter (_a_) les plus
grands obstacles ; l'ardeur de la pénitence, qui doit vaincre tous les
délais ; l'intégrité de la pénitence, qui doit anéantir tous les crimes
et n'en laisser aucun reste. Je commencerai aujourd'hui à établir
l'espérance des pécheurs par la possibilité de leur conversion, après
avoir imploré le secours d'en haut. _Ave, Maria_.

 

Les pécheurs aveugles et mal avisés arrivent enfin par leurs désordres à
l'extrémité de misère qui leur a été souvent prédite. Ils ont été assez
avertis qu'ils travaillaient à leurs chaînes par l'usage licencieux de
leur liberté ; qu'ils rendaient leurs passions invincibles en les
flattant, et qu'ils gémiraient quelque jour de s'être engagés si avant
dans la voie de perdition, qu'il ne leur serait (_b_) presque plus
possible de retourner sur leurs pas. Ils ont méprisé cet avis. Ce que
nous faisons librement et où notre seule volonté nous porte, nous nous
imaginons facilement que nous le pourrons aussi défaire sans peine.
Ainsi une âme craintive, qui commençant à s'éloigner de la loi (_c_) de
Dieu, n'a pas encore perdu la vue de ses jugements, se laisse emporter
aux premiers péchés, espérant de s'en retirer quand elle voudra ; et
très-assurée, à ce qu'elle pense, d'avoir toujours en sa main sa
conversion, elle croit en attendant qu'elle peut donner quelque chose à
son humeur. Cette espérance l'engage, et bientôt le désespoir lui
succède. Car l'inclination au bien sensible , déjà si puissante par
elle-même, étant fortifiée et enracinée par une longue habitude, cette
âme ne fait plus que de vains efforts pour se relever ; et retombant
toujours sur ses plaies, elle se sent si exténuée, que ce changement de
ses mœurs et ce retour à la droite voie qu'elle trouvait si facile,
commence à lui paraître impossible.

Cette impossibilité prétendue, c'est, mes frères, le plus grand obstacle
de sa conversion. Car quelle apparence d'accomplir jamais ce que
l'impuissance et le désespoir ne permet plus même de tenter? Au
contraire c'est alors, dit le saint Apôtre, que les pécheurs se laissent
aller et que « désespérant de leurs forces, ils se laissent

 

(_a_) _Var._ : L'efficace de la pénitence, capable de surmonter.....—
(_b_) Soit.— (_c_) De la voie.

 

453

 

emporter sans retenue à tous leurs désirs (_a_) : » _Desperantes
semetipsos tradiderunt impudicitiœ, in operationem immunditiœ omnis_
(1). Telle est, Messieurs, leur histoire (_b_) : l'espérance leur fait
faire les premiers pas, le désespoir les retient et les précipite au
fond de l'abîme.

Encore qu'ils y soient tombés par leur faute, il ne faut pas toutefois
les laisser périr ; ayons pitié d'eux, tendons-leur la main ; et comme il
faut qu'ils s'aident eux-mêmes par un grand effort, s'ils veulent se
relever de leur chute, pour leur en donner le courage, ôtons-leur avant
toutes choses cette fausse impression, qu'on ne peut vaincre ses
inclinations ni ses habitudes vicieuses : montrons-leur clairement par
ce discours que leur conversion est possible.

J'ai appris de saint Augustin (2) qu'afin qu'une entreprise soit
possible à l'homme, deux choses lui sont nécessaires : il faut
premièrement qu'il ait en lui-même une puissance, une faculté, une vertu
proportionnée à l'exécution ; et il faut secondement que l'objet lui
plaise, à cause que le cœur de l'homme ne pouvant agir sans quelque
attrait, on peut dire en un certain sens que ce qui ne lui plaît pas lui
est impossible. C'est aussi pour ces deux raisons que la plupart des
pécheurs (_c_) endurcis désespèrent de leur conversion, parce que leurs
mauvaises habitudes, si souvent victorieuses de leurs bons desseins
(_d_), leur font croire qu'ils n'ont point de force contre elles (_e_) ;
et d'ailleurs quand même ils les pourraient vaincre, cette vie sage et
composée qu'on leur propose leur paraît sans goût, sans attrait et sans
aucune douceur ; de sorte qu'ils ne se sentent pas assez (_f_) de
courage pour la pouvoir embrasser.

Ils ne considèrent pas, Messieurs, la nature de la grâce chrétienne qui
opère dans la pénitence. Elle est forte, dit saint Augustin (3), et
capable de surmonter toutes nos faiblesses ; mais sa force,

 

1 _Ephes_., IX, 19. — 2 _De Spirit. et litter.,_ cap. III, n. 5.— 3
_Ibid_., cap. XXIX, n. 51.

 

(_a_) _Var._ : Que les pécheurs s'abandonnent et que « désespérant
d'eux-mêmes, ils se livrent sans retenue à tous leurs désirs.»— (_b_)
Leur aventure.— (_c_) Que les pécheurs. — (_d_) De leurs bonnes
résolutions. — (_e_) Qu’ils n’ont point de force pour les surmonter.—
(_f_) De sorte qu'ils n'ont pas assez...

 

454

 

dit le même Père, est dans sa douceur et dans une suavité céleste qui
surpasse tous les plaisirs que le monde vante. Madeleine abattue aux
pieds de Jésus, fait bien voir que cette grâce est assez puissante pour
vaincre les inclinations les plus engageantes (_a_) ; et les larmes
qu'elle répand pour l'avoir perdue, suffisent pour nous faire entendre
la douceur qu'elle trouve à la posséder. Ainsi nous pouvons montrer à
tous les pécheurs par l'exemple de cette sainte, que s'ils embrassent
(_b_) avec foi et soumission la grâce de la pénitence, ils y trouveront
sans aucun doute et assez de force pour les soutenir, et assez de
suavité pour les attirer ; et c'est le sujet de ce discours.

 


PREMIER  POINT.

 

Il n'est que trop vrai, Messieurs, qu'il n'y a point de coupable qui
n'ait ses raisons ; les pécheurs n'ont pas assez fait s'ils ne joignent
l'audace d'excuser leur faute à celle de la commettre ; et comme si
c'était peu à l'iniquité de nous engager à la suivre, elle nous engage
encore à la défendre. Toujours ou quelqu'un nous a entraînés, ou quelque
rencontre imprévue nous a engagés contre notre gré ; tout autre que nous
aurait fait de même. Que si nous ne trouvons pas hors de nous sur quoi
rejeter notre faute, nous cherchons quelque chose en nous qui ne vienne
pas de nous-mêmes, notre humeur, notre inclination, notre naturel. C'est
le langage ordinaire de tous les pécheurs, que le prophète Isaïe nous a
exprimé bien naïvement dans ces paroles qu'il leur fait dire (_c_) : «
Nous sommes tombés comme des feuilles, mais c'est que nos iniquités nous
ont emportés comme un vent : » _Cecidimus quasi folium universi, et
iniquitates nostrae quasi ventus abstulerunt nos_ (1). Ce n'est jamais
notre choix ni notre dépravation volontaire ; c'est un vent impétueux
qui est survenu, c'est une force majeure, c'est une passion violente à
laquelle , quand nous nous sommes laissé dominer (_d_) longtemps, nous
sommes bien aises de croire qu'elle est invincible. Ainsi nous n'avons
plus besoin

 

1 _Isa.,_ LXIV,
6.                                                                                       

 

(_a_) _Var._ : Les plus corrompues. — (_b_) S'ils reçoivent. — (_c_)
C'est le discours ordinaire de tous les pécheurs que je reconnais
exprimé bien naïvement dans ces paroles :.... — (_d_) Maîtriser.

 

455

 

de chercher d'excuse ; notre propre crime s'en sert à lui-même, et nous
ne trouvons point de moyen plus fort pour notre justification que
l'excès de notre malice.

Si pour détruire cette vaine excuse, nous reprochons aux pécheurs qu'en
donnant un si fort ascendant (_a_) sur nos volontés à nos passions et à
nos humeurs, ils ruinent la liberté de l'esprit humain, ils détruisent
(_b_) toute la morale, et que par un étrange renversement ils justifient
tous les crimes et condamnent toutes les lois, cette preuve (_c_)
quoique forte n'aura pas l'effet que nous prétendons, parce que c'est
peut-être ce qu'ils demandent, que la doctrine des mœurs soit anéantie
et que chacun n'ait de lois que ses désirs. Il faut donc les convaincre
par d'autres raisons, et voici celle de saint Chrysostome dans l'une de
ses _Homélies sur la première Epître aux Corinthiens_ (1).

« Ce qui est absolument impossible à l'homme, nul péril, nulle
appréhension, nulle nécessité ne le rend possible. » Qu'un ennemi vous
poursuive avec un avantage si considérable que vous soyez contraint de
prendre la fuite, la crainte qui vous emporte peut bien vous rendre
léger et précipiter votre course ; mais quelque extrémité qui vous
presse, elle ne peut jamais vous donner des ailes dans lesquelles vous
trouveriez un secours présent pour vous dérober tout d'un coup à une
poursuite si violente ((_d_), parce que la nécessité peut bien aider nos
puissances et nos facultés naturelles, mais non pas en ajouter d'autres.
Or est-il que dans l'ardeur la plus insensée de nos passions,
non-seulement une crainte extrême, mais une circonspection modérée, mais
la rencontre d'un homme sage, mais une pensée survenue ou quelque autre
dessein nous arrête et nous fait vaincre notre inclination (_e_). Nous
savons bien nous contraindre devant les personnes de respect ; et certes
sans recourir à la crainte, celui-là est bien malheureux, qui ne connaît
pas par expérience qu'il peut du moins modérer par la raison l'instinct
aveugle de son humeur. Mais ce qui se peut

 

1 _Homil_. II.

 

(_a_) _Var._ : Un tel ascendant. — (_b_) Ils renversent. — (_c_) Cette
raison.— (_d_) Elle ne peut jamais vous donner des ailes, encore que
vous y trouveriez un secours présent contre une poursuite si violente. —
(_e_) Ou quelque autre dessein peut bien nous retenir, — nous arrêter.

 

456

 

modérer avec un effort médiocre, sans doute se pourrait dompter si on
ramassait toutes ses forces. Il y a donc en nos âmes une faculté
supérieure, qui étant mise en usage, pourrait réprimer nos inclinations ;
et si elles sont invincibles, c'est parce qu'on ne se remue pas pour
leur résister.

Mais sans chercher bien loin des raisons, je ne veux que la vie de la
Cour pour faire voir aux hommes qu'ils se peuvent vaincre. Qu'est-ce que
la vie de la Cour? faire céder toutes ses passions au désir d'avancer
(_a_) sa fortune. Qu'est-ce que la vie de la Cour? dissimuler tout ce
qui déplaît et souffrir tout ce qui offense, pour agréer à qui nous
voulons. Qu'est-ce encore que la vie de la Cour? étudier sans cesse la
volonté d'autrui et renoncer pour cela, s'i est nécessaire, à nos plus
chères inclinations. Qui ne le fait pas, ne sait point la Cour : qui ne
se façonne point à cette souplesse, c'est un esprit rude et maladroit,
qui n'est propre ni pour la fortune ni pour le grand monde. Chrétiens,
après cette expérience, saint Paul va vous proposer de la part de Dieu
une condition bien équitable : _Sicut exhibuistis membra vestra servire
immunditiœ et iniquitati ad iniquitatem, ita nunc exhibete membra vestra
servire justitiœ in sanctificationem_ (1) : « Comme vous vous êtes
rendus les esclaves de l'iniquité et des désirs séculiers, en la même
sorte rendez-vous esclaves de la sainteté et de la justice. »
Reconnaissez, chrétiens, combien on est éloigné d'exiger de vous
l'impossible, puisque vous voyez au contraire qu'on ne vous demande que
ce que vous faites. « Faites, dit-il, pour la justice ce que vous faites
pour la vanité, » pour la fortune : contraignez-vous pour la raison
(_b_). Vous vous êtes tant de fois surmontés vous-mêmes pour servir à
l'ambition, surmontez-vous quelquefois pour servir à la grâce et à
l'Évangile (_c_). C'est beaucoup se relâcher, pour un Dieu, de ne
demander que l'égalité ; toutefois il ne refuse pas ce tempérament, tout
prêt à se relâcher beaucoup au-dessous. Car quoi que vous entrepreniez
pour son service, quand aurez-vous

 

1 _Rom.,_ VI, 19.

 

(_a_) _Var._: De faire... — (_b_) ... pour la vanité: » contraignez-vous
pour la justice.— (_c_) Vous vous contraignez pour la vanité,
contraignez-vous pour la justice. Vous vous êtes tant de fois surmontés
vous-mêmes pour servir à l'ambition et à la fortune, surmontez-vous
quelquefois pour servir à Dieu et à la raison.

 

457

 

égalé les peines de ceux que le besoin (_a_) engage au travail,
l'intérêt aux intrigues de la Cour, l'honneur aux emplois de la guerre,
l'amour à de longs mépris (_b_), le commerce à des voyages immenses et à
un exil perpétuel de leur patrie ; et pour passer à des choses de nulle
importance, le divertissement et le jeu à des veilles, à des fatigues, à
des inquiétudes incroyables? Quoi donc! n'y au-ra-t-il que le nom de
Dieu qui apporte des obstacles invincibles à toutes les entreprises
généreuses? Faut-il que tout devienne impossible , quand il s'agit de
cet Etre qui mérite tout, dont la recherche au contraire devait être
d'autant plus facile, qu'il est toujours prompt à secourir ceux qui le
désirent (_c_), toujours prêt à se donner à ceux qui l'aiment?

Je n'ignore pas, chrétiens, ce que les pécheurs nous répondent. Ils
avouent qu'on se peut contraindre, et même qu'on se peut vaincre dans
l'ordre des choses sensibles, et que l'âme peut faire, un effort pour
détacher ses sens d'un objet, lorsqu'elle les rejette aussitôt sur
quelque autre bien qui les touche aussi et qui soit capable de les
soutenir ; mais que de laisser comme suspendu cet amour né avec nous pour
les biens sensibles, sans lui donner aucun appui, et de détourner le
cœur tout à coup à une beauté, quoique ravissante , mais néanmoins
invisible, c'est ce qui n'est pas possible à notre faiblesse. Chrétiens,
que vous répondrai-je? Il n'y a rien de plus faible, mais il n'y a rien
de plus fort que cette raison ; rien de plus aisé à réfuter, mais rien
de plus malaisé à vaincre. Je confesse qu'il est étrange que ce que peut
une passion sur une autre, la raison ne le puisse pas. Je dis : rien de
plus aisé à réfuter ; car comme il est ridicule dans une maison de voir
un serviteur insolent qui a plus de pouvoir sur ses compagnons que le
maître n'en a sur lui et sur eux, ainsi c'est une chose indigne que dans
l'homme, où les passions doivent être esclaves , une d'elles plus
impérieuse exerce plus d'autorité (_d_) sur les autres que la raison qui
est la maîtresse n'est capable d'en exercer sur toutes ensemble : cela
est indigne, mais cela est. Cette raison est devenue toute

 

(_a_) _Var._: La nécessité.— (_b_) A de longs services. — (_c_) A prêter
la main à ceux qui le cherchent. — (_d_) Une d'elles plus audacieuse ait
plus d'autorité.

 

458

 

sensuelle ; et s'il se réveille quelquefois en elle quelque affection du
bien éternel pour lequel elle était née, le. moindre souffle des
passions éteint cette flamme errante et volage et la replonge tout
entière dans le corps (_a_) dont elle est esclave. Que ne dirait ici la
philosophie de la force, de la puissance, de l'empire de la raison, qui
est la reine de la vie humaine, de la supériorité naturelle de cette
fille du Ciel sur ces passions tumultueuses, téméraires enfants de la
terre, qui combattent contre Dieu et contre ses lois? Mais que sert de
représenter à cette reine dépouillée les droits et les privilèges de sa
couronne qu'elle a perdus, de son sceptre qu'elle a laissé tomber de ses
mains? Elle doit régner, qui ne le sait pas? Mais ne perdez pas le temps
(_b_), ô philosophes, à l'entretenir de ce qui doit être ; il faut lui
donner le moyen de remonter sur son trône et de dompter ses sujets
rebelles.

Chrétiens, suivons Madeleine, allons aux pieds de Jésus ; c'est de là
qu'il découle sur nos cœurs infirmes une vertu toute-puissante qui nous
rend et la force et la liberté. Là se brise le cœur ancien, là se forme
le cœur nouveau. La source étant détournée, il faut bien que le ruisseau
prenne un autre cours : le cœur étant changé, il faut bien que les
désirs s'appliquent ailleurs. Que si la grâce peut vaincre
l'inclination, ne doutez pas, chrétiens, qu'elle ne surmonte aussi
l'habitude. Car qu'est-ce que l'habitude, sinon une inclination
fortifiée ? Mais nulle force ne peut égaler celle de l'Esprit qui nous
pousse. S'il faut fondre de la glace, il fera souffler son Esprit,
lequel, comme le vent du midi, relâchera la rigueur du froid, et du cœur
le plus endurci sortiront les larmes de la pénitence : _Flabit Spiritus
ejus et fluent aquœ_ (1). Que s'il faut faire encore un plus grand
effort, il enverra son Esprit de tourbillon qui pousse violemment les
murailles : _Quasi turbo impellens parietem_ (2) ; son Esprit qui
renverse les montagnes et qui déracine les cèdres du Liban : _Spiritus
Domini subvertens montes_ (3). Madeleine abattue aux pieds de Jésus par
la force de cet Esprit, n'ose plus lever cette tête qu'elle portait
autrefois si haute pour attirer les regards ; elle renonce à ses funestes
victoires qui la

 

1 _Psal._ CXLVII, 18. — 2 _Isa_., XXV, 4. — 3 III _Reg_., XIX, 11.

 

(_a_) _Var._ : Dans la cliair. — (_b_) Mais au lieu de perdre le
temps...

 

459

 

mettaient dans les fers (_a_) : vaincue et captivée elle-même, elle pose
toutes ses armes aux pieds de celui qui l'a conquise ; et ces parfums
précieux, et ces cheveux tant vantés, et même ces yeux qu'elle rendait
trop touchants, dont elle éteint tout le feu dans un déluge de larmes
(_b_). Jésus-Christ l'a vaincue, cette malheureuse conquérante ; et parce
qu'il l'a vaincue, il la rend victorieuse d'elle-même et de toutes ses
passions. Ceux qui entendront cette vérité, au lieu d'accuser leur
tempérament, auront recours à Jésus, qui tourne les cœurs où il lui
plaît ; ils n'imputeront point leur naufrage à la violence de la tempête ;
mais ils tendront les mains (_c_) à celui dont le Psalmiste a chanté «
qu'il bride la fureur de la mer, et qu'il calme quand il veut ses flots
agités : » _Tu dominaris potestati maris, motum autem fluctuum ejus tu
mitigas_ (1).

Il se plaît d'assister les hommes ; et autant que sa grâce leur est
nécessaire, autant coule-t-elle volontiers sur eux. « Il a soif, dit
saint Grégoire de Nazianze (2) ; mais il a soif qu'on ait soif de lui.
Recevoir de sa bonté , c'est lui bien faire ; exiger de lui, c'est
l'obliger ; et il aime si fort à donner, que la demande même à son égard
tient lieu d'un présent (_d_). » Le moyen le plus assuré pour obtenir
son secours, c'est de croire qu'il ne nous manque pas ; et j'ai appris de
saint Cyprien « qu'il donne toujours à ses serviteurs autant qu'ils
croient recevoir, » tant il est bon et magnifique : _Dans credentibus
tantùm quantum se crédit accipere qui sumit_ (3).

Ne doutez donc pas, chrétiens, si votre conversion est possible. Dieu
vous promet son secours : est-il rien, je ne dis pas d'impossible , mais
de difficile avec ce soutien ? Que si l'ouvrage de votre salut (_e_) par
la grâce de Dieu est entre vos mains, « pourquoi voulez-vous périr,
maison d'Israël? _Et quare moriemini, domus Israël ? Nolo mortem
peccatoris_. Convertissez-vous et vivez (1). » Ne dites pas toujours :
Je ne puis. — Il est vrai, tant que vous ne

 

1 _Psal_. LXXXVIII, 10. — 2 _Orat_. XL., p. 657. — 3 _Epist_. VIII _ad
Martyr. et Confess_., p. 17. — 4 Ezech., XVIII, 31, 32.

 

(_a_) _Var._ : Elle renonce à ses malheureuses conquêtes, — à ses
honteuses conquêtes, qui la chargeaient elle-même d'un joug trop
infâme.— (_b_) Et même ces yeux trop touchants, dont, elle éteint tout
le feu dans les larme. — (_c_) au lieu d'attribuer leur naufrage à la
violence de la tempête, ils tendront les mains à celui..... — (_d_) «
Est un présent. » — (_e_) Que s votre salut.

 

460

 

ferez pas le premier pas, le second sera toujours impossible. Quand vous
donnerez tout à votre humeur et à votre pente naturelle, vous ne pourrez
vous soutenir contre le torrent, etc. — Mais que cela soit possible,
trouverai-je quelque douceur dans cette nouvelle vie dont vous me parlez
? — C'est ce qui nous reste à considérer.

 


SECOND  POINT.

 

Je n'ai pas de peine à comprendre que les pécheurs en souffrent beaucoup
quand il faut tout à fait se donner à Dieu, s'attacher à un nouveau
maître et commencer une vie nouvelle. Ce sont des choses, Messieurs, que
l'homme ne fait jamais sans quelque crainte ; et si tous les changements
nous étonnent, à plus forte raison le plus grand de tous, qui est celui
de la conversion. Laban pleure amèrement et ne peut se consoler de ce
qu'on lui a enlevé ses idoles : _Cur furatus es deos meos_ (1) ? Le
peuple insensé s'est fait des dieux qui le précèdent, des dieux qui
touchent ses sens ; et il danse, et il les admire, et il court après, et
il ne peut souffrir qu'on les lui ôte. Ainsi l'homme sensuel voyant
qu'on veut abattre par un coup de foudre ces idoles pompeuses qu'il a
élevées (_a_), rompre ces attachements trop aimables, dissiper toutes
ces pensées qui tiennent une si grande place en son cœur malade, il se
désole sans mesure (_b_) : dans un si grand changement, il croit que
rien ne demeure en son entier et qu'on lui ôte même tout ce qu'on lui
laisse. Car encore qu'on ne touche ni à ses richesses, ni à sa
puissance, ni à ses maisons superbes, ni à ses jardins délicieux,
néanmoins il croit perdre tout ce qu'il possède, quand on lui en
prescrit un autre usage que celui qui lui plaît depuis si longtemps.
Comme un homme qui est assis à une table délicate, encore que vous lui
laissiez toutes les viandes, il croirait toutefois perdre le festin,
s'il perdait tout à coup le goût qu'il y trouve et l'appétit qu'il y
ressent : ainsi les pécheurs, accoutumés à se servir de leurs biens pour
contenter leur humeur et leurs passions, se persuadent que tout leur
échappe, si cet usage leur manque. Quoi !

 

1 _Genes_., XXXI, 30.

 

(_a_) _Var._: Erigées. — (_b_) Il s'afflige amèrement.

 

461

 

craindre ce qu'on aimait, n'aimer plus rien que pour Dieu! Que
deviendront ces douceurs et ces complaisances, et tout ce qu'il ne faut
pas penser en ce lieu et bien moins répéter en cette chaire ? Que
ferons-nous donc? que penserons-nous ? quel objet, quel plaisir, quelle
occupation? Cette vie réglée leur semble une mort, parce qu'ils n'y
voient plus ces délices, cette variété qui charme les sens, ces
égarements agréables où ils semblent se promener avec liberté, ni enfin
toutes les autres choses sans lesquelles ils ne trouvent pas la vie
supportable.

Que dirai-je ici, chrétiens? Comment ferais-je goûter aux mondains des
douceurs qu'ils n'ont jamais expérimentées? Les raisons en cette matière
sont peu efficaces, parce que pour discerner ce qui plaît, on ne connaît
de maître que son propre goût, ni de preuve que l'épreuve même (_a_).
Que plût à Dieu, chrétiens, que les pécheurs pussent se résoudre à
goûter combien le Seigneur est doux ! Ils reconnaîtraient par expérience
qu'il est de tous ces désirs irréguliers qui s'élèvent en la partie
sensuelle comme des appétits de malades : tant que dure la maladie,
nulle raison ne les peut guérir ; aussitôt qu'on se porte bien, sans y
employer de raison, la santé les dissipe par sa propre force et ramène
la nature à ses objets propres : _Haec omnia desideria tollit sanitas_
(1).

Et toutefois, chrétiens, malgré l'opiniâtreté de nos malades et malgré
leur goût dépravé, tâchons de leur faire entendre non point par des
raisons humaines, mais par les principes de la foi, qu'il y a des
délices spirituelles qui surpassent les fausses douceurs de nos sens et
toutes leurs flatteries. Pour cela, sans user d'un grand circuit, il me
suffit de dire en un mot que Jésus-Christ est venu au monde. Si je ne me
trompe, Messieurs, nous vîmes (_b_) hier assez clairement qu'il y est
venu pour se faire aimer. Un Dieu qui descend parmi les éclairs et qui
fait fumer de toutes parts la montagne de Sinaï par le feu qui sort de
sa face (_c_), a dessein de se faire craindre ; mais un Dieu qui
rabaisse sa grandeur et tempère sa majesté pour s'accommoder à notre
portée, un Dieu qui

 

1 S. August., _Serm._ CCLV, n. 7.

 

(_a_) _Var._ : Chacun ne connaît d'autre maître que son propre goût ; on
ne veut point être persuadé par des arguments, mais convaincu par
l'épreuve même. (_b_) Nous fîmes voir... — (_c_) Qui s'allume devant sa
face.

 

462

 

se fait homme pour attirer l'homme par cette bonté populaire dont hier
nous admirions la condescendance, sans doute a dessein de se faire
aimer. Or est-il que quiconque se veut faire aimer, il est certain qu'il
veut plaire ; et si un Dieu nous veut plaire, qui ne voit qu'il n'est
pas possible que la vie soit ennuyeuse dans son service (à) ?

C'est, Messieurs, par ce beau principe, que le grand saint Augustin a
fort bien compris (1) que la grâce du Nouveau- Testament, qui nous est
donnée par Jésus-Christ, est une chaste délectation, un agrément
immortel, un plaisir spirituel et céleste qui gagne les cœurs (_b_) :
car puisque Jésus-Christ a dessein de plaire, il ne doit pas venir sans
son attrait. Nous ne sommes plus ce peuple esclave et plus dur (_c_) que
la pierre sur laquelle sa loi est écrite, que Dieu fait marcher dans un
chemin rude (_d_) à grands coups de foudre, si je puis parler de la
sorte, et par des terreurs continuelles ; nous sommes ses enfants
bien-aimés auxquels il a envoyé son Fils unique pour nous gagner par
amour. Croyez-vous que celui qui a fait vos cœurs manque de charmes pour
les attirer, d'appas pour leur plaire et de douceur pour les entretenir
dans une sainte persévérance (_e_) ? Ah ! cessez ; ne soupirez plus
désormais après les plaisirs de ce corps mortel ; cessez d'admirer cette
eau trouble que vous voyez sortir (_f_) d'une source si corrompue. Levez
les yeux, chrétiens, voyez cette fontaine si claire et si vive qui
arrose, qui rafraîchit, qui enivre la Jérusalem céleste. Voyez la liesse
et le transport, les chants, les acclamations, les ravissements de cette
cité triomphante. C'est de là que Jésus-Christ nous a apporté un
commencement de sa gloire dans le bienfait de sa grâce, un essai de la
vision dans la foi, une partie de la félicité dans l'espérance : enfin
un plaisir intime qui ne trouble pas la volonté, mais qui la calme ; qui
ne surprend pas la raison , mais qui l'éclairé ; qui ne chatouille pas le
cœur dans sa surface, mais

 

1 _De Spirit. et litter_., cap. XXXVIII, n. 49 ; _De Grat. Christ_., cap.
XXXV, n. 83, et alibi.

 

(_a_)  _Var._ : Et si un Dieu vont plaire, par conséquent il est
impossible que la vie soit  ennuyeuse..... — (_b_)  Une chaste
délectation et un agrément céleste qui gagne..... — (_c_) Et plus pesant
— (_d_) Dans une voie dure. — (_e_) Pour les affermir dans son saint
amour. — (_f_) Ne buvez plus de cette eau trouble que vous voyez
découler...

 

463

 

qui l'attire tout entier à Dieu par son centre : _Trahe nos post te_
(1).

Si vous voulez voir par expérience combien cet attrait est doux,
considérez Madeleine. Quand vous voyez un enfant attaché de toute sa
force à la mamelle, qui suce avec ardeur et empressement cette douce
portion de sang que la nature lui sépare si adroitement de toute la
masse et lui assaisonne elle-même de ses propres mains, vous ne demandez
pas s'il y prend plaisir, ni si cette nourriture lui est agréable. Jetez
les yeux sur Madeleine, voyez comme elle court toute transportée à la
maison du Pharisien pour trouver celui qui l'attire. Elle n'a point de
repos jusqu'à ce qu'elle se soit jetée à ses pieds ; mais regardez comme
elle les baise, avec quelle ardeur elle les embrasse ; et après cela ne
doutez jamais que la joie de suivre Jésus ne passe toutes les joies du
monde, non-seulement celles qu'il donne, mais même celles qu'il promet,
toujours plus grandes que celles qu'il donne.

Que si vous êtes effrayés par ses larmes, par ses sanglots, par
l'amertume de sa pénitence, sachez, mes frères, que cette amertume est
plus douce que tous les plaisirs. Nous lisons dans l'Histoire sainte,
c'est au premier livre d’Esdras, que lorsque ce grand prophète eut
rebâti le temple de Jérusalem que l'armée assyrienne avait renversé, le
peuple mêlant tout ensemble et le triste souvenir de sa ruine et la joie
de la voir si bien réparée, tantôt élevait sa voix en des cris lugubres,
et tantôt poussait jusqu'au ciel des chants de réjouissance (_a_) ; en
telle sorte, dit l'auteur sacré, « qu'on ne pouvait distinguer les
gémissements d'avec les acclamations : » _Nec poterat quisquam agnoscere
vocem clamans laetantium et vocem fletûs populi_ (2). C'est une image
imparfaite de ce qui se fait dans la pénitence. Cette âme contrite et
repentante voit le temple de Dieu renversé en elle, et l'autel et le
sanctuaire si saintement consacré sous le titre du Dieu vivant : hélas!
ce ne sont point les Assyriens, c'est elle-même qui a détruit cette
sainte et magnifique structure, pour bâtir en sa place un temple
d'idoles ; et elle pleure, et elle gémit, et elle ne veut point recevoir
de consolation. Mais au milieu de ses pleurs elle voit que cette maison

 

1 _Cant.,_ I, 3. — 2 I _Esdr._ III, 13

 

(_a_) _Var._ ; D'allégresse.

 

464

 

sacrée se relève ; bien plus, ce sont ses larmes et sa douleur même qui
redressent ses murailles abattues, érigent de nouveau cet autel si
indignement détruit, commencent à faire fumer dessus un encens agréable
à Dieu et un holocauste (_a_) qui l'apaise. Elle se réjouit parmi ses
larmes ; elle voit qu'elle trouvera dans l'asile d'une bonne conscience
une retraite assurée , que nulle violence ne peut forcer (_b_), si bien
qu'elle peut sans crainte y retirer ses pensées, y déposer ses trésors,
y reposer ses inquiétudes ; et quand tout l'univers serait ébranlé, y
vivre tranquille et paisible sous les ailes du Dieu qui l'habite et y
préside. Qu'en jugez-vous, chrétiens? Une telle vie est-elle à charge?
Cette âme à laquelle (_c_) sa propre douleur procure une telle grâce,
peut-elle regretter ses larmes? Ne se croira-t-elle pas beaucoup plus
heureuse de pleurer ses péchés aux pieds de Jésus (_d_), que de rire
avec le monde et se perdre parmi ses joies dissolues? Et combien donc
est agréable la vie chrétienne, « où les regrets mêmes ont leurs
plaisirs, où les larmes portent avec elles leur consolation! » _Ubi et
fletus sine gaudio non est,_ dit saint Augustin (1).

Mais je prévois, chrétiens, une dernière difficulté contre les saintes
vérités que j'ai établies. Les pécheurs étant convaincus par la force et
par la douceur de la grâce de Jésus-Christ qu'il n'est pas impossible de
changer de vie, nous font une autre demande, si cela se peut à la Cour
et si l'âme y est en état de pouvoir goûter ces douceurs célestes. Que
cette question est embarrassante ! Si nous en croyons l'Évangile, il n'y
a rien de plus opposé que Jésus-Christ et le monde ; et de ce inonde,
Messieurs, la partie la plus éclatante et par conséquent la plus
dangereuse, chacun sait assez que c'est la Coin. Comme elle est et le
principe et le centre de toutes les affaires du monde, l'ennemi du genre
humain y jette tous ses appâts, y étale toute sa pompe. Là se trouvent
les passions les plus fines, les intérêts les plus délicats, les
espérances les plus engageantes. Quiconque a bu de cette eau, il
s'entête ; il est tout changé par une espèce d'enchantement ; c'est un
breuvage

 

1 Enarr. _in Psal._ CXLV.

 

(_a_) _Var._ : Un sacrifice. — (_b_) Elle voit qu'elle trouvera dans ce
sanctuaire un asile et une retraite que nulle violence..... (_c_) A qui.
— (_d_) Combien aime-t-elle mieux pleurer ; — combien trouve-t-elle plus
doux de pleurer ses péchés.... !

 

465

 

charmé qui enivre les plus sobres, et la plupart de ceux qui en ont
goûté ne peuvent plus goûter autre chose ; en sorte que Jésus-Christ ni
ses vérités (_a_) ne trouvent presque plus de place en leurs cœurs.

Et toutefois, chrétiens, pour ne pas jeter dans le désespoir des âmes
que le Fils de Dieu a rachetées, disons qu'étant le Sauveur de tous, il
n'y a point de condition ni d'état honnête qui soit exclu du salut qu'il
nous a donné par son sang. Puisqu'il a choisi quelques rois pour être
enfants de son Église, et qu'il a sanctifié quelques Cours par la
profession de son Évangile, il a regardé en pitié et les princes et
leurs courtisans ; et ainsi il a préparé des préservatifs pour toutes
leurs tentations, des remèdes pour tous leurs dangers, des grâces pour
tous leurs emplois. Mais voici la loi qu'il leur impose : ils pourront
faire leur salut, pourvu qu'ils connaissent bien leurs périls ; ils
pourront arriver en sûreté, pourvu qu'ils marchent toujours en crainte
et qu'ils égalent leur vigilance à leurs besoins, leurs précautions à
leurs dangers, leur ferveur aux obstacles qui les environnent : _Tuta si
sollicita, secura si attonita_ (1). Qu'on se fasse violence ; cette
douceur vient de la contrainte : renversez Ninive ; renversez la Cour.

O  Cour vraiment auguste et vraiment royale, que je puisse voir tomber
par terre l'ambition qui t'emporte, les jalousies qui te partagent, les
médisances qui te déchirent, les querelles qui t'ensanglantent, les
délices qui te corrompent, l'impiété qui te déshonore !

 

1 Tertull., _De Idololat_., n. 24.

 

(_a_) _Var._ : Ni son Évangile.

 

[Précédente]

[Accueil]

[Suivante]

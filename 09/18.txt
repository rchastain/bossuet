[III CARÊME SDI]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Carême I
I Carême II
I Carême III
I Carême IV
I Carême Plan
I Carême Ld
I Carême Vd
II Carême I
II Carême II
II Carême Ld
Honneur
II Carême Jd I
II Carême Jd II
III Carême I
III Carême II
III Carême Mardi
III Carême Vdi
III Carême Sdi
III Carême Sdi abr.
IV Carême I
IV Carême II
IV Carême III
IV Carême Mardi abr.
IV Carême Mcdi Plan
IV Carême Vdi
Brièveté Vie frg
Passion I
Passion II
Passion III
Passion Mardi
Sem. Passion (3) I
Sem. Passion (3) II
Sem. Passion (3) III
Vendr. Passion I
Vendr. Passion II
Sem. Passion abrg.
Rameaux I
Rameaux II
Rameaux III
Rameaux IV

 



SERMON POUR LE SAMEDI DE LA IIIE SEMAINE DE CARÊME, SUR LES JUGEMENTS HUMAINS (_A_).


 

_Nemo te condemnavit? Quae dixit : Nemo, Domine. Dixit autem Jesus : Nec
ego te condemnabo ; vade et jam ampliùs noli peccare_.

 

Personne ne t'a condamnée ? dit Jésus à la femme adultère ; laquelle lui
répondit : Personne, Seigneur (_b_). Et Jésus lui dit : Je ne te
condamnerai pas aussi ; va et dorénavant ne pèche plus. _Joan.,_ VIII,
10, 11.

 

Quel est, Messieurs, ce nouveau spectacle? Le juste prend le parti des
coupables ; le censeur des mœurs dépravées désarme les zélateurs de la
loi, élude leur témoignage, arrête toutes leurs

 

1 _Prov_., XVIII, 9.

 

(_a_) Prêché dans le Carême de 1661, aux Grandes Carmélites de la rue
Saint-Jacques.

Le manuscrit porte, écrit de la main de Bossuet : « Aux Carmélites. »
Cette indication suffit.

Le même sermon lui prêché plus tard, en 1669, à l'Oratoire, dans la rue
Saint Honoré ; et la reine entendit cette fois le grand orateur. En effet
la _Gazette_ du 20 avril 1669 renferme ces mots: « Le 14 avril, dimanche
des Rameaux, Madame entendit dans l'église des Prêtres de l'Oratoire la
prédication de M. l'abbé Bossuet. » c'est à cette occasion que l'auteur
ajouta l'allocution qu'on trouvera dans la péroraison, commençant par
ces mots: « Réglons donc tous nos jugements...» Comme il n'avait pas
reconnu la place de cette allocution, Déforis l'avait rejetée avec
d'autres morceaux détachés après les sermons, sous le titre de _Pensées
chrétiennes et morales,_ n° 14. Elle était restée là jusqu'à ce jour.

(_b_) _Var._: Non, Seigneur.

 

273

 

poursuites ; en un mot Jésus, le chaste Jésus, après s'être montré (_a_)
si sévère aux moindres regards immodestes, défend aujourd'hui
publiquement une adultère publique ; et bien loin de la punir (_b_) étant
criminelle, il la protège hautement étant accusée, et l'arrache au
dernier supplice étant convaincue. Voyez comme il renverse les choses :
au lieu de confondre la coupable, il l'encourage ; au lieu d'encourager
les accusateurs, il les confond ; et changeant toute la rigueur de la
peine en un simple avertissement de ne pécher plus, il ne craint pas de
faire revivre l'espérance abattue de la pécheresse (_c_) et d'effacer
pour ainsi dire de ses propres mains la honte qui couvrait justement sa
face impudique. Il y a quelque mystère caché dans cette conduite du
Sauveur des âmes, et il en faut aujourd'hui chercher le secret après
avoir imploré la grâce du Saint-Esprit par l'intercession de la sainte
Vierge. _Ave_.

 

Je commencerai ce discours en vous faisant le récit de l'histoire de
notre évangile, afin que vous laissiez d'abord épancher vos cœurs dans
une sainte contemplation de la clémence incomparable du Sauveur des
âmes. Les Juifs lui amènent avec grand tumulte cette misérable adultère,
et le font l'arbitre de son supplice. « La femme que nous vous
présentons, disent-ils, a été surprise en adultère ; Moïse nous a
commandé de lapider de tels criminels ; mais vous, Maître,
qu'ordonnerez-vous? » _Tu ergo, quid dicis_ (1) ? C'est ce que disent
les pharisiens. Mais Jésus, qui lisant dans le fond des cœurs, voyait
qu'ils étaient poussés, non point par le zèle de la justice qui craint
la contagion des mauvais exemples, mais par l'impatience d'un zèle amer
ou par l'orgueil fastueux d'une piété affectée, ne rougit ni devant
Dieu, ni devant les

 

1 _Joan.,_ VIII, 4, 5.

 (_a_) _Var._ : Lui qui s'est montré. — (_b_) Condamner. — (_c_) Son
espérance abattue.

 

274

 

hommes, de prendre en main la défense de cette impudique. « Celui de
vous qui est innocent, qu'il jette, dit-il, la première pierre (1). »
Ils se retirent confus, et je ne vois plus, dit saint Augustin, que le
médecin avec la malade, et la chasteté même avec l'impudique : je vois
la grande et extrême misère avec la grande et extrême miséricorde :
_Remansit peccatrix et salvator, remansit œgrota et medicus, remansit
misera et misericordia_ (2).

Cette pauvre femme étonnée, après avoir échappé des mains des coupables
qui avoient eu honte de la condamner, se croyait perdue sans ressource,
regardant devant ses yeux la justice même et se voyant appelée à son
tribunal, lorsque Jésus, l'aimable Jésus, toujours facile, toujours
indulgent, « non par la conscience d'aucun péché, mais par une bonté
infinie, » rassura son âme tremblante par ces aimables paroles que la
douceur même a dictées : « Nul, dit-il, ne t'a condamnée, et je ne te
condamnerai pas non plus que les autres. » De même que s'il eût dit : «
Si la malice t'a pu épargner (_a_), pourquoi craindrais-tu l'innocence?
» _Si malitia tibi parcere potuit, quid metuis innocentiam_ (3)? Je suis
un Dieu patient, qui pardonne volontiers les iniquités ; j'en veux aux
crimes et non aux personnes, et je supporte les péchés afin de sauver
les pécheurs : « Va donc, et seulement ne pèche plus : » _Vade, et jam
ampliùs noli peccare_.

Voilà, Messieurs, un rapport fidèle de ce que raconte saint Jean dans
l'évangile de cette journée : quelles seront là-dessus nos réflexions?
Je découvre de toutes parts des instructions importantes que nous
pouvons tirer de cet évangile ; mais il faut réduire toutes nos pensées
à un objet fixe et déterminé ; et parmi ce nombre infini de choses qui
se présentent, voici à quoi je m'arrête. Les deux vices les plus
ordinaires et les plus universellement étendus que je vois dans le genre
humain, c'est un excès de sévérité et un excès d'indulgence ; sévérité
pour les autres, et indulgence pour nous-mêmes. Saint Augustin l'a bien
remarqué et l'a exprimé élégamment en ce petit mot : _Curiosum genus ad
cognoscendam_

 

1 _Joan.,_ VIII, 7. — 2 S. August., _Serm_. XII, n. 5. — 3 S. August.,
_Epist_. CLIII _ad Macedon_., n. 15.

 

(_a_) _Var._ : T'a pu pardonner.

 

2751

 

_vitam alienam, desidiosum ad corrigendam suam_ (1) : «Ah! dit-il , que
les hommes sont diligents à reprendre (_a_) la vie des autres, mais
qu'ils sont lâches et paresseux à corriger leurs propres défauts ! »
Voilà donc deux mortelles maladies qui affligent le genre humain : juger
les autres en toute rigueur, se pardonner tout à soi-même ; voir le fétu
dans l'œil d'autrui, ne voir pas la poutre dans le sien ; faire
vainement le vertueux par une censure indiscrète , nourrir ses vices
effectivement par une indulgence criminelle ; enfin n'avoir un grand zèle
que pour inquiéter le prochain . et abandonner cependant sa vie à un
extrême relâchement dans toutes les parties de la discipline (_b_).

O  Jésus, opposez-vous à ces deux excès, et apprenez aux hommes pécheurs
à n'être rigoureux qu'à leurs propres crimes. C'est ce qu'il fait dans
notre évangile ; et cette même bonté, qui réprime la licence de juger les
autres, éveille la conscience endormie pour juger sans miséricorde ses
propres péchés. C'est pourquoi il avertit tout ensemble , et ces
accusateurs échauffés qui se rendent inexorables envers le prochain ,
qu'ils modèrent leur ardeur inconsidérée ; et cette femme trop
indulgente à ses passions, qu'elle ne donne plus rien à ses sens (_c_).
Vous, dit-il, pardonnez aux autres, et ne les jugez pas si sévèrement ;
et vous, ne vous pardonnez rien à vous-même, et désormais ne péchez
plus. C'est le sujet de ce discours.

 


PREMIER POINT.

 

Cette censure rigoureuse que nous exerçons sur nos frères, est une
entreprise insolente et contre les droits de Dieu et contre la liberté
publique. Le jugement appartient à Dieu, parce qu'il est le Souverain ;
et lorsque nous entreprenons de juger nos frères sans en avoir sa
commission, nous sommes doublement coupables, parce que nous nous
rendons tout ensemble et les supérieurs

 

1  _Confess_., lib. X, cap. III,

 

(_a_) _Var._ : A rechercher. — (_b_) Et s'abandonner à un extrême
relâchement pour soi-même.— (_c_) C'est pourquoi il dit tout ensemble,
et à ces accusateurs échauffés qui se rendent inexorables envers le
prochain, qu'ils modèrent leur ardeur inconsidérée ; et à cette femme
trop indulgente à ses passions, qu'elle ne leur donne plus rien
désormais.

 

276

 

de nos égaux et les égaux de notre supérieur, violant ainsi par un même
attentat et les lois de la société et l'autorité de l'empire. Pour nous
opposer, si nous le pouvons, à un si grand renversement des choses
humaines (_a_), il nous faut chercher aujourd'hui des raisons simples et
familières, mais fortes et convaincantes.

Pour les exposer avec ordre, distinguons avant toutes choses deux sortes
de faits et deux sortes d'hommes que nous pouvons condamner ; ou plutôt
ne distinguons rien de nous-mêmes, mais écoutons la distinction que nous
donne l'Apôtre. Il y en a dont les actions sont manifestement
criminelles, et d'autres dont les conduites peuvent avoir un bon et un
mauvais sens. Il faut aujourd'hui poser des maximes pour bien régler
notre jugement dans ces deux rencontres, de peur qu'il ne s'égare et ne
se dévoie. Cette distinction est très-importante, et saint Paul n'a pas
dédaigné de la remarquer lui même, écrivant ces mots à saint Timothée :
« Il y a des hommes, dit-il, dont les péchés sont manifestes et
précèdent le jugement que nous en faisons ; et aussi il y en a d'autres
qui suivent le jugement (_b_) : » _Quorumdam hominum peccata manifesta
sunt, pracedentia ad judicium ; quosdam autem et subsequuntur_ (1).

Ce passage de l'Apôtre est assez obscur, mais l'interprétation de saint
Augustin nous éclaircira sa pensée. Il y a donc des actions, dit saint
Augustin (2), qui portent leur jugement en elles mêmes et dans leurs
propres excès. Par exemple, pour nous restreindre aux termes de notre
évangile, un adultère public, c'est un crime si manifeste, que nous
pouvons condamner sans témérité ceux qui en sont convaincus, parce que
la condamnation que nous en faisons est si clairement précédée par celle
qui est empreinte dans la malice de l'acte, que le jugement que nous en
portons ne pouvant jamais être faux, ne peut par conséquent être
téméraire. Mais il y a d'autres actions dont les motifs sont douteux et
les intentions incertaines, qui peuvent être expliquées, ainsi que je
l'ai dit, d'un

 

1 I _Timoth_., V, 24. — 2 _De Serm._ _Domini in monte,_ lib. II, cap.
XVIII, n. 60.

 

(_a_) _Var_.: Afin d'empêcher, si nous le pouvons, un si grand
renversement des choses humaines. — (_b_) Dont le jugement suit les
actions.

 

277

 

bon ou d'un mauvais sens. De telles actions, dit l'Apôtre, ne portent
pas en elles-mêmes leur jugement, parce qu'il ne nous paroli pas dans
quel esprit, on les fait ; si bien que dans le jugement que nous en
faisons, nous accommodons ordinairement, non point notre pensée à la
chose, mais la chose ta notre pensée. Ainsi, dit le saint Apôtre, le
jugement ne précède pas dans la chose même ; ii.ms ne recevons pas la
loi, mais nous la donnons sans autorité. La sentence que nous prononçons
n'est donc qu'une pure idée, le songe d'un homme qui veille, le jeu ou
l'égarement d'un esprit qui bâtit en l'air et qui feint (_a_) des
tableaux dans les nues ; mais le jugement véritable suivra en son temps.

Car viendra le grand jour de Dieu, où tous les secrets des cœurs seront,
découverts, tous les conseils publiés, toutes les intentions éclaircies ;
et en attendant, chrétiens, le jugement du Seigneur n'ayant pas encore
paru, celui que nous porterions, en cela même que très-souvent il
pourrait être douteux et trompeur, serait toujours nécessairement
téméraire et dangereux. Voilà les deux états de notre prochain, sur
lesquels nous pouvons juger. O Dieu! que d'excès dans l'un et dans
l'autre! que de soupçons téméraires! que de préjugés iniques! que de
jugements précipités! _Delicta quis intelligit_ (1) ? Qui pourra
entendre tous ces crimes? qui pourra démêler tous ces embarras? Pour
vous en donner l'ouverture, je vous propose en un mot une maxime
générale que je mets devant votre vue comme un flambeau lumineux, sous
la conduite duquel vous pourrez ensuite descendre au détail des vices
particuliers dans lesquels nous tombons par nos jugements.

Cette merveilleuse lumière que j'ai aujourd'hui à vous proposer, c'est,
Messieurs, cette vérité, que nous devons suivre Dieu et juger autant
qu'il décide. Car ce beau commandement de ne juger pas. si souvent
répété dans les Écritures, ne s'étend pas jusqu'à nous défendre de
condamner ce que Dieu condamne ; au contraire c'est notre devoir de
conformer notre jugement à celui de sa vérité. Non, non, ne croyez pas,
chrétiens, que ce soit le dessein de notre. Sauveur de faire un asile au
vice, de le mettre à couvert du blâme

 

1 _Psal._ XVIII, 13.

 

(_a_) _Var._ : Qui fait.

 

278

 

et de le laisser triompher sans contradiction (_a_) ; il veut qu'on le
trouble, qu'on l'inquiète, qu'on le blâme, qu'on le condamne. Il faut
condamner hautement les crimes publics et scandaleux ; il faut aller
quelquefois en les reprenant jusqu'à la dureté et à la rigueur (_b_). «
Reprends-les durement, » dit le saint Apôtre : _Increpa illos dure_ (1) ;
c'est-à-dire qu'il faut presser les pécheurs et leur jeter pour ainsi
dire quelquefois au front des vérités toufes sèches pour les faire
rentrer en eux-mêmes, parce que la correction qui a deux principes, la
charité et la vérité, doit emprunter ordinairement une certaine douceur
de la charité qui est douce et compatissante, mais elle doit aussi
souvent emprunter quelque espèce de rigueur et de dureté de la vérité
qui est inflexible.

Vous voyez donc qu'il nous est permis, bien plus, qu'il nous est ordonné
de condamner hardiment les conduites scandaleuses dès pécheurs publics,
parce que le jugement de Dieu précédant le nôtre, nous ne craignons pas
de nous égarer. Mais voici la règle immuable que nous devons observer :
c'est de suivre Dieu simplement, sans rien usurper pour nous-mêmes.
Telle est la règle assurée que sa vérité rend souveraine, son équité
infaillible, sa simplicité vénérable. Mais nous péchons doublement
contre l'équité de cette règle. Cardans sa simplicité elle ne laisse pas
d'avoir deux parties nécessairement enchaînées : la première, de suivre
Dieu, et au contraire nous jugeons plus que Dieu ne juge ; la seconde, de
ne rien usurper pour nous, et au contraire en jugeant les crimes nous
nous attribuons ordinairement une injuste supériorité sur les personnes,
qui nous inspire une aigreur cachée ou un superbe dédain (_c_).

Par exemple, car il faut venir au détail des choses, et j'ai promis

d'y descendre, cet homme est voluptueux, et cet autre est injuste

et violent. Vous condamnez leur conduite, et vous ne la condamnez

pas témérairement, puisque la loi divine la condamne aussi. Mais

 

1 _Tit_., I, 13.

 

(_a_) _Var._ : Que ce soit le dessein de notre Sauveur qu'on épargne le
vice, ni qu'il triomphe.— (_b_) Il faut condamner les crimes publics et
scandaleux ; bien loin qu'il nous soit défendu de les condamner, il nous
est commandé de les reprendre et d'aller quelquefois en les reprenant
jusqu'à la dureté et à la ligueur. — (_c_) Un dédain fastueux.

 

279

 

si vous les regardez, dit saint Augustin (1), comme des malades
incurables, si vous vous éloignez d'eux comme de pécheurs incorrigibles,
vous laites injure à Dieu et vous ajoutez à son jugement. Vous avez vu
ces personnes dans des pratiques dangereuses ; vous blâmez ces pratiques,
et vous faites bien, puisque l'Écriture les blâme. Mais vous jugez de
l'état présent par les désordres de la vie passée ; vous dites avec le
Pharisien : Si l'on savait quelle est cette Femme! et vous ne regardez
pas, non plus que lui, qu'elle est peut-être changée parla pénitence :
vous ne jugez plus selon Dieu, et vous passez les bornes qu'il vous a
prescrites. Ne jugez donc plus désormais ni de l'avenir par le présent,
ni du présent par le passé. Car ce jugement n'est pas selon Dieu ni
selon ses saintes lumières. « Chaque jour, dit l'Écriture, a sa malice
 (2). » Ainsi lorsque vous découvrez quelque désordre visible, au lieu
d'outrager vos frères par des invectives cruelles, espérez plutôt un
temps meilleur et plus pur (_a_), et tempérez par cette espérance
l'amertume de votre zèle qui s'emporte avec trop d'excès. Ne jugez pas
de l'état présent par vos connaissances passées. Car ignorez-vous les
miracles qu'opère l'Esprit de Dieu dans la conversion des cœurs?
Peut-être que ce vieux pécheur est devenu un autre homme par la grâce de
la pénitence. Si vous découvrez encore en sa vie quelque reste de
faiblesse humaine, gardez-vous bien de conclure que c'est un trompeur et
un hypocrite ; ne dites pas, comme vous faites : Ah! le cœur commence à
paraître, le naturel s'est fait voir à travers le masque dont il se
couvrait. Car, ô Dieu! ô juste Dieu! quel est ce raisonnement? Quoi!
s'ensuit-il qu'on soit un démon, parce qu'on n'est pas un ange ; ou que
l'embrasement dure encore, parce que l'on voit quelque fumée ou quelque
noirceur ; ou que la campagne soit inondée, parce que la rivière en se
retirant a laissé peut-être quelques eaux en des endroits plus profonds ;
ou que les passions dominent encore, parce qu'elles ne sont pas
peut-être tout à fait domptées? Vous dites que c'est malice, et c'est
peut-être imprudence ; vous dites que c'est habitude, et c'est peut-être
chaleur et emportement.

 

1 _De Serm. Domini in monte,_ ubi supra. — 2 _Matth.,_ VI, 34.

 

(_a_) _Var._ : Plus heureux.

 

280

 

Ah! cet homme que vous blâmez d'une façon si cruelle, fait peut-être
beaucoup davantage. Non-seulement il se blâme, mais il se condamne, mais
il se châtie, mais il gémit de son mal qu'il voit sans doute devant Dieu
bien plus grand sans comparaison que vos jugements indiscrets ne le font
paraître à vos yeux. Cessez donc de vous égaler à la puissance suprême
par la témérité de juger vos frères. Blâmez ce que Dieu blâme, condamnez
ce que Dieu condamne ; mais ne passez point ces limites (_a_) sacrées.
«Ne soyez point sages plus qu'il ne faut ; mais soyez sages selon la
mesure (1), » c'est-à-dire ne jugez pas plus que Dieu n'a voulu juger.
Autant qu'il a plu à ce grand Dieu de nous découvrir ses jugements, ne
craignez point de les suivre ; mais croyez que tout ce qui est au delà
est un abîme effroyable où notre audace insensée trouvera un naufrage
infaillible (_b_). Ce n'est pas assez, chrétiens ; et nous avons remarqué
que même en nous élevant contre les péchés (_c_) publics, nous tombons
dans un autre excès ; nous exerçons sur les autres (_d_) une espèce de
tyrannie, nous prenons contre eux un esprit d'aigreur ou un esprit de
dédain, et devenons tellement censeurs que nous oublions que nous sommes
frères. Tel était le vice des pharisiens ; ce n'était pas la compassion
de notre commune faiblesse qui leur faisait reprendre les péchés des
hommes, ils se tiraient hors du pair ; et comme s'ils eussent été les
seuls impeccables, ils parlaient toujours dédaigneusement des pécheurs
et des publicains. Ils s'érigeaient en censeurs publics, non point pour
guérir les plaies et corriger les péchés, mais pour s'élever au-dessus
des autres et étaler magnifiquement leur orgueilleuse justice. C'est
pourquoi le Seigneur Jésus les voyant approcher de lui dans cet esprit
dédaigneux, il les confond par cette parole : « Celui, dit-il, qui est
innocent, qu'il jette la première pierre (2). »

Apprenons de là, chrétiens, en quel esprit nous devons juger, même des
crimes les plus scandaleux : gardons-nous de tirer aucun avantage de la
censure que nous en faisons. Car n'avons-nous pas reconnu que ce n'est
pas à nous de rien prononcer, mais de

 

1 _Rom.,_ XII, 3. — 2 _Joan.,_ VIII, 7.

 

(_a_) _Var._: Ces bornes.— (_b_) Sa perte infaillible.— (_c_) Les
scandales.— (_d_) Nos frères.

 

281

 

suivre humblement ce que Dieu prononce? La lumière de vérité qui brille
en nos âmes et y condamne les dérèglements (_a_) que nos frères nous
rendent visibles dans leurs actions criminelles, n'est pas une
prérogative (_b_) qui nous soit donnée pour prendre ascendant sur eux ;
mais c'est une impression qui se fait en nous de la justice supérieure
par laquelle nous serons jugés tous ensemble. Ainsi prononçant parle
même arrêt leur condamnation et la vôtre, pouvez-vous en tirer aucun
avantage, et ne devez-vous pas au contraire être saisis de frayeur et de
tremblement? Considérez le Sauveur et voyez dans quel esprit de
condescendance il dit à la femme adultère : Je ne te condamnerai pas. Si
la justice même est si indulgente, faut-il que la malice soit
inexorable? Si le juge est si patient, le criminel ose-t-il être
rigoureux? Car enfin si le crime que vous condamnez, si cet infâme
adultère qui vous fait dédaigner cette pécheresse n'est pas dans votre
cœur par consentement, il n'est pas moins dans le fond de votre malice,
ou dans celui de votre faiblesse. Ignorez-vous, chrétiens, de quelle
sorte les péchés s'engendrent en nous? Ils y naissent comme des vers :
_Os fatuorum ebullit stultitiam_ (1) ; non engendrés parle dehors, mais
conçus et bouillonnants au dedans de la pourriture invétérée de notre
substance (_c_) et du fond malheureusement fécond de notre corruption
originelle. Ainsi quand les crimes que vous blâmez ne seraient point
dans vos consciences par une attache actuelle, ils sont enfermés
radicalement dans ce foyer intérieur de votre corruption ; et si jamais
ils en sortent par une attache effective, en condamnant votre frère,
n'aurez-vous pas parlé contre vous et foudroyé votre tête? Et quand nous
ne tomberions jamais dans ce même crime, ne tombons-nous pas tous les
jours dans de semblables excès, également condamnés (_d_) par cette
suprême Vérité qui est l'arbitre de la vie humaine? Car celui qui a dit
: « Tu ne tueras pas, » a défendu aussi l'impudicité ; et quoique les
tables des commandements soient partagées en plusieurs articles, c'est
la même lumière  très-simple de la justice divine qui autorise tous les

 

1 Prov., XV, 2.

 

(_a_) _Var._ : Les désordres. — (_b_) Une connaissance. — (_c_) Mais
conçus et formés de la pourriture intérieure de notre substance. — (_d_)
Qui sont également condamnés.

 

282

 

préceptes, proscrit tous les crimes, réprouve toutes les transgressions.

« Toi donc qui juges les autres, tu te condamnes toi-même, » comme dit
l'Apôtre (1). Par conséquent, chrétiens, si nous osons condamner nos
frères, et nous le devons quelquefois, quand leurs crimes sont
scandaleux, ne condamnons pas leurs excès comme en étant éloignés ; que
ce ne soit pas pour nous mettre à part, mais pour entrer tous ensemble
dans un sentiment intime et profond et de nos communs devoirs et de nos
communes faiblesses. Ainsi nous souvenant de ce que nous sommes, ne nous
laissons jamais emporter à ces invectives cruelles (_a_), à ces
dérisions outrageuses qui détournent malicieusement contre la personne
l'horreur qui est due au vice. C'est un jeu cruel et sanglant qui
renverse tous les fondements de l'humanité (_b_). « Un innocent, dit
Tertullien parlant contre les jeux des gladiateurs (c'en est ici une
image), ne fait jamais son plaisir du supplice d'un coupable : »
_Innocens de supplicio alterius lœtari non potest_ (2). Que si c'est une
cruauté de se réjouir du supplice de son frère, quelle horreur, quel
meurtre, quel parricide de se faire un jeu, de se faire un spectacle, de
se faire un divertissement de son crime même !

Si nous devons être si réservés dans les péchés scandaleux, quelle doit
être notre retenue dans les choses cachées et douteuses? A quoi
pensons-nous, mes frères, de nous déchirer mutuellement par tant de
soupçons injustes? Hélas! que le genre humain est malheureusement
curieux! chacun veut voir ce qui est caché et juger des intentions.
Cette humeur curieuse et précipitée fait que ce qu'on ne voit pas on le
devine ; et comme nous ne voulons jamais nous tromper, le soupçon devient
bientôt une certitude, et nous appelons conviction ce qui n'est tout au
plus qu'une conjecture. Mais c'est l'invention de notre esprit, à
laquelle nous applaudissons et que nous accroissons sans mesure. Que si
parmi ces soupçons notre colère s'élève, nous ne voulons plus l'apaiser,
parce que « nul ne trouve sa colère injuste : » _Nulli irascenti ira sua
videtur injusta_ (3). Ainsi l'inquiétude nous prend ; et par cette

 

1 _Rom.,_ II, 1. — 2 _De Spect_., n. 19. — 3 S. August., _Epist_.
XXVIII, n. 2.

 

(_a_) _Var._ : .Sanglantes. — (_b_) De la charité.

 

283

 

inquiétude nourrie par nos défiances, souvent nous nous battons contre
une ombre, ou plutôt l'ombre nous fait attaquer le corps. Nous frappons
de peur d'être prévenus, nous vengeons une offense qui n'est pas encore
: _Ipsà sollicitudine priùs malum facimus quàm patimur_ (1) : voyez le
progrès de l'injustice. Mon Dieu, je renonce devant vous à ces
dangereuses subtilités de notre esprit qui s'égare. Je veux apprendre de
votre bonté et de votre sainte justice à ne présumer pas aisément le
mal, à voir et non à deviner, à ne précipiter pas mon jugement, mais à
attendre le vôtre.

Vous me dites que si j'agis de la sorte, je serai la dupe publique,
trompé tous les jours mille et mille fois ; et moi, je vous réponds à mon
tour : Eh quoi ! ne craignez-vous pas d'être si malheureusement
ingénieux à vous jouer de l'honneur et de la réputation de vos
semblables? J'aime beaucoup mieux être trompé que de vivre éternellement
dans la défiance, fille de la lâcheté et mère de la dissension.
Laissez-moi errer, je vous prie, de cette erreur innocente que la
prudence, que l'humanité, que. la vérité même m'inspire. Car la prudence
m'enseigne à ne précipiter pas mon jugement, l'humanité m'ordonne de
présumer plutôt le bien que le mal ; et la vérité même m'apprend de ne
m'abandonner pas témérairement à condamner les coupables, de peur que
sans y penser je ne flétrisse les innocents par une condamnation
injurieuse.

 


SECOND POINT.

 

Il pourrait sembler, chrétiens, que c'est presser trop mollement cette
pécheresse à se censurer elle-même, que de lui ordonner simplement de ne
pécher plus, et la traiter cependant avec une telle indulgence ; mais il
faut vous faire comprendre qu'il n'y a rien de plus efficace pour
rappeler une âme étonnée au sentiment de ses crimes.

Nous pouvons voir nos péchés ou dans la justice de Dieu, ou dans ses
miséricordes et dans les trésors de ses bontés infinies. Je soutiens, et
il est vrai, que si la justice nous les fait voir d'une manière plus
terrible, la bonté nous les fait sentir d'une manière plus vive et plus
pénétrante. Nos péchés sont contraires, je vous

 

1 S. August., _Serm_. CCCVI, n. 9.

 

284

 

l'avoue, à la justice de Dieu qui les punit ; mais ne le sont-ils pas
beaucoup plus à la bonté de Dieu qui les efface? Que faites-vous, ô
justice? Vous laissez le crime, et vous y ajoutez la peine. Mais vous, ô
bonté, ô miséricorde, vous ôtez tout ensemble la peine et le crime ; et
en pardonnant au pécheur, vous portez au fond de son cœur par votre
indulgence la lumière la plus perçante pour confondre son ingratitude.
La justice tonne et foudroie : que fait-elle par ses foudres et par son
tonnerre? Elle remplit l'imagination de la terreur de la peine. La bonté
va bien plus avant, qui par ses facilités et ses compassions fait sentir
au dedans l'horreur de la faute. Au milieu du bruit que fait la justice,
le cœur troublé se resserre et à peine se sent-il lui-même, (_a_) Les
douceurs de la bonté le dilatent pour recevoir les impressions du
Saint-Esprit ; tout s'épanche, tout se découvre, et jamais on ne sent
mieux son indignité que lorsqu'on se sent prévenu par une telle
profusion de grâces.

Quand Joseph se découvrit à ses frères et qu'il leur dit ces paroles :
«Je suis Joseph votre frère, que vous avez vendu en Égypte, ils furent
saisis d'une grande horreur (1) ; » ils sentirent bien qu'ils avoient mal
fait de le livrer de la sorte. Mais lorsqu'il commença non-seulement à
les rassurer, mais à les excuser, et qu'il leur dit ces paroles : « Eh !
ne vous affligez pas de m'avoir vendu ; ce n'a pas tant été par votre
malice que par un conseil de Dieu, qui voulait vous préparer ici un
libérateur par une telle aventure (2). » Et lorsqu'il « les embrassa et
qu'il pleura sur chacun d'eux en particulier : » _Et ploravit super
singulos_ (3), ah ! les reproches les plus sanglants qu'il aurait pu
inventer contre eux, n'eussent pas été capables de les faire entrer dans
le sentiment de leurs crimes à l'égal de ces larmes, de cette tendresse,
de ces embrassements imprévus d'un frère si outragé, et néanmoins si bon
et si tendre et si bienfaisant. Il en est de même de notre grand Dieu.

 

1 _Genes.,_ XLV, 3, 4. — 2 _Ibid_., 5, 7, 8. — 3 _Ibid_., 15.

 

(_a_) Note marg. : Le mouvement dans la crainte..... le cœur se trouble
et à

peine se sent-il lui-même ; il se resserre en lui-même et voudrait se
cacher à ses propres yeux : il fuit de toute sa force la colère qui le
poursuit ; et pour fuir plus précipitamment, il voudrait pouvoir se
séparer de soi-même, parce qu'il trouve toujours dans son fond un Dieu
vengeur.

 

285

 

Qu’il tonne, qu'il menace et qu'il foudroie ; qu'il crie à mon âme
étonnée par la bouche de son prophète : Tu m'as quitté, infidèle ; tu
t'es abandonnée à tous les passants, épouse volage et parjure : _Tu
autem fornicata es cum amatoribus multis_ (1) ; j'entre à la vérité dans
le sentiment de mes horribles infidélités ; mais lorsqu'il ajoute après :
« Toutefois retourne à moi, et je te recevrai, dit le Seigneur, » c'est
ce qui achève de percer mon cœur, et je ne vois jamais mieux mes
ingratitudes qu'au milieu de ces bontés si peu méritées. Non, mes
frères, il n'y a rien de plus efficace pour nous faire rentrer en
nous-mêmes : ces bontés si gratuites, si abondantes, si inespérées, si
surprenantes, poussent l'âme jusqu'à son néant ; et les larmes d'un père
attendri qui tombent sur le cou de son prodigue, lui font bien mieux
sentir son indignité que les reproches amers par lesquels il aurait pu
le confondre.

Venez donc ici, chrétiens, et écoutez votre Sauveur qui vous montre vos
ingratitudes. Ce n'est pas la voix de son tonnerre ni le cri de sa
justice irritée que je veux faire retentir à vos oreilles : parlez,
amour ; parlez, indulgence ; parlez, bontés attirantes d'un Dieu qui est
venu chercher les pécheurs, qui leur veut faire sentir leur indignité,
non par la violence de ses reproches, mais par l'excès de ses grâces ;
non en prononçant leur sentence , mais en leur accordant leur pardon
(_a_). C'est la méthode du Sauveur des âmes. Il ne dit rien de fâcheux
ni aux pécheurs, ni aux publicains qui conversaient avec lui. Il tourne
toute son indignation contre les pharisiens hypocrites dont le superbe
chagrin s'opposait à la conversion des pécheurs. Pour lui, qui était
venu rechercher et porter sur ses épaules ses brebis perdues, il ne
rebute point les pécheurs par un dédain accablant et par des paroles
désespérantes. Il ne dit rien de rude ni à Madeleine, ni à la
Samaritaine, ni à la femme adultère ; et sans les confondre par ses
reproches, il laisse faire cet ouvrage et à l'excès de leurs crimes et à
l'excès de ses grâces.

Ah! il n'y a plus moyen de lui résister ; il faut mourir de regret
d'avoir offensé si indignement une telle miséricorde. Car d'où

 

1 Jerem., III, 1.

(_a_) _Var_. : Leur absolution.

 

286

 

vient cette facilité et cette indulgence? Est-ce qu'il n'a pas horreur
des péchés, lui qui vient mourir pour les expier? Est-ce qu'il n'a pas
la puissance de les châtier, lui entre les mains duquel toutes les
créatures sont autant de foudres? Est-ce que les paroles lui manquent
pour convaincre nos ingratitudes, lui, mes frères, dont le moindre mot
pouvait laisser sur le front une impression de honte éternelle? D'où
vient qu'il se tait et qu'il dissimule? C'est qu'il connaît nos
faiblesses, c'est qu'il a pitié de nos maux. Encore une fois, mes
frères, il faut mourir de regret ; et en même temps qu'il nous dit : Je
ne te condamne pas, il faut ramasser ensemble tout ce qu'il y a dans nos
âmes et de force et d'infirmité, et de lumières et de ténèbres, et de
péchés et de grâces, pour nous condamner nous-mêmes et confondre devant
sa face nos trahisons et nos ingratitudes (_a_).

D'autant plus, chrétiens, et voici ce qu'il y a de plus fort, que cette
indulgence lui coûte bien cher. C'est ici ce qu'il faut entendre, c'est
ici ce qui doit presser un cœur chrétien. Si Jésus nous est facile et
indulgent, il a acheté, mes frères, cette indulgence qu'il a pour nous
par des rigueurs inouïes (_b_), qu'il a souffertes en lui-même. Il n'a
pardonné aucun crime, il n'a dit aucune parole de miséricorde, de
douceur, de condescendance, qui ne lui ait coûté tout son sang. Car que
méritait le pécheur d'un Dieu irrité, sinon des menaces, des rebuts, des
arrêts de mort éternelle ? Mais Jésus notre saint pontife, pontife
vraiment charitable et compatissant à nos maux , a voulu nous traiter
avec indulgence ; et pour acquérir ce beau droit de nous traiter, quoique
indignes, avec une bonté paternelle, il s'est abandonné volontairement à
des rigueurs insupportables (_c_).

Venez à la croix, Madeleine ; venez-y, ô femme adultère de notre
évangile ; voyez les coups de foudre, voyez les rigueurs, voyez le poids
des vengeances qui accable ce Dieu-Homme. Voyez le ciel et la terre
conjurant sa perte, les hommes furieux, son Père implacable, l'enfer
déchaîné contre lui. O quel excès de rigueur ! C'est par là qu'il a
mérité de vous pouvoir traiter doucement. Le

 

(_a_) Var. : Nos perfidies. — (_b_) Par une rigueur insupportable ; —par
une extrême rigueur. — (_c_) Inouïes.

 

287

 

croyiez-vous, pauvres âmes, lorsqu'il vous parlait si obligeamment?
croyiez-vous que cette douceur lui coûtât si cher? Vous croyiez
peut-être alors qu'il vous faisait une grâce qui ne lui coûtait autre
chose que d'ouvrir seulement son cœur, trésor inépuisable de compassion
; et il faisait un échange ; et pour faire luire sur vous un rayon de
faveur divine, il se dévouait intérieurement à des rigueurs infinies, à
des duretés intolérables. A vous donc toute la douceur, à lui toutes les
amertumes : à vous les consolations, à lui les délaissements : à vous la
facilité, le pardon, la condescendance ; à lui les foudres, à lui les
tempêtes et tout ce que peut inventer une colère inflexible et
inexorable. Mes frères, c'est à ce prix que Jésus nous est indulgent.
Pouvons-nous après cela arrêter les yeux sur les bontés qu'il exerce,
sans avoir le cœur pénétré de ce que lui coûtent nos crimes? Autant de
grâces qu'il nous donne, autant de péchés qu'il nous remet ; autant de
fois qu'il nous dit : Je ne te condamnerai pas, et il nous le dit à
chaque moment, nous devons croire, mes frères, qu'il étale autant de
fois à nos yeux toutes les rigueurs de sa croix et toute l'horreur du
Calvaire. Et comme à chaque moment son enfer devait s'ouvrir sous nos
pieds, autant d'instants qu'il nous accorde pour prolonger le temps de
la pénitence, autant nous dit-il de fois : Vois, je ne te condamne pas,
puisque je t'attends ; je ne te condamne pas, puisque je t'invite ; je
ne te condamne pas, puisque je te presse et que je ne cesse de te dire :
Retourne, prévaricateur, et tu vivras ; retournez, enfants perfides ;
retournez, épouses déloyales ; « et pourquoi voulez-vous périr, maison
d'Israël (1) ? » Donc, mes frères, autant de moments que Jésus nous
attend à la pénitence, autant de fois, non sa voix mortelle, mais ce qui
est beaucoup davantage, sa bonté, sa miséricorde, sa patience déclarée,
son sang, sa grâce, son Saint-Esprit nous disent au fond du cœur : Je
ne te condamne pas ; va, et désormais ne pèche plus. Et tout cet excès de
miséricorde dont nous ressentons le fruit, nous rappelle aux rigueurs
horribles qui en ont été la racine. Donc, ô Jésus, ô divin Jésus! que
vos miséricordes sont pressantes ! ah ! dans le moment que je les
ressens, je vois toutes vos plaies se rouvrir, tout votre sang se
déborder. Il

 

1 Ezech., XXXIII, 11.

 

288

 

faut pleurer du sang, pour le mêler avec celui que vos tendresses et mes
duretés, que vos bontés et mes ingratitudes vous ont fait répandre.

Laissons-nous toucher, chrétiens, à cet excès de miséricorde, et
apprenons aujourd'hui à voir toute l'horreur de nos crimes dans la grâce
qui nous les remet. « Affliger et contrister l'Esprit de Dieu ! »
_Nolite contristare Spiritum sanctum_ (1) ; cette affliction ne marque
pas tant l'injure qui est faite à sa sainteté par notre injustice, que
la violence que souffre son amour méprisé et sa bonne volonté frustrée
par notre résistance opiniâtre. Affliger le Saint-Esprit, c'est-à-dire
l'amour de Dieu opérant en nous pour lui gagner nos cœurs par sa bonté !
Il se mesure avec nous par les tendresses de son amour, par les
empressements de sa miséricorde. Combien la dureté est-elle inhérente,
si elle ne s'amollit pas, etc. !

Réglons donc tous nos jugements sur celui de Jésus-Christ. Madame, voilà
la règle que se propose sans doute une princesse si éclairée ; c'est la
seule qui est digne d'une âme si grande et d'un esprit si bien faitetsi
pénétrant. Vos lumières seront toujours pures, quand elles seront
dirigées par les lumières d'en haut. On louera plus que jamais ce juste
discernement, ce jugement exquis, ce goût délicat, quand vous
continuerez à goûter les célestes vérités et à préférer les biens que
l'Évangile nous présente, à tous ceux que le monde nous donne et à tous
ceux qu'il nous promet, beaucoup plus grands que ceux qu'il nous donne.
Tous les peuples, déjà gagnés à Votre Altesse royale par une forte
estime et par une juste et très-respectueuse inclination, y joindront
une vénération qui n'aura point de limites et qui portera votre gloire à
un si haut point, qu'il n'y aura rien au-dessus que la gloire même des
saints et la félicité éternelle.

 

1 _Ephes_., IV, 30.

 

[Précédente]

[Accueil]

[Suivante]

[PASSION MARDI]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
I Carême I
I Carême II
I Carême III
I Carême IV
I Carême Plan
I Carême Ld
I Carême Vd
II Carême I
II Carême II
II Carême Ld
Honneur
II Carême Jd I
II Carême Jd II
III Carême I
III Carême II
III Carême Mardi
III Carême Vdi
III Carême Sdi
III Carême Sdi abr.
IV Carême I
IV Carême II
IV Carême III
IV Carême Mardi abr.
IV Carême Mcdi Plan
IV Carême Vdi
Brièveté Vie frg
Passion I
Passion II
Passion III
Passion Mardi
Sem. Passion (3) I
Sem. Passion (3) II
Sem. Passion (3) III
Vendr. Passion I
Vendr. Passion II
Sem. Passion abrg.
Rameaux I
Rameaux II
Rameaux III
Rameaux IV

 



SERMON POUR LE MARDI DE LA SEMAINE DE LA PASSION, SUR LA SATISFACTION (_A_).


 

_Non potest mundus odisse vos ; me autem odit, quia  ego testimonium
perhibeo de illo, quòd opera ejus mala sunt_.

 

Le monde ne saurait vous haïr ; mais pour moi, il me hait, parce que je
rends témoignage contre lui, que ses œuvres sont mauvaises. _Joan.,_
VII, 7.

 

L'évangile du jour nous apprend que le Sauveur va en Jérusalem pour y
célébrer la fête des Tabernacles. Cette fête des Tabernacles était comme
un mémorial éternel du long et pénible pèlerinage des enfants d'Israël
allant à la terre promise, et tout ensemble représentait le pèlerinage
des enfants de Dieu allant à leur céleste patrie.

Brève explication de cette fête. Nous lisons au Lévitique que parmi le
grand nombre de victimes qu'on offrait à Dieu pendant le cours de cette
solennité, on ne manquait pas de lui présenter

 

(_a_) Prêché à Metz, en 1688.

Ce sermon trahit son origine par des indices certains, mais il renferme
moins de tours et de ternies surannés que d'autres essais du jeune
archidiacre ; c'est pourquoi notre date est fixée dans les derniers
temps de l'époque de Metz. Mais voici quelque chose de plus précis.
Après avoir dit que Dieu menaça de renverser Ninive, mais que Ninive
elle-même se renversa devant Dieu par le changement de sa conduite,
l'auteur continue dans la péroraison : « Armons-nous de zélé, que chacun
renverse Ninive en soi-même. Ville de Metz, que n'es-tu ainsi renversée
!... Plût à Dieu que je visse à bas et les tables de tes débauches, et
les banquets de tes usuriers, et les retraites honteuses de tes
impudiques! Plût à Dieu que j'entende bientôt cette bienheureuse
nouvelle: Toute la ville de Metz est abattue, mais elle est heureusement
abattue aux pieds des confesseurs, devant les tribunaux de la pénitence
qui sont érigés de toutes parts dans ce temple auguste !» Ou a lu le nom
de la ville de Metz en toutes lettres. D'un autre côté, ces « tribunaux
de la pénitence qui sont érigés de toutes parts » indiquent assez
clairement, d'accord avec l'histoire, le temps de la mission qui fut
donnée en 1638. Quant aux usuriers de la ville de Metz, ils étaient
passés en proverbe ; c'étaient des Juifs retors et fourbes, qui payaient
par d'indignes exactions un généreux asile.

 

439

 

tous les jours un sacrifice pour le péché. Par là que devons-nous
apprendre, sinon que pendant le temps de notre voyage nous devons offrir
à Dieu tous les jours le sacrifice pour nos péchés? Et quel est ce
sacrifice pour nos péchés, sinon les satisfactions qui sont les vrais
fruits de la pénitence ? C'est de quoi nous parlerons... Assistance du
Saint-Esprit.

 

Ce que dit le Fils de Dieu, que le monde le liait à cause du témoignage
qu'il rend que ses œuvres sont mauvaises, se vérifie particulièrement
dans le sacrement de la pénitence. C'est principalement dans la
pénitence que Jésus-Christ rend témoignage contre les péchés. Il rend
bien témoignage contre les péchés par la prédication de la parole. Car
sa parole n'est autre chose qu'une lumière que Dieu élève au milieu de
l'Église, afin que les œuvres de ténèbres soient découvertes et
condamnées, mais cela ne se fait qu'en général ; au lieu que dans le
sacrement de la pénitence, Dieu parle à la conscience d'un chacun de ses
péchés particuliers ; non-seulement il ordonne qu'on les accuse, mais
encore qu'on les condamne et qu'on les punisse. De là les satisfactions
que l'on nous impose , les peines et les pénitences qu'on nous commande.
C'est aussi pour cette raison que plusieurs fuient Jésus-Christ dans la
pénitence : _Quia testimonium perhibeo_. Ils évitent de se confesser,
parce qu'ils appréhendent, disent-ils, de trouver quelque confesseur
fâcheux et sévère. Pour leur ôter cette pensée lâche qui entretient leur
impénitence, expliquons toute la matière de la satisfaction selon les
sentiments de l'Église et du saint concile de Trente : 1° la nécessité
de la satisfaction ; 2° quelle elle doit être ; 3° dans quel esprit nous
la devons faire.

 


PREMIER POINT.

 

La nécessité. Il ne faudrait point chercher d'autres preuves que les
exemples des saints pénitents : faut en rapporter quelques-uns. Si tous
ceux auxquels Dieu a inspiré le désir de la pénitence, il leur inspire
aussi dans le même temps la volonté de le satisfaire, on doit conclure
nécessairement que ces deux choses sont inséparables ; et si nous
refusons de suivre les pas de ceux qui nous ont

 

440

 

précédés dans la voie de la pénitence, nous ne devons jamais espérer le
pardon qu'ils ont obtenu : ce que nous verrons encore plus évidemment,
si nous concevons la raison par laquelle ils se sentaient pressés de
satisfaire à Dieu pour leurs crimes. C'est qu'ils étaient très-persuadés
que pour se relever de la chute où le péché nous a fait tomber, il ne
suffit pas de changer sa vie, ni de corriger ses mœurs déréglées. Car,
comme remarque excellemment le grand saint Grégoire, « ce n'est pas
assez pour payer ses dettes que de n'en faire plus de nouvelles, mais il
faut acquitter celles qui sont créées ; et lorsqu'on injurie quelqu'un,
il ne suffit pas pour le satisfaire de mettre fin aux injures que nous
lui disons, mais encore outre cela la justice nous ordonne de lui en
faire réparation ; et lorsqu'on cesse d'écrire, il ne s'ensuit pas pour
cela qu'on efface ce qui est déjà écrit, il faut passer la plume sur
l'écriture que nous avons faite, ou bien déchirer le papier (1). » Il en
est de même de nos péchés. Tout autant de péchés que nous commettons,
autant de dettes contractons-nous envers la justice divine. Il ne suffit
donc pas de n'en faire plus de nouvelles, mais il faut payer les
anciennes ; et lorsque nous nous abandonnons au péché, quelle injure ne
disons-nous pas contre Dieu? Nous disons qu'il n'est pas notre créateur,
ni notre juge, ni notre Père , ni notre Sauveur, etc. Est-ce donc assez,
chrétiens, de cesser de lui dire de telles injures, et ne sommes-nous
pas obligés de plus à lui en faire la satisfaction nécessaire ? Enfin
quand nous péchons, nous écrivons sur nos cœurs : _Peccatum Juda
scriptum est stylo ferreo_..... _super latitudinem cordis eorum_ (2). Ne
croyons donc pas faire assez lorsque nous ne continuons pas d'écrire ;
cela n'efface pas ce qui est écrit. Il faut passer la plume, par les
exercices laborieux qui nous sont prescrits dans la pénitence, sur ces
tristes et malheureux caractères ; il faut déchirer le papier sur lequel
ils ont été imprimés, c'est-à-dire qu'il faut déchirer nos cœurs :
_Scindite corda vestra_ (3) ; ainsi ils seront effacés.

Mais pour pénétrer jusque dans le fond cette vérité catholique,
considérons sérieusement quelle est la nature de la pénitence. Le
sacrement de la pénitence est un échange mystérieux qui se fait,

 

1 _Pastor_., III part., cap. XXX. — 2 _Jerem.,_ XVII, 1. — 3 _Joel_.,
II, 13.

 

441

 

par la bonté divine, de la peine éternelle en une temporelle : _Quòd si
ipsi sibi judices fiant et veluti suae iniquitatis ultores, hic in se
voluntariam pœnam severissimœ animadversionis exerceant, temporalibus
pœnis mutabunt œterna supplicia_ (1). Et la raison en est évidente. Car
par le sacrement de la pénitence se fait la réconciliation de l'homme
avec Dieu. Or, dans une véritable réconciliation, on se relâche de part
et d'autre. Voyez de quelle sorte Dieu se relâche ; dès la première
démarche, il nous quitte la peine éternelle. Quelle serait, pécheur, ton
ingratitude, si tu refusais de te relâcher, en subissant volontairement
la peine temporelle qui t'est imposée? Si tu rejettes cette condition,
la réconciliation ne se fera pas. Car Dieu use tellement de miséricorde,
qu'il n'abandonne pas entièrement les intérêts de sa justice, de peur de
l'exposer au mépris : _Nullus debitae gravioris pœna accipit veniam,
nisi qualemcumque etsi longe minorem quant debebat, solverit pœnam ;
atque ita impertitur à Deo largitas misericordiae, ut non relinquatur
etiam justitia disciplina_ (2).

Il faut donc peser la condition sous laquelle Dieu oublie nos crimes et
se réconcilie avec nous ; c'est à charge que nous subirons quelque peine
satisfactoire, pour reconnaître ce que nous devons à sa justice infinie
qui se relâche de l'éternelle. Aussi voyons-nous clairement cette
condition importante dans les paroles du compromis qu'il a voulu passer
avec nous pour se réconcilier. Car remarquez ici, chrétiens, le mystère
de la réconciliation dans le sacrement de la pénitence. Dans ce
différend mémorable entre Dieu et l'homme pécheur, afin d'accorder les
parties, on commence à convenir d'arbitre, et on passe le compromis. Cet
arbitre, c'est Jésus-Christ, grand pontife et médiateur de Dieu et des
hommes. Mais Jésus-Christ se retirant de ce monde, il subroge les
prêtres en sa place et leur remet le compromis en main. Toutes les deux
parties conviennent de ces arbitres. Dieu en convient, puisque c'est son
autorité qui les établit ; les hommes aussi en conviennent, lorsqu'ils
se viennent jeter à leurs pieds. Il faut donc que ces arbitres
prononcent ; mais de quelle sorte prononceront-ils?

 

1 Julian. Pomer., _De Vitâ contempl.,_ lib. II, cap. VII, n. 2. — 2 S.
August., lib. _De Contin_., cap. VI, n. 15.

 

442

 

Suivant les termes du compromis. Lisons donc les termes du compromis, et
voyons les conditions sous lesquelles Dieu se relâche.

Voici comme il est couché dans les Écritures : _Quœcumque solveritis
super terram, erunt soluta et in cœlo_ (1). Voilà les paroles par
lesquelles Dieu se relâche. Faites-donc, arbitres établis de Dieu , ce
que Jésus-Christ vous permet ; et déliez entièrement le pécheur, sans
lui rien imposer pour son crime. Chrétiens, cela ne se peut. Car
achevons de lire le compromis : _Quaecumque ligaveritis super terram,
erunt ligata et in cœlo_. Il lui est donc permis de délier ; mais il lui
est ordonné de lier : voilà l'ordre qui lui est prescrit, et cette loi
doit être la nôtre. Car ce mystérieux compromis ayant été signé des
parties, il leur doit servir de loi immuable. Jésus-Christ l'a signé de
son sang au nom de son Père et comme procureur spécial établi par lui
pour cette réconciliation. Tu l'as aussi signé, pécheur, quand tu t'es
approché du prêtre en vertu de cette parole et de ce traité.
Jésus-Christ l'observe de son côté, et il te remet volontiers la peine
éternelle. Que reste-t-il donc maintenant, sinon que tu l'exécutes de ta
part avec une exacte fidélité? (Exhortation à satisfaire. Passage au
second point. ) Cette nécessité de la satisfaction étant solidement
appuyée, voyons à présent quelle elle doit être.

 


SECOND  POINT.

 

Je dis, pour ne point flatter les pécheurs, qu'elle doit être
très-sévère et très-rigoureuse ; et quand je l'appelle très-rigoureuse,
ce n'est pas qu'effectivement nous dussions l'estimer telle. Car si nous
considérons attentivement de quelle calamité nous délivre cet échange
miséricordieux qui se fait dans la pénitence, rien ne pourrait nous
paraître dur, si bien que cette pénitence n'est dure qu'à cause de notre
lâcheté et de notre extrême délicatesse. Mais afin de la surmonter,
appuyons invinciblement cette rigueur salutaire par le saint concile de
Trente ; et vous proposant trois raisons par lesquelles ce saint concile
établit la nécessité de satisfaire, faisons voir manifestement qu'elles
prouvent la sévérité que je prêche.

 

1 _Matth.,_ XVIII, 18.

 

443

 

La première raison des Pères de Trente, c'est que si la justice divine
abandonnait entièrement tous ses droits, si elle relâchait aux pécheurs
tout ce qui leur est dû pour leurs crimes, ils n'auraient pas l'idée
qu'ils doivent avoir du malheur dont ils ont été délivrés ; « et
estimant leur faute légère par la trop grande facilité du pardon, ils
tomberaient aisément dans de plus grands crimes. » De là vient que dans
ce penchant et sur le bord de ce précipice, pour ne point lâcher la
bride à la licence des hommes, Dieu en leur quittant la peine éternelle,
« les retient comme par un frein par la satisfaction temporelle : »
_quasi fraeno quodam,_ dit le saint concile de Trente (1).

Et certainement, chrétiens, il est bien aisé de connaître que tel est le
conseil de Dieu et l'ordre qu'il lui plaît de tenir avec les hommes. Car
il n'y a aucune apparence que ce Père miséricordieux en relâchant la
peine éternelle, en voulût réserver une temporelle, s'il n'y était porté
par quelque raison importante. Et quelle raison y aurait-il qu'après
s'être relâché si facilement d'une dette si considérable, c'est-à-dire
la damnation et l'enfer , il fit le dur et le rigoureux sur une somme de
si peu de valeur comme est la satisfaction temporelle ? il quitte
libéralement cent millions d'or, et il fait le sévère pour cinq sous? Il
fait quelque chose de plus ; car il y a bien moins de proportion entre
l'éternité de peines dont il nous tient quittes, et la satisfaction
qu'il exige dans le temps. D'où vient donc cette sévérité dans une si
grande indulgence ? Dieu est-il contraire à lui-même, et celui qui donne
tant pourquoi veut-il réserver si peu de chose ? C'est par un conseil de
miséricorde qui l'oblige à retenir les pécheurs, de peur qu'ils ne
retombent dans de nouveaux crimes. Il sait que la nature des hommes
portée d'elle-même au relâchement, abuse de la facilité du pardon pour
passer au libertinage. Il sait que s'il laissait agir sa miséricorde
toute seule, sans laisser aucune marque de sa justice , il exposerait
l'une et l'autre à un mépris tout visible à cause de la dureté de nos
cœurs. Ainsi donc en se relâchant, il ne se relâche pas tout à fait. La
justice ne quitte pas tous ses droits ; et s'il ne l'emploie plus à
punir les pécheurs comme ils le méritent, par

 

1 _Sess._ XIV, cap. VIII.

 

444

 

une damnation éternelle, il l'emploie du moins à les retenir dans le
respect et dans la crainte par quelque reste de peine qu'il leur impose.
Que si ces peines sont si légères qu'elles ne soient pas capables de
donner de l'appréhension aux pécheurs, qui ne voit que par cette lâcheté
nous éludons manifestement le conseil de Dieu? Un _Pater,_ un _Ave
Maria,_ un _Miserere_ peut-il faire sentir à un pécheur qui a commis de
grands crimes quelle est l'horreur de son péché, quel est le péril d'où
il est tiré et la peine qui lui était due? Il faut quelque chose de plus
rigoureux.

Prenez donc garde, ô confesseurs ; ce n'est pas moi qui vous parle,
c'est le concile de Trente qui vous avertit, c'est Dieu même qui vous
ordonne de prendre garde à ses intérêts : Je les remets, dit-il, en vos
mains. Déliez, je vous le permets ; mais liez, puisque je l'ordonne.
Vous êtes les juges que j'ai établis, vous êtes les ministres de ma
bonté et de ma justice. Usez de ma miséricorde, mais ne l'abandonnez pas
au mépris des hommes par une molle condescendance. Faites sentir aux
pécheurs l'horreur du crime qu'ils ont commis, par quelque satisfaction
convenable ; et tâchez par là de les retenir dans la voie de perdition
dans laquelle ils se précipitent , de peur que votre facilité ne leur
soit une occasion de libertinage et qu'abusant de votre indulgence, ils
ne fassent une nouvelle injure au Saint-Esprit par leurs fréquentes
rechutes.

La seconde raison du concile, c'est que la satisfaction est
très-nécessaire pour remédier aux restes des péchés et déraciner les
habitudes vicieuses. Pour entendre profondément cette excellente raison,
il faut remarquer que le péché a une double malignité. Il a de la
malignité en lui-même, et il en a aussi dans ses suites. Il a de la
malignité en lui-même, parce qu'il nous sépare de Dieu. Il a de la
malignité dans ses suites, parce qu'il abat les forces de l'âme et y
laisse une certaine impression pour retomber dans de nouvelles fautes.
C'est ce qu'on appelle l'habitude vicieuse ; et cette vicieuse habitude
ne s'éteint pas, encore que le péché cesse. Elle demeure donc dans nos
cœurs comme une pépinière de nouveaux péchés ; c'est un germe que le
péché effacé laisse dans les âmes, par lequel il espère revivre bientôt
; c'est une racine empoisonnée, qui dans peu fera repousser cette
mauvaise herbe.

 

445

 

C'est pour détruire ces restes maudits, c'est pour arracher ces
habitudes mauvaises, que le concile de Trente a déterminé que la
satisfaction était nécessaire. Et la raison en est évidente. Car
qu'est-ce autre chose qu'une habitude, sinon une forte inclination ? Et
comment la peut-on combattre, sinon en faisant effort sur soi-même par
les exercices mortifiants de la pénitence ? D'où je conclus, en passant
plus outre, que cette pénitence doit être sévère, parce que
l'inclination est puissante. C'est ce qui fait dire à saint Augustin
qu'il faut faire une pénitence rigoureuse, « afin, dit ce grand
personnage, que la coutume de pécher cède à la violence de la pénitence
: » _Ut violentiœ pœnitendi cedat consuetudo peccandi_ (1).

Il faut donc nécessairement que la pénitence ne soit pas molle ; il faut
qu'elle ait de la violence pour surmonter la mauvaise habitude, parce
que la mauvaise habitude donne une nouvelle force et une nouvelle
impétuosité à l'inclination naturelle que nous avons au mal par la
convoitise : si bien que l'habitude est un nouveau poids ajouté à celui
de la convoitise. Que si nous apprenons par les Écritures qu'il faut que
nous nous fassions violence pour résister à la convoitise, combien plus
en devons-nous faire à une convoitise fortifiée par une longue habitude
! Ne t'imagine donc pas, ô pécheur, que tu puisses résister à un si
grand mal par une pénitence légère ; que tu puisses te dépouiller de
cette ivrognerie si enracinée par quelque petite application à une
prière courte et souvent mal faite? Il faut avoir recours nécessairement
à cette violence salutaire de la pénitence ; il faut se mortifier par des
jeûnes et réprimer les dépenses excessives de tes débauches par
l'abondance de tes aumônes : _Ut violentiae pœnitendi cedat consuetudo
peccandi_.

La troisième raison du concile, et qui me semble la plus touchante,
c'est que nous devons satisfaire à Dieu par les peines salutaires de la
pénitence, pour nous rendre conformes à Jésus-Christ. C'est lui en
effet, chrétiens, qui est ce parfait pénitent qui a porté la peine de
tous les péchés, en se faisant la victime qui les expie. Si bien que
pour lui être semblables dans le sacrement de la

 

1 Tract, XLIX in _Joan.,_ n. 19.

 

446

 

pénitence, il faut que nous nous rendions des victimes mortifiées par
les peines salutaires qu'elle nous impose. Car, mes frères, il faut
remarquer que les sacrements de l'Église, comme ils tirent toute leur
vertu de la passion de notre Sauveur, aussi en doivent-ils porter en
eux-mêmes et imprimer sur nous une vive image. Ainsi dans le sacrement
de la sainte table nous annonçons la mort de Notre-Seigneur, comme dit
le divin Apôtre (1). Ainsi dans la pensée du même docteur nous sommes «
ensevelis avec Jésus-Christ dans le saint baptême (2) ; » et c'est
pourquoi l'Église ancienne plongeait entièrement dans les eaux tous les
fidèles qu'elle baptisait, pour représenter plus parfaitement cette
sépulture spirituelle. Ainsi dans la confirmation on imprime sur nos
fronts la croix du Sauveur, pour nous marquer d'un caractère éternel qui
nous doit rendre semblables à Jésus-Christ crucifié. N'y aura-t-il donc,
chrétiens, que le sacrement de la pénitence qui ne gravera point sur
nous l'image de la mort de notre Sauveur? Non, il n'en sera pas de la
sorte, dit le saint concile de Trente. La pénitence étant un second
baptême, il faut que ce qui a été dit du premier soit encore vérifié
dans le second ; que. «tout autant que nous sommes qui sommes baptisés en
Jésus-Christ, soyons baptises en sa mort : » _In morte ipsius baptizati
sumus_ (3). Et comment est-ce que la pénitence imprime sur nos corps la
mort de Jésus? Ecoutez parler le sacré concile : C'est alors, dit-il,
que nous subissons quelque peine pour nos péchés, que nous nous
baptisons dans nos larmes et dans les exercices laborieux que l'on nous
impose, « d'où vient aussi que la pénitence est nommée un baptême
laborieux (4). » Et par là ne voyez-vous pas combien la pénitence doit
être sévère?

Nous apprenons du sacré concile que nous devons nous rendre conformes à
Jésus-Christ crucifié par les pénitences que nous subissons. Ah! mon
Sauveur, quand je considère votre tête couronnée d'épines, votre chair
si cruellement déchirée, etc., je dis aussitôt en moi-même : Pauvre ver
écorché, quoi! une courte prière, un _Pater,_ un _Ave Maria,_ un
_Miserere_ sont-ils capables

 

1 I _Cor.,_ XI, 20. — 2 _Rom.,_ VI, 1.— 3 _Ibid_., 3. — 4 Sess. XIV  _De
Pœnit.,_

cap. II.

 

447

 

de nous crucifier avec vous ? Ne faut-il point d'autres clous pour
percer nos pieds qui tant de fois ont couru au crime, et nos mains qui
se sont souillées du bien d'autrui par tant d'usures cruelles ? Il faut
quelque chose de plus pénible, et c'est pourquoi le sacré concile
avertit sagement les confesseurs qu'ils donnent des pénitences
proportionnées. _Debent ergo sacerdotes Domini, quantum spiritus et
prudentia suggesserit, pro qualitate criminum et pœnitentium facultate,
salutares et convenientes satisfactiones injungere_ (1). Et ce qu'il
leur prescrit d'user de prudence, sachez et entendez, ô pécheurs, que ce
n'est pas pour les faire relâcher à cette condescendance molle et
languissante que votre cœur insensible et impénitent exige d'eux. Car
cette prudence qu'on leur ordonne, n'est pas cette fausse prudence de la
chair qui flatte les vices et les désirs corrompus des hommes ; c'est
une prudence spirituelle qui sacrifie la chair pour sauver l'esprit.
C'est pourquoi le concile dit : _Quantum spiritus et prudentia
suggesserit_ : Ayez de la prudence, dit ce saint concile, non pas une
prudence qui suive la chair, mais une prudence guidée par L'esprit :
_spiritus et prudentia._ Et afin de leur faire craindre un relâchement
excessif, il les avertit sagement que s'ils agissent trop indulgemment
avec les pécheurs, en leur ordonnant des peines très-légères pour des
péchés très-griefs, ils se rendent participais des crimes des autres. O
sentence vraiment terrible! One répondront devant Dieu ces confesseurs
lâches et complaisants, qui auront corrompu par leur facilité criminelle
la sévérité de la discipline, lorsqu'ils verront d'un côté s'élever
contre eux les Pères qui ont fait les canons, et particulièrement ceux
de Trente, qui les ont avertis si sérieusement du péril où les engageait
leur fausse et cruelle miséricorde ; et de l'autre les pécheurs mêmes
dont ils auront lâchement flatté les inclinations corrompues? C'est
vous, diront-ils, qui nous avez damnés ; c'est votre pitié inhumaine,
c'est votre indulgence pernicieuse. O Seigneur, faites-nous justice
contre ces ignorants médecins, qui pour trop épargner le membre pourri,
ont laissé couler le venin au cœur ; contre ces lâches conducteurs, qui
ont mieux aimé nous abandonnera la licence par une flatterie

 

1 Concil. Trident., _Sess_. XIV, cap. VIII.

 

448

 

dangereuse, que de nous retenir sur le penchant par une discipline
salutaire. Que reste-t-il donc, chrétiens, sinon que les prêtres et les
confesseurs évitent cette double accusation des pontifes et des conciles
qui les reprendront d'avoir méprisé leurs lois, et des pécheurs qui se
plaindront justement de ce qu'ils n'ont pas guéri leurs blessures? Ahl
disait à ce sujet autrefois un très-saint évêque de France, je ne me
sens pas assez innocent pour me vouloir charger des péchés des autres,
et je n'ai pas assez d'éloquence pour pouvoir répondre aux accusations
qu'intenteront un jour contre moi tant de saints et admirables prélats
qui ont fait les lois des conciles : _Ego me in hoc periculo mittere
omnino non audeo, quia nec talia sunt merita mea, ut aliorum peccata in
me excipere prœsumam, nec tantam eloquentiam habeo, ut ante tribunal
Christi contra tot ac tantos sacer dotes qui canones statuerunt, dicere
audeam_. Voilà quels doivent être les sentiments des confesseurs.
Achevons et disons un mot de la disposition des pénitents.

 


TROISIÈME  POINT.

 

Deux dispositions qui semblent contraires, avec lesquelles il faut
accomplir sa pénitence, la joie et la douleur : la joie, en considérant
non la peine qu'elle nous fait souffrir, mais celle d'où elle nous tire ;
la douleur amère pour plusieurs raisons, mais nous dirons en particulier
une qui regarde la satisfaction. C'est que les confesseurs inclinent
toujours à la miséricorde ; et quelque soin qu'ils aient de ne se point
écarter des bornes d'une juste sévérité, néanmoins l'amour paternel que
Dieu leur inspire pour leurs pénitents et l'expérience qu'ils ont par
eux-mêmes de l'infirmité, fait qu'ils penchent toujours beaucoup plus du
côté de la douceur. Eh donc! y a-t-il rien de plus nécessaire que de
suppléer le défaut de la peine corporelle par l'abondance de la douleur?
C'est cette douleur qui a apaisé Dieu sur les Ninivites ; c'est elle qui
prenant en main la cause de Dieu, a détourné le cours de sa vengeance.
Dieu les menaçait de les renverser, et ils se sont renversés eux-mêmes
en détruisant par les fondements toutes leurs inclinations corrompues.
De quoi vous plaignez-vous, ô Seigneur? Voilà votre

 

449

 

parole accomplie ; vous avez dit que Ninive serait renversée, elle s'est
en effet renversée elle-même. Ninive est véritablement renversée, en
tournant en bien ses mauvais désirs. Ninive est véritablement renversée,
puisque le luxe de ses habits est changé en un sac et en un cilice, la
superfluité de ses banquets en un jeune austère, la joie dissolue de ses
débauches aux saints gémissements de la pénitence : _Subvertitur plané
Ninive, dum calcatis deterioribus studiis in meliora convertitur ;
subvertitur plané, dùm purpura in cilicium, affluentia injejunium,
laetitia mutatur in fletum_ (1). O ville utilement renversée!

Chrétiens, armons-nous de zèle ; que chacun renverse Ninive en soi-même,
etc. Ville de Metz, que n'es-tu ainsi renversée! Je désire ta grandeur
et ton repos autant qu'il se peut, et plut à Dieu que je visse descendre
sur toi les bénédictions que je te souhaite ! Toutefois ne t'offense
pas, si j'ose désirer aujourd'hui que tu sois entièrement renversée.
Plût à Dieu que je visse à bas et les tables de tes débauches, et les
banquets de tes usuriers, et les retraites honteuses de tes impudiques!
Plût à Dieu que j'entende bientôt cette bienheureuse nouvelle : Toute la
ville de Metz est abattue, mais elle est heureusement abattue aux pieds
des confesseurs, devant les tribunaux de la pénitence qui sont érigés de
toutes parts dans ce temple auguste! Que tardes-tu, ô ville?
Renverse-toi par la pénitence ; cette chute te relèvera jusqu'à la gloire
éternelle.

 

1 S. Eucher., homil. _De Pœnit. Niniv_., tom. VI _Biblioth. Patr_., p.
646.

[Précédente]

[Accueil]

[Suivante]

[7 PSAUMES PÉNITENCE]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Avertissement
I - Catéchisme - I
I - Catéchisme - II
II - Catéchisme - I
II - Catéchisme - II
Catéchisme Fêtes
Prières ecclésiatisques
Prières
Entendre la Messe
Oraisons dimanches
Collectes Saints
Office de l'Église
Psaumes Vêpres
Hymnes Ste Vierge
Hymnes Commun Sts
Vêpres des morts
7  Psaumes Pénitence
Litanies Saints
Litanies Nom de Jésus
Litanies Sainte Vierge
Exercice confession
Exercice communion
Pratiques de dévotion
Jubilé
Circa Dilectionem
Cerfroid
Exposition foi
Statuts et Ordonnances
Ordonnances 1698
Abbaye de Jouarre



 



LES  SEPT  PSAUMES DE LA PÉNITENCE.


 

 LES  SEPT  PSAUMES DE LA PÉNITENCE.

 PSAUME  6.

 PSAUME  31.

 PSAUME 37.

 PSAUME 50.

 PSAUME 101.

 PSAUME 129.

 PSAUME 142.

 

 

PSAUME  6.

 

David malade demande pardon à Dieu qui l'a frappé, et le prie de guérir
les plaies de son âme.

 

_Domine, ne in furore tuo arguas me,_ etc.

 

1. Seigneur, ne me reprenez pas dans votre fureur, et ne me châtiez pas
dans votre colère.

2.  Ayez pitié de moi, Seigneur, car je languis de faiblesse :
guérissez-moi, Seigneur, parce que le trouble m'a saisi jusqu'au fond
des os.

3.  Mon âme est toute troublée ; mais vous, Seigneur, jusqu'à quand
différerez-vous?

4. Seigneur, tournez-vous vers moi, et délivrez mon âme : sauvez-moi à
cause de votre miséricorde ;

5. Car nul dans la mort ne se souvient de vous : qui publiera vos
louanges dans le sépulcre?

6.  Je me suis lassé à force de gémir : je laverai toutes les nuits mon
lit de mes pleurs ; je l'arroserai de mes larmes.

7.  Mon œil a été troublé de fureur (1) : j'ai vieilli au milieu de tous
mes ennemis.

8.  Retirez-vous de moi (2) vous tous qui commettez  l'iniquité ; car le
Seigneur a exaucé la voix de mes pleurs.

9.  Le Seigneur m'a accordé mes demandes ; le Seigneur a reçu ma prière.

10. Que tous mes ennemis rougissent (3) et soient saisis d'étonnement ;
qu'ils retournent en arrière, et soient couverts de honte.

 

1. J'étais comme hors de moi dans les violents transports de la maladie.

2. Les ennemis de David attendaient sa mort pour s'en réjouir, mais Dieu
lui a prolongé la vie.

3. Il souhaite à ses ennemis une sainte honte de leur malice, afin
qu'ils se convertissent.

 

309

 

PSAUME  31.

 

David ressent le bonheur d'une âme à qui Dieu a pardonné ses péchés, et
représente cette grâce sous la figure d'un malade qui guérit.

 

_Beati quorum remissae sunt iniquitates,_ etc.

 

1. Heureux ceux dont le.«iniquités sont pardon nées, et dont les péchés
sont couverts !

2.  Heureux celui à qui le Seigneur n'a point imputé de péché, et dont
l'esprit est sans déguisement!

3.  Parce que je me suis tu (1), mes os se sont envieillis, tandis que
je crois tout le jour.

4. Car votre main s'est appesantie sur moi durant le jour et durant la
nuit : je me suis converti dans ma douleur, lorsque j'ai été percé d'une
épine (2).

5.  Je vous ai découvert mon péché, et je ne vous ai point caché mon
iniquité.

6. J'ai dit : Je confesserai contre moi-même mon iniquité au Seigneur ;
et vous avez remis l'impiété de mon péché.

7.  C'est pour cela que tous les saints vous adresseront leurs prières
au temps favorable :

8.  Afin que dans le déluge des eaux (3), elles n'approchent point
d'eux.

9. Vous êtes mon refuge contre les maux qui m'environnent : ô Dieu, qui
êtes ma joie, délivrez-moi de ceux qui m'assiègent (4).

10.  Je vous donnerai (5) l'intelligence, et je vous instruirai dans la
voie par laquelle vous devez marcher : je tiendrai mes yeux arrêtés sur
vous.

11. Ne devenez pas semblable au cheval et au mulet, qui n'ont point
d'intelligence.

12. Serrez avec le mors et la bride (6) la bouche de ceux qui ne veulent
pas s'approcher de vous.

13. Les pécheurs seront frappés de plusieurs plaies : mais la
miséricorde environnera celui qui espère en Dieu.

14. Justes, réjouissez-vous au Seigneur, et tressaillez de joie ; et
glorifiez-vous en lui, vous tous qui avez le cœur droit.

 

PSAUME 37.

 

David ressent les plaies profondes que la longue habitude du péché a
faites en lui, et prie Dieu de le regarder en pitié.

 

_Domine, ne in furore tuo arguas me,_ etc.

 

1 Parce que j'ai été  longtemps sans vouloir confesser mes péchés, mes
forces se  sont affaiblies,  et je  suis réduit maintenant à faire des
plaintes continuelles.

2 D'une sainte componction.

3 Les eaux signifient les misères de la vie et la corruption du monde.

4 Des démons qui me tentent et des hommes qui me portent au mal.

5 C'est Dieu qui parle à lame pénitente pour la consoler.

6  Il prie Dieu de réprimer les mauvais désirs des hommes rebelles et
indociles

 

310

 

1. Seigneur, ne me reprenez pas dans votre fureur, et ne me chutiez pas
dans votre colère.

2.  Car vous m'avez percé de vos flèches, et vous avez appesanti votre
main sur moi.

3.  Il n'y a plus rien de sain dans ma chair à la vue de votre colère ;
il n'y a point de paix dans mes os à la vue de mes péchés.

4. Car mes iniquités se sont élevées au-dessus de ma tête ; et elles
m'ont accablé comme un poids insupportable.

5.  La pourriture et la corruption s'est formée dans mes plaies, à cause
de ma folie.

6. Je suis plongé dans la misère : je suis continuellement tout courbé ;
je passe tout le jour dans la tristesse.

7.  Mes reins sont remplis d'illusions (2) : et il n'y a plus rien de
sain dans ma chair.

8.  J'ai été affligé et humilié jusqu'à l'excès ; je pousse du fond de
mon cœur des sanglots et des cris.

9. Tous mes désirs vous sont connus, Seigneur: et mon gémissement ne
vous est point caché.

10.  Mon cœur est troublé ; mes forces me quittent ; et la lumière même de
mes yeux m'a abandonné.

11. Mes amis et mes proches sont venus vers moi, et se sont élevés
contre moi (3) :

12.  Ceux qui étaient auprès de moi s'en sont éloignés ; et ceux qui
cherchaient à m'ôter la vie, me faisaient violence.

13.  Ceux qui cherchaient à me faire du mal ont publié des mensonges ; et
ils méditaient quelque tromperie pendant tout le jour.

14. Pour moi, j'étais comme un sourd qui n'entend point, et comme un
muet qui n'ouvre point la bouche.

15.  Je suis devenu comme un homme qui n'entend plus, et qui n'a rien à
répliquer.

16.  Parce que j'ai mis en vous, Seigneur, toute mon espérance : vous
m'exaucerez, ô Seigneur mon Dieu.

17.  Car je me suis dit à moi-même : A Dieu ne plaise que je devienne un
sujet de joie à mes ennemis, qui ont déjà parlé insolemment de moi,
lorsque mes pieds se sont ébranlés?

18. Je suis préparé au châtiment, et ma douleur est toujours devant mes
yeux.

19.  Je confesserai mon iniquité, et je serai sans cesse occupé du désir
d'expier mon péché.

20. Et toutefois mes ennemis vivent, et sont devenus plus puis-sans que
moi ; et le nombre de ceux qui me haïssent injustement s'accroît tous
les jours.

 

1 Dans mon intérieur.

2 La sensualité remplit mon esprit de mauvaises pensées,

3 Cela est arrivé à David dans la révolte de son fils Absalon.

 

311

 

21. Ceux qui rendent le mal pour le bien médisaient de moi, parce que
j'embrasse la justice.

22. Ne m'abandonnez point, ô Seigneur mon Dieu ; ne vous éloignez point
de moi.

23. Hâtez-vous de me secourir, ô Seigneur, Dieu de mon salut.

 

PSAUME 50.

 

Regrets et prières de David, quand le prophète Nathan lui reprocha de la
paît de Dieu le crime qu'il avait commis avec Bethsabée.

 

_Miserere mei, Deus,_ etc.

 

1.  Ayez pitié de moi, Seigneur, selon votre grande miséricorde ;

2.  Et effacez mon péché selon la multitude de vos compassions.

3.  Lavez-moi de plus en plus de mon iniquité ; et purifiez-moi de mon
péché.

4. Car je reconnais mon iniquité ; et mon péché est toujours devant moi.

5.  J'ai péché contre vous seul, et j'ai fait le mal en votre présence :
afin que vous soyez trouvé juste (1) dans vos paroles et victorieux dans
les jugements (2) qu'on fera de vous.

6. J'ai été conçu en iniquité, et ma mère  m’a conçu dans  le péché.

7.  Vous aimez la vérité : vous m'avez découvert ce qu'il y a
d'incertain (3) et de caché dans votre sagesse.

8. Vous jetterez sur moi de l'eau avec l'hysope, et je serai purifié (4)
: vous me laverez, et je deviendrai plus blanc que la neige.

9. Vous me ferez entendre une parole de joie et de consolation (5) : et
mes os humiliés tressailliront d'allégresse (6).

10.  Détournez votre face de mes péchés, et effacez toutes mes offenses.

11.  O Dieu, créez en moi un cœur pur, et renouvelez l'esprit droit dans
mes entrailles.

12.  Ne me rejetez pas de devant votre face ; et ne retirez pas de moi
votre Esprit-Saint.

13.  Rendez-moi la joie de votre salut, et fortifiez-moi par l'esprit
principal (8).

11. J'apprendrai vos voies aux méchants, et les impies se convertiront à
vous.

 

1 J’avoue mon crime caché, afin qu'on voie que votre prophète a eu
raison de m'en reprendre de votre part.

2 Quand Dieu souffre longtemps les péchés sans les punir, les hommes
l'accusent de ne les voir pas ou d'être trop indulgent ; mais on voit par
mon exemple que Dieu songe à les punir, lorsque nous y pensons le moins.

3 A notre égard, parce que nous n'en pouvons pas pénétrer le fond.

4 On jetait  l'eau avec l'hysope dans la purification des lépreux, et
des autres personnes immondes. (_Levit_., XIV, 6 ; _Num_., XIX, 18.)

5 C'est la parole de pardon qui fut prononcée à David pat Nathan, et qui
est prononcée aux pécheurs par les prêtres.

6  La joie dans l'intérieur suit l'humiliation.

7  L'esprit de fermeté et de persévérance.

 

312

 

15. Délivrez-moi du sang (1), ô Dieu, ô Dieu mon Sauveur : et ma langue
publiera avec joie votre justice.

16. Seigneur, ouvrez mes lèvres, et ma bouche chantera vos louanges.

17.  Si vous aimiez les sacrifices, je vous en offrirais : mais les
holocaustes (2) ne vous sont pas agréables.

18.  L'esprit affligé est le sacrifice que Dieu demande : ô Dieu, vous
ne mépriserez pas un cœur contrit et humilié.

19.  Seigneur, traitez Sion selon votre miséricorde, et bâtissez les
murs de Jérusalem (3).

20.  Vous agréerez alors le sacrifice de justice, les offrandes et les
holocaustes : et on vous offrira des veaux (4) sur votre autel.

 

1 Du sang d'Urie que j'ai répandu, et qui crie vengeance contre moi.

2 L'holocauste était un sacrifice où la victime était entièrement
consumée par le feu, et il signifie le cœur du chrétien tout embrasé par
la charité.

3 Sous la figure de Jérusalem et de Sion, il représente l' Église et
l'âme pénitente , dont il faut réparer les ruines.

4 Les sacrifices des animaux étaient la figure du sacrifice de
Jésus-Christ immolé sur la croix, et tous les jours offert sur les
autels.

 

PSAUME 101.

 

Il déplore la captivité du peuple de Dieu dans Babylone, et il demande
le rétablissement de Sion : une âme pauvre et désolée demande aussi à
son exemple, d'être rétablie par la grâce.

 

_Domine, exaudi orationem meam,_ etc.

 

1.  Seigneur, écoutez ma prière ; et que mes cris s'élèvent jusqu'à
vous.

2. Ne détournez pas de moi votre face : quelque jour que je sois dans
l'affliction, prêtez l'oreille à ma voix :

3.  Quelque jour que je vous invoque , hâtez-vous de me secourir.

4. Car mes jours se sont évanouis comme la fumée, et mes os se sont
desséchés, comme du bois prêt à prendre feu.

5. J'ai été frappé comme l'herbe, et mon cœur est devenu sec, parce que
j'ai oublié de manger mon pain.

6.  Mes os tiennent à ma peau à force de gémir et de soupirer.

7.  Je suis devenu semblable au pélican des déserts, et au hibou des
lieux solitaires.

8.  J'ai passé les nuits en veille, et je suis devenu semblable au
passereau seul sur le toit.

9.  Mes ennemis me faisaient des reproches durant tout le jour, et ceux
qui me louaient faisaient des imprécations contre moi :

10. Parce que je mangeais la cendre comme le pain, et je mêlais mon
breuvage de mes larmes,

11. A cause de votre colère et

 

313

 

de votre indignation : parce qu'en m'élevant, vous m'avez écrasé.

12.  Mes jours se sont évanouis comme l'ombre, et je suis devenu sec
comme l'herbe.

13.  Mais vous, Seigneur, vous demeurez éternellement ; et la mémoire de
votre nom passe de race en race.

14. Vous vous élèverez, et vous aurez pitié de Sion, puisque le temps
est venu d'avoir compassion d'elle, le temps en est venu :

15. Puisque ses pierres sont aimées de vos serviteurs, et que la terre
où elle était les attendrit (1)

16. Les nations craindront votre nom, et les rois de la terre publieront
votre gloire ;

17. Parce que le Soigneur rebâtira Sion, et il se montrera dans sa
gloire.

18.  Il a tourné ses regards sur la prière des humbles, et il n'a pas
méprisé leurs voeux.

19.  Que ceci soit écrit pour la race qui viendra ; et le peuple qui sera
créé louera le Seigneur.

20.  Parce qu'il a regardé du haut de son sanctuaire : le Seigneur a
jeté les yeux du ciel en terre,

21. Pour écouter les gémissements des captifs : pour mettre en liberté
les enfants de ceux qu'on a mis à mort .

22.  Afin qu'ils louent le nom du Seigneur dans Sion, et qu'ils chantent
ses louanges dans Jérusalem.

23. Lorsque les peuples s'uniront ensemble avec les rois, pour servir le
Seigneur.

24. Il lui dit dans sa force (2) : Faites-moi connaître la brièveté de
mes jours.

25.  Ne me tirez pas du monde à la moitié de ma vie : vos années
dureront dans la suite de tous les âges.

26. Seigneur, vous avez fondé la terre dès le commencement, et les cieux
sont l'ouvrage de vos mains.

27.  Ils périront ; mais vous, vous demeurerez : ils vieilliront tous
comme un vêlement.

28.  Vous les changerez comme un manteau, et ils changeront de forme :
mais vous êtes toujours le même, et vos années n'auront pas de fin.

29. Les enfants de vos serviteurs habiteront sur la terre, et leur
postérité sera éternellement heureuse.

 

1 Les Juifs aimaient jusqu'aux ruines de leur patrie et du temple, et en
chérissaient la poussière, où ils venaient offrir leurs dons.

2 Quelque forte que paroisse sa santé, il craint de mourir sans avoir vu
Jérusalem rétablie, et prie Dieu de prolonger ses jours jusqu'à ce
temps.

 

PSAUME 129.

 

Le pécheur abîmé dans ses crimes, n'attend de secours que de l'infinie
miséricorde de Dieu.

 

_De profundis clamavi,_ etc.

 

1. Seigneur, je m'écrie vers vous

 

314

 

du fond de  l'abîme :  Seigneur écoutez ma voix.

2. Que vos oreilles soient attentives à la prière que je vous fais.

3. Seigneur, si vous examinez nos péchés, qui pourra subsister devant
vous?

4. Mais en vous est la source des miséricordes ; et je vous ai attendu,
Seigneur, à cause de votre loi (1).

5. Mon âme a attendu le Seigneur, à cause de sa parole : mon âme a
espéré au Seigneur.

6. Que depuis le point du jour jusqu'à la nuit, Israël espère au
Seigneur :

7. Car au Seigneur appartient la miséricorde, et la rédemption que nous
trouvons en lui est très-abondante.

8. Il rachètera lui-même Israël de tous ses péchés.

 

 

PSAUME 142.

 

David accablé de maux, prie Dieu de ne le traiter pas selon la rigueur
de ses jugements, mais de le conduire dans ses voies. Ce Psaume convient
à l'état où était David, lorsque la caverne où il était réfugie tut
environnée par les troupes de Saül qui le poursuivait à mort, (I Reg.,
XII.) Il exprime aussi l'état du pécheur environné de péchés et de
tentations, qui ne se peut sauver que par miracle, comme David.

 

_Domine, exaudi orationem meam, auribus percipe_. etc.

 

1. Seigneur, écoutez ma prière ; prêtez l'oreille à ma demande selon
votre vérité ; exaucez-moi selon votre justice.  

2.  N'entrez point en jugement avec votre serviteur : parce que nul
homme vivant ne pourra être trouvé juste devant vous.

3.  Car l'ennemi a poursuivi mon âme (2) : il m'a toute ma vie humilié
sur la terre.

4. Il m'a mis dans des lieux obscurs (3), comme les morts ensevelis
depuis longtemps : mon esprit a été accablé d'ennui, mon cœur a été en
moi-même tout saisi de trouble.

5.  Je me suis souvenu des siècles passés ; j'ai médité sur toutes vos
œuvres, et sur les ouvrages de vos mains.

6. J'ai élevé mes mains vers vous ; mon âme est devant vous comme une
terre sans eau (4).

7.  Hâtez-vous, Seigneur, de m'exaucer ; mon esprit tombe en défaillance.

8. Ne détournez pas votre face de dessus moi, de peur que je ne sois
semblable à ceux qui descendent dans le lac.

9. Prévenez-moi en votre miséricorde dès le matin : parce que j'espère
en vous.

 

1 Par laquelle vous promettez le pardon à ceux qui ont recours à votre
bonté.

2 David était comme enterré dans la caverne, et ses ennemis qui ne
croyaient pas qu'il leur pût échapper, le regardaient comme mort.

3, 4 Il représente une âme qui attend la grâce.

 

315

 

10. Faites-moi connaître la voie par laquelle je dois marcher : puisque
j'ai élevé mon âme vers vous.

11. Seigneur, délivrez-moi de mes ennemis, j'ai recours à vous :
enseignez-moi à faire votre volonté, parce vous êtes mon Dieu.

12. Votre bon esprit me conduira dans un chemin droit : vous me donnerez
la vie, Seigneur, dans voire justice pour la gloire de votre nom.

13. Vous tirerez mon âme de l'affliction, et vous ferez périr tous mes
ennemis (1), selon votre miséricorde.

14. Vous ferez périr tous ceux qui affligent mon âme : parce que je suis
votre serviteur.

 

_Ant_. Seigneur, ne vous souvenez pas de nos fautes, ni de celles de nos
proches, et ne vous vengez pas de nos péchés.

 

1 David ne désire pas que ces maux arrivent à Saül ni à ses autres
ennemis, mais il prévoit  la punition de leur endurcissement.

 

[Précédente]

[Accueil]

[Suivante]


U

UBIQUITÉ (l') enseignée par Luther sur ce fondement, que Dieu étant
partout, et la divinité étant unie dans Jésus-Christ à l'humanité, cette
humanité doit être partout, XIV, 88. L'ubiquité est érigée en dogme par
la plupart des Églises luthériennes, XIV, 347. Elle fait pencher
Mélanchthon vers les sacramentaires, XIV, 341. On la trouve établie dans
le livre de la Concorde, quoiqu'on prétende y répéter la Confession
d"Augsbourg, qu'on combat en effet, XIV, 346,347. Le but des luthériens,
en établissant l'ubiquité était de fermer la bouche aux sacramentaires,
qui disaient que Dieu ne pouvait mettre le corps de Jésus-Christ en
plusieurs lieux à la fois, XIV, 348. Ce dogme est toléré dans les
luthériens par les calvinistes au synode de Charenton, XV, 48.

UNION des Saints avec Jésus-Christ, VIII, 27. Discours sur l'union de
Jésus-Christ avec l'âme fidèle, X, 568. L'union des Églises n'est pas,
selon Basnage, du premier dessein de Jésus-Christ, ce qu'on réfute par
les paroles mêmes du Sauveur, XVII, 191. On prouve, par saint Paul, que
les Églises chrétiennes étaient établies pour ne faire ensemble, au
dedans et au dehors, qu'une seule Église catholique, XVII, 192. On
démontre, par l'Écriture, que les Églises s'unissaient dans la doctrine,
et même dans le fond de la discipline, et que le consentement commun
tenait lieu de règle, XVII, 193.

UNITAIRES. _Voy._ Sociniens.

UNITÉ de Dieu. Les Personnes divines se rapportent à un seul principe.
La théologie des Pères profonde sur ce point. La hauteur d'un si grand
mystère fait quelquefois trouver des difficultés dans les explications
que les saints Pères en donnent, XVI, 42, 43.

L'unité de l'Église constitue sa force et sa beauté, XI, 412, 592 ; XII,
652 et _suiv._ Quelle est l'origine de l'unité de l'Église, XI, 417,
Cette unité parvenue jusqu'à nous, par une succession continuelle,
_ibid._

 

476

 

Merveilles du mystère de l'unité de l'Église, XXVII, 305 et _suiv._
Saint Pierre est choisi pour le consommer, XXVII, 313 et _suiv._

Sermon sur l'unité de l'Église, XI, 58. Ce discours est approuvé à Rome,
XXVIII, 128. _Voy._ Bossuet, Église.

UNIVERS. Spectacle qu'il présente ; sage économie qui s'y fait remarquer
: objections des incrédules sur les désordres apparents de l'univers,
XI, 169. Bel ordre de l'univers, XI, 233.

UNIVERSITÉ (l') de Paris, écrit à Clément VII pour l'engager à abdiquer,
XXI, 569. Elle a toujours persisté dans la doctrine du cardinal d'Ailly
et de Gerson, sur la puissance du Pape et des conciles : pourquoi on
l'appelle la doctrine de l'Ecole de Paris, XXII, 488 et suivantes.

URBAIN II, Pape, anime dans le concile de Clermont, les princes et les
peuples à se croiser contre les Sarrasins, XXV, 48.

URBAIN VI, Pape pendant le grand schisme, qui ne put être terminé que
par le concile de Pise, XXII, 223. _Voy._ Pise.

URSULINES de Meaux ; exhortation que Bossuet leur fait dans sa visite
pastorale, X, 493 et _suiv._ Avec quelle vigilance elles doivent
travailler à l'éducation des enfants qu'on leur confie, X, 526 et
_suiv._ Zèle qu'elles doivent avoir pour leur perfection, X, 541. Union
qui doit régner entre elles, X, 545.

USURE (Traité de V). Dans l'ancienne loi, l'usure était défendue
d'Israélite à Israélite, et cette usure était tout profit au-delà du
prêt, XXXI, 21. L'esprit de la loi est de défendre l'usure comme quelque
chose d'inique, XXXI, 24. L'Église a toujours cru cette défense de
l'usure obligatoire sous l'Évangile, XXXI, 26 et _suiv._ Cette défense a
dû même être perfectionnée dans la loi nouvelle, XXXI, 39. Il est de foi
que l'usure est défendue à tous et envers tous, XXXI, 43. L'opinion
contraire est sans fondement, XXXI, 44 et _suiv._ La loi divine qui
défend l'usure, défend en même temps tout ce qui y est équivalent, XXXI,
54. L'Église catholique, et c'est la tradition constante de tous les
Pères et de tous les siècles, a toujours compris que l'usure ou prêt à
intérêts, c'est-à-dire, le gain retiré du prêt, était défendu entre les
frères par les livres de Moïse, des prophètes et de l'Évangile, XXII,
709.

L'usure est défendue dans l'Écriture, III, 530. Grotius la justifie, IV,
443. L'usure est défendue envers les riches et les pauvres, hors le cas
de _lucrum cessans_ et de _damnum emergens,_ XXII, 709, 724 et _suiv._
Censure de l'Assemblée de 1700 sur cette matière, XXII, 755 et _suiv._
Usage des commerçants de Hollande dans les prêts. De quelle manière se
conduisent ceux d'entre eux qui veulent éviter l'usure, XXVI, 326.

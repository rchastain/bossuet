
O

O : c'est le cri du cœur ; toute l'éloquence s'y trouve, XXVII, 598.

OATÉS, ministre protestant, accuse faussement les catholiques anglais
d'une conspiration, XXI, 543 et note.

OBÉISSANCE. Sa nécessité dans la vie religieuse, XXVIII, 431. Elle en
est le fondement, X, 531. Ses avantages, XII, 190. Ses effets, XXVIII,
197, 198, 284. La grâce du salut y est attachée, XXVII, 628. Elle doit
être intérieure, X, 540. Doit régler les dispenses, XXVIII, 373. Exemple
d'obéissance dans les personnes qui concourent à la présentation de
Jésus-Christ au Temple, XI, 266 et _suiv._ Joie qu'a Bossuet de
l'obéissance des religieuses dont il est chargé, XXVIII, 438.

L'obéissance est due au Pape, à moins que ce qu'il ordonne ne soit
contre la foi, XXI, 103,114. Les évêques lui promettent obéissance
suivant les Canons, XXII, 202. L'obéissance promise au Pape dans la
profession de foi de Pie IV, ne suppose pas son infaillibilité, XXII,
465.

Quelle est l'obéissance que les sujets doivent au prince, XXIV, 8 et
_suiv._ Elle ne doit être altérée par aucun prétexte, XXIV, 11, 12.
L'impiété des princes et la persécution n'en dispensent pas, XXIV, 14.
L'obéissance aux puissances souveraines, recommandée par les

 

374

 

apôtres aux premiers chrétiens, était d'obligation, et non-seulement de
conseil et de plus grande perfection, XV, 287 et _suiv._ _Voy._ Sujets.

OBLATION (l') de l'Eucharistie retranchée dans la messe luthérienne,
XIV, 130, 131. Comment l'oblation profite à tout le monde, XIV, 132.
_Voy._ Messe. En quel sens on offre dans la messe pour la rédemption du
genre humain. Les ministres contraints d'approuver ce sens, XIV, 224.

OBRECHT (Ulric), préteur royal de Strasbourg, fait abjuration entre les
mains de Bossuet ; lui donne des éclaircissements sur diverses matières
pour ses ouvrages contre les protestants, XXX, 492 et _suiv._

OCHlN (Bernardin), moine apostat et marié, ennemi déclaré de la divinité
de Jésus-Christ, appelé en Angleterre pour en réformer l'Église, XIV,
301. _Voy._ Réformation anglicane.

OECOLAMPADE (Jean) prend parti pour Carlostad contre Luther dans la
dispute sacramentaire, XIV, 66. Il était, comme Mélanchtbon, fort
modéré. Son caractère : sa piété tendre. Il se fait religieux dans uu
âge mûr: sort de son monastère ; prêche la Réforme, et se mortifie à la
mode des nouveaux réformés, en épousant une jeune fille, XIV, 72, 73. Au
lieu de son ancienne candeur, il ne montre plus que dissimulation et
artifice, et meurt accablé des coups du diable, selon Luther, XIV, 145.
il avait averti Bucer que ses équivoques de la présence réelle
sacramentelle étaient une pure illusion. Les Suisses s'échauffent en sa
faveur contre Luther, XIV, 151. Sa conférence avec les vaudois qu'il
engage dans la nouvelle Réforme, XIV, 517, 518, 319. Il croit qu'on peut
prier les saints de prier pour nous, et en obtenir des secours, XVI,
342.

OEUVRES. Celles de Dieu, selon Luther, quand elles seraient toujours
laides, sont d'un mérite éternel, au lieu que celle des hommes, quoique
belles en apparence, sont des péchés mortels, XIV, 25. Le mérite des
bonnes œuvres a sa source dans la charité habituelle, XXVI, 440. Ce
mérite provient de la grâce sanctifiante qui est donnée gratuitement au
nom de Jésus-Christ. Erreurs graves que nous imputent les protestants
sur cette matière, XIII, 64 et _suiv._ La nécessité des bonnes œuvres
condamnée tout d'une voix, par les luthériens, dans l'assemblée de
Vorms, XIV, 338. Les luthériens, en niant leur nécessité, condamnent
cette proposition : Les bonnes œuvres sont nécessaires au salut. Jurieu
dit qu'il déteste cette erreur, et pourtant la tolère dans les
luthériens, XV, 342. De la justification par les œuvres, XIII, 422 et
_suiv._ Du mérite des bonnes œuvres, XIII, 432. Sentiments de l'ancienne
Église sur cette matière : de saint Augustin : erreurs de Pelage, XIII,
434 et _suiv._ Doctrine du concile de Trente sur le mérite des œuvres,
XIII, 437 et _suiv._ Quelle est la nature du mérite de nos œuvres, XIII,
442 et _suiv._ Du mérite que l'Ecole appelle de

 

375

 

condignité, XIII, 444. Les bonnes œuvres des justes sont-elles pures de
tout péché et agréables à Dieu? XVII, 417 et _suiv.,_ 446 et _suiv_.,
475 et _suiv._ Leur mérite, XVII, 308, 419, 438, 477, 478, 501, 502. 551
; XVIII, 12. Leur nécessité pour le salut, XVII, 420, 421, 449, 478,
557. Les œuvres satisfactoires sont admises dans la Confession
d’Augsbourg, XIV, 118.

OFFICE divin. Explication des choses qui s'y répètent le plus souvent,
V, 208 et _suiv._ Les principaux Psaumes, Cantiques et Hymnes qui s'y
chantent, traduits en français par Bossuet, V, 254. et _suiv._ Sagesse
de l'Église dans la distribution des divers Offices, où tous les
mystères des deux Testaments sont célébrés et renouvelés, avec une
pieuse commémoration des saints hommes qui ont été sanctifiés par ces
mystères, XXVI, 354, 355. _Voy._ Bréviaire.

OISIVETÉ. Combien elle est nuisible aux États, XXIV, 200.

OKAM, moine schismatique, est, suivant quelques-uns, l'auteur du
sentiment soutenu depuis par les docteurs de Paris à Constance, XXI, 62,
63.

OLIER (Jean-Jacques), curé de Saint-Sulpice à Paris. Eloges que lui
donne Bossuet : il l'appelle en témoignage sur la suspension des
puissances dans l'oraison, XIX, 613. Il le range parmi les bons
spirituels, XXVII, 568. Il demande des éclaircissements sur deux de ses
lettres, XXVIII, 673, 674.

OLYMPIADES. Elles tirent leur nom des jeux Olympiques, XXIV, 279.

ONCTION en Jésus-Christ, VII, 223. Ce n'est autre chose que la divinité,
VIII, 163, 164. Elle lui a été donnée par le Saint-Esprit, VII, 223 et
_suiv._ Quel en est l'effet, VII, 225. Elle doit nous inspirer deux
vertus principales, VII, 226 et _suiv._

La Réforme anglicane ôte du sacrement de l'Extrême-Onction, l'onction
qu'elle dit introduite dans le dixième siècle, quoique le saint pape
Innocent en ait parlé dans le quatrième, XIV, 305. _Voy._
Extrême-Onction.

OPÉRATIONS. _Voy._ Ame, Entendement, Esprit, Sens.

OPINION, sa définition, XXIII, 64. Son ascendant sur les hommes, IX,
135.

OPTAT (S.), évêque de Milève, enseigne que l'Église est dans l'État, et
qu'on doit prier pour le prince, même païen, XXI, 352, 353.

ORAISON. Ses règles enseignées par les apôtres et les Pères, XVIII, 369,
370. Comment on en doit juger, XVIII, 371, 372. Comment elle est cachée
aux ames simples, XVIII, 373. Son éloge, et difficulté d'en parler,
XVIII, 375. Comment l'oraison des solitaires était continuelle, XVIII,
511 et _suiv._ Moyen de la perpétuer, XVIII, 313 et _suiv._ ; même
pendant le sommeil, XVIII, 516. Comment l'oraison ne se connaît pas
elle-même, XVIII, 463 et _suiv_.

 

376

 

Quelle est la meilleure manière de faire oraison, VII, 573, 574 ; XXVII,
441, 443, 526, 559, 560 ; XXVIII, 127, 128. Caractères de la véritable
oraison, XXVII, 645. En quoi elle consiste, et sa perfection, XXVIII,
137, 138. Défauts à éviter dans l'oraison, XXVIII, 212. Quelle doit être
la présence de Dieu dans l'oraison, XXVIII, 142, 143. Conditions
nécessaires pour faire une oraison agréable à Dieu, IX, 268. Sa nature ;
quel est celui qui ne prie pas, IX, 260, 261. Comment l'oraison est une
espèce de mort, IX, 267. C'est dans l'oraison que la gloire de Dieu
éclate sur nous, IX, 109. Union admirable qui se fait de l'âme avec Dieu
dans l'oraison, X, 574. Pratique de l'oraison continuelle, XXVII, 550.
Comment il faut considérer Jésus-Christ dans l'oraison, XXVII, 567, 568.
La maladie n'est pas contraire à la perfection de l'oraison, XXVII, 424.
On doit s'humilier dans l'oraison, XII, 558. Pourquoi on ne connaît pas
ce qu'on y fait, XXVII, 579. Il n'est pas besoin de le savoir, XXVII,
606. Point d'oraison plus forte que celle qui part d'une chair mortifiée
par la pénitence, et d'une âme dégoûtée des plaisirs du siècle, X, 239.
La miséricorde et la toute-puissance de Dieu sujets d'oraison, XXVII,
446. Danger des oraisons extraordinaires, XXVII, 576. Ni Cassien, ni
saint Jean Climaque ne parlent de l'oraison de quiétude, XXVIII, 246.
Bossuet n'est pas contraire à cette oraison, XXVIII, 249, 267.

Origine de l'oraison passive ; explication des termes, XVIII, 518, 519 et
_suiv._ Ce que c'est précisément, XVIII, 521. En quel sens elle est
surnaturelle, XVIII, 523. Sentiments de sainte Thérèse et du bienheureux
Jean de la Croix, XVIII, 523, 524, 576. L'état passif expliqué en six
propositions, XVIII, 523 et _suiv._ Le libre arbitre y agit, XVIII, 527.
Cet état est de peu de durée, XVIII, 528. Cette oraison ne peut être
commune à tous, XVIII, 534, 535 et _suiv._ Toute perfection ne consiste
pas dans cet état, XVIII, 538. C'est une grâce gratuite, sans laquelle
on peut se sauver, XVIII, 539 et _suiv._ Où peut mener l'idée
perpétuelle passiveté, XVIII, 542 et _suiv._

S'opposer au livre des _Maximes des Saints,_ ce n'est point mettre
l'oraison en péril, XIX, 375, 376, 377. La perfection ne consiste point
dans l'oraison de quiétude, XIX, 518 et _suiv._ XIX, 597. Ce que c'est
que l'oraison de quiétude et d'union, XIX, 584, 583. La suspension de
l'intellect y est surnaturelle et discontinue, XIX, 586, 589, 590.
Extases et ravissements, XIX, 587. Sentiments de saint Jean de la Croix
sur cette matière, XIX, 589 ; de saint François de Sales, 593 ; d'Alvarez,
594 ; de tous les mystiques, 596, 597. Les grâces extraordinaires
d'oraison ne prouvent pas toujours la sainteté d'une âme, XIX, 598. La
suspension des puissances de l'âme attestée par sainte Thérèse, XIX, 606
et _suiv._ et par M. Olier, 613. Niée par M. de Cambray sans aucune
preuve, XIX, 613 et _suiv._ La foi ne souffre pas de cette

 

377

 

suspension des puissances, XIX, 608 et _suiv._ L'amour effectif peut
être séparé de l'oraison de quiétude, XIX, 609. Crime de traiter de
fanatisme ces dons extraordinaires d'oraison, XIX, 610 et _suiv._ l'âme
toujours active dans l'oraison, selon M. de Cambray, XIX,619 et _suiv._
Force du libre arbitre, XIX, 620. Saint Bernard cité à faux, XIX, 622 et
_suiv._

_Méthode facile pour faire l'oraison de simple présence de Dieu,_ VII,
504 et _suiv._

Oraison dominicale. Explication de cette prière, V, 82, 83 ; VI, H, 39.
Paraphrase de chaque demande appliquée à la pratique de la charité, VI,
116 et _suiv._

L'Oraison dominicale est supprimée par les quiétistes, XVIII, 440,441 et
_suiv_. Elle contient les demandes de la grâce et de la persévérance,
XVIII, 445, 446, 489. De la rémission des péchés, XVIII, 452. Ce qu'on
demande par le pain de chaque jour, XVIII, 497 ; et par votre régne
arrive, etc., XVIII, 506. Doctrine de saint Augustin sur cette prière,
XVIII, 655 et _suiv._ Elle est expliquée par ce Père et par les prières
de l'Église, IV, 385. Par saint Cyprien, Tertullien, etc., IV, 387 et
_suiv._

ORAISONS FUNÈBRES, XII, 439 et _suiv._

ORANGE (le second concile d') a défini qu'on doit demander la
persévérance, XVIII, 490.

ORANGE (le prince d') persécuté pour cause de religion, remue toute
l'Allemagne, XXV, 578.

ORATEUR. Trois choses contribuent à le rendre agréable et efficace, XII,
230. _Voy._ Prédicateurs.

ORATOIRE. Congrégation fondée par le cardinal de Bérulle. Quel en était
le but et l'esprit, XII, 646.

ORDINATION. Elle communique la plénitude du Saint-Esprit, XII, 64.
L'ordination des pasteurs conservée dans l'Église romaine, de l'aveu de
Luther, XIV, 137. La forme de l'ordination réglée par le Parlement en
Angleterre, XIV, 298. La validité des ordinations n'y est fondée que sur
la formule de la liturgie d'Edouard VI, XIV, 417. Les frères de Bohème
dérobent l'ordination dans l'Église catholique, XIX, 555.

ORDONNANCE et instruction pastorale sur les États d'oraison XVIII, 351
et _suiv._

Ordonnance pour défendre la lecture du Nouveau Testament de Trévoux,
III, 379 et _suiv._ — Ordonnances notifiées aux Ursulines de Meaux, X,
512, 513.

ORDRE (l') ne peut être exclu des sacrements communs à toute l'Église,
XIII, 73. Définition de ce sacrement : en quoi il consiste, V, 15, 104.
Ordre. L'ordre admirable qui paraît dans les choses humaines, considéré

 

378

 

par rapport au jugement dernier : comparaison tirée de l'optique, qui
rend cette vérité sensible, X, 226. ORGANES. _Voy._ Corps, Sens.

ORGUEIL. Sa définition ; il cause notre ruine, VIII, 267 ; XI, 133. C'est
la maladie la plus dangereuse de l'homme, VIII, 170 ; XI, 460, 540. Il
monte toujours, et ne cesse jamais d'enchérir sur ce qu'il est, IX, 140 ;
XI, 470, 471 ; XII, 519. Il attribue tout à soi-même, VIII, 437. Il a
fait tomber les anges rebelles, IX, 25 et _suiv._ C'est la plus
dangereuse et la plus pressante de toutes les passions, X, 158. Nature
de ce péché, XXVIII, 171, 172. Combien il est à craindre, X, 159 ;
XXVIII, 269. Pensées sur l'orgueil, X, 617. L'amour-propre est la racine
de l'orgueil, VII, 436, 437 et _suiv._ Ce que l'orgueil ajoute à
l'amour-propre, VII, 441. Description de la chute de l'homme, qui
consiste principalement dans son orgueil, VII, 443 et _suiv._ Effets de
l'orgueil, VII, 444, 449 ; XXVII, 588. Ses désordres, ym, 480, 481.
Comparaison de l'homme amoureux des louanges avec la femme infatuée de
sa beauté, VII, 447 et _suiv._ Dieu punit l'orgueil en lui donnant ce
qu'il demande, VII, 452 et _suiv._ Comment il arrive aux chrétiens de se
glorifier en eux-mêmes, VII, 438. D'où vient ce penchant de l'homme à
s'attribuer le bien qu'il tient de Dieu, VII, 460 et _suiv._ Caractère
d'un orgueilleux, X, 296.

ORIGÉNE prouve la divinité de Jésus-Christ par le texte du Psaume XLIV :
_Sedes tua, Deus,_ etc., I, 427. Il entend du péché originel ces paroles
de David : _Ecce in iniquitatibus,_ etc., I, 428, 429. Estime que saint
Jérôme faisait de ses commentaires sur l'Écriture, I, 575. Origène
invoque l'ange du baptême, II, 338. Il explique comment les martyrs
concourent à la rédemption du genre humain, il, 341 et _suiv._ Passage
de ce Père sur les persécutions, II, 440. Autre passage sur l'obéissance
due aux puissances séculières, XXI, 545. Ses œuvres ont été autrefois
défendues ; on peut les lire à cause de la piété qui y règne, XXVIII, 37.

L'origénisme est condamné par Théophile d'Alexandrie : son jugement est
confirmé par le consentement commun et devient définitif, XXII, 182,
183.

ORLÉANS. _Voy._ Louis.

ORLÉANS. C'est où se fit la première paix des calvinistes révoltés, XV,
521. On y tint un synode pour entretenir la guerre, XV, 537.

OSIANDRE (André) luthérien, invente l'impanation et l'invination, XIV,
52. Il abandonne son église de Nuremberg, dans la crainte des peines
dont menaçait l'intérim, et se retire en Prusse, XIV, 327. Sa doctrine
prodigieuse sur la justification, _ibid._ Il plaisantait sur tout, et
avait, selon Calvin, l'esprit profane. Mélanchthon blâme son arrogance,
ses rêveries et les prodiges de ses opinions, XIV, 328. Il

 

379

 

trouble l'Université de Kœnisberg, où enflé de la faveur du prince, il
publie hautement sa doctrine ; ce qu'il n'avait osé faire du vivant de
Luther, XIV, 330. On épargne ses erreurs dans l'assemblée de Vormes. Il
en triomphe en Prusse, où il rend sa doctrine dominante, XIV, 339.

OSIUS, évêque de Cordoue, distingue les droits des deux puissances, XXI,
282. Il préside au concile de Nicée, XXII, 14, note.

OTHON (saint), évêque de Bamberg, est attaché à l'empereur Henri IV,
déposé. Le Pape ne l'en reprend pas, XXI, 407, 408 et _suiv._ Son vrai
nom est _Udon,_ XXI, 410 et note.

OTHON, évêque de Frisingue, atteste la nouveauté du pouvoir que
s'attribuait Grégoire VII de déposer les rois, XXI, 130 et _suiv._

OTHON Ier parvient à l'empire par le concours du Pape et des Romains,
XXI, 377 et _suiv._

OTHON IV est déposé par Innocent III, XXI, 447.

OUÏE. _Voy._ SENSATIONS.

OXFORD (concile d') tenu contre les Albigeois, appelés Poplicains, XIV,
481. _Voy._ Manichéens.

OZIAS, roi de Juda, règne avec gloire, XXIV, 279. Il est frappé de lèpre
pour avoir entrepris sur les droits du sacerdoce, XXIV, 70, 242, 279.

Il fut toujours roi, XXI, 211.

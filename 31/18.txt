
368

N

NABUCHODONOSOR II, roi de Babylone, prend une première fois Jérusalem,
et transporte à Babylone une partie de ses habitants, XXIV, 284, 418. Il
prend Jérusalem pour la seconde fois ; détruit le temple, et en donne le
trésor au pillage, XXIV, 418. Pendant qu'il admire sa grandeur, Dieu le
frappe, lui ôte l'esprit, et le range parmi les bêtes, XXIV, 419, 420.

NAISSANCE. Trois vices de la nôtre, XI, 375. Elle a des marques
indubitables de notre commune faiblesse, XII, 695. Une religieuse de
grande naissance doit l'oublier, XXVIII, 369, 370. Bossuet préfère pour
la vie religieuse les personnes de naissance, XXVIII, 374.

NATHANAEL est amené par saint Philippe à Jésus-Christ, VII, 384.

NATIVITÉ de la sainte Vierge. Sermons pour cette fête, XI, 64, 65 et
_suiv._ Combien elle est glorieuse pour Marie, XI, 93. Elle est, dans sa
nativité, un Jésus-Christ ébauché, XI, 67. Celte nativité est la fête
des hommes, XI, 121,122. Biens qu'elle nous apporte, XI, 126. _Voy._
Noël.

NATURE. Ce qu'on entend par ce mot, XIII, 176. Nature innocente et
nature corrompue ; différence de ces deux états, d'après les principes
donnés sur la liberté, XXIII, 472. Pourquoi le Fils de Dieu s'est revêtu
de la nature humaine, X, 410.

NAUMBOURG (assemblée de) par les luthériens, pour convenir de laquelle
des quatre éditions de la Confession d'Augsbourg ils se serviraient. La
chose demeure indécise, XIV, 345.

NAVARRE. _Voy._ AZPILCUETA.

NAZZARl (François) savant très-distingué par son érudition et ses
écrits, il est le premier auteur du Journal des Savants qui fut
entrepris en Italie, à l'imitation de celui qui s'imprimait à Paris. Il
est l'auteur de la traduction italienne du livre de l'Exposition : ce
qu'il était, XXVI, 191.

NÉANT. Combien absurde qu'il y ait un seul moment où rien ne soit,
XXIII, 189. Combien l'âme répugne au néant, XXIII, 248. Le néant est
l'origine des créatures, IX, 7. Néant de l'homme, IX, 360 ; XII, 481.

NÉCESSITÉS de la vie. Avec quels soins paternels la Providence y
pourvoit, IX, 293 et _suiv._

NÉERCASSEL (Jean de), évêque de Castorie, vicaire apostolique en
Hollande, XVVI, 191. Sa lettre à l'abbé de Pontchâteau, au sujet du
livre de l'Exposition. Plaisir qu'il a éprouvé à le lire. Il le fait
traduire en hollandais, et en ferait une version latine, s'il n'espérait
que l'auteur se chargeât lui-même de ce soin, XXVI, 192. Il indique
quelques endroits qu'il croit susceptibles d'explication ou de

 

369

 

correction, _ibid._ Il fait l'éloge de Bossuet, et loue sa bonté, XXVI,
193. Son respect pour l'illustre prélat l'empêche de lui écrire, de peur
de lui enlever un moment au milieu de ses utiles travaux, XXVI, 196,
197. Il approuve les explications que Bossuet avait données à certains
endroits de son livre, et lui annonce qu'une traduction hollandaise en
est achevée, XXVI, 197, 198. Il lui demande la traduction latine de ce
livre : qu'il donnera tous ses soins pour qu'elle soit bien imprimée,
XXVI, 202. Il compose quatre traités sur le culte des saints, qu'il
envoie à Bossuet, XXVI, 198. Ses excuses de ce que la traduction latine
de l'Exposition a été si mal imprimée, XXVI, 251, 252. Il offre de faire
traduire en hollandais l'Avertissement que Bossuet avait ajouté à son
livre dans une nouvelle édition, et désire que cet Avertissement soit
traduit en latin par l'abbé Fleury, XXVI, 268. Eloges qu'il fait du
_Discours sur l'Histoire_ _universelle,_ soumet à Bossuet ses moyens de
faire passer dans le Nord son livre de l'_Exposition,_ l'exhorte à
répondre à un livre de Spanheim, et lui annonce le prodigieux débit du
livre de Jurieu, intitulé : _La politique du Clergé de France,_ XXVI,
272, 274 et _suiv._ Sa réfutation par Arnauld. _Voy._ Arnauld. Il avait
composé un petit ouvrage sur l'Amour pénitent, afin de porter les
fidèles à entrer dans la voie étroite du salut, XXVI, 3-20. Succès de
cet ouvrage en plusieurs pays, XXVI, 323, 324. Il craint les murmures de
la pénitencerie romaine contre son livre, au sujet de ce qu'il contient
sur l'usage des clefs ; motifs qui le rassurent, XXVI, 324. Bossuet lui
demande des renseignements sur un livre intitulé : Traité des Billets,
XXVI, 325. Il satisfait à sa demande, et lui explique la coutume des
négociants de Hollande sur le prêt, XXVI, 326 et _suiv._.

NÉGLIGENCE. Ses suites funestes, VIII, 115,116. Combien elle est
coupable dans un prince : maux dont elle est la cause, XXIII, 30 et
_suiv._

NÉHÉMIAS rebâtit Jérusalem, XXIV, 297. Il réforme les abus, XXIV, 299,
424. Son amour pour sa patrie, XXIV, 389, 390. Il soulage le peuple
accablé, XXIII, 543, 544. Sa fermeté, XXIII, 572. Ses soins pour le
culte de Dieu, XXIV, 67. Il est le modèle des bons gouverneurs, XXIV,
220.

NEMROD est le premier des conquérants, XXIV, 266, 267, 3S0. NERFS.
_Voy._ Corps.

NÉRON, empereur, commence la guerre contre les Juifs, et la persécution
contre les chrétiens. Il fait mourir saint Pierre et saint Paul, XXIV,
328, 329, 468. Il se tue lui-même, XXIV, 469. Monstre du genre humain,
VIII, 302, 421.

NESMOND (Henri de), évêque de Montauban : son Mémoire sur les moyens de
ramener'les nouveaux convertis, XXVII, 18à.

NESTORIUS, patriarche de Constantinople, divise la personne de

 

370

 

Jésus-Christ, XXIV, 347. Il est condamné par le Pape saint Célestin,
ensuite par le concile d'Ephèse, XXII, 17, 18 et _suiv._ Ce concile
observa envers lui les formes canoniques, XX, 568. Son erreur allait à
nier la divinité de Jésus-Christ, XX, 579. La manière dont il la niait
ne pouvait être dissimulée, XX, 581. Et ce n'était point, comme le dit
Dupin, une dispute de mots, XX, 583. Pente de l'abbé Dupin à excuser
l'hérésiarque et ses partisans, XX, 609, 611, 612 et _suiv._

Les nestoriens sont mis par Jurieu au nombre des sociétés vivantes,
quoiqu'ils renversassent le fondement de la foi, non directement, selon
lui, mais seulement par des conséquences, XV, 280. Leur schisme est le
plus ancien dans le christianisme ; ce qui n'empêche pas de les
confondre, en leur montrant l'époque de leur rupture, XVII, 97, 98.

NEUSTADT (Christophe Royas de Spinola, évêque de). Ses talents, ses
tentatives pour réunir les protestants d'Allemagne. Il reçoit à ce sujet
des pleins pouvoirs de l'empereur Léopold, XVII, 358 et _suiv._ Ce que
dit Leibniz de ses négociations, XVIII, 126.

NICAISE (l'abbé), chanoine de la Sainte-Chapelle de Dijon. Bossuet lui
écrit au sujet des ouvrages de M. Spon, XXVI, 261, 262. Sur quelques
autres écrits, XXVI, 279, 382, 383.

NICÉE (concile de), premier œcuménique. Quelques-uns croient qu'il jugea
la question de la rebaptisation, XXI, 66, 67. Ses décrets contre les
ariens tirèrent leur force du consentement commun, XXII, 13,14. Osius y
préside au nom de saint Sylvestre, _ibid._ note. Le concile ne demande
pas la confirmation du Pape, XXII, 110, 111.

L'addition faite à son symbole par le concile de Constantinople, pour
condamner une nouvelle hérésie, qui niait la divinité du Saint-Esprit,
n'est pas une variation ; et les protestants ne peuvent s'en autoriser
pour défendre les variations de leurs Confessions de foi, XIV, 99.
Jurieu prétend que ses idées sur l'Église sont les mômes que celles du
concile de Nicée, qui, selon lui, ne rejeta pas de la communion de
l'Église tous les hérétiques ; en quoi il se contredit lui-même, XV,
102, 103. Ce concile a été formé contre les principes du ministre, qui
veut que les sectes hérétiques aient voix délibérative dans les
conciles, XV, 124. Il croit trouver l'inégalité du Père et du Fils dans
ces paroles du Symbole : Dieu de Dieu, lumière de lumière. Les Pères qui
avoient assisté à ce concile, ont compris ces paroles tout différemment
du ministre, XVI, 48, 49. Il soutient aussi que ce concile a enseigné
deux nativités du Verbe. Cette erreur réfutée par saint Athanase, XVI,
52 et _suiv._ Jurieu fait même admettre aux Pères de Nicée trois
nativités du Fils, XVI, 59, 60.

Le second concile de Nicée, septième général, admet après examen les
lettres du pape Adrien, XXI, 82 et _suiv._ ; XXII, 77. Il ne demande

 

371

 

point de confirmation au Pape, XXII, 118. Les François refusent de le
recevoir, XXII, 77, 78 et _suiv._ _Voy._ Francfort.

NICODÈME. Jésus lui explique la renaissance spirituelle, VI, 396 et
_suiv_.

NICOLAS I (S.), Pape, marque exactement les bornes des deux puissances,
dans sa lettre à Michel III. Il n'est point auteur du Canon Omnes cité
par Gratien, non plus que Nicolas II, XXI, 264. Il excommunie Lothaire,
mais ne parle pas de le déposer, XXI, 336, 337. Réponse peu exacte qu'il
donne aux Bulgares touchant le baptême, XXII, 239.

NICOLAS III. Sa bulle _Exiit,_ touchant la règle de saint François,
XXII, 246, 247 et _suiv._

NICOLAS V reçoit Félix et les évêques du concile de Bâle, comme
catholiques, XXI, 711 et _suiv._

NICOLE (Pierre). Bossuet aimait à recevoir des marques de son amitié et
de son approbation, XXVI, 460. Il priait Dieu qu'il le conservât pour
soutenir la cause de son Église, dont ses ouvrages lui parois-soient un
arsenal, XXVI, 461 .

NINIVE. Sa fondation, XXIV, 266. Sa grandeur, XXIV, 273, 596. Elle est
prise et détruite, XXIV, 284. Vivacité de la douleur des Ninivites à la
prédication de Jonas, X, 385.

NINUS, fils de Bel, fonde le premier empire des Assyriens, XXIV, 273,
596.

NOAILLES (Louis-Antoine de), évêque de Châlons, puis archevêque de Paris
et cardinal. Bossuet se réjouit de sa nomination à l'archevêché de
Paris, XXVIII, 255. M. de Noailles approuve le livre des Réflexions
morales, m, 306 et _suiv._ Il y enseigne clairement le contraire des
cinq propositions, III, 308. Il est accusé de jansénisme par l'auteur du
Problème ecclésiastique, III, 309. Ce que c'était que ce libelle, XXX,
242. Bossuet défend M. de Noailles, III, 309. Il lui envoie une
correction pour l'Avertissement sur le livre des Pièflexions morales, et
se réjouit de le voir appelé à défendre la doctrine de saint Augustin,
XXVII, 85. Bossuet compose pour lui l'Instruction pastorale sur la grâce
et la prédestination, V, 463 et _suiv._ Il envoie cette Instruction à
Rome au nom de M. de Noailles, XXIX, 22. Combien il désirait la voir
approuvée, XXIX, 23, 24, 30, 33, 40. Le cardinal Casanate en est
satisfait, XXIX, 32, 42.

M. de Noailles est chargé d'examiner les écrits de Fénelon et de Madame
Guyon sur l'oraison, XX, 101, 102. Il approuve le livre de Bossuet sur
les _Etats d'oraison,_ XVIII, 377. Il se déclare avec Bossuet et
l'évêque de Chartres contre le livre des _Maximes_ des Saints, XIX, 495
et _suiv._ ; 508 et _suiv._ Fénelon veut le détacher de Bossuet, XXIX,
74. Lettres de M. de Noailles à l'abbé Bossuet, relativement à l'affaire

 

372

 

du quiétisme, XXIX, 288, 358, 379, 398, 413, 414, 421, 422, 428, 435,
447, 451, 484,497, 524, 532, 541 ; XXX, 4, 30, 38, 51, 61, 78, 107,120,
131, 143, 160, 199, 221, 252, 281, 291, 317, 337, 367, 469. Il est
jaloux de l'évêque de Meaux, XXX, 3.

Bossuet le félicite de sa promotion au cardinalat, XXVIII, 109. Ce
cardinal préside l'assemblée du clergé de 1700, XXII, 721. L'évêque de
Meaux l'engage à défendre l'épiscopat contre l'entreprise du chancelier,
XXXI, 63, 70, 79, 80, 83, 84, 85 et _suiv._ _Voy._ Bossuet, Fénelon,
Guvon, Pontchartrain, Simon.

NOAILLES (duc de), frère du cardinal, commanda dans le Roussillon et fut
fait maréchal de France en 1693, XXVI, 339, note. Bossuet le prie de
n'être point en peine de certains papiers, _ibid._ Il se réjouit de voir
le duc revenu à la santé, il l'entretient des canons des conciles,
_ibid._ Il prie le duc d'écrire en sa faveur à M. de Naves, XXVI, 340.

NOBLESSE. Elle n'est souvent qu'une pauvreté vaine, ignorante et
grossière, oisive, qui se pique de mépriser tout ce qui lui manque, X,
556 et _suiv._ Quelle est la véritable noblesse, XI, 373, 374. De quelle
sorte la noblesse est recommandable, XII, 697 et _suiv._ Noblesse de
l'homme, VIII, 292.

NOÉ, ou le déluge, seconde époque de l'histoire, XXIV, 266. Noé est seul
réservé avec sa famille pour la réparation du genre humain, XXIV, 265,
377. Il partage la terre entre ses trois enfants, XXIV, 266. Ses enfants
n'épousèrent pas leurs sœurs, comme le dit Jurieu, XIV, 192, 193. De
quoi Noé était la figure, VIII, 402.

NOÉL. Elévations sur l'étable et la crèche, VII, 268, 269. L'ange
annonce Jésus aux bergers, VII, 270 et _suiv._ Marques pour le
reconnaître, VII, 271, 272. Cantique des anges, VII, 272 et _suiv._
Commencement de l'Évangile, VII, 274. Les bergers à la crèche, VII, 275,
276. Silence et admiration de Marie, VII, 276,277 et _suiv._ Instruction
sur le mystère de ce jour, V, 152. Ce que nous y honorons, VIII, 195,196
et _suiv._ Sujets de méditation pour cette fête, XXVII, 419, 598 ;
XXVIII, 106. Sur les trois messes de ce jour, XXVIII, 107. Pensées
pieuses sur cette fête, XXVIII, 125, 126. _Voy._ Jésus-Christ, Sauveur,
Verbe.

NOGUIER, ministre protestant, attaque le livre de l'Exposition : il
avance que Bossuet adoucit et exténue les dogmes de sa religion, XIII,
2. Il accuse les catholiques d'idolâtrie ; les compare aux manichéens,
ariens, etc., XIII, 123.

NOR1S (Henri), cardinal, envoie à Bossuet son Apologie des moines de
Scythie, XXVIII, 673. Estime qu'en fait Bossuet, XXVIII, 676. Il le
remercie de ce présent, XXVI, 517. Eloges qu'il donne aux dissertations
de ce cardinal, XXIX, 3. Il le soupçonne d'un peu de froid à son égard,
XXIX, 15. Ce qu'il pense du livre de Fénelon, XXIX, 283. Il est choisi
avec le cardinal Ferrari pour présider aux conférences des

 

373

 

examinateurs de ce livre, XXIX, 297. Expédient proposé par ce cardinal
pour abréger, XXIX, 454, 455. L'abbé Bossuet le voit pour cette affaire,
XXX, 24, 25, 45, 54. Ce cardinal désapprouve les lettres de Fénelon à
l'évêque de Chartres, XXX, 74. Comment il s'explique dans les
congrégations, XXX, 122. Altercation entre lui et le cardinal de
Bouillon, XXX, 268, 269. Il est un des rédacteurs du Bref contre le
livre des _Maximes,_ XXX, 277.

NORMANDS. Leurs ravages en France, XXV, 32, 36, 38.

NOURRITURE, y éviter la délicatesse, XXVII, 580. _Voy._ Corps.

NOUVEAUTÉ. Amour incroyable de la nouveauté ; son origine, XI, 177. Ses
effets, XI, 178. Quelle nouveauté nous représente l'Église dans le
mystère du Verbe fait chair, XI, 178, 179, 180,1S5.

NOUVELLES CATHOLIQUES, manière de les instruire, XXVII, 443, 444. La
patience et la douceur sont le seul moyen de les gagner, XXVIII, 458.
_Voy._ Catholiques.

NOVATIEN, antipape. Son schisme éteint par le consentement commun, XXI,
96.

NOVICES. Avis pour leur conduite, XXVIII, 229, 271, 366, 368, 369, 476.

Ce qu'il faut pratiquer étant novice, XXVII, 618, 622. NUMA POMPILIUS,
roi de Rome, règle les mœurs et la religion, XXIV, 281, 282, 642.

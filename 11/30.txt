[BERNARDINE II]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Conception I
Conception II
Conception III
Nativ. Marie I
Nativ. Marie II
Nativ. Marie III
Nativ. Marie précis
Présent. Marie précis
Annonciation I
Annonciation II
Annonciation III
Annonciation IV
Exorde Annonciation
Visitation I
Visitation-Entretien
Visitation II
Purification I
Purification II
Purification III
Assomption I
Assomption II
Assomption Plan
Assomption Veille
Rosaire
Scapulaire
Mlle de Bouillon
V. Nlle Catholique I
V. Nlle Catholique II
Bernardine I
Bernardine II
M-Th de la Vieuville
M-A de Beauvais
Prof. Epiphanie
Prof. Exaltation
Prof. s/ Virginité
Marie-Anne de Bailly
Mme de La Vallière
Unité de l'Église

 



SECOND SERMON POUR LA VÊTURE D'UNE POSTULANTE BERNARDINE (_A_).


 

_Si vos Filius liberaverit, verè liberi eritis_.

Vous serez vraiment libres, quand le Fils vous aura délivrés. _Joan.,_
vin.

 

Cette jeune fille se présente à vous, Mesdames, pour être admise dans
votre cloitre, comme dans une prison volontaire. Ce ne sont point des
persécuteurs qui l'amènent : elle vient touchée du mépris du monde ; et
sachant qu'elle a une chair qui par la corruption de notre nature est
devenue un empêchement à l'esprit, elle s'en veut rendre elle-même la
persécutrice par la mortification et la pénitence. La tendresse d'une
bonne mère n'a pas été capable de la rappeler aux douceurs de ses
embrassements : elle a surmonté les obstacles que la nature tâchait
d'opposer à sa généreuse résolution ; et l'alliance spirituelle qu'elle
a contractée avec vous par le Saint-Esprit, a été plus forte que celle
du sang. Elle préfère la blancheur de saint Bernard à l'éclat de la
pourpre, dans laquelle nous pouvons dire qu'elle a pris naissance ; et
la

 

(a) Prêché dans un couvent de Bernardine?, vers 1661.

Point de difficulté pour le lieu ; car l'orateur, s'adressant à des
religieuses, parle longuement de saint Benoit leur Patriarche, et de
saint Bernard leur Père.

Quant à la date, si l'on en juge par le style du discours, on doit la
fixer vers l'année 1661.

Déforis fait la remarque suivante : « Ce discours a pour objet les mêmes
vérités que le précédent ; mais comme il les traite fort différemment et
contient beaucoup de choses nouvelles, nous nous sommes bornés à en
retrancher le commencement, qui était absolument semblable au début du
premier sermon. »

Reproduit d'après les premières éditions.

 

446

 

pauvreté de Jésus-Christ lui plaît davantage que les richesses dont le
siècle l'aurait vue parée. Bien qu'elle sache qu'aux yeux des mondains
un monastère est une prison, ni vos grilles, ni votre clôture ne
l'étonnent pas : elle veut bien renfermer son corps, afin que son esprit
soit libre à son Dieu ; et elle croit aussi bien que Tertullien (1) que
comme le monde est une prison, en sortir c'est la liberté.

Et certes, ma très-chère Sœur, il est véritable que depuis la rébellion
de notre nature, tout le monde est rempli de chaînes pour nous. Tant que
l'homme garda l'innocence que son Créateur lui avait donnée, il était le
maître absolu de tout ce qui se voit dans le monde ; maintenant il en est
l'esclave, son péché l'a rendu captif de ceux dont il était né
souverain. Dieu lui dit dans l'innocence des commencements : Commande à
toutes les créatures : _Subjicite terram ; dominamini piscibus maris, et
volatilibus cœli, et universis animantibus_ (2) : « Assujettis-toi la
terre, et domine sur les poissons de la mer, sur les oiseaux du ciel et
sur tous les animaux. » Au contraire, depuis sa rébellion : Garde-toi de
toutes les créatures. Il n'y en a point dans le monde qui ne croie
qu'elle le doit avoir pour sujet, depuis qu'il ne l'est plus de son Dieu
: c'est pourquoi les uns vomissent pour ainsi dire contre lui tout ce
qu'elles ont de malignité ; et si les autres montrent leurs appas ou
étalent leurs ornements, c'est dans le dessein de lui plaire trop, et de
lui ravir par cet artifice tout ce qui lui reste de liberté. Les
créatures, dit le Sage, sont autant de pièges tendus de toutes parts à
l'esprit de l'hommes. L'or et l'argent lui sont des liens, desquels son
cœur ne peut se déprendre ; les beautés mortelles l'entraînent captif,
le torrent des plaisirs l'emporte ; cette pompe des honneurs mondains,
toute vaine qu'elle est, éblouit ses yeux ; le charme de l'espérance lui
ôte la vue ; en un mot, tout le monde semble n'avoir d'agrément que pour
l'engager dans sa servitude, par une affection déréglée.

Et après cela ne dirons-nous pas que ce monde n'est qu'une prison , qui
a autant de captifs qu'il a d'amateurs (_a_) ? De sorte

 

1 _Ad Mart_., n. 2. — 2 _Genes_., I, 28. — 3 _Sap_., XIV, 11.

 

(_a_) Var. :  Autant de captifs que d'amateurs.

 

447

 

que vous tirer du monde, c'est vous tirer des fers et de l'esclavage ;
et la clôture où vous vous jetez n'est pas ' comme les hommes se le
persuadent, une prison où votre liberté soit contrainte , mais un asile
fortifié où votre liberté se défend contre ceux qui s'efforcent de
l'opprimer : c'est ce que je me propose de vous faire entendre avec le
secours de la grâce. Mais afin que nous voyions éclater la vraie
jouissance de la liberté dans les maisons des vierges sacrées,
distinguons avant toutes choses trois sortes de captivité dans le monde.

Il y a dans le siècle trois lois qui captivent : il y a premièrement la
loi du péché ; après, celle des passions et des convoitises ; et la
troisième est celle que le siècle nomme la nécessité des affaires et la
loi de la bienséance mondaine. Et en premier lieu, le péché est la plus
infâme des servitudes, où la lumière de la grâce étant toute éteinte,
l'âme est jetée dans un cachot ténébreux , où elle souffre de la
violence du diable tout ce que souffre une ville prise de la rage d'un
ennemi implacable et victorieux. Que les passions nous captivent, c'est
ce qui paraît par l'exemple d'un riche avare, qui ne peut retirer son
âme engagée parmi ses trésors, et parce que Dieu défend aux Israélites
d'épouser des femmes idolâtres, de peur , dit-il, qu'elles n'amollissent
leurs cœurs et les entraînent après des dieux étrangers (1). Et d'où
vient cela, chrétiens, si ce n'est que les passions ont certains liens
invisibles , qui tiennent nos volontés asservies?

Mais j'ose dire que le joug le plus empêchant que le monde impose à ceux
qui le suivent, c'est celui de l'empressement des affaires et la
bienséance du monde. C'est là ce qui nous dérobe le temps ; c'est là ce
qui nous dérobe à nous-mêmes ; c'est ce qui rend notre vie tellement
captive, dans cette chaîne continuée de visites, de divertissements,
d'occupations qui naissent perpétuellement les unes des autres, que nous
n'avons pas la liberté de penser à nous. O servitude cruelle et
insupportable, qui ne nous permet pas de nous regarder ! C'est ainsi que
vivent les enfants du siècle ! Parmi tant de servitudes diverses, nous
nous imaginons être libres. De quelque liberté que nous nous flattions,
jamais

 

1 _Exod_., XXXIV, 16.

 

448

 

nous ne serons vraiment libres, jusqu'à ce que le Fils de Dieu nous ait
délivrés.

Mais qui sont ceux qui seront plutôt délivrés par votre toute-puissante
bonté, ô miséricordieux Sauveur des hommes, si ce n'est ces âmes pures
et célestes qui ont tout quitté pour l'amour de vous? C'est donc vous,
mes très-chères Sœurs, c'est vous que je considère comme vraiment
libres, parce que le Fils vous a délivrées de la triple servitude qu'on
voit dans le monde, du péché, des passions, de l'empressement. Le péché
doit être exclu du milieu de vous, par l'ordre et la discipline
religieuse ; les passions y perdent leur force, par l'exercice de la
pénitence ; la loi de la prétendue bienséance , que la vanité humaine
s'impose, n'y est pas reçue, par le mépris qu'on y fait du monde : et
ainsi l'on y peut jouir pleinement de la liberté bienheureuse que le
Fils de Dieu à rendue à l'homme : _Si vos Filius liberaverit, verè
liberi eritis_. C'est ce que j'espère vous faire entendre aujourd'hui,
avec le secours de la grâce.

 


PREMIER POINT.

 

C'est une juste punition de Dieu, que l'homme après avoir méprisé la
solide possession des biens véritables que son Créateur lui avait
donnés, soit abandonné à l'illusion des biens apparents. Les plaisirs du
paradis ne lui ont pas plu ; il sera captif des plaisirs trompeurs qui
mènent les âmes à la perdition : il ne s'est pas voulu contenter de
l'espérance de l'immortalité bienheureuse , il se repaîtra d'espérances
vaines, que souvent le mauvais succès et toujours la mort rendra
inutiles : il n'a point voulu de la liberté qu'il avait reçue de son
souverain ; il se plaira dans la liberté imaginaire que sa raison volage
lui a figurée. Justement, certes, justement, Seigneur ; car il est juste
que ceux-là n'aient que de faux plaisirs, qui ne veulent pas les
recevoir de vos mains ; qu'ils n'aient qu'une fausse liberté, puisqu'ils
ne veulent pas la tenir de vous ; et enfin qu'ils soient livrés à
l'erreur, puisqu'ils ne se contentent pas de vos vérités.

En effet considérons, mes très-chères Sœurs, quelle image de liberté se
proposent ordinairement les pécheurs. Qu'elle est fausse,

 

449

 

qu'elle est ridicule, qu'elle est, si je puis parler ainsi, chimérique !
Ecoutons-les parler, et voyons de quelle liberté ils se vantent. Nous
sommes libres , nous disent-ils , nous pouvons faire ce que nous
voulons. Mes Sœurs, examinons leurs pensées, et nous verrons combien ils
se trompent ; et nous confesserons devant Dieu dans l'effusion de nos
cœurs que nul pécheur ne peut être libre , que tous les pécheurs sont
captifs. Tu peux faire ce que tu veux, et de là tu conclus : Je suis
libre. Et moi je te réponds au contraire : Tu ne peux pas faire ce que
tu veux, et quand tu le pourrais, tu n'es pas libre. Montrons
premièrement aux pécheurs qu'ils ne peuvent pas ce qu'ils veulent.

Et certainement nous pourrions leur dire qu'ils ne peuvent pas ce qu'ils
veulent, puisqu'ils ne peuvent pas empêcher que leur fortune ne soit
inconstante, que leur félicité ne soit fragile , que ce qu'ils aiment ne
leur échappe, que la vie ne leur manque comme un faux ami au milieu de
leurs entreprises, et que la mort ne dissipe toutes leurs pensées. Nous
pourrions leur dire véritablement qu'ils ne peuvent pas ce qu'ils
veulent, puisqu'ils ne peuvent pas empêcher qu'ils ne soient trompés
dans leurs vaines prétentions. Ou ils les manquent, ou elles leur
manquent : ils les manquent, quand ils ne parviennent pas à leur but ;
elles leur manquent, quand obtenant ce qu'ils veulent ils n'y trouvent
pas ce qu'ils cherchent. C'est ainsi que nous pouvons montrer aux
pécheurs qu'ils ne peuvent pas ce qu'ils veulent.

Mais pressons-les de plus près encore, et déplorons l'aveuglement de ces
malheureux qui se vantent de leur liberté , pendant qu'ils gémissent
dans un si honteux esclavage. Ah ! les misérables captifs, ils ne
peuvent pas ce qu'ils veulent le plus ; ce qu'ils détestent le plus, il
faut qu'il arrive. Que prétendez-vous, ô pécheur, dans ces plaisirs que
vous recherchez, dans ces biens que vous amassez par des voleries ; que
prétendez-vous? — Je veux être heureux. — Et pourquoi ! heureux même
malgré Dieu? Insensé, qui vous imaginez avoir aucun bien contre la
volonté du souverain bien ! digne certes qu'on dise de vous ce que nous
lisons dans les Psaumes : « Voilà l'homme qui n'a pas mis son secours en
Dieu, mais qui a espéré dans la multitude de ses richesses et s'est plu

 

450

 

dans sa vanité (1). » Mais non-seulement vous ne pouvez obtenir ce que
vous avez le plus désiré ; ce que vous détestez le plus, il faut qu'il
arrive ; cette justice divine qui vous poursuit, ces étangs de feu et de
soufre , ce grincement de dents éternel. Car quelle force vous peut
arracher des mains toutes-puissantes de Dieu, que vous irritez par vos
crimes et dont vous attirez sur vous les vengeances ?

Telle est la liberté de l'homme pécheur : malheureux, qui croyant faire
ce qu'il veut, attire sur lui nécessairement ce qu'il veut le moins ;
qui pour trop faire ses volontés , par une étrange contradiction de
désirs, s'empêche lui-même d'être ce qu'il veut, c'est-à-dire heureux
(_a_) ; qui s'imagine être vraiment libre, parce qu'il est en effet trop
libre à pécher, c'est-à-dire libre à se perdre ; et qui ne s'aperçoit
pas qu'il forge ses fers par l'usage de sa liberté prétendue ! Et de là
nous pouvons apprendre que ce n'est pas être vraiment libre, que de
faire ce que nous voulons ; mais que notre liberté véritable, c'est de
faire ce que Dieu veut. De là vient que nous lisons dans notre évangile
, que les hommes sont vraiment libres quand le Fils les a délivrés : où
nous devons entendre, mes Sœurs, que le Fils de Dieu nous parlant d'une
liberté véritable, nous explique assez qu'il y en a aussi une fausse.

La fausse liberté, c'est de vouloir faire sa volonté propre ; mais notre
liberté véritable, c'est que notre volonté soit soumise à Dieu : car
puisque nous sommes nés sous la sujétion de Dieu, notre liberté n'est
pas une indépendance. Cette affectation de l'indépendance, c'est la
liberté de Satan et de ses rebelles complices, qui ont voulu s'élever
eux - mêmes contre l'autorité souveraine. Loin de nous une liberté si
funeste, qui a précipité ces esprits superbes dans une servitude
éternelle ! Pour nous, songeons tellement que nous sommes libres, que
nous n'oubliions pas que nous sommes des créatures, et des créatures
raisonnables, que Dieu a faites à sa ressemblance. Puisque notre liberté
est la liberté d'une créature, il faut nécessairement qu'elle soit
soumise, et qu'il y ait

 

1 _Psal._ LI, 9.

(_a_) _Var._ : Empêche lui-même l'exécution de sa volonté principale,
qui est d'être heureux.

 

451

 

de la servitude mêlée. Mais il y a une servitude honteuse, qui est la
destruction de la liberté ; et une servitude honorable, qui en est la
perfection. S'abaisser au-dessous de sa dignité naturelle , c'est une
servitude honteuse : c'est ainsi que font les pécheurs ; c'est pourquoi
ils ne sont pas libres. S'abaisser au-dessous de celui-là seul qui est
seul naturellement souverain, c'est une servitude honorable, qui est
digne d'un homme libre, et qui fait l'accomplissement de la liberté. En
est-on moins libre, pour obéir à la raison et à la raison souveraine,
c'est-à-dire à Dieu? N'est-ce pas au contraire une dépendance vraiment
heureuse, qui nous assujettissant à Dieu seul, nous rend maîtres de
nous-mêmes et de toutes choses

C'est ainsi que le Sauveur voulut être libre : il était libre
certainement, car il était Fils et non pas esclave ; mais il mit l'usage
de sa liberté à être obéissant à son Père. Comme c'est la liberté qu'il
a recherchée , c'est aussi celle qu'il nous a promise. « Vous serez,
dit-il, vraiment libres, quand le Fils vous aura délivrés : » vous aurez
une liberté véritable , quand le Fils vous l'aura donnée. Et quelle
liberté vous donnera-t-il, sinon celle qu'il a voulue pour lui-même ?
c'est-à-dire d'être dépendant de Dieu, dont il est si doux de dépendre,
et le service duquel vaut mieux qu'un royaume, parce que cette même
soumission, qui nous met au-dessous de Dieu, nous met en même temps
au-dessus de tout. C'est pourquoi je ne puis m'empêcher, ma Sœur, de
louer votre résolution généreuse, en ce que vous avez voulu être libre,
non point à la mode du monde, mais à la mode du Sauveur des âmes ; non de
la liberté dangereuse que l'esprit de l'homme se donne à lui-même, mais
de celle que Jésus promet à ses serviteurs.

Les enfants du siècle croient être libres, parce qu'ils errent deçà et
delà dans le monde, éternellement travaillés de soins superflus, et ils
appellent leur égarement une liberté : à peu près comme des enfants qui
se pensent libres, lorsqu'échappés de la maison paternelle, ils courent
sans savoir où ils vont. Telle est la liberté des pécheurs.

C'est vous, c'est vous, Mesdames, qui jouissez d'une liberté véritable,
parce que vous ne vous contraignez que pour servir Dieu.

 

452

 

Et qu'on ne pense pas que cette contrainte diminue tant soit peu votre
liberté ; au contraire, c'en est la perfection. Car d'où vient que vous
vous mettez dans cette salutaire contrainte, sinon pour vous imposer à
vous-mêmes une heureuse nécessité de ne pécher pas? Et cette sainte
nécessité de ne pécher pas, n'est-ce pas la liberté véritable? Ne
croyons pas, mes Sœurs, que ce soit une liberté, de pouvoir pécher ; ou
s'il y a de la liberté à pouvoir pécher, disons avec saint Augustin que
c'est une liberté égarée, une liberté qui se perd. La première liberté,
dit saint Augustin, c'est de pouvoir ne pécher pas ; la seconde et la
plus parfaite, c'est de ne pouvoir plus pécher (1). C'est la liberté des
saints anges et de toute la société des élus, que la félicité éternelle
met dans la nécessité de ne pécher plus : c'est la liberté de la céleste
Jérusalem ; cette nécessité, c'est leur béatitude ; et jamais nous ne
serons plus libres, que quand nous ne pourrons plus servir au péché.
C'est la liberté de Dieu même, qui peut tout et ne peut pécher. C'est à
cette liberté qu'on tend dans les cloîtres, lorsque par tant de saintes
contraintes, par tant de salutaires précautions, on tâche de s'imposer
une loi de ne pouvoir plus servir au péché.

 


SECOND  POINT.

 

Voilà la servitude du péché exclue de la vie retirée et religieuse par
les observances de la discipline : voyons si elle n'est pas aussi
délivrée de celle des passions et des convoitises par l'exercice de la
pénitence. Pour cela, considérons une belle doctrine de saint Augustin :
« Il y a, dit-il, deux sortes de maux : il y a des maux qui nous
blessent, il y a des maux qui nous flattent : les maladies, les
passions. Les passions nous flattent, et en nous flattant elles nous
captivent. Ceux-là nous les devons supporter ; ceux-ci nous les devons
modérer : les premiers, par la patience et par le courage ; les seconds,
par la retenue et la tempérance : » _Alia quœ per patientiam sustinemus,
alia quœ per continentiam refrenamus_ (2). Or Dieu, qui dispose toutes
choses par une providence très-sage, et qui ne veut pas tourmenter les
siens par des afflictions inutiles, a voulu que ces derniers maux
servissent de remède pour guérir

 

1 _De Corrept. et Grat_., cap. XII, n. 33. — 2 _Cont. Jul_., lib. V,
cap. V, n. 22.

 

453

 

les autres : je veux dire que les maux qui nous affligent doivent
corriger en nous ceux qui flattent. Ils étaient donnés en punition de
notre péché ; mais par la miséricorde divine ce qui était une peine
devient un remède, et « le châtiment du péché est tourné à l'usage de la
justice : » _In usus justitiœ peccati pœna conversa est_ (1). La raison
est que la force de ceux-ci consiste dans le plaisir, et que toute la
pointe du plaisir s'émousse par la souffrance.

C'est pourquoi la mortification... dans les cloîtres ; et si la chair y
est contrainte, c'est pour rendre l'esprit plus libre. C'est le rendre
plus libre, que de brider son ennemi et le tenir en prison tout chargé
de chaînes. C'est ce qui fait dire à l'Apôtre : « Je ne travaille pas en
vain ; mais je châtie mon corps et je le réduis en servitude (2). » Ce
n'est pas travailler en vain que de mettre en liberté mon esprit. J'ai,
dit-il, un ennemi domestique : voulez-vous que je le fortifie, que je le
rende invincible par ma complaisance ? J'ai des passions moins
traitables que ne sont des bêtes farouches : voulez-vous que je les
nourrisse? Ne vaut-il pas bien mieux que j'appauvrisse mes convoitises,
qui sont infinies, en leur refusant ce  qu'elles demandent? Tellement
que la vraie liberté d'esprit, c'est de contenir nos affections
déréglées par une discipline forte et vigoureuse, et non pas de les
contenter par une molle condescendance.

C'est ainsi qu'ont été libres les grands personnages, qui vous ont donné
cette règle que vous professez. D'où vient que saint Benoit votre
Patriarche, sentant que l'amour des plaisirs mortels qu'il avait presque
éteint par ses grandes austérités se réveillait tout à coup avec
violence, se déchire lui-même le corps par des ronces et des épines, sur
lesquelles son zèle le jette (3) ? N'est-ce pas qu'il veut briser les
liens charnels qui menacent son esprit de la servitude ? C'est pour cela
que saint Bernard votre Père a cherché un salutaire rafraîchissement
dans les neiges et dans les étangs glacés (4), où son intégrité attaquée
s'est fait un rempart contre les délices du siècle. Ses sens étaient de
telle sorte

 

1 S. August., _De Civit. Dei,_ lib. XIII, cap. IV. — 2 I _Cor.,_ IX, 26,
27. — 3 S. Greg. Mag., _Dialog_., lib. II, cap. II. — 4 _Vit. S.
Bernard_., lib. I, cap. III, n. 6.

 

454

 

mortifiés, qu'il ne voyait plus ce qui se présentait à ses yeux (1). La
longue habitude de mépriser le plaisir du goût, avait éteint en lui
toute la pointe de la saveur : il mangeait de toutes choses sans choix ;
il buvait de l'eau ou de l'huile indifféremment, selon qu'il les avait
le plus à la main (2). Si quelques-uns trouvaient trop rude ce long et
horrible silence, il les avertissait que. s'ils considéraient
sérieusement l'examen rigoureux que le grand Juge fera des paroles, ils
n'auraient pas beaucoup de peine à se taire. Il excitait en lui
l'appétit, non par les viandes, mais par les jeûnes ; non par la
délicatesse ni par le ragoût, mais par le travail : et toutefois pour
n'être pas entièrement dégoûté de son pain d'avoine et de ses légumes,
il attendait que la faim les rendît un peu supportables. Il couchait sur
la dure ; mais il y attirait le sommeil par la psalmodie de la nuit et
par l'ouvrage de la journée : de sorte que dans cet homme les fonctions
même naturelles étaient causées non tant par la nature que par la vertu.

Quel homme plus libre que saint Bernard? Il n'a point de passions à
contenter, il n'a point de fantaisie à satisfaire, et il n'a besoin que
de Dieu. Les gens du monde, au lieu de modérer leurs convoitises, sont
contraints de servir à celles d'autrui. Saint Augustin, parlant à un
grand seigneur : « Vous, qui devez réprimer vos propres cupidités, vous
êtes contraint de satisfaire celles des autres : » _Qui debuisti
refrenare cupiditates tuas, explere cogeris aliénas_ (3). C'est à cette
liberté que vous aspirez, c'est l'héritage que saint Bernard a laissé à
toutes les maisons de son ordre.

Mais voyez l'aveuglement du monde. Comme si nous n'étions pas encore
assez captifs par le péché et les convoitises, il s'est fait lui-même
d'autres servitudes. Il a fait des lois comme pour imiter Jésus-Christ,
mais plutôt pour le contredire. Il ne faut pas souffrir les injures, on
vous mépriserait : il faut avoir de l'honneur dans le monde, il faut se
rendre nécessaire, il faut vivre pour le public et pour les affaires :
_Patriae et imperio reique vivendum est_ (4). C'est une loi à votre
sexe... Le temps de se parer, des visites. La bienséance est une loi qui
nous ôte tout le temps, qui fait qu'il

 

1 _Lib_. III, cap. II, n. 4. — 2 Lib. I, cap. VII. — 3 _Ad Bonif_.,
epist. CCXX, n. 6. — 4 Tertull., _De Pallio,_ n. 5.

 

455

 

se perd véritablement. Tout le temps se perd, et on n'y attache rien de
plus immobile que lui. Le temps est précieux, parce qu'il aboutit à
l'éternité ; on ne demande qu'à le passer ; à peine avons-nous un moment
à nous ; et celui que nous avons, il semble qu'il soit dérobé. Cependant
la mort vient avant que nous puissions avoir appris à vivre ; et alors
que nous servira d'avoir mené une vie publique, puisqu'enfin il nous
faudra faire une fin privée? Mais que dira le monde ? Et pourquoi
voulons-nous vivre pour les autres, puisque nous devons enfin mourir
pour nous-mêmes ? _Nemo alii vivit, moriturus sibi_ (1).

Que si le monde a ses contraintes, que je vous estime, ma très-chère
Sœur, qui estimant trop votre liberté pour la soumettre aux lois de la
terre, professez hautement de ne vouloir vous captiver que pour l'amour
de celui qui étant le maître de toutes choses , s'est rendu esclave pour
l'amour de nous, afin de nous exempter de la servitude. C'est dans cette
voie étroite que l'âme est dilatée par le Saint-Esprit, pour recevoir
l'abondance des grâces divines. Déposez donc , ma très-chère Sœur, cet
habit, cette vaine pompe et toute cette servitude du siècle : vous êtes
libre à Jésus-Christ, son sang vous a mise en liberté, ne vous rendez
point esclave des hommes.

 

[Précédente]

[Accueil]

[Suivante]

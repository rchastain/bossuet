# Éditeur

Éditeur multiplateforme pour les [œuvres complètes de Bossuet au format texte](https://gitlab.com/rchastain/bossuet).

## Recherche

Permet de chercher un mot dans l'ensemble des fichiers, selon plusieurs modes de comparaison :

- Littéral
- Littéral étendu (sans tenir compte de la casse)
- Expression régulière
- Approximatif (1)
- Approximatif étendu (sans tenir compte de la casse) (1)

(1) Utilise l'unité [ApproxStrUtils](https://www.gausi.de/approxstrutils-en.html).

## Édition

En cliquant sur les correspondances trouvées, le fichier concerné s'ouvre dans une deuxième fenêtre, où le fichier peut être édité.

## Conversion en UTF-8

Permet également de convertir les fichiers en UTF-8, soit seulement en mémoire, soit en mémoire *et* sur le disque.

Les fichiers ayant été tous convertis, cette fonction a été désactivée :

```pascal
    (*
    BTConvert.Enabled := TRUE;
    CBConvertFiles.Enabled := TRUE;
    *)
```

## Compilation

L'éditeur est un projet [Lazarus](https://www.lazarus-ide.org/index.php?page=downloads).

## Capture d'écran

![](editeur.png)

program Editeur;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, Forms,
  MainForm, EditForm;

{$R *.res}

begin
  Application.Title:='Editeur';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, FMMain);
  Application.CreateForm(TEditForm, FMEdit);
  Application.Run;
end.


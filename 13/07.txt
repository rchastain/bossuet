[EXP. FRAGMENTS IV-I]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Exposition
Exp. Lettres
Exp. Fragments I
Exp. Fragments II
Exp. Fragments III
Exp. Fragments IV-I
Exp. Fragments IV-II
Exp. Fragments IV-III
Exp. Fragments IV-IV
Exp. Fragments V
Avert. Réf. Cat.
Réfut. Catéch. de Ferry
Première Vérité
Seconde Vérité
Conf. M. Claude Avert.
Préparation
Conférence M. Claude
Conférence Suite
Réflexions sur M. Claude

 



QUATRIÈME FRAGMENT : SUR L'EUCHARISTIE.


 



FRAGMENTS RELATIFS À l'EXPOSITION DE l'EUCHARISTIE : I  .


 

 

 I. — Réflexions préliminaires de l'auteur sur les fragments suivants.

 II — Règle générale pour découvrir les mystères de la foi. Application
de cette règle à l'Écriture sainte.

 III. — Malheurs de ceux qui veulent écouter les raisonnements humains
dans les mystères de Dieu, et dans l'explication de son Écriture.

 IV. — Contradictions des prétendus réformés et de l'Anonyme en
particulier. Avantages qu'il donne aux sociniens.

 V. — Conséquences de ce discours : le premier principe qu'il faut
poser  pour entendre l'Écriture sainte, c'est qu'il n'y a rien qu'il ne
faille croire quand Dieu a parlé.

 VI. — Application de ce principe au mystère de l'Eucharistie.

 VII — Intention de Jésus-Christ dans l'institution de l'Eucharistie. La
loi des sacrifices.

 VIII. — Abus que l'Anonyme fait de cette parole de Jésus-Christ mourant
: Tout est consommé.

 

 


I. — Réflexions préliminaires de l'auteur sur les fragments suivants.

 

Il y a deux endroits de l'_Exposition_ où je me suis plus étendu que je
n'avais fait dans les autres : l'un où il s'agit de la présence réelle,
l'autre où il s'agit de l'autorité de l'Église. L'auteur de la réponse,
qui ne veut pas prendre la peine de considérer mon dessein et qui ne
tâche que d'en tirer quelque avantage, sans se soucier d'en expliquer
les motifs, conclut de là que j'ai été fort embarrassé sur tous les
autres sujets ; et que m'étant trouvé plus au large sur ceux-ci, j'ai
donné plus de liberté à mon style. Qu'il croie, à la bonne heure, que
les matières les plus importantes de nos controverses soient aussi
celles où nous nous sentons les plus forts et les mieux fondés. Mais il
ne fallait pas dissimuler que la véritable raison qui m'a obligé à
traiter plus amplement celles-ci, c'est qu'ayant examiné la doctrine des
prétendus réformés sur ces deux articles, j'ai trouvé qu'ils n'avaient
pu s'empêcher de laisser dans leur catéchisme ou dans d'autres actes
aussi authentiques de leurs églises, des impressions manifestes de la
sainte doctrine qu'ils avaient quittée. J'ai cru que la divine
Providence l'avait permis de la sorte pour abréger les disputes. En
effet comme parmi toutes nos controverses la matière de la présence
réelle est sans doute la plus difficile par son objet, et que celle de
l'Église est la plus importante par ses conséquences, c'est
principalement sur ces deux articles que nous avons à désirer de
faciliter le retour à nos adversaires : et nous regardons comme une
grâce singulière que Dieu fait à son Église d'avoir voulu que, sur deux
points si nécessaires, ses enfants qui se sont retirés de son unité
trouvassent dans leur croyance des principes qui les ramènent à la
nôtre. C'est pour leur conserver cet avantage que je leur ai

 

201

 

remis devant les yeux leur propre doctrine, après leur avoir exposé la
nôtre. Mais pour le faire plus utilement, je ne me suis pas contenté de
remarquer les vérités qu'ils nous accordent ; j'ai Toula marquer les
raisons par lesquelles ils sont conduits à les reconnaître, afin qu'on
comprenne mieux que c'est la force de la vérité qui les oblige à nous
avouer des choses si considérables, et qui semblaient si éloignées de
leur premier plan.

C'est pour cela que j'ai proposé, dans l'exposition de ces deux
articles, quelques-uns des principaux fondements sur lesquels la
doctrine catholique est appuyée. On y peut remarquer certains principes
de notre doctrine, dont l'évidence n'a pas permis à nos adversaires
eux-mêmes de les abandonner tout à fait, quelque dessein qu'ils aient eu
de les contredire : et les réponses de notre auteur achèveront de faire
voir qu'il est absolument impossible que ceux de sa communion disent
rien de clair ni de suivi, lorsqu'ils exposent leur croyance sur ces
deux points.

Nous parlerons dans la suite de ce qui regarde l'Église ; maintenant il
s'agit de considérer la présence réelle du corps et du sang de
Jésus-Christ dans l'Eucharistie. Il ne s'agit donc pas encore de savoir
si le corps est avec le pain, ou si le pain est changé au corps ; cette
difficulté aura son article à part : mais il est important, pour ne rien
confondre, de regarder séparément la matière de la présence réelle, sans
parler encore des difficultés particulières que les prétendus réformés
trouvent dans la transsubstantiation.

J'entreprends donc de faire voir qu'après les réponses de notre auteur,
on doit tenir pour certain que la doctrine des prétendus réformés n'est
pas une doctrine suivie ; qu'elle se dément elle-naême ; et que, plus ils
tentent de s'expliquer, plus leurs détours et leurs contradictions
deviennent visibles.

On verra au contraire en même temps que la doctrine catholique se
soutient partout ; et que si d'un côté elle se met fort peu eu peine de
s'accorder avec la raison humaine et avec les sens, de l'autre elle
s'accorde parfaitement avec elle-même et avec les grands principes du
christianisme, dont personne ne peut disconvenir.

 

202

 


II — Règle générale pour découvrir les mystères de la foi. Application de cette règle à l'Écriture sainte.

 

Il y a ici deux choses à considérer : 1° la règle générale qu'il faut
suivre pour découvrir les mystères de la religion chrétienne ; 2° ce qui
touche en particulier celui de l'Eucharistie. On verra dans l'une et
dans l'autre de ces deux choses, combien les sentiments de l'Église
catholique sont droits, et combien sont étranges les contradictions des
prétendus réformés.

La règle générale pour découvrir les mystères de notre foi, c'est
d'oublier entièrement les difficultés qui naissent de la raison humaine
et des sens, pour appliquer toute l'attention de l'esprit à écouter ce
que Dieu nous a révélé avec une ferme volonté de le recevoir, quelque
étrange et quelque incroyable qu'il nous paraisse.

Ainsi pour se rendre propre à entendre l'Écriture sainte, il fout avoir
tout à fait imposé silence au sens humain, et ne se servir de sa raison
que pour remarquer attentivement ce que Dieu nous dit dans ce divin
Livre.

En effet il n'y a jamais que deux sortes d'examen à faire dans la
lecture d'un livre : l'un, pour entendre le sens de l'auteur ; l'autre,
pour considérer s'il a raison et juger du fond de la chose. Mais comme
ce dernier examen cesse tout à fait lorsqu'on voit certainement que Dieu
a parlé, la raison ne doit plus servir de rien que pour bien entendre ce
qu'il veut dire.

Il est même vrai généralement de tous les livres que lorsqu'il ne s'agit
que d'en concevoir le sens, il faut se servir de son esprit pour
recueillir simplement sans aucune discussion du fond, ce qui résulte de
la suite du discours. Les livres qui sont dictés par le Saint-Esprit ne
doivent pas être lus avec moins de simplicité ; et nous devons au
contraire nous attacher d'autant plus à recueillir ce qu'ils portent
sans y mêler nos raisonnements, que nous sommes très-assurés que la
vérité y est toute pure.

Que si nous trouvons quelque obscurité dans les paroles de l'Écriture,
ou que le sens nous en paraisse douteux, alors comme l'Écriture a été
donnée pour être entendue, et qu'en effet elle l'a

 

203

 

été, il n'y aurait rien de plus raisonnable que de voir de quelle
manière elle a été prise par nos pères : car nous verrons en son lieu
que le sens qui a d'abord frappé les esprits et qui s'est toujours
conservé, doit être le véritable. Mais d'appeler la raison pour rejeter
ou pour recevoir une certaine interprétation, selon que la chose qu'elle
contient paraîtra ou plus ou moins raisonnable à l'esprit humain, c'est
anéantir l'Écriture, c'est en détruire tout à fait l'autorité.

 


III. — Malheurs de ceux qui veulent écouter les raisonnements humains dans les mystères de Dieu, et dans l'explication de son Écriture.

 

Aussi voit-on par expérience que si peu qu'on veuille écouter les
raisonnements humains dans les mystères de Dieu et dans l'explication de
son Écriture, on tombe dans l'un de ces deux malheurs, ou que la foi en
l'Écriture s'affaiblit, ou qu'on en force le sens par des
interprétations violentes.

Tant d'infidèles, qu'on voit répandus même dans le milieu du
christianisme, sont tombés dans ce premier malheur : et les égarements
effroyables des sociniens sont l'exemple le plus visible du second. Ces
hérétiques et les infidèles conviennent dans cette pensée : c'est Dieu
qui a donné la raison à l'homme, il faut donc que l'Écriture s'accorde
avec la raison humaine, ou l'Écriture n'est pas véritable. Mais après
avoir marché ensemble jusque-là, l'endroit où ils commencent à se
séparer, c'est que les uns ne pouvant accommoder l'Écriture sainte à ce
qu'ils se sont imaginés être raisonnable, l'abandonnent ouvertement ; et
les autres la tordent avec violence, pour la faire venir malgré elle à
ce qu'ils pensent.

Ainsi ces derniers posant pour principe que la raison ne peut souffrir
ni la Trinité, ni l'Incarnation, ils concluent que les passages où toute
l'Église a cru voir ces vérités établies, ne peuvent pas avoir le sens
qu'elle y donne, parce que ces choses, disent-ils, sont impossibles ; et
ensuite ils tournent tous leurs efforts à imaginer dans l'Écriture un
sens qui s'accorde avec leurs pensées.

Il n'y a personne qui ne voie que ce n'est pas écouter l'Écriture sainte
que de la lire dans cet esprit ; et qu'au contraire s'il fallait

 

204

 

suivre cette méthode pour l'interpréter, il n'y aurait presque aucun
livre qui fût plus mal entendu que celui-là, ni expliqué de plus
mauvaise foi. Car lorsqu'on examine les livres et les auteurs
ordinaires, par exemple Cicéron ou Pline, il n'arrivera pas, si peu
qu'on soit raisonnable, qu'on se mette dans l'esprit un certain sens
qu'on veuille nécessairement y trouver ; mais on est prêt à recevoir
celui qui sort pour ainsi dire des expressions et de la suite du
discours. Au contraire si on lit l'Écriture sainte selon la méthode des
sociniens, on viendra à cette lecture avec certaines idées qui ne sont
point prises dans ce livre, auxquelles on voudra toutefois que ce livre
s'accommode pour ainsi dire malgré qu'il en ait. Ces téméraires
chrétiens ne sont pas moins opposés à l'autorité de l'Écriture que les
infidèles déclarés, puisque nous les voyons enfin recourir, aussi bien
que les infidèles, à la raison et au sens humain comme à la première
règle et au souverain tribunal.

Il ne faut donc pas écouter ces dangereux interprètes de l'Écriture, qui
n'y veulent rien trouver qui ne contente la raison humaine, sous
prétexte que c'est Dieu qui nous l'a donnée. Il est vrai, Dieu nous l'a
donnée pour notre conduite ordinaire ; mais il a voulu que la
connaissance des mystères de la religion vînt d'une lumière plus haute,
dont nous ne serons jamais éclairés si nous ne soumettons toute autre
lumière à ses règles invariables.

Ce n'est pas que la droite raison soit jamais contraire à la foi ; mais
il n'a pas plu à Dieu que nous sussions toujours le moyen de les
accorder ensemble. Il faut avoir pénétré le fond des conseils de Dieu
pour faire parfaitement cet accord : et il dépend de l'entière
compréhension de la vérité, que Dieu nous a réservée pour la vie future.
En attendant, nous devons marcher sous la conduite de la foi, dans les
mystères divins et surnaturels ; nous y appellerons la raison seulement
pour écouter ce que Dieu dit et faire qu'elle s'y accorde, non en
contentant ses pensées, mais en les faisant céder à l'autorité de Dieu
qui nous parle.

 


IV. — Contradictions des prétendus réformés et de l'Anonyme en particulier. Avantages qu'il donne aux sociniens.

 

Messieurs delà religion prétendue réformée demanderont peut-être

 

205

 

en ce lieu d'où vient le soin que je prends d'éclaircir une vérité ,
dont ils sont d'accord avec nous. En effet aucune raison ne les a pu
empêcher de confesser la Trinité, l'Incarnation et le Péché originel, et
tant d'autres articles de la religion, qui choquent si fort le sens
humain : et pour venir à celui que nous traitons, il est vrai qu'après
avoir exposé dans leur Confession de foi « que Jésus-Christ nous y
nourrit de la propre substance de son corps et de son sang, ils ajoutent
que ce mystère surpasse en sa hautesse la mesure de notre sens, et tout
ordre de nature ; » et enfin a qu'étant céleste il ne peut être
appréhendé c'est-à-dire conçu que par la foi (1)… »

Il (l'Anonyme) avait dit auparavant, a qu'il ne s'agit pas ici de savoir
si Jésus-Christ est véritable, ou s'il est puissant pour faire ce qu'il
dit ; ce serait la dernière impiété que de balancer un moment sur l'un
et sur l'autre ; il s'agit uniquement du sens de ce qu'il dit (2) » Et
encore, dans un autre endroit : « Il ne s'agit nullement de ce que Dieu
peut, car Dieu peut tout ce qu'il veut, mais du sens de ses paroles
seulement : il faut s'attacher à sa volonté, qui est la seule règle de
notre créance, aussi bien que celle de nos actions. S'il est vrai qu'il
s'agisse du sens de ses paroles seulement (3) ; » si c'est là
_uniquement_ ce que nous avons à considérer, nous n'avons plus à nous
mettre en peine de rechercher par des principes de philosophie, si Dieu
peut faire qu'un corps soit en divers lieux, ou qu'il y soit sans son
étendue naturelle, ou que ce qui paraît pain à nos sens soit en effet le
corps de Notre-Seigneur. Car si on nous peut forcer d'entrer dans ces
discussions, si l'intelligence des paroles de Notre-Seigneur dépend
nécessairement de la résolution de semblables difficultés, nous sortons
de l'état où l'auteur nous avait mis, et le sens des paroles de
Notre-Seigneur n'est plus seulement et uniquement ce que nous avons à
considérer.

Mais qu'il est difficile à l'esprit humain de se captiver entièrement
sous l'obéissance de la foi. Ceux qui disent que ce mystère passe en sa
hauteur toute la mesure du sens humain, veulent néanmoins nous
assujettir à résoudre les difficultés que

 

1 Art XXXVI. — 2 Anon., p. 259. — 3 _Ibid_. p. 254.

 

206

 

le sens humain nous propose. Notre auteur, qui donne pour règle que nous
avons à considérer seulement et uniquement le sens des paroles de
Jésus-Christ, abandonne dans l'application ce qu'il a posé en général,
et rend une règle si nécessaire absolument inutile.

Une si étrange contradiction se peut remarquer en moins de deux pages.
Il approuve ce que j'avais dit que pour entendre les paroles de
Notre-Seigneur, nous n'avions à considérer que son intention. « C'est,
dit-il, un bon principe, pourvu qu'il soit bien prouvé ; car Jésus-Christ
peut tout ce qu'il veut, et tout ce qui veut se fait comme il veut (1).
» Il semble, selon ces paroles, que nous sommes tout à fait délivrés des
raisonnements humains sur la possibilité du mystère dont il s'agit. Mais
il ne faut que tourner la page ; nous nous trouverons rengagés plus que
jamais dans ces dangereuses subtilités. « Il ne s'agit pas, dit-il, si
Dieu peut la chose, mais si la chose est possible en elle-même, ou si
elle n'implique pas contradiction (2). » Si après nous être appliqués à
connaître la volonté de Dieu par sa parole sur l'accomplissement de
quelque mystère, par exemple sur celui du Verbe incarné, il nous faut
encore essuyer une discussion de métaphysique sur la possibilité de la
chose en elle-même, c'est justement ce que demandent les sociniens. Et
certes il ne suffit pas de se plaindre, comme fait l'auteur, que l'on
compare ceux de son parti à ces hérétiques. Il ferait bien mieux de
considérer s'il ne favorise pas, sans y penser, leurs erreurs, et s'il
ne les aide pas à introduire la raison humaine dans les questions de la
foi. En effet que prétend l'auteur, lorsqu'il veut que dans les mystères
de la religion on vienne à examiner _si la chose est possible en
elle-même, ou si elle n'implique pas contradiction?_ Faudra-t-il que le
chrétien, après qu'il a recherché dans les Écritures ce qui nous y est
enseigné sur la personne de Notre-Seigneur, s'il trouve que cette
Écriture nous fait entendre qu'il est Dieu et homme, tienne toutefois ce
sens en suspens jusqu'à ce qu'en examinant si la chose est possible en
elle-même, il ait trouvé le moyen de contenter sa raison humaine? C'est
donner gain de cause aux sociniens, et renverser manifestement

 

1 Anon., p. 179. — 2 _Ibid._ p. 180.

 

207

 

l'autorité de l'Écriture. Il faut donc savoir établir la foi par des
principes plus fermes, et apprendre au chrétien qu'il trouve tout
ensemble par un seul et même moyen et la possibilité et l'effet, quand
il montre dans l'Écriture ce que Dieu veut et ce qu'il dit. Ainsi le
sens de cette Écriture doit être fixé immuablement, sans avoir égard aux
raisons que l'esprit humain peut imaginer sur la possibilité de la
chose. On pourra entrer après, si l'on veut, dans «cette discussion ; et
une telle discussion sera regardée peut-être comme un honnête exercice
de l'esprit humain. Mais cependant la foi des mystères et l'intelligence
de l'Écriture sera établie indépendamment de cette recherche.

Ce principe fait voir clairement que tout ce que l'esprit humain peut
imaginer sur l'impossibilité du mystère de la Trinité, ou sur celui de
l'Incarnation, ou sur la présence réelle, ne doit pas même être écouté,
quand il s'agit d'établir la foi : si nous sommes solidement chrétiens,
tout cela n'aura aucun poids pour nous porter à un sens plutôt qu'à un
autre, ni au figuré plutôt qu'au littéral. Et il faut uniquement
considérer à quoi nous portera l'Écriture même.

Cependant quoique notre auteur convienne avec nous de ce principe, et
que lui-même nous donne pour règle que nous avons à considérer
_seulement et uniquement_ le sens des paroles de Jésus-Christ, il ne
craint pas toutefois d'embarrasser son esprit de cette discussion, si la
chose est possible en elle-même ; et ensuite il fait valoir contre nous
tous les arguments de philosophie qu'on oppose à notre croyance. Tant il
est vrai que le sens humain nous entraîne insensiblement à ses pensées,
et affaiblit dans l'application les principes dont la vérité nous avait
touchés d'abord.

En effet l'auteur s'était proposé de nous expliquer les raisons qui le
déterminent au sens figuré, et il les voulait trouver dans l'Écriture, «
Qu'y a-t-il de plus naturel et de plus raisonnable, dit-il, que
d'entendre l'Écriture sainte par elle-même (1)? » Il rapporte après,
entre autres passages, ceux qui disent que Jésus-Christ est monté aux
cieux ; et enfin il conclut ainsi : « Il est

 

1 Anon., p. 175.

 

208

 

donc naturel de prendre ces paroles : _Ceci est mon corps_ dans un sens
mystique et figuré, qui s'accommode seul parfaitement avec tous les
autres passages de l'Écriture (1).» Mais il n'a pas voulu remarquer que
ces passages ne concluraient rien contre nous, « n'y avait mêlé, pour
les soutenir, cette raison purement humaine d’être au ciel
corporellement et sur la terre par représentation ne sont pas, dit-il,
deux sens opposés : mais n'être plus avec nous, ou être corporellement
dans le ciel, et ne laisser pas d'être à toute heure entre les mains des
hommes, sont deux terni contradictoires et incompatibles (2). » On voit
que pour tirer quelque chose des passages de l'Écriture, qui disent que
Jésus-Christ est au ciel, il est obligé de supposer qu'il n'est pas
possible à Dieu de faire qu'un même corps soit en même temps en divers
lieux. C'est ce que ni lui ni les siens n'ont pas même prétendu prouver
par aucun passage de l'Écriture : c'est donc une opposition qui naît
purement de l'esprit humain, à qui ils nous avaient promis d'imposer
silence.

Tel est le procédé ordinaire des prétendus réformés. Ils nom promettent
toujours d'expliquer l'Écriture par l'Écriture, et d'exclure par cette
méthode le sens littéral que nous embrassons : mais on voit dans
l'exécution que le raisonnement humain prévaut toujours dans leur esprit
: et on peut voir aisément que l'attachement invincible qu'ils y ont les
porte insensiblement au sens figuré.

En effet nous voyons sans cesse revenir ces raisons humaines. L'auteur
avait exposé les raisons tirées a de la nature des sacrements et du
style de l'Écriture. Ces raisons suffisent,» dit-il (3). Et ce sont
certainement les seules qu'il faut apporter, parce que ce sont les
seules qui semblent tirées des principes du christianisme. Mais quoique
nos adversaires disent que ces preuves suffisent, il faut bien qu'ils ne
se fient pas tout à fait à de telles preuves qu'il nous est aisé de
détruire, puisqu'ils y joignent aussitôt, pour les soutenir, des
arguments de philosophie. « On pourrait ajouter ici, dit notre auteur,
plusieurs autres raisons du fond, pour montrer que le dogme de la
présence réelle n'est pas seulement au-dessus

 

1 Anon., p. 176. — 2 _Ibid._ — 3 _Ibid_. 178.

 

209

 

delà raison, comme les mystères delà Trinité et de l'Incarnation, mais
directement contre la raison (1). » Il est vrai qu'il n'étend pas ces
raisonnements, pour « ne pas entrer trop avant dans la question, » comme
il dit lui-même. Il montre toutefois l'état qu'il en fait, lorsqu'il les
appelle les _raisons du fond_. Mais voyons à quoi elles tendent. Est-ce
que toutes les fois que quelqu'un objectera qu'un point de la foi n'est
pas seulement au-dessus de la raison, mais directement contre la raison,
il faudra entrer avec lui dans cet examen? Si cela est, les Sociniens
ont gagné leur cause ; nous ne pouvons plus empêcher que ces dangereux
hérétiques ne réduisent les questions de la foi à des subtilités de
philosophie, et qu’ils n'en fassent dépendre l'explication de
l'Écriture. Car ils prétendent que la Trinité et l'Incarnation ne sont
pas seulement au-dessus de la raison, mais directement contre la raison.
Ils ont tort, direz-vous, de le prétendre. Ils ont tort, je l'avoue ;
mais il faut connaître tout le tort qu'ils ont. Car ils ont tort même de
prétendre que de tels raisonnements puissent être admis ou seulement
écoutés, lorsqu'il s'agit de la foi et de l'intelligence de l'Écriture.

Quoi que les hérétiques puissent jamais dire et de quelques raisons
qu'ils se vantent, le fidèle n'aura jamais autre chose à tore, selon vos
propres principes, qu'à considérer seulement et uniquement le sens de ce
que Dieu dit. Donc les raisonnements humains ne seront pas même écoutés
; et vous faites triompher les sociniens, si vous les introduisez par
quelque endroit dans les questions de la foi.

Vous le faites néanmoins. Vous appelez ces raisons les _raisons du
fond,_ tant elles vous paraissent considérables : mais elles sont du
fond de la philosophie, et non du fond du christianisme ; du fond du sens
humain, et non du fond de la religion. S'il faut écouter de telles
raisons dans la matière de l'Eucharistie, on ne peut plus les bannir
d'aucun autre endroit de la religion, et nous verrons régner partout la
raison humaine.

 

1 Anon., p. 178.

 

210

 


V. — Conséquences de ce discours : le premier principe qu'il faut poser  pour entendre l'Écriture sainte, c'est qu'il n'y a rien qu'il ne faille croire quand Dieu a parlé.

 

Il résulte de ce discours que le premier principe qu'il faut poser pour
entendre l'Écriture, c'est qu'il n'y a rien qu'il ne faille croire quand
Dieu a parlé : de sorte qu'il ne faut pas mesurer à nos conceptions le
sens de ses paroles, non plus que ses conseils à nos pensées, ni les
effets de son pouvoir à nos expériences. Ainsi nous lirons l'institution
de l'Eucharistie avec cette préparation. Que si l'ordre des conseils de
Dieu et les desseins de son amour envers les hommes demandent que le
Fils nous donne son propre corps, sans y changer autre chose que la
manière ordinaire connue de nos sens, nous écouterons uniquement ce que
Dieu dit ; et loin de forcer les paroles de l'Écriture sainte pour
l'accommoder à notre raison et au peu que nous connaissons de la nature,
nous croirons plutôt que le Fils de Dieu forcera par sa puissance
infinie toutes les lois de la nature, pour vérifier ses paroles dans
leur intelligence la plus naturelle   

 


VI. — Application de ce principe au mystère de l'Eucharistie.

 

Et pour entrer dans nos sentiments sur le mystère de l'Eucharistie, il
ne faut que demeurer ferme dans les maximes que nous avons déjà posées :
c'est que nous n'avons point à nous mettre en peine de la possibilité de
la chose, ni de toutes les difficultés qui embarrassent la raison
humaine, et que nous n'avons à considérer que la volonté de
Jésus-Christ.

Nous devons supposer, selon ce principe, « qu'il ne lui a pas été plus
difficile, comme il a été dit dans l'_Exposition,_ de faire que son
corps fût présent dans l'Eucharistie, en disant : Ceci est mon corps,
que de faire qu'une femme soit délivrée de sa maladie, en disant :
_Femme, tu es délivrée de ta maladie_ (1) ; ou de faire que la vie soit
conservée à un jeune homme, en disant à son père : _Ton fils est vivant_
(2) ; ou de faire que les péchés du paralytique lui soient remis, en lui
disant : _Tes péchés te sont remis_ (3) »

Il faut donc déjà qu'on nous avoue que si le Fils de Dieu a

 

1 Luc, XIII, 12. — 2 _Joan_., IV, 50. — 3 _Marc_., II, 5 ; _Expos.,_
art. 10.

 

211

 

Voulu que son corps fût présent dans l'Eucharistie, il l'a pu faire, en
disant ces paroles : « Ceci est mon corps. » L'auteur de la Réponse ne
me conteste cette vérité en aucun endroit de son livre ; il demande
seulement qu'on lui fasse voir l'intention de Notre-Seigneur (1). Il est
juste de le satisfaire ; et la chose ne sera pas malaisée, si on reprend
ce que j'ai dit dans l'_Exposition_.

 


VII — Intention de Jésus-Christ dans l'institution de l'Eucharistie. La loi des sacrifices.

 

J'ai demandé seulement qu'on nous accordât que lorsque le Fils de Dieu a
dit ces paroles : « Prenez, mangez ; ceci est mon corps donné pour vous,
» il a eu dessein d'accomplir ce qui nous était figuré dans les anciens
sacrifices, où les Juifs mangeaient la victime en témoignage qu'ils
participoient à l'oblation, et que c'était pour eux qu'elle était
offerte.

Je ne répéterai pas ce que je pense avoir expliqué très-nettement dans
l'_Exposition_ ; mais je dirai seulement que c'est une vérité qui n'est
pas contestée, que les Juifs mangeaient les victimes dans le dessein de
participer au sacrifice, selon ce que dit saint Paul : « Considérez ceux
qui sont Israélites selon la chair : celui qui mange les victimes
n'est-il pas participant de l'autel (2)?» Toute la question est donc de
savoir s'il est vrai que Notre-Seigneur ait eu dessein d'accomplir dans
l'Eucharistie cette figure ancienne, et comment il l'a accomplie. Sur
cela notre auteur nous répond deux choses : il nie en premier lieu que
Notre-Seigneur ait eu dessein d'accomplir cette figure, quand il a dit :
« Ceci est mon corps ; » il dit secondement qu'en tout cas die
s'accomplit par une manducation spirituelle.

La première de ces réponses est insoutenable ; et il ne faut qu'écouter
les raisonnements de l'auteur, pour en découvrir la faiblesse. Il me
reproche « qu'au lieu de raisons, je donne des comparaisons ou des
rapports et des convenances : comme si l'on ne savait pas, poursuit-il,
que les comparaisons et les exemples peuvent bien éclaircir les choses
prouvées, mais qu'elles ne prouvent pas (3). »

 

1 Anon., p. 179. — 2 I _Cor_., X, 18. — 3 Anon., p. 181.

 

212

 

Je ne sais pourquoi il n'a pas compris qu'en parlant des sacrifices
anciens, je ne lui apporte pas de simples comparaisons, mais des figures
mystérieuses de la Loi, dont Jésus-Christ qui en est la fin nous devait
l'accomplissement. Il ne peut désavouer que Notre-Seigneur ne soit
figuré par ces anciennes victimes, et ne dût être immolé comme elles.
Mais il croit dire quelque chose de considérable, quand il ajoute a
qu'il ne faut pas presser ces sortes de rapports au delà de ce qui est
marqué dans les Écritures, pour en faire des dogmes de foi (1). » Je
conviens de ce principe ; et j'avoue qu'il n'est pas permis d'établir la
foi sur des convenances imaginaires, qui ne seraient pas appuyées sur
les Écritures. Mais ne veut-il pas ouvrir les yeux pour voir que ce
n'est pas moi qui ai fait le rapport dont il s'agit? Il est clairement
dans la chose même, il est dans les paroles de Notre-Seigneur :
« Prenez, mangez, ceci est mon corps donné pour vous ; » et il n'est pas
moins clair que nous devons manger notre victime, qu'il n'est vrai
qu'elle a été immolée. C'est pour cela que Notre-Seigneur a prononcé ces
paroles : « Prenez, mangez ; ceci est mon corps donné pour vous.» Il
ordonne lui-même que nous le mangions comme ayant été immolé et donné
pour nous : et on est réduit à une étrange extrémité, quand il faut,
pour se soutenir, nier une vérité si constante…

 


VIII. — Abus que l'Anonyme fait de cette parole de Jésus-Christ mourant : Tout est consommé.

 

Mais certainement il n'est pas juste de faire dire tout ce qu'on veut à
l'Écriture ; et il est bon de remarquer à l'occasion d'un passage dont
les prétendus réformés abusent si visiblement, la manière peu sérieuse
avec laquelle ils appliquent l'Écriture sainte dans les matières de foi.

Je demande à l'Anonyme quel usage il prétend faire de cette parole de
Jésus-Christ mourant. Veut-il dire qu'à cause que le Fils de Dieu a dit
à la croix : « Tout est consommé, » tout ce qui se fait hors de la croix
ne sert de rien à l'accomplissement de ses mystères ; de sorte que c'est
en vain que nous recherchons à la

 

1 P. 182.

 

213

 

sainte table quelque partie de cet accomplissement? Il n'y a personne
qui ne voie combien cette prétention serait ridicule. Est-ce donc qu'il
n'y a plus aucune partie du mystère de Jésus-Christ, qui doive
s'accomplir après sa mort? Quoi! ce qui avait été prédit de sa
résurrection ne de voit-il pas avoir sa fin, comme ce qui avait été
prédit de sa croix ? Notre Pontife ne devait-il pas entrer au ciel après
son sacrifice, comme le pontife de la Loi entrait dans le sanctuaire
après le sien? Et l'accomplissement de cette excellente figure, que
saint Paul nous a si bien expliquée, ne regardait-il pas la perfection
du sacrifice de Jésus-Christ?

Il se faut donc bien garder d'entendre que toutes les prédictions ,
toutes les figures anciennes, en un mot tous les mystères de
Jésus-Christ soient accomplis précisément par sa mort. Aussi les paroles
de Notre-Seigneur ont-elles un autre objet ; et lorsqu'un moment avant
que de rendre l'âme il a dit : « Tout est consommé, » c'est de même que
s'il eût dit : Tout ce que j'avais à faire en cette vie mortelle est
accompli, et il est temps que je meure.

Il n'y a qu'à lire le saint Évangile pour y découvrir ce sens. « Jésus
sachant, dit l'Évangile, que toutes choses étaient accomplies, afin que
l'Écriture fût accomplie, dit : J'ai soif (1). » Il vit qu'il fallait
encore accomplir cette prédiction du Psalmiste : « Ils m'ont présenté du
fiel pour ma nourriture, et ils m'ont donné du vinaigre à boire dans ma
soif (2). » Après donc qu'on lui eut présenté ce breuvage amer, qui
devait être le dernier supplice de sa passion, et qu'il en eut goûté
pour accomplir la prophétie, saint Jean remarque qu'il dit : « Tout est
consommé, et qu'ayant baissé la tête, il rendit l'esprit (3). »
C'est-à-dire manifestement qu'il avait mis fin à tout ce qu'il devait
accomplir dans le cours de sa vie mortelle, et qu'il n'y avait plus rien
désormais qui dût l'empêcher de rendre à Dieu son âme sainte ; ce qu'il
fit en effet au même moment, comme saint Jean le rapporte : « Il dit :
Tout est consommé ; et ayant baissé la tête, il rendit l'esprit. »

On voit donc que cette parole ne doit pas être restreinte en particulier
aux figures qui représentent son sacrifice ; mais qu'elle

 

1 _Joan_., XIX, 28. — 2 Psal. LXVIII, 22. — 3 _Joan_., XIX, 30.

 

214

 

s'étend aux autres choses qui regardent sa personne ; et que l'intention
de Notre-Seigneur n'est pas de nous dire qu'il accomplit tout par sa
mort, mais plutôt de nous faire entendre que tout qu'il avait à faire en
ce monde étant accompli, il était temps qu’il mourût.

On voit par là un fils très-obéissant et très-fidèle à son Père qui
ayant considéré avec attention tout ce qu'il lui a prescrit pour cette
vie dans les Écritures, l'accomplit de point en point, et ne veut pas
survivre un moment à l'entière exécution de ses volontés (a).

Que si toutefois on veut nécessairement que cette parole « Tout est
consommé, » regarde l'accomplissement des sacrifices anciens, nous
n'empêcherons pas qu'on ne dise que Jésus-Christ y mis fin par sa mort,
et qu'il sera désormais la seule victime agréable à Dieu : mais qu'on ne
pense pas pour cela se servir de ce qu'il a accompli à la croix, pour
détruire ce qu'il accomplit à la sainte table. Là il a voulu être
immolé, ici il lui a plu d'être reçu d'une manière merveilleuse ; là il
accomplit l'immolation des victimes anciennes, ici il en accomplit la
manducation.

Aussi faut-il à la fin reconnaître cette vérité. Nos adversaires ne
peuvent nier qu'il ne faille manger notre victime ; et ils croient avoir
satisfait à cette obligation, en disant qu'ils la mangent par la foi.
C'est leur seconde réponse où ils sont, s'il se peut, encore plus mal
fondés que dans la première. Mais écoutons sur quoi il s'appuient :
« Bien loin, dit l'auteur de la Réponse, qu'il faille entendre
littéralement tous les rapports » qui sont avec Jésus Christ et les
victimes anciennes, «nous savons que l'Apôtre oppose partout l'esprit de
l'Évangile à la lettre de Moïse ; » d'où il conclut « qu'il faut que sous
l'Évangile les chrétiens prennent tout spirituellement, » et ensuite,
qu'ils se contentent d'une manducation spirituelle _et par la foi_ (1).

Mais que ne poussent-ils leur principe dans toute la suite ; pourquoi ne
disent-ils pas que Jésus-Christ devait être immolé,

 

1 Anon., p. 183.

(a) _Note marg_. : Faire voir la vérité constante des preuves par
l'absurdité d réponses plutôt que de suivre les preuves dans toute leur
étendue.

 

215

 

non par une mort effective, mais par une mort spirituelle et mystique
(1)? C'est sans doute que Notre-Seigneur nous a fait voir en mourant
aussi réellement qu'il a fait, qu'en tournant tout au mystique et au
spirituel on anéantit enfin ses conseils.

Pourquoi nos adversaires ne veulent-Us pas que sans préjudice du sens
spirituel, qui accompagne partout les mystères de l'Évangile , il ait pu
rendre la manducation de son corps aussi effective que sa mort (2) ? Car
il faut apprendre à distinguer l'essence des choses d'avec la manière
dont elles sont accomplies. Jésus-Christ est mort aussi effectivement
que les animaux qui ont été immolés en figure de son sacrifice : mais il
n'a point été traîné par farce à l'autel ; c'est une victime obéissante
qui va de son bon gré à la mort ; il a rendu l'esprit volontairement, et
sa mort est autant m effet de puissance que de faiblesse : ce qui ne
peut convenir à aucune autre victime. Ainsi il nous donne à manger la
chair de ce sacrifice d'une manière divine et surnaturelle, et
infiniment différente de celle dont on mangeait les victimes anciennes :
mais, comme il a été dit dans l'_Exposition,_ en relevant la manière et
lui ôtant tout ce qu'elle a d'indigne d'un Dieu, il ne nous a rien été
pour cela de la réalité ni de la substance.

Ainsi quand il a dit ces paroles : « Prenez, mangez ; ceci est mon corps,
» ce qu'il nous ordonne de prendre, ce qu'il nous présente pour le
manger, c'est son propre corps ; et son dessein a été de nous le donner
non en figure, ni en vertu seulement, mais réellement et en substance.
C'est l'intention de ses paroles, et la mite de ses conseils nous oblige
à les entendre à la lettre. N'importe que le sens humain s'oppose à
cette doctrine : car il faut, malgré ses oppositions, que l'ordre des
desseins de Dieu demeure ferme. C'est cet ordre des conseils divins que
Jésus-Christ veut Bons faire voir en instituant l'Eucharistie ; et que de
même qu'il a choisi la croix pour y accomplir en lui-même l'immolation
des victimes anciennes, il a aussi établi la sainte table pour en
accomplir la manducation : si bien que malgré tous les raisonnements
humains la manducation de notre victime doit être aussi réelle à la
sainte table, que son immolation a été réelle à la croix.

 

1 Anon., p. 184.— 2 _Ibid._ p. 186.

 

216

 

C'est ce qui oblige les catholiques à rejeter le sens figuré pour
tourner tout au réel et à l'effectif. Et c'est aussi ce qui force les
prétendus réformés à chercher ce réel autant qu'ils peuvent. Car c'est
ici qu'on m'objecte « que je me méprends perpétuellement sur ce réel. La
manducation, dit l'Anonyme, ou la participation du corps de Jésus-Christ
est très-réelle (1). » On a vu plus amplement en un autre lieu combien
fortement il s'explique sur cette réalité, et comme il se fâche contre
moi quand je dis que notre doctrine mène au réel plus que la sienne :
nous en parlerons encore ailleurs ; mais il faut en attendant, qu'il nous
avoue, que si nous avons réellement dans l'Eucharistie le corps de
Notre-Seigneur, son objet a été réellement dans ce mystère de nous le
donner : et ensuite que quand il a dit : « Ceci est mon corps, » il faut
entendre : Ceci est mon corps réellement et non en figure, ni en vertu,
mais en vérité et en substance…

 

 

 

[Précédente]

[Accueil]

[Suivante]

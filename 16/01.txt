[REMARQUES]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Avertissements VI-I
Avertissements VI-II
Avertissements VI-III
Lettres Burnet
Hérésies
Communion I
Communion II
Trad. Comm. I
Trad. Comm. II

 

 



REMARQUES HISTORIQUES.


 

Nous avons parlé, dans le précédent volume, du _Sixième et dernier
avertissement_ aux protestants : il nous reste à parler ici des deux
ouvrages qui ont pour sujet la communion.

 


I.

 

La communion sous une ou sous deux espèces ne présente aucune
difficulté. Saint Paul dit : « Jésus-Christ ressuscité des morts ne
meurt plus (1) : » donc le sang de Jésus-Christ ne peut plus être séparé
de son corps : donc en recevant son divin corps, on reçoit a la fois son
sang adorable. Le même Apôtre suppose manifestement cette doctrine,
lorsqu'il dit encore : « Celui qui mange ce pain ou boit le calice du
Seigneur indignement, est coupable du corps et du sang du Seigneur (2).
» Dans les premiers siècles du christianisme, l'Église donnait souvent
aux fidèles la communion sous une seule espèce ; et plus tard, dans la
crainte de l'effusion, elle refusa formellement aux laïques l'usage du
vin consacré.

En attaquant l'Église Luther n'attaqua pas sa discipline dans
l'administration de la sainte Cène ; la communion sous une ou sous deux
espèces lui paraissait de peu d'importance ; il reléguait cette
controverse parmi _les choses de néant,_ et chargeait d'invectives
Carlostadt _qui mettait la réformation dans ces bagatelles_. Son
indifférence sur ce point,

 

1 Rom., VI, 9 — 2 I _Cor_., XI, 37.

 

II

 

tout ensemble et son entêtement contre l'Église, se révèle dans la
singulière déclaration que voici : « Si un concile par hasard ordonnait
ou permettait de sa propre autorité les deux espèces, nous ne voudrions
pas les prendre ; mais, en dépit du concile et de son ordonnance, nous
n'en prendrions qu'une, ou nous ne prendrions ni l'une ni l'autre, et
nous maudirions ceux qui prendraient les deux par l'autorité d'un tel

concile et d'un tel décret. »

Autre réformateur, autre inspiration du Saint-Esprit. On avait vu la
controverse sur la communion agiter violemment, à la voix de Jean Huss,
toute la Bohème ; Calvin, aussi rusé que peu délicat dans le choix des
moyens, s'empara de cette misérable chicane comme d'un levier puissant
pour soulever les masses ; il réclama l'usage de la coupe pour les
laïques ; il dit aux défenseurs de la foi de ses pères : En refusant aux
fidèles une espèce eucharistique, vous leur refusez une partie
essentielle du sacrement ; en séparant le pain et le vin dans la
communion, vous séparez de nouveau le corps et le sang de Jésus-Christ ;
vous commettez un attentat sacrilège.

Dans le dix-septième siècle, la controverse religieuse porta la lumière
sur une foule de questions ; les explications des théologiens, les
instructions des évêques , surtout les écrits de Bossuet éclairèrent les
peuples séduits par des imputations calomnieuses ; les prédicats les
plus emportés, craignant la censure de la conscience publique,
retranchaient souvent de leur évangile les mots de _superstition
catholique, d'idolâtrie romaine, de Pape antéchrist_. Vaincus sur ce
terrain, en rendant les armes d'une main, les ministres saisirent de
l'autre la machine du réformateur de Genève ; ils remirent en avant la
communion sous une seule espèce, criant plus haut que jamais à la
mutilation du plus saint des divins mystères. Ces déclamations, qui
montraient pour ainsi dire un objet sensible aux yeux de la multitude ,
frappèrent vivement les esprits grossiers dans la Réforme ; les simples
protestants reprochaient aux défenseurs de l'Église, dans les
discussions religieuses, le retranchement de la coupe (1), et ceux-là
mêmes qui reconnaissaient la vérité catholique ne s'y soumettaient pas
sans répugnance. Au milieu de ces plaintes et de ces clameurs, Jurieu
renouvela dans un écrit sur l'Eucharistie, toutes les attaques de ses
devanciers contre la communion sous une espèce. Alors Bossuet voulut
arracher à la Réforme son arme la plus dangereuse ; il publia le _Traité
de la Communion sous les deux espèces_. C'est ici le commencement de ce
long combat qui mit si souvent aux prises le plus zélé des évêques et
l'hérétique le plus fougueux, la raison éclairée par la vérité divine et
la passion surexcitée par l'erreur religieuse, le génie catholique et le
fanatisme protestant.

 

1 Mémoire de l'abbé Ledieu, 1682.

 

III

 

Dans son traité sur la communion, Bossuet parle du fait constant et de
ses causes certaines, de la pratique de l'Église et des principes qui
lui servent de fondement : d'où deux parties. Dans la première, il
prouve que l'Église donnait, dès les premiers siècles, l'Eucharistie
sous une ou sous deux espèces, sans qu'on ait jamais songé que la
soustraction de l'une ou de l'autre nuisit à l'intégrité du sacrement.
Quatre coutumes incontestables établissent cette vérité : la communion
des malades , la communion des enfants, la communion domestique, la
communion publique et solennelle. Voilà le fait constant. Dans la
deuxième partie, venant aux principes, l'auteur montre qu'il n'y a
d'indispensable dans les sacrements que ce qui tient à leur substance ;
qu'ainsi l'on n'est pas obligé de faire en les administrant tout ce qu'a
fait le Sauveur en les instituant. Les protestants reconnaissent
eux-mêmes ces principes : c'est pour cela qu'ils ne plongent pas dans
l'eau ceux qu'ils baptisent, quoique Jésus-Christ ait dit : _Baptisez,_
c'est-à-dire _plongez,_ et qu'il ait reçu le baptême par immersion ;
c'est pour cette raison qu'ils ne donnent la Cène ni à table ni dans un
souper, encore bien que Jésus-Christ l'ait donnée de cette manière.
Pourquoi donc veulent-ils que cette parole : _Buvez-en tous,_ commande
rigoureusement l'usage du calice? On essaierait en vain de répondre à
ces arguments. Les protestants de bonne foi rendirent hommage à la
science, au talent, à la modération du théologien catholique : Le
_Traité de la Communion,_ dit Bayle, « m'a paru fort délicat, fort
spirituel, et d'une honnêteté envers nous qui ne peut être assez louée ;
serré, judicieux, il est déchargé de tout ce qui ne fait pas à la
question. »

Jurieu avait donné son écrit contre la communion sous une espèce, en
1681 : Bossuet publia le _Traité de la Communion sous les deux espèces_
en 1682, chez Marbre-Cramoisy, dans un petit volume in-12. La seconde
édition, revue par l'auteur, parut en 1686, chez le même éditeur et sous
le même format.

 


II.

 

Les ministres protestants ne voyaient pas sans alarme s'évanouir la
dernière imputation calomnieuse qui éloignait leurs dupes de l'Église
catholique. Deux répondirent au _Traité de la Communion_ : l'un, pasteur
à Rouen, nommé Laroque, qui avait déjà produit un ouvrage sur
l'Eucharistie ; l'autre, aident zélateur, folliculaire inconnu, qui est
resté caché sous le voile de l'anonyme.

Les deux tenants du protestantisme outragent, chacun à sa façon, le
défenseur du catholicisme. « Le premier, dit Bossuet, me traite avec
beaucoup plus de civilité en apparence, et l'autre affecte au contraire
je ne sais quoi de chagrin et de rigoureux. Mais il n'importe pour le
fond ; car enfin avec des tons différents, ni l'un ni l'autre ne
m'épargnent. »

 

IV

 

Quant à la doctrine , ils prouvent fort bien ce que tous les catholiques
reconnaissent d'une voix unanime, que l'on communiait ordinairement dans
l'ancienne Église sous les deux espèces ; mais qu'on ne communiât pas
souvent sous une seule, qu'on crût nécessaire de les recevoir les deux
pour recevoir tout le sacrement : voilà le fond de la controverse, et
voilà ce que ne prouvent pas les ministres. Ils cherchent partout à
fourvoyer le lecteur hors de la question.

Bossuet reprit la plume, et composa la _Tradition défendue sur la
matière de la communion sous une seule espèce_. Il divisa cet ouvrage en
trois pariies. Dana la première , il montre que les réformés, se
trouvant dans l'impossibilité de déterminer par l'Évangile ce qui est
essentiel à la communion, ne peuvent démêler cette matière que par
l'autorité de l'Église et de la tradition ; puis il fait voir dans la
deuxième que la tradition de tous les siècles, dès l'origine du
christianisme, établit la liberté d'user indifféremment d'une seule
espèce ou des deux ensemble. En même temps qu'il fournit celte double
démonstration, le redoutable polémiste poursuit ses adversaires dans
tous leurs détours, il déjoue tous leurs stratagèmes et les force dans
leur dernier retranchement.

Mais si la communion sous une ou sous deux espèces est indifférente, de
quel droit l'Église ôte-t-elle aux fidèles une liberté qui leur est
acquise par l'Évangile? Pour enlever aux protestants la possibilité de
cette réponse, Bossuet voulait montrer que l'Église a le pouvoir de
déterminer les choses indifférentes, et que sa discipline dans
l'administration de l'Eucharistie repose clairement sur la parole de
Dieu. Il annonça donc le dessein d'ajouter à son ouvrage une troisième
partie ; mais il vit changer les circonstances qui en faisaient
l'opportunité ; d'ailleurs il méditait déjà l'_Histoire des Variations,_
et les gallicans lui demandaient la défense des quatre articles décrétés
dans l'assemblée générale du clergé de France en 1882 : il n'écrivit
point la dissertation sur le pouvoir et la pratique de l'Église dans
l'administration de la sainte table. Cependant son ouvrage présente un
tout complet.

Comme Bossuet n'avait pas terminé complètement la défense du _Traité sur
la Communion,_ de même il ne la donna pas au public. Son neveu, l'évêque
de Troyes, toujours occupé dans le monde de mille soins divers, n'eut
pas le temps de sortir cet ouvrage, non plus que plusieurs autres, des
rayons de sa bibliothèque : c'est l'abbé Leroy qui le publia pour la
première fois dans le troisième volume des _Œuvres posthumes,_ en 1753.

[Accueil]

[Suivante]

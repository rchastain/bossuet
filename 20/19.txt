[MYSTIQUE CITÉ DE DIEU]

[Précédente]

[Accueil]

[Remonter]
Bibliothèque

Accueil
Remonter
Relation Quiétisme
Réponses Quiétisme I
Réponses Quiétisme II
Réponses Quiétisme III
Réponses Quiétisme IV
Réponse d'un théologien
Réponse aux Préjugés
Passages Éclaircis I
Passages Éclaircis II
Passages Éclaircis III
Mandement
Réflexions
Relation et Actes
Mémoire M. Dupin
Hstoire des Conciles I
Histoire des Conciles II
Histoire des Conciles III
Histoire des Conciles IV
Mystique Cité de Dieu

 



REMARQUES SUR LE LIVRE INTITULÉ : LA MYSTIQUE CITÉ DE DIEU, ETC.


 

Traduite de l'espagnol, etc., à Marseille, etc.

 

Le seul dessein de ce livre porte sa condamnation. C'est une fille qui
entreprend un journal de la vie de la sainte Vierge, où est celle de
Notre-Seigneur, et où elle ne se propose rien moins que d'expliquer jour
par jour et moment par moment tout ce qu'ont fait et pensé le Fils et la
Mère, depuis l'instant de leur conception jusqu'à la fin de leur vie ;
ce que personne n'a jamais osé.

On trouve dans quelques révélations qui n'obligent à aucune croyance,
certaines circonstances particulières de la vie de Notre-Seigneur ou de
sa sainte Mère : mais qu'on ait été au détail et à toutes les minuties
que raconte celle-ci de dessein formé, et comme par un ordre exprès de
Dieu, c'est une chose inouïe.

Le titre est ambitieux jusqu'à être insupportable. Cette religieuse
appelle elle-même son livre, _Histoire divine,_ ce qu'elle répète sans
cesse ; par où elle veut exprimer qu'il est inspiré et révélé de Dieu
dans toutes ses pages. Aussi n'est-ce jamais elle, mais toujours Dieu et
la sainte Vierge par ordre de Dieu qui parlent ; et c'est pourquoi le
titre ajoute que cette _Histoire divine_ a été manifestée « dans ces
derniers siècles par la sainte Vierge, à la sœur Marie de Jésus (a). »
On trouve de plus dans l'espagnol, que « cette vie est manifestée dans
ces derniers siècles pour être une nouvelle lumière du monde, une joie
nouvelle à l'Église catholique, et une nouvelle consolation et sujet de
confiance au genre humain. » Il faut garder tous ces titres pour le
Nouveau

 

(a) D'Agréda.

 

621

 

Testament : l'Écriture est la seule histoire qu'on peut appeler divine.
La prétention d'une nouvelle révélation de tant de sujets inconnus doit
faire tenir le livre pour suspect et réprouvé dès l'entrée. Ce titre au
reste est conforme à l'esprit du livre.

Le détail est encore plus étrange. Tous les contes qui sont ramassés
dans les livres les plus apocryphes, sont ici proposés comme divins, et
on y en ajoute une infinité d'autres avec une affirmation et une
témérité étonnante.

Ce qu'on fait raconter à la sainte Vierge dans le chapitre XV, sur la
manière dont elle fut conçue, fait horreur et la pudeur en est offensée.
Ce chapitre est un des plus longs, et suffit seul pour faire interdire à
jamais tout le livre aux âmes pudiques. Cependant les religieuses s'y
attacheront d'autant plus, qu'elles verront une religieuse qu'on donne
pour une béate, demeurer si longtemps sur celte matière.

        Au même chapitre, après avoir dit combien de temps il faut
naturellement pour l'animation d'un corps humain, elle décide que Dieu
réduisit ce temps, qui devait être de quatre-vingt jours ou environ, à
sept jours seulement. Ce jour de la conception de la sainte Vierge,
dit-elle, _fut pour Dieu comme un jour de fête de Pâque, aussi bien que
pour toutes les créatures,_ (pag. 237, 238).

C'est, dit-on, _une chose admirable_ que ce petit corps animé, qui
n'était pas plus grand qu'une abeille (p. 241), et dont à peine _on
pouvait distinguer les traits,_ dès le premier moment _pleurât et versât
des larmes dans le sein de sa mère, pour déplorer le péché_ (p. 251).

Tous les discours de sainte Anne, de saint Joachim, de la sainte Vierge
même, de Dieu et des anges, sont rapportés dans un détail qui seul doit
faire rejeter tout l'ouvrage, n'y ayant que vues, pensées et
raisonnements humains.

Depuis le troisième chapitre jusqu'au huitième, ce n'est autre chose
qu'une scolastique raffinée, selon les principes de Scot. Dieu lui-même
en fait des leçons et se déclare scotiste, encore que la religieuse
demeure d'accord que le parti qu'elle embrasse est le moins reçu dans
L'Ecole. Mais quoi ! Dieu l'a décidé, et il l'en faut croire.

 

622

 

Elle outre ces principes scotistiques, jusqu'à faire dire à Dieu que le
décret de créer le genre humain a précédé celui de créer les anges.

Tout est extraordinaire et prodigieux dans cette prétendue histoire. On
croit ne rien dire de la sainte Vierge, ni du Fils de Dieu, si l'on n'y
trouve partout des prodiges, tel qu'est par exemple, l'enlèvement de la
sainte Vierge dans le ciel en corps et en âme, incontinent après sa
naissance, et une infinité de choses semblables, dont on n'a jamais ouï
parler, et qui n'ont aucune conformité avec l'analogie de la foi.

On ne voit rien, dans la manière dont parlent à chaque page Dieu, la
sainte Vierge et les anges, qui ressente la majesté des paroles que
l'Écriture leur attribue. Tout y est d'une fade et languissante longueur
; et néanmoins cet ouvrage se fera lire par les esprits faibles, comme
un roman d'ailleurs assez bien tissu, et assez, élégamment écrit ; et
ils en préféreront la lecture à celle de l'Évangile, parce qu'il
contente la curiosité, que l'Évangile veut au contraire amortir ; et
l'histoire de l'Évangile ne leur paraîtra qu'un très-petit abrégé de
celle-ci.

Ce qu'il y a d'étonnant, c'est le nombre d'approbations qu'a trouvées
cette pernicieuse nouveauté. On voit entre autres choses que l'ordre de
saint François, par la bouche de son général, semble l'adopter, comme
une nouvelle grâce faite au monde par le moyen de cet ordre. Plus on
fait d'efforts pour y donner cours, plus il faut s'opposer à une fable,
qui n'opère qu'une perpétuelle dérision de la religion.

On n'a encore lu que ce qui a été traduit ; mais en parcourant le reste,
on en voit assez pour conclure que ce n'est ici que la vie de
Notre-Seigneur et de sa sainte Mère changée en roman, et un artifice du
démon pour faire qu'on croie mieux connaître Jésus-Christ et sa sainte
Mère par ce livre que par l'Évangile.

 

FIN DU VINGTIÈME VOLUME.

[Précédente]

[Accueil]

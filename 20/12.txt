[MANDEMENT]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Relation Quiétisme
Réponses Quiétisme I
Réponses Quiétisme II
Réponses Quiétisme III
Réponses Quiétisme IV
Réponse d'un théologien
Réponse aux Préjugés
Passages Éclaircis I
Passages Éclaircis II
Passages Éclaircis III
Mandement
Réflexions
Relation et Actes
Mémoire M. Dupin
Hstoire des Conciles I
Histoire des Conciles II
Histoire des Conciles III
Histoire des Conciles IV
Mystique Cité de Dieu



 



MANDEMENT DE MONSEIGNEUR L'ÉVÊQUE DE MEAUX,


 

Pour la publication de la Constitution de notre saint Père le Pape
Innocent XII, du 12 de mars 1639. portant condamnation et défense du
livre intitule : _Explication_ des _Maximes_ des Saints sur la vie
intérieure, etc.

 

Jacques Bénigne par la permission divine Evêque de Meaux : aux doyens
ruraux, curés et vicaires, et à tous les fidèles de notre diocèse, salut
et bénédiction en Notre-Seigneur.

Dans l'obligation où nous sommes de condamner les fausses spiritualités,
même dans les livres où elles paraissent avec leurs plus belles
couleurs, quoique toujours sans l'autorité de l'Écriture et sans le
témoignage des Saints ; nous parlerons avec d'autant plus de confiance,
que cette condamnation est précédée d'une Constitution apostolique, où
la foi de saint Pierre et de l'Église romaine, mère et maîtresse des
Églises, s'est expliquée en ces termes :

Condamnation et défense faite par notre très-saint Père Innocent par la
Providence divine Pape XII ; du livre imprimé à Paris en 1097, sous ce
titre : _Explication_ des _Maximes_ des Saints sur la vie intérieure,
etc.

Innocent Pape XII, pour perpétuelle mémoire. Comme il est venu à la
connaissance de notre Siège apostolique, qu'un certain livre français
avait été mis au jour sous ce titre : EXPLICATION DES MAXIMES DES SAINTS
SUR LA VIE INTÉRIEURE, PAR MESSIRE FRANÇOIS DE SALIGNAC FÉNELON,
_archevêque duc de Cambray, précepteur de Messeigneurs les ducs de
Bourgogne, d'Anjou et de Berry. A Paris, chez Pierre Auboüyn, Pierre_

 

473

 

_Emery, Charles Clousier,_ 1697 ; et que le bruit extraordinaire que ce
livre avait d'abord excité en France, à l'occasion de la doctrine qu'il
contient, comme n'étant pas saine, s'était depuis tellement répandu,
qu'il était nécessaire d'appliquer notre vigilance pastorale à y
remédier ; Nous avons mis ce livre entre les mains de quelques-uns de
nos vénérables Frères les Cardinaux de la sainte Église romaine, et
d'autres Docteurs en théologie, pour être par eux examiné avec la
maturité que l'importance de la matière semblait demander. En exécution
de nos ordres, ils ont sérieusement et pendant un long temps examiné
dans plusieurs congrégations, diverses propositions extraites de ce même
livre, sur lesquelles ils nous ont rapporté de vive voix et par écrit ce
qu'ils ont jugé de chacune. Nous donc, après avoir pris les avis de ces
mêmes Cardinaux et Docteurs en théologie, dans plusieurs Congrégations
tenues à cet effet en notre présence, désirant autant qu'il nous est
donné d'en haut prévenir les périls qui pourraient menacer le troupeau
du Seigneur qui nous a été confié par ce Pasteur éternel ; de notre
propre mouvement et de notre certaine science, après une nuire
délibération et par la plénitude de l'autorité apostolique, condamnons
et réprouvons, par la teneur des présentes, le livre susdit, en quelque
lieu et en quelque autre langue qu'il ait été imprimé, de quelque
édition et de quelque version qui s'en soit faite, ou qui s'en puisse
faire dans la suite, d'autant que par la lecture et par l'usage de ce
livre, les fidèles pourraient être insensiblement induits dans des
erreurs déjà condamnées par l'Église catholique : et outre cela, comme
contenant des propositions, qui, soit dans le sens des paroles, tel
qu'il se présente d'abord, soit eu égard à la liaison des principes,
sont téméraires, scandaleuses, malsonnantes, offensent les oreilles
pieuses, sont pernicieuses dans la pratique, et même erronées
respectivement. Faisons défense à tous et un chacun des fidèles, même à
ceux qui devraient être ici nommément exprimés, de l'imprimer, le
décrire, le lire, le garder et s'en servir, sous peine
d'excommunication, que les contrevenants encourront par le fait même et
sans autre déclaration. Voulons et commandons par l'autorité
apostolique, que quiconque aura ce livre chez soi, aussitôt qu'il

 

474

 

aura connaissance des présentes lettres, le mette sans aucun délai entre
les mains des Ordinaires des lieux, ou des inquisiteurs d'hérésie :
nonobstant toutes choses à ce contraires. Voici quelles sont les
propositions contenues au livre susdit, que nous avons condamnées, comme
nous venons de marquer, par notre jugement et censure apostolique,
traduites du français en latin.

I. Il y a un état habituel d'amour de Dieu, qui est une charité pure et
sans aucun mélange du motif de l'_intérêt propre_... Ni la crainte des
châtiments, ni le désir des récompenses, n'ont plus de part à cet amour.
On n'aime plus Dieu ni pour le mérite, ni pour la perfection, ni pour le
bonheur qu'on doit trouver en l'aimant l.

II. Dans l'état de la vie contemplative ou unitive, on perd tout motif
intéressé de crainte et d'espérance (2).

III. Ce qui est essentiel dans la direction, est de ne faire que suivre
pas à pas la grâce avec une patience, une précaution et une délicatesse
infinie. Il faut se borner à laisser faire Dieu, et ne (porter) jamais
au pur amour, que quand Dieu par l'onction intérieure commence à ouvrir
le cœur à cette parole, qui est si dure aux âmes encore attachées à
elles-mêmes, et si capable ou de les scandaliser ou de les jeter dans le
trouble (3).

IV. Dans L'état de la sainte indifférence, lame n'a plus de désirs
volontaires et délibérés pour son intérêt, excepté dans les occasions où
elle ne coopère pas fidèlement à toute sa grâce (4).

V. Dans cet état de la sainte indifférence, on ne veut rien pour soi ;
mais on veut tout pour Dieu : on ne veut rien pour être parfait ni
bienheureux pour son propre intérêt ; mais on veut toute perfection et
toute béatitude, autant qu'il plait à Dieu de nous faire vouloir ces
choses par l'impression de sa grâce (5).

VI. En cet état on ne veut plus le salut, comme salut propre, comme
délivrance éternelle, comme récompense de nos mérites, comme le plus
grand de tous nos intérêts ; mais on le veut d'une volonté pleine, comme
la gloire et le bon plaisir de Dieu, comme une chose qu'il veut, et
qu'il veut que nous voulions pour lui (6).

 

1 _Explic. des Maximes,_ etc., p. 10, 11, 15, etc. — 2 _Ibid_., p. 23,
24, etc.— 3 _Ibid.,_ p. 35. — 4 _Ibid.,_ p. 59, 50. — 5 _Ibid.,_ p. 52.
— 6 _Ibid.,_ p. 52, 53.

 

475

 

VII. L'abandon n'est que l'abnégation ou renoncement de soi-même , que
Jésus-Christ nous demande dans l'Évangile, après que nous aurons tout
quitté au dehors. Cette abnégation de nous-mêmes n'est que pour
l'intérêt propre... Les épreuves extrêmes où cet abandon doit être
exercé, sont les tentations par lesquelles Dieu jaloux veut purifier
l'amour en ne lui faisant voir aucune ressource, ni aucune espérance
pour son intérêt propre, même éternel (1).

VIII. Tous les sacrifices que les âmes les plus désintéressées font
d'ordinaire sur leur béatitude éternelle sont conditionnels... Mais ce
sacrifice ne peut être absolu dans l'état ordinaire. Il n'y a que le cas
de ces dernières épreuves où ce sacrifice devient en quelque manière
absolu (2).

IX. Dans les dernières épreuves une âme peut être invinciblement
persuadée d'une persuasion réfléchie, et qui n'est pas le fond intime de
la conscience, qu'elle est justement réprouvée de Dieu (3).

X. Alors l'âme divisée d'avec elle-même, expire sur la croix avec
Jésus-Christ, en disant : _O Dieu, mon Dieu! pourquoi m'avez-vous
abandonné ?_ Dans cette impression involontaire de désespoir, elle fait
le sacrifice absolu de son _intérêt propre_ pour l'éternité (4).

XI. En cet état une âme perd toute espérance pour son propre intérêt:
mais elle ne perd jamais dans la partie supérieure, c'est-à-dire dans
ses actes directs et intimes, l'espérance parfaite, qui est le désir
désintéressé des promesses (5).

XII. Un directeur peut alors laisser faire à cette âme un acquiescement
simple à la perte de son _intérêt propre,_ et à la condamnation juste où
elle croit être de la part de Dieu (6).

XIII. La partie inférieure de Jésus-Christ sur la croix ne communiquait
pas à la supérieure son trouble involontaire (7).

XIV. Il se fait dans les dernières épreuves pour la purification de
l'amour, une séparation de la partie supérieure de l'âme d'avec
l'inférieure... Les actes de la partie inférieure dans cette séparation,

 

1 _Explicat. des Max.,_ etc., p. 72, 73. — 2 _Ibid.,_ p. 87. — 3 _Ibid._
— 4 _Ibid.,_ p. 90. — 5 _Ibid.,_ p. 90, 91. — 6 _Ibid.,_ p. 91. — 7
_Ibid_., p. 122.

 

 

476

 

sont d'un trouble entièrement aveugle et involontaire, parce que tout ce
qui est intellectuel et volontaire est de la partie supérieure (1).

XV. La méditation consiste dans des actes discursifs qui sont faciles à
distinguer les uns des autres..... Cette composition d'actes discursifs
et réfléchis est propre à l'exercice de l'amour intéressé (2).

XVI. Il y a un état de contemplation si haute et si parfaite, qu'il
devient habituel, en sorte que toutes les fois qu'une âme se met en
actuelle oraison, son oraison est contemplative et non discursive. Alors
elle n'a plus besoin de revenir à la méditation, ni à ses actes
méthodiques (3).

XVII. Les âmes contemplatives sont privées de la vue distincte, sensible
et réfléchie de Jésus-Christ en deux temps différents :... premièrement,
dans la ferveur naissante de leur contemplation :... secondement une âme
perd de vue Jésus-Christ dans les dernières épreuves (4).

XVIII. Dans l'état passif,.... on exerce toutes les vertus distinctes,
sans penser qu'elles sont vertus : on ne pense en chaque moment qu'à
faire ce que Dieu veut, et l'amour jaloux fait tout ensemble qu'on ne
veut plus être vertueux (pour soi), et qu'on ne l'est jamais tant que
quand on n'est plus attaché à l'être (5).

XIX. On peut dire en ce sens que l'âme passive et désintéressée ne veut
plus même l'amour en tant qu'il est sa perfection et son bonheur, mais
seulement en tant qu'il est ce que Dieu veut de nous

XX. Les âmes transformées,.. en se confessant doivent détester leurs
fautes, se condamner et désirer la rémission de leurs péchés , non comme
leur propre purification et délivrance, mais comme chose que Dieu veut,
et qu'il veut que nous voulions pour sa gloire (7).

XXI. Les saints mystiques ont exclus de l'état des âmes transformées les
pratiques de vertus (8).

 

1 _Explic. des Max.,_ etc., p. 121, 123.— 2 _Ibid.,_ p. 164, 165.— 3
_Ibid.,_ p. 176.—  4 _Ibid.,_ p. 191, 195. —  5 _Ibid.,_ p. 223, 225. —
6 _Ibid.,_ p. 226. — 7 _Ibid.,_ p. 241. — 8 _Ibid.,_ p. 253.

 

477

 

XXII. Quoique cette doctrine ( du pur amour ) fût la pure et simple
perfection de l'Évangile marquée dans toute la tradition, les anciens
pasteurs ne proposaient d'ordinaire au commun des justes que les
pratiques de l'amour intéressé proportionnées à leur grâce (1).

XXIII. Le pur amour fait lui seul toute la vie intérieure, et devient
alors l'unique principe et l'unique motif de tous les actes délibérés et
méritoires (2).

Au reste nous n'entendons point, par la condamnation expresse de ces
propositions, approuver aucunement les autres choses contenues au même
livre. Et afin que ces présentes lettres viennent plus aisément à la
connaissance de tous, et que personne n'en puisse prétendre cause
d'ignorance, nous voulons pareillement, et ordonnons par l'autorité
susdite, qu'elles soient publiées aux portes de la basilique du Prince
des apôtres, de la Chancellerie apostolique, et de la Cour générale au
Mont Citorio, et à la tète du Champ de Flore dans la ville, par l'un de
nos huissiers suivant la coutume, et qu'il en demeure des exemplaires
affichés aux mêmes lieux : en sorte qu'étant ainsi publiées, elles aient
envers tous et un chacun de ceux qu'elles regardent, le même effet
qu'elles auraient étant signifiées et intimées à chacun d'eux en
personne ; voulant aussi qu'on ajoute la même foi aux copies et aux
exemplaires même imprimés des présentes lettres, signés de la main d'un
notaire public et scellés du sceau d'une personne constituée en dignité
ecclésiastique, tant en jugement que dehors, et par toute la terre,
qu'on ajouterait à ces mêmes lettres représentées et produites en
original. Donné à Rome, à sainte Marie-Majeure, sous l'Anneau du
Pêcheur, le douzième jour de mars M. DC. XCIX, l'an huitième de notre
pontificat.

_ _

_Signé_ J. F. Card. Albano.

 

Et plus bas :

 

_L'an de N. S. J. C. 1699, indiction septième, le 13 de mars, et du
pontificat de notre saint Père le Pape par la Providence divine Innocent
XII, fan huitième, le Bref susdit a été affiché et publié_

 

1 _Explic. des Max.,_ etc., p. 261. — 2 _Ibid.,_ p. 272.

 

478

 

_aux portes de la Basilique du Prince des apôtres, de la grande Cour
d'Innocent, à la tête du Champ de Flore, et aux autres lieux de la ville
accoutumés, par moi François Périno, huissier de notre très-saint Père
le Pape._

 

_Signé,_ Sébastien Vasello, Maître des huissiers.

 

Une censure si claire et si solennelle a eu tout l'effet qu'on en
pouvait espérer ; le même esprit de la tradition qui a fait parler le
Chef visible de l'Église, lui a uni les membres : toutes les provinces
ecclésiastiques de ce royaume ont reçu et accepté la Constitution, avec
le respect et la soumission ordinaire : et nous avons eu la consolation
tant désirée et tant espérée, de voir Monseigneur l'archevêque de
Cambray s'y soumettre le premier, _simplement, absolument, et sans
aucune restriction_ (1) : en ajoutant même depuis, quelque pensée qu'il
ait pu avoir de son livre, qu'il renonçait à son jugement, pour se
conformer simplement à celui du souverain Pontife.

Ainsi on ne songe plus à défendre un livre avec lequel on aurait à
craindre, selon la Constitution, _d'induire_ les pieux lecteurs _à des
erreurs déjà condamnées par l'Église catholique,_ et on renonce en
termes exprès _à toute pensée de l'expliquer,_ après que le saint Siège
en a condamné les propositions en toutes manières, _soit dans leur sens
naturel, soit dons la liaison de leurs principes_.

Les ennemis de l'Église, si attentifs aux divisions qui semblaient s'y
élever, peuvent voir par cet exemple, que ce n'est pas en vain qu'elle
se glorifie en Notre-Seigneur du remède qu'il a opposé aux dissensions,
en donnant un Chef aux évêques et à l'Église visible, avec lequel tout
le corps garde l'unité.

Nous rendons grâces à Dieu d'avoir inspiré à notre saint Père le Pape
Innocent XII, digne successeur de saint Pierre, une censure qui prévoit
si bien les inconvénients des nouvelles spiritualités tant dans la
spéculative que dans la pratique, avec une si ferme volonté de surmonter
les travaux d'un examen si pénible : et adhérant à son jugement, nous
condamnons le livre susdit,

 

1 _Proc. verbal de la Prov. de Camb., imprimé à Paris,_ p. 16.

 

479

 

intitulé : _Explication des Maximes des Saints sur la vie intérieure_ ,
etc. et lesdites vingt-trois propositions, avec les mêmes qualifications
de la Constitution apostolique, sans approuver les autres.

A CES CAUSES : Nous vous mandons de publier dans vos prunes et
prédications, la Constitution ci-dessus traduite, avec notre présent
mandement, pour être suivie et exécutée dans tout notre diocèse, selon
sa forme et teneur : ordonnons qu'elle sera enregistrée au greffe de
notre officialité, pour y avoir recours et être procédé par les voies de
droit contre les contrevenants : Défendons à toutes personnes de lire
ledit livre, même de le garder, sous toutes les peines portées par la
Constitution ; enjoignant sous les mêmes peines à ceux qui en auraient
quelque exemplaire, de nous le remettre incessamment entre les mains.

Nous vous mandons pareillement, d'envoyer et signifier ces présentes à
tous curés et vicaires, communautés séculières et régulières de notre
diocèse, et autres qu'il appartiendra, soi-disant exempts et non
exempts, pour être lues, publiées et exécutées dans la même forme. Donné
à Meaux, dans notre Palais épis-copal, le seizième jour du mois d'août,
l'an mil six cent nonante-neuf.

 

_Signé_ + J. BÉNIGNE, Ev. de Meaux.

 

Et plus bas :

 

_Par le commandement de mondit Seigneur,_

Royer.

 

Lu et publié en synode, le jeudi troisième jour de septembre, l'an mil
six cent nonante-neuf.

 

FIN DU MANDEMENT DE M. l'ÊVÊQLE LE MEAUX.

[Précédente]

[Accueil]

[Suivante]

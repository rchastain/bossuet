[INSTR. II - TOME IV]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Apocalypse-Rév.
Excidio Babylonis
Réflexions Morales
Instructions Trévoux
Avertissement
Instr. I - Général
Instr. I - Rem. partic.
Instr. I - Grotius
Instr. I - M. Simon
Instr. II - Grotius
Instr. II - Tome I
Instr. II - Tome II
Instr. II - Tome III
Instr. II - Tome IV
Plan - Théologie



TOME QUATRIEME.


 ÉPÎTRE A PHILÉMON.

 LXe   PASSAGE,   ET   REMARQUE.

 ÉPÎTRE AUX HÉBREUX.

 LXIe PASSAGE,   ET  REMARQUE.

 LXIIe PASSAGE.

 REMARQUE.

 LXIIIe PASSAGE.

 REMARQUE.

 LXIVe PASSAGE.

 REMARQUE.

 LXVe PASSAGE.

 REMARQUE.

 I ÉPITRE DE SAINT PIERRE.

 LXVIe   PASSAGE.

 REMARQUE.

 I ÉPITRE DE SAINT JEAN.

 LXVIIe PASSAGE.

 REMARQUE.

 LXVIIIe PASSAGE, ET REMARQUE.

 SAINT JUDE.

 LXIXe PASSAGE.

 REMARQUE.

 SUR L'APOCALYPSE

 LXXe PASSAGE.

 CONCLUSION DE CES REMARQUES,

 


ÉPÎTRE A PHILÉMON.

 

LXe   PASSAGE,   ET   REMARQUE.

 

Dans la traduction du verset 21 : « J'espère que vous m'écouterez : »
pourquoi non : « Que vous m'obéirez, » comme la Vulgate et tous les
autres traduisent, conformément à l'original? La note est encore plus
mauvaise , puisqu'elle ose même rejeter le terme à obéir comme
impérieux, quoique saint Paul s'en, serve

 

570

 

en cet endroit et partout : ce qui tourne directement contre l'Apôtre,
et ne peut servir qu'à un visible affaiblissement de l'autorité
ecclésiastique.

 


ÉPÎTRE AUX HÉBREUX.

LXIe PASSAGE,   ET  REMARQUE.

 

Au chapitre I, verset 3, texte même : «A la droite de Dieu, » rien ne
devait empêcher de traduire comme dit la lettre et comme porte la note :
« A la droite de la Majesté ou de la souveraine Majesté, » en y ajoutant
l'explication. C'est se rendre auteur, et non pas traducteur, que de
faire si souvent de tels changements.

 

LXIIe PASSAGE.

 

Sur ces mots : « Vous êtes mon fils, » tirés du _Psal_. II, verset 7, la
note porte que « l'Apôtre veut montrer par ce passage des Psaumes que
Jésus-Christ n'est pas Fils de Dieu comme les anges, qui sont
quelquefois appelés _fils de Dieu,_ mais qu'il l'est d'une manière
spéciale. »

 

REMARQUE.

 

Il devait donc dire que jamais les anges ne sont appelés de ce nom en
cette sorte, ni au nombre singulier et par excellence. On ne leur a
jamais dit, ni « je vous ai engendré, » ni que c'a été « aujourd'hui ; »
ce qui dénote le jour de l'éternité, selon l'explication des deux
Cyrilles et des autres Pères. L'auteur ne sait qu'affaiblir les passages
qui établissent la divinité, et c'est le fruit qu'on peut retirer de ses
critiques. Par cette même raison il se contente de dire que Jésus-Christ
_est Fils de Dieu_ d'une manière spéciale, ce que les sociniens ne
refusent pas, comme nous l'avons souvent remarqué : mais pour parler en
théologien et en catholique, il fallait encore ajouter que cette manière
spéciale d'être fils est d'être vraiment fils, vraiment engendré et né
de la substance de son père ; autrement on supprime les vrais caractères
personnels et substantiels du Fils de Dieu. On va voir encore d'autres
effets de cet affaiblissement de la saine théologie, par rapport à la
divinité de Jésus-Christ.

 

571

 

LXIIIe PASSAGE.

 

Dans la même note sur le verset 5 : « Je vous ai engendré aujourd'hui :
» — « Saint Paul, dit le traducteur, applique avec les Juifs de son
temps au Messie par un _deras_ , ou sens sublime et spirituel, ce qui
s'entendait à la lettre de David élevé sur le trône. »

 

REMARQUE.

 

On voit ici un effet de l'esprit des sociniens et de Grotius, qui
éludent les prophéties au sens véritable, et les réduisent en un sens
mystique et spirituel : le critique entre ici trop visiblement dans cet
esprit faute d'expliquer, comme il devait, que son « _deras,_ ou sens
sublime et spirituel, » est souvent le sens véritable, et que celui de
saint Paul en cet endroit est proprement et directement de la première
intention du Saint-Esprit, puisque même l'élévation de David à la
royauté n'épuise pas la grandeur de cette expression : « Dieu m'a dit, »
à moi proprement et uniquement : « Vous êtes mon fils, » unique et par
excellence : « Je vous ai, » non pas adopté, mais « engendré » de mon
sein ; et le reste que je ne dois pas prouver, mais supposer en ce lieu,
puisque même je l'ai démontré ailleurs ».

Ainsi ceux qui ne voient ici que David proprement et naturellement, ne
prennent que l'écorce de la lettre et en abandonnent l'esprit : comme il
parait par la suite du texte, tant du psaume que de saint Paul, et par
la tradition de toute l'Église, ainsi qu'on le pourra voir dans notre
exposition sur ce _Psaume_ (2), si on daigne y jeter les yeux.

Selon ces principes, qui sont de la foi et de la tradition expresse de
l'Église, il ne faut pas dire avec l'auteur que saint Paul « applique ce
passage à Jésus-Christ avec les Juifs de son temps : » c'est trop
resserrer la tradition que de la réduire « au temps de Jésus-Christ : »
ce n'est pas ici une application à Jésus-Christ comme à un sujet
étranger au texte ; mais une explication naturelle et véritable, qui
étant du dessein premier et principal du

 

1 Voyez _Suppl. in Psal_., et la _Dissert, prélim. sur Grotius_. — 2
Voyez _in Psal_. II, et _Suppl. in Psal_.

 

572

 

Saint-Esprit, a été transmise de main en main aux Juifs spirituels, et
en effet s'est conservée par une tradition dont les Juifs ne marquent
point d'origine, jusqu'au temps de Jésus-Christ et au delà.

C'est une chose à déplorer que l'explication ennemie des prophéties soit
insinuée si fortement dans une traduction du Nouveau Testament, qu'on
met entre les mains du peuple , et qu'on lui apprenne, conformément à
l'ancien esprit des critiques précédentes (1), à éluder les prophéties
qui sont le fondement de notre foi.

 

LXIVe PASSAGE.

 

Dans la note sur le verset 6 du même chapitre I de _l'Epître aux
Hébreux,_ « il explique le premier-né, c'est-à-dire le Fils unique, ce
qu'il a de plus cher ; et saint Paul prouve encore par là que
Jésus-Christ est Fils de Dieu d'une manière spéciale, et non comme les
anges. »

 

REMARQUE.

 

Il ne dira donc jamais qu'il est Fils unique, parce que seul il est
engendré de la substance de son Père et de même nature que lui : et il
ne sera _Fils unique_ que parce qu'il est _le plus cher,_ sans vouloir
sortir à cet égard des idées des sociniens par aucune remarque précise.

Nous avons souvent remarqué que la _manière spéciale_ des sociniens pour
la filiation de Jésus-Christ, c'est que Dieu en lui donnant une mère
Vierge, a suppléé par son Saint-Esprit la vertu d'un père charnel, et
seul lui tient lieu de père : ce qui suffit bien pour le distinguer des
anges, mais non pour le faire Fils de Dieu par nature et proprement. Si
nos critiques ignorent un si grand mystère, ou ne daignent en faire
mention, pourquoi font-ils les maîtres en Israël et s'ingèrent-ils à
expliquer l'Évangile?

 

LXVe PASSAGE.

 

Il s'agit ici de l'endroit de Jérémie, XXX, 31, cité par saint Paul,

 

1 Voyez ci-dessus, _Dissert, sur Grotius_.

 

573

 

_Hebr_., VIII, 8, que j'ai déjà remarqué (1) ; mais il y faut ajouter ce
qui suit.

 

REMARQUE.

 

Nous trouverons donc M. Simon toujours favorable à la grâce pélagienne,
c'est-à-dire extérieure et rien de plus, et toujours sous la conduite de
Grotius et des sociniens : « J'écrirai ma loi dans leur cœur, » etc.,
_Hebr_., VIII, 10, c'est-à-dire, selon Grotius : « Je ferai qu'ils
sauront tous ma loi par cœur, _memoriter,_ par la multitude des
synagogues où elle sera enseignée trois fois la semaine. » Crellius : «
Je leur donnerai des moyens d'en conserver le souvenir perpétuel ; » ce
que Grotius avait imité ; et après eux M. Simon : « Je leur donnerai des
lois qu'ils retiendront et qu'ils observeront en les comprenant
facilement. » Jusqu'ici ils ne sortent pas de la loi et de la doctrine,
comme disait saint Augustin ; c'est-à-dire qu'ils ne vont pas plus avant
que Pelage et Cœlestius, sans soupçonner seulement cette grâce si
clairement définie par le concile de Milève, « où non content de nous
enseigner ce qu'il faut faire, Dieu nous le fait encore aimer et pouvoir
(2) ; » ce que j'ai voulu ajouter exprès pour donner lieu au lecteur de
remarquer qu'il n'a encore rien vu et ne verra rien dans cette
traduction et dans ces notes, qui ressente le vrai esprit du
christianisme, c'est-à-dire celui de la grâce.

 


I ÉPITRE DE SAINT PIERRE.

 

LXVIe   PASSAGE.

 

« Et qui est-ce qui voudra vous nuire, si vous êtes zélés pour le bien?»
I _Petr_., III, 13.

 

REMARQUE.

 

Il faudra donc toujours changer le texte, et y mêler du sien ? Le texte
porte : « Qui est-ce qui vous nuira, ou qui vous fera du mal? » Ce qui
ne signifie pas seulement : « Qui est-ce qui voudra vous nuire ; » mais
encore, qui le pourra quand il le voudrait?

 

1 _Inst., Rem. sur Grotius,_ n. 17. — 2 _Concil. Milevit_. II, _contra
Pelag. et Cœlest_., can. IV ; tom. II _Concil_., Labb., p. 1537 ; sive
_Concil. African. aut Carthag_., an. 418.

 

574

 

 

Mais il a fallu suivre Grotius, qui explique ainsi : _Hoc vult, pauci
erunt qui vobis nocere velint,_ etc. ; « peu de gens voudront vous nuire
: » et la note de Grotius devient le texte de notre auteur.

 


I ÉPITRE DE SAINT JEAN.

 

LXVIIe PASSAGE.

 

« Il n'y a point de crainte où est l'amour ; mais l'amour parfait bannit
la crainte, » I _Joan_., IV, 18 ; où la note porte : « C'est-à-dire celui
qui aime Dieu véritablement, ne craint point de souffrir pour lui. »

 

REMARQUE.

 

Il ne s'agit point ici de souffrir pour Dieu : l'Apôtre venait de dire
au verset précédent : « L'amour que nous avons pour Dieu est parfait en
nous, lorsque nous avons confiance au jour du jugement : » en sorte que
nous n'en soyons point troublés ; ainsi la crainte que saint Jean a
dessein d'exclure est celle du jugement, qu'il veut que nous attendions
avec plus de confiance que de frayeur. Il nous montre donc «l'amour
parfait» comme le principe de la confiance qui bannit la crainte
inquiète des sévères jugements de Dieu : c'est le sens qui se présente
d'abord, et où nous mène la suite du discours : toute la théologie
adopte ce sens après saint Augustin, qui l'appuie en cent endroits ; mais
le traducteur lui préfère une autre explication moins convenable, et ôte
à l'Ecole un passage dont elle se sert pour expliquer la nature de
l'amour parfait, qui inspire la confiance et qui exclut la terreur.

 

LXVIIIe PASSAGE, ET REMARQUE.

 

Il s'agit ici du fameux passage : _Tres sunt qui testimonium dant in
cœlo_ : « Il y en a trois qui rendent témoignage dans le ciel, » I
_Joan_., v, 7 ; sur lequel il ne remarque autre chose, sinon que «
certains critiques de Rome, sous le pape Urbain VIII, quoiqu'ils ne
trouvassent dans aucuns manuscrits grecs toutes ces paroles, ont jugé
qu'il les fallait conserver. » C'est en vérité trop affaiblir ce
passage, que de n'alléguer pour le soutenir que le sentiment de ces
_censeurs romains_ qu'on ne connaît pas.

Si l'auteur voulait alléguer quelque autorité des derniers siècles,

 

575

 

il avait devant les yeux l'inviolable autorité du concile de Trente et
celle de la Vulgate ; s'il voulait remonter plus haut dans la tradition,
il n'ignorait pas les passages exprès de saint Fulgence, qui confirment
la leçon commune. Et ce qui est encore de plus important, il n'ignorait
pas cette célèbre confession de foi de toute l'Église d'Afrique, au roi
Hunéric, où ce texte de saint Jean est employé de mot à mot (1). Un
passage positif vaut mieux tout seul que cent omissions, surtout quand
c'est un passage d'une aussi savante église que celle d'Afrique, qui dès
le cinquième siècle a mis ce passage en preuve de la foi de la Trinité
contre les hérétiques qui la combattaient. On ne doit pas oublier qu'une
si savante église allègue comme incontestable le texte dont il s'agit ;
ce qu'elle n'aurait jamais fait s'il n'avait été reconnu, même par les
hérétiques. Il n'y a rien qui démontre mieux l'ancienne tradition qu'un
tel témoignage ; aussi vient-elle bien clairement des premiers siècles ;
et on la trouve dans ces paroles de saint Cyprien au livre de l'_Unité
de l'Église_ Le Seigneur dit : « Moi et mon Père nous ne sommes qu'un ;
» et il est encore écrit du Père, du Fils, et du Saint-Esprit : « Et ces
trois sont un, » et _hi tres unum sunt_. Où cela est-il écrit nommément
et distinctement du Père, du Fils, et du Saint-Esprit, sinon en saint
Jean, au texte dont il s'agit? Le même saint Cyprien se sert encore du
même passage pour appuyer son sentiment sur la nullité du baptême de
tous les hérétiques : « Si celui, dit-il, qui est baptisé par les
hérétiques (marcionites) est fait le temple de Dieu, je demande de quel
Dieu? Si c'est du Créateur, il ne peut pas en être le temple, puisqu'il
ne le reconnaît pas : si c'est de Jésus-Christ, il n'en peut non plus
être le temple, lui qui nie que Jésus-Christ soit Dieu : si c'est du
Saint-Esprit, puisque ces trois ne sont qu'un, _Cum hi très unum sint,_
comment le Saint-Esprit peut-il être ami de celui qui est ennemi du Père
et du Fils (2)? »

Voilà donc un second passage de saint Cyprien, pour démontrer qu'il a lu
dans saint Jean que le Père, le Fils, et le Saint-Esprit sont
expressément les trois qui ne sont qu'un : ainsi la

 

1 _Vict. Vit_., lib. III.— 2 _Epist. ad Jubaian., de hœr. bapt_.

 

576

 

leçon commune est établie au troisième siècle, et se trouve dans deux
passages exprès d'un aussi grand docteur que saint Cyprien ; les Anglais
même l'avouent dans la dernière édition de ce Père : et il ne faut pas
s'étonner qu'une leçon si ancienne se trouve établie au cinquième siècle
dans l'autorité où nous l'avons vue.

Si j'avais à traiter ce passage à fond, il me serait aisé de démontrer
par la suite du texte de saint Jean qu'il y manquerait quelque chose, si
cet endroit en était ôté ; mais il me suffit d'avoir montré le mauvais
dessein du traducteur, qui n'a voulu que faire douter, comme il avait
toujours fait, du texte de la Vulgate, puisqu'il s'attache encore à lui
opposer le grec et quelques autres versions. Voilà comme il se corrige,
en laissant dans son Nouveau Testament un monument immortel de ses
premières répugnances.

 


SAINT JUDE.

LXIXe PASSAGE.

 

Sur le verset 4 du chapitre unique de saint Jude, où se lisent ces
paroles : « Leur sentence de condamnation est écrite depuis longtemps, »
la note porte : « Saint Jude a voulu marquer par cette expression, qu'il
y a longtemps que ces impies étaient destinés à commettre ces impiétés.
»

 

REMARQUE.

 

Par qui destinés, si ce n'est par un décret fatal de la puissance divine
? Calvin n'a jamais rien proféré de plus impie pour faire Dieu auteur du
péché. L'auteur ne s'est aperçu d'une impiété si manifeste que sur la
fin de décembre 1702 ; car le carton qu'il a fait pour y remédier est de
cette date. Ainsi l'impiété a couru un an durant : on ne donne aucune
marque de repentir d'un tel blasphème, ni aucun avis aux simples qui ont
avalé ce poison. On ne crie pas moins à l'injustice contre les censures
d'un livre où l'impiété est si déclarée ; on croit être quitte de tout
par un carton inventé si longtemps après un si grand mal, et qui devient
ce qu'il peut.

 

577

 


SUR L'APOCALYPSE

LXXe PASSAGE.

 

Je ne dirai rien sur l'_Apocalypse,_ puisque j'ai déjà remarqué que dès
la préface de ce divin Livre, le traducteur dégrade saint Jean, dont il
ne fait « qu'une espèce de prophète (1). » Je pourrais encore ajouter
que pour s'être attaché sans discernement aux explications de Grotius,
qui bâtit sur le fondement d'une date visiblement fausse (2), il fait
deviner à saint Jean des choses passées devant les yeux de cet apôtre ;
en sorte qu'il faut par un seul trait effacer la plupart de ses
prédictions : et c'est la raison la plus apparente pour laquelle le
traducteur n'a osé donner aux révélations de saint Jean le titre absolu
de prophéties.

 


CONCLUSION DE CES REMARQUES,

 

_Où l'on touche un amas d'erreurs, outre toutes les précédentes._

 

Si l'on joint maintenant à ces remarques celles de l'_Instruction_
précédente, on voit que les fautes en sont innombrables, même , celles
où la foi est directement attaquée.

Je déclare au reste que si je m'arrête aux remarques que j'ai proposées,
ce n'est pas que j'aie dessein d'approuver les autres fautes, qui sont
infinies, de la nouvelle version et de ses notes : et afin qu'on ne
pense pas que ce soit un discours en l'air, je pourrais encore ajouter
que le traducteur a dit « qu'il n'y a de véritable résurrection que
celle des justes, » _Joan_., XI, 25 : ce qui donne lieu à une erreur qui
était commune parmi les Juifs, et qui leur est commune en partie avec
les sociniens, lorsqu'ils assurent qu'en effet nuls autres que les
justes ne ressuscitent pour être immortels ; « qu'il a dit avec trop peu
de précaution, que ce fut principalement après la résurrection que
Jésus-Christ entrant dans le ciel,... fut pontife selon l'ordre de
Melchisédech, _Hebr_., V, 6, puisque l'Apôtre au verset suivant établit
le plein exercice de son sacerdoce, lorsqu'il était sur la terre, où
ayant offert d'humbles

 

1 Ire _Inst_., _Rem. sur la Préf_., XIe pass., n. 7. — 2 Voyez nos
_Notes sur l'Apoc_., Préf.. n. 26.

 

578

 

prières avec de grands cris et avec larmes, il fut exaucé à cause de son
respect ; » ce qui enferme le fondement de ses fonctions sacerdotales :
qu'il a dit d'une manière téméraire et vague que la multiplicité des
paroles reprises par Jésus-Christ dans la prière, _Matth.,_ VI, 7, ne
consistait « que dans une longue répétition des mêmes mots, » ainsi
qu'il l'a inséré dans le texte même en traduisant : « Ne rebattez pas
les mêmes paroles ; » ce qui tendrait à condamner, non-seulement
plusieurs saintes pratiques de l'Église même dans son service public,
mais encore les Psaumes de David, et jusqu'à la prière de Jésus-Christ
dans son agonie, où il passa plusieurs heures à répéter le même
discours, _eumdem sermonem dicens,_ _Matth.,_ XXVI, 44 : qu'il a dit sur
saint _Luc.,_ XX, 35, que « par le siècle on entend le monde ; »
directement contre le texte, qui parle de ceux « qui seront jugés dignes
de ce siècle-là, » c'est-à-dire du siècle à venir, par opposition aux
enfants de ce siècle-ci, c'est-à-dire du siècle présent, _filii hujus
sœculi,_ verset 34 : qu'il a dit trop généralement et mal à propos que «
les gentils ne croyaient pas que la fornication fût un péché, » _Act.,_
XV, 20 ; et n'a pas assez distingué ce qui était défendu dans le décret
des apôtres par une convenance d'avec ce qui l'était par la loi
naturelle que les gentils devaient sentir au fond de leur conscience,
encore qu'ils ne voulussent pas ouvrir entièrement les yeux à la lumière
qui condamnait tous ces désordres : qu'il a dit que la prophétie d'Amos,
citée par saint Jacques, « était seulement un sens mystique et
spirituel, » _Act.,_ XV, 16, au lieu que bien constamment c'est une
prédiction des plus précises pour la conversion des gentils et pour les
temps du Messie : qu'il a dit a que ces mots, _esprit et ange,_ doivent
être pris pour la même chose, » _Act.,_ XXIII, 8 ; ce qui serait avancé
trop négligemment, et à l'exclusion de l'ame qui est aussi un esprit :
qu'il a dit aussi à cette occasion où il s'agissait d'un dogme, « qu'on
ne doit pas exiger des apôtres une expression fort exacte, » ce qui
prononcé indistinctement, induit une confusion universelle dans les
dogmes, et renverse les conclusions que les Pères et toute la théologie
tirent des paroles de l'Écriture.

Je ne finirais jamais, si je voulais rapporter les négligences,

 

579

 

l'inexactitude, les affectations, les singularités du traducteur. On ne
peut presque ouvrir son livre sans y trouver de nouvelles fautes. Au
lieu de traduire : « Nous étions naturellement dignes de la colère de
Dieu, » _Eph_., II, 3, il fallait mettre comme dans la note: « enfants
de colère, » etc., qui est un terme consacré. C'est la coutume
perpétuelle du traducteur, que ce qu'il réserve pour son texte soit
presque toujours le plus mauvais. Il allègue saint Jérôme dans son
commentaire sur cet endroit de l'_Epître aux Ephesiens,_ pour rendre le
mot naturellement par celui d'_entièrement_ : mais il oublie les
derniers mots de ce docte Père, où il conclut « qu'en tout cas, si on
reçoit cette signification, elle doit être exposée selon ses
explications précédentes, » dans lesquelles, pour expliquer la
corruption naturelle du genre humain, il y avait « compris la
concupiscence qui nous porte au mal dès nos premiers ans, et le péché
que le diable a introduit dans le monde, c'est-à-dire le péché originel.
»

Il n'est pas permis d'oublier ce que nous avons remarqué ailleurs (1),
mais en passant : c'est le silence étonnant de M. Simon sur les textes
qui établissent la divinité du Saint-Esprit. Tout en est plein dans
l'Évangile. Nous avons suivi l'auteur comme pas à pas sur tout le texte
sacré, sans y trouver un seul mot pour le grand sujet dont nous parlons.
Jésus-Christ promet d'envoyer le Saint-Esprit après son départ de ce
monde, pour y tenir sa place, pour y suppléer sa présence et nous
consoler de son éloignement, pour nous enseigner toute vérité et nous
suggérer au dedans ce que le Sauveur avait prêché au dehors ; il prend du
sien, il le glorifie comme étant son Esprit, ainsi que celui du Père, et
l'esprit de vérité : toutes fonctions que le Saint-Esprit ne pouvait
faire à la place de Jésus-Christ, s'il était son inférieur : il est donc
de même rang, de même ordre, de même autorité : c'est lui qui fait les
prophètes, les prédicateurs, tous les justes et tous les enfants de
Dieu, en habitant dans leurs cœurs, y répandant la grâce et la charité
avec lui-même, qui en est la source. Tout cela passe devant les yeux de
M. Simon, sans qu'il daigne en relever un seul mot : il pouvait du moins
remarquer que mentir au Saint-Esprit, c'est

 

1 Ire _Inst.,_ _Rem. sur la Pref.,_ 2e pass., n. 3.

 

580

 

mentir à Dieu. Quand il n'y aurait que les passages où nous sommes
appelés le _temple du Saint-Esprit,_ c'en serait assez pour nous faire
dire avec saint Grégoire de Nazianze : Un membre de Jésus-Christ ne doit
pas être le temple d'une créature. Quand il n'y aurait que la
consécration de l'homme nouveau en égalité au nom du Père, du Fils, et
du Saint-Esprit, il n'en faudrait pas davantage pour conclure avec le
môme saint : Non, je ne veux pas être consacré au nom de mon
conserviteur, ni enfin en un autre nom qu'en celui d'un Dieu : un petit
mot sur quelqu'un de ces passages eût bien valu quelques-unes de ces
misérables critiques dont l'auteur a rempli son livre : le Saint-Esprit
est représenté comme le tout-puissant instigateur de toutes les bonnes
pensées et l'auteur de tout l'intérieur de la grâce, qui contient la
consommation de l'œuvre de Dieu ; mais nous avons déjà remarqué que M.
Simon ne connaît guère cet intérieur, et qu'il affecte partout d'en
éloigner l'idée.

C'en est assez, et il me suffit d'avoir démontré que l'auteur fait ce
qu'il lui plaît du texte de l'Évangile, sans autorité et sans règle ;
qu'il n'a aucun égard à la tradition, et qu'il méprise partout la loi du
concile de Trente, qui nous oblige à la suivre dans l'interprétation des
Écritures ; qu'il ne se montre savant qu'en affectant de perpétuelles et
dangereuses singularités, et qu'il ne cesse de substituer ses propres
pensées à celles du Saint-Esprit ; que sa critique est pleine de
minuties, et d'ailleurs hardie, téméraire, licencieuse, ignorante, sans
théologie, ennemie des principes de cette science ; et qu'au lieu de
concilier les saints docteurs et d'établir l'uniformité de la doctrine
chrétienne par toute la terre, elle allume une secrète querelle entre
les Grecs et les Latins dans des matières capitales ; qu'enfin elle tend
partout à affaiblir la doctrine et les sacrements de l'Église, en
diminue et en obscurcit les preuves contre les hérétiques et en
particulier contre les sociniens, leur fournit des solutions, leur met
en main des défenses pour éluder ce qu'il a dit lui-même contre leurs
erreurs, et ouvre une large porte à toute sorte de nouveautés.

 

FIN DES INSTRUCTIONS SUR LA VERSION DE TRÉVOUX.

 

 

[Précédente]

[Accueil]

[Suivante]

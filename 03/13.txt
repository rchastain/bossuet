[INSTR. II - TOME III]

[Précédente]

[Accueil]

[Remonter]

[Suivante]
Bibliothèque

Accueil
Remonter
Remarques
Apocalypse-Rév.
Excidio Babylonis
Réflexions Morales
Instructions Trévoux
Avertissement
Instr. I - Général
Instr. I - Rem. partic.
Instr. I - Grotius
Instr. I - M. Simon
Instr. II - Grotius
Instr. II - Tome I
Instr. II - Tome II
Instr. II - Tome III
Instr. II - Tome IV
Plan - Théologie



TROISIEME TOME,


QUI   FAIT   LE   SECOND   VOLUME.

 

 ÉPÎTRE AUX ROMAINS.

 XLIIIe PASSAGE,   ET   REMARQUE.

 XLIVe PASSAGE.

 REMARQUE.

 XLVe PASSAGE.

 REMARQUE.

 XLVIe  PASSAGE.

 REMARQUE.

 XLVIIe PASSAGE.

 REMARQUE.

 XLVIIIe  PASSAGE.

 REMARQUE.

 I AUX ÉPHÉSIENS

 XLIXe   PASSAGE,    ET   REMARQUE.

 L e PASSAGE,   ET REMARQUE.

 LIe  PASSAGE.

 REMARQUE.

 LIIe PASSAGE.

 REMARQUE.

 LIIIe REMARQUE.

 REMARQUE.

 II  AUX CORINTHIENS.

 LIVe   PASSAGE.

 REMARQUE.

 LVe PASSAGE.

 REMARQUE.

 ÉPITRE AUX ÉPHÉSIENS.

 LVIe   PASSAGE,   ET   REMARQUE.

 ÉPÎTRE AUX COLOSSIENS.

 LVIIe   PASSAGE,   ET   REMARQUE.

 II AUX THESSALONICIENS.

 LVIIIe   PASSAGE,   ET   REMARQUE.

 LlXe PASSAGE, ET REMARQUE.

 


ÉPÎTRE AUX ROMAINS.

 

XLIIIe PASSAGE,   ET   REMARQUE.

 

Dans la note sur le verset 4 du chapitre I, l'auteur insinue que
Jésus-Christ n'a été prédestiné pour être, le Fila de Dieu que par
rapport à sa résurrection ; mais il ne faut pas oublier ce qui est
certain, et ce qui aussi a été constamment enseigné par saint Augustin
et ensuite par saint Thomas, et par toute la théologie, comme le vrai
sens de saint Paul, que c'est par une prédestination

 

554

 

purement gratuite qu'un certain homme particulier, qui est Jésus-Christ,
a été uni à la personne du Verbe plutôt que tout autre qui pouvait être
élevé au même honneur. L'auteur a osé reprendre en divers endroits cette
excellente doctrine par de mauvaises critiques : il tâche encore ici de
l'embarrasser. Mais au reste, comme il demeure d'accord que Jésus-Christ
a été prédestiné à être Fils de Dieu selon la divinité qui lui est unie,
cette remarque servira seulement de précaution contre les
embrouille-mens et les équivoques de la note du traducteur.

 

XLIVe PASSAGE.

 

Voici la note sur le verset : _Je vivais autrefois sans loi,_ Rom., VII,
9. « Ces paroles montrent que saint Paul parle en sa personne, d'un
homme qui vivait avant la loi, ou de celui qui n'est point encore
régénéré ; » à quoi il ajoute cette réflexion : « Saint Augustin était
dans ce sentiment avec la plupart des anciens Pères, avant ses disputes
contre les pélagiens. »

 

REMARQUE.

 

Je ne veux point entrer dans le fond de cette question, et encore moins
obliger l'auteur à préférer le sentiment de saint Augustin. Mais aussi
pourquoi décider magistralement entre deux interprétations si célèbres ?
S'il avait bien considéré les raisons, je ne dirai pas de saint
Augustin, mais celles qui ont obligé Cassien, sans doute peu attaché à
ce Père, à le préférer dans cette occasion à son maître saint
Chrysostome, il ne se serait peut-être pas tant pressé de prononcer sa
sentence, qu'une note de quatre lignes ne pouvait guère appuyer. Si la
chose était aussi claire qu'il se l'imagine, et que celui dont parle
saint Paul constamment eût vécu avant la loi, comment est-ce que cet
Apôtre lui fait dire « que la loi est bonne, vers. 16, et qu'elle est
spirituelle, » vers. 14 ; et encore : « Je me plais dans la loi de Dieu
selon l'homme intérieur, » vers. 22 ? Est-ce là le discours d'un homme
sans grâce, ou d'un homme dans la grâce et dont la régénération était
non-seulement commencée, mais encore fort avancée, puisqu'il se délecte
déjà dans la loi de Dieu : ce qui n'arrive qu'au juste, en quelque sorte

 

555

 

accoutumé à la vertu ? D'ailleurs il n'y a rien de plus faible que ce
passage dont l'auteur fait tout son appui : « Je vivais autrefois sans
la loi, » vers. 9. Car ignore-t-on que l'homme qui est dans la grâce de
Dieu et qui goûte déjà la loi, n'a pas commencé par là ; et qu'il a «
autrefois » été sans elle, livré à ses passions et à ses vices? Je ne
parle pas ainsi pour prendre parti, mais pour montrer à celui qui le
prend si légèrement, qu'il a trop précipité ses décisions.

Mais ce n'est pas ce que sa note a de plus mauvais : on y ressent une
secrète malignité contre saint Augustin, et son affectation à le
contredire, en insinuant après Grotius que ce grand homme est toujours
allé en reculant, et que depuis sa dispute « contre les pélagiens, » au
lieu de profiter dans ses travaux par son application à cette matière,
il a désappris ce qu'il savait.

On a vu une si claire réfutation de cette accusation des faux critiques
(1), qu'il n'y a qu'à y renvoyer le sage lecteur, et observer seulement
que les notes de M. Simon ne sont qu'une suite et une application des
principes qu'il a posés dans ses critiques.

 

XLVe PASSAGE.

 

_Aux Romains,_ VIII, 30. Sur ces paroles de saint Paul : « Ceux qu'il a
justifiés, il les a aussi glorifiés, » après la petite critique sur le
terme _magnificavit,_ « a rendu grands, » que le latin avait autrefois
et conserve encore dans quelques anciens manuscrits, au lieu de
_glorificavit,_ « a glorifiés, _rendus glorieux,_ » la note dit « que
saint Chrysostome, et les plus savants commentateurs grecs après lui,
ont entendu (ce terme de _glorifiés_) des dons du Saint-Esprit, que
reçoivent ceux qui ont été faits enfants de Dieu par le baptême : ce
qu'il appuie par le Scoliaste Syrien, qui a expliqué le même mot des
dons de faire des miracles, que les premiers chrétiens recevaient dans
leur baptême par l'imposition des mains et qui les rendaient célèbres :
» voilà comme il appuie ce sens : et venant à l'autre, il dit seulement
: « Saint Augustin et l'Ecole entendent cela de la gloire éternelle à
laquelle arriveront infailliblement tous les prédestinés. »

 

1 _Dissert. sur Grotius,_ n. 14, 15, etc.

 

556

 

REMARQUE.

 

L'interprétation de la gloire éternelle est ici absolument nécessaire :
1° par le texte même où la gradation manifeste nous mène naturellement,
« de la prédestination à la vocation, de la vocation à la justification,
et enfin de la justification à la gloire éternelle: » où se termine
l'ouvrage de notre salut et le grand mystère de Dieu sur les élus.

2° La même chose parait par toute la suite du chapitre, vers. 16, 17,
29, 30, et par l'aveu de l'auteur sur ce dernier verset, où lui-même il
entend la gloire éternelle sous le mot _glorificavit,_ comme fait aussi
toute l'Ecole, ainsi qu'il le reconnaît.

Cependant cette interprétation, qui est comme l'on voit celle de toute
la théologie, est celle-là même que l'auteur tâche d'affaiblir par ces
moyens.

Premièrement, en l'attribuant à l'Ecole, dont il donne une triste idée
dans tous ses livres : secondement, en l'attribuant à saint Augustin
seul, au lieu qu'il devait mettre avec saint Augustin tous les Pères qui
ont combattu les pélagiens sous sa conduite, lesquels ne sont pas en
petit nombre : troisièmement, saint Augustin même est maltraité dans ses
écrits,  et n'est guère considéré par les critiques de sa façon que
comme le premier des scolastiques : quatrièmement, en opposant au
_glorificare_ de la Vulgate l'ancienne leçon _magnificare,_ quoiqu'il
soit certain que le _glorificare_ soit meilleur, comme étant conforme au
grec de mot à mot _edoxase_ : cinquièmement, en opposant à saint
Augustin et à l'Ecole, saint Chrysostome et « les plus sa vans
commentateurs grecs, » par l'autorité desquels on voit qu'il veut
affaiblir celle de l'Ecole, quoique constamment préférable pour les
raisons qu'on vient d'entendre.

Or en cela il se trompe encore ; car il tronque saint Chrysostome, dont
voici les propres paroles : « Il les a justifies par la régénération du
baptême ; il les a glorifiés par la grâce, par l'adoption. » Je veux que
par la grâce on entende, non pas la grâce justifiante contre le sens
naturel, mais les seuls dons du Saint-Esprit. Saint Chrysostome
n'attribue pas la glorification à ces

 

557

 

dons seuls, mais il y joint _l'adoption,_ et il ne faut point entendre
celle qui arrive dans la régénération, que ce Père avait déjà exprimée
par le terme de _justifiés_ et de _régénérés_: «mais l'adoption parfaite
des enfants de Dieu, » après laquelle soupire toute créature, ainsi
qu'il est dit dans ce chapitre, vers. 21, 22, 23, et où la «
résurrection des corps est comprise, » conformément à cette parole de
Notre-Seigneur, _Luc.,_ XX, 30: « Ils seront enfants de Dieu, parce
qu'ils sont enfants de la résurrection. » Ainsi manifestement la
glorification dont parle saint Chrysostome contient la gloire céleste :
Théophylacte et les autres, qui sont sans doute du nombre de ceux que
l'auteur appelle _les plus savants commentateurs grecs,_ parlent de
même.

Il faut encore observer sur cette note que l'auteur, selon sa coutume,
affaiblit dans l'intérieur les vrais avantages des chrétiens, en les
réduisant à ce qui _les rend célèbres,_ comme s'ils n'avaient pas une
autre gloire à attendre, ou que celle-ci lut la principale.

En général on voit un dessein, et ici et partout ailleurs, d'opposer les
Grecs aux Latins, et particulièrement à saint Augustin, en quoi il y a
une double faute : la première, de commettre les Pères entre eux au lieu
de les concilier comme il est facile ; la seconde, de ne marquer pas que
les Pères qui ont écrit expressément contre les hérésies,  sont
constamment préférables dans l'explication des passages qui en regardent
la réfutation, ainsi qu'il est certain par expérience et que tous les
théologiens en sont d'accord, après saint Augustin et Vincent de Lérins
: non que les Pères soient contraires entre eux dans le fond, mais parce
que ceux qui ont traité expressément les questions s'expliquent aussi
d'une manière plus expresse et plus précise.

 

XLVIe  PASSAGE.

 

Sur ces mots: « Les élus de Dieu, » _Rom_., VIII, 33, la note porte :

« C'est-à-dire les fidèles que Dieu a choisis pour embrasser la loi

évangélique. »

 

REMARQUE.

 

La notion est fausse : les élus sont ceux dont il est écrit « qu'ils ne
peuvent être déçus, » _Matth.,_ XXIV, 24. Tout est plein de

 

558

 

pareils endroits qui montrent que le mot d'_élus_ ne doit pas être
expliqué simplement par _fidèles_ ; et que lorsqu'il se prend ainsi,
c'est à cause qu'on doit présumer par la charité que les fidèles
persévéreront jusqu'à la fin. Tout le monde remarquera naturellement que
ces idées de l'auteur sont de l'esprit des sociniens, qui ne veulent pas
reconnaître le mystère de l'élection et de la prédestination.

On voit par ces dernières observations que l'auteur change le langage
ecclésiastique ; et qu'en général partout le livre où il détourne les
passages de l'Écriture, qui tiennent lieu de principes dans l'Ecole, il
induit insensiblement une nouvelle théologie.

 

XLVIIe PASSAGE.

 

_Anathema a Christo,_ Rom., IX, 3. L'auteur traduit : « Anathème à cause
de Jésus-Christ ; » il répète dans la note ce qu'on a vu dans la
préface, « que la particule grecque _apo_ et la latine _à,_ se prennent
quelquefois chez les Hébreux pour la causale _propter,_ à cause de ; »
dont il assure « qu'on trouve des exemples dans l'Ancien et dans le
Nouveau Testament. » Dans le reste de la note il réfute saint
Chrysostome, comme n'ayant pas entendu ce que veut dire le mot
_d'anathème,_ qui ne signifie autre chose qu'exécration , ce que saint
Paul ni ne voulait, ni ne pouvait être. On se peut souvenir ici qu'il
avance dans sa préface que c'est faute d'avoir pris garde à cet
hébraïsme, qu'aucun traducteur ni commentateur n'a parfaitement exprimé
ce passage de saint Paul : de sorte qu'il est seul à le bien traduire.

 

REMARQUE.

 

Nous l'avons déjà repris d'avoir abandonné la Vulgate ; et pour montrer
qu'il l'abandonne sans raison, comme j'ai promis de le faire voir, je
n'ai qu'à dire qu'il ne suffit pas d'alléguer un hébraïsme ; il faut
nommer des auteurs et ne pas traduire à sa fantaisie, puisque s'il y a
peut-être un ou deux endroits, ce que nous allons examiner, où _apo_
signifie _propter,_ on en peut produire cinq cents où il faut traduire
autrement.

Le traducteur nomme des auteurs, mais qui sont tous contre

 

559

 

lui: et quels auteurs? C'est saint Chrysostome avec toute son Ecole,
sans en excepter les plus savants, saint Isidore de Damiette, Théodoret
et les autres, qui font sans doute partie de ces savants commentateurs
que le traducteur a accoutumé de nous vanter avec raison. J'y ai ajouté
en d'autres ouvrages (1) saint Basile, saint Grégoire de Nazianze, saint
Jérôme, Bède, qui tous en énonçant ou en supposant la signification
ordinaire du terme _apo,_ montrent que le _propter_ du traducteur ne
leur est pas seulement venu dans la pensée : ce qui ne peut être arrivé
sans quelque raison qu'il faudra trouver, si nous voulons expliquer ce
passage à fond.

Commençons par les exemples que l'auteur allègue en l'air sans en avoir
marqué un seul, et surtout considérons ceux du Nouveau Testament ou de
saint Paul même, qui seraient les plus convenables. Je passerai au
traducteur dans toutes les Epltres de cet apôtre, dans tout le Nouveau
Testament, un seul endroit où il est dit que « Jésus-Christ fut exaucé à
cause de son respect, » _pro sua reverentià,_ _Hebr_., V, 7 ; dans le
grec *-h, « à cause de, » quoique d'autres qui ne sont pas méprisables
aient traduit autrement ; mais quand il faudrait traduire comme veut
l'auteur, doit-on conclure, encore un coup, pour un seul endroit de
saint Paul et du Nouveau Testament, où _apo_ voudra dire _propter,_
qu'on doive au hasard, indéfiniment, et sans aucune raison particulière,
le tourner ainsi quand on voudra?

La connaissance de cet hébraïsme n'est pas si rare, qu'on ne le trouve
chez les bons auteurs ; et Estius le rapporte, _Hebr_., V, 7, sur ce mot,
_pro reverentia_: mais le traducteur demeure d'accord que l'application
de cet hébraïsme au passage dont nous parlons, ne s'est présentée qu'à
lui seul, et n'est venue dans l'esprit, ni à Estius, ni à aucun autre
commentateur grec ou latin.

C'est aussi de quoi nous venons de dire qu'il se doit trouver quelque
raison, et en effet en voici une très-simple et très-naturelle : c'est
que _l'apo,_ lorsqu'il est uni comme ici à une personne, _apo Kristou_ :
« de Jésus-Christ, » ne se trouve jamais pris pour propter, « à cause de
; » ni pour autre chose que pour à, à Christo : « de Jésus-Christ. »

 

1 Divers écrits: _Préf., Passages éclaircis_ ; _Addition_ à la même, et
au chap V

 

560

 

Ces termes, _propter Christum,_ « à cause, ou pour l'amour de
Jésus-Christ, » sont bien connus de l'Apôtre : on trouve partout
lorsqu'il s'agit des personnes: _Propter te, propter nos, propter
electos, propter Deum, propter Christum_. Mais tous ces endroits et les
autres à l'infini de même nature, ont leur particule consacrée qui est
_dia_ et non pas _apo_ : pourquoi donc cet endroit ici sera-t-il le seul
où saint Paul se serve _d'apo_ ? On trouve encore pour exprimer les
causes finales le _propter_ mille et mille fois, et _apo_ n'y est jamais
employé. Si je voulais descendre à un détail d'observations
particulières, je pourrais dire que dans ces passages de l'ancienne
version des Septante : _Turbatus est à furore oculus meus ; non est
semitas in carne mea à facie irœ tuœ,_ et les autres en très-petit
nombre où _apo_ est mis pour _propter,_ désignent des causes actives ou
efficientes : on a est troublé par la colère » comme par une cause
active : « la colère de Dieu » est la cause pareillement efficiente, «
qui altère notre santé, » et ainsi du reste. Il ne s'agit pas ici des
personnes pour l'amour desquelles on veut quelque chose : il s'agit des
choses qui nous mettent en certains états. C'est un fait constant : il
ne faut point ici chercher de raison du sens que l'on donne à ces façons
de parler. Pour l'ordinaire, il n'y en a point d'autre que le style des
auteurs, ou en tout cas l'usage des langues, leur génie, leur propriété.
Quoi qu'il en soit, il est bien certain, comme nous venons de le
remarquer, que _l'api_ pour _propter,_ « à cause de, » ne se trouve ni
dans saint Paul, ni dans tout le Nouveau Testament, lié avec une
personne, tel qu'est ici Jésus-Christ, _api Kristou_. Si l'Apôtre eût eu
alors dans l'esprit le désir d'être anathème pour l'amour de
Jésus-Christ comme pour la fin de ce désir, le _dia,_ qui était si
familier en ce sens, se serait présenté tout seul, et il n'aurait pas eu
besoin d'aller chercher cet _apo,_ dont à peine se serait-il servi une
fois, et jamais en cas pareil. Il ne veut donc pas lui donner de sens
extraordinaire, et lui laisse sa force et sa signification accoutumée ,
qu'on trouve partout dans ses _Epîtres_ et dans toutes les Écritures, et
qu'aussi on voit jusqu'ici, comme l'auteur en convient, reconnu sans
exception par tous les interprètes, parmi lesquels nous avons compté six
ou sept des plus savants Pères.

 

561

 

Mais peut-être qu'il est forcé à cet hébraïsme par quelque nécessité?
Point du tout: le traducteur fait accroire à saint Chrysostome qu'il
n'entendait pas anathème, qui veut toujours dire exécration, en mauvaise
part : ce que saint Paul ne pouvait pas être en demeurant, comme il le
voulait, en état de grâce. Telle est la seule objection du traducteur,
et il ne veut pas sentir que ce Père ne l'entendait pas autrement,
puisque s'il croit que saint Paul s'offrit à être traité d'exécrable et
à être séparé de Jésus-Christ en un certain sens, c'était en
sous-entendant qu'il s'y offrait seulement s'il était possible, sans
préjudice de l'état de sainteté et de la grâce où il espérait demeurer
toujours.

Au reste si la question ne méritait peut-être pas en ce lieu tant de
discussion, il importait de faire connaître à quel prix on met ici les
hébraïsmes, et avec quelle facilité on abandonne le texte de la Vulgate,
quoique conforme à l'original grec, en faveur d'une interprétation qui
n'a pour appui que les conjectures d'un traducteur licencieux.

 

XLVIIIe  PASSAGE.

 

« Que Dieu brise le Satan sous vos pieds, » aux Rom., XVI, 20 ; c'est ce
que porte la traduction ; et la note : « Le Satan, c'est-à-dire
l'adversaire ; » à quoi elle ajoute : « Il y a néanmoins de l'apparence
qu'il a eu aussi en vue le diable. »

 

REMARQUE.

 

Il faut toujours à l'auteur quelque petit raffinement : on savait bien
que Satan veut dire _adversaire_ : mais il fallait dire que ce terme
général est devenu partout dans l'Écriture le nom propre du diable, et
que jamais il ne se trouve en un autre sens dans tout le Nouveau
Testament. Il est donc incontestable que saint Paul a voulu parler du
démon, qu'il ne pouvait pas désigner plus clairement que par son propre
nom ; et quand l'auteur réduit cette explication, qui constamment est la
seule véritable, à une simple apparence, je ne sais que deviner, si ce
n'est qu'il veut raffiner et se singulariser à quelque prix que ce soit.

 


I AUX ÉPHÉSIENS

 

XLIXe   PASSAGE,    ET   REMARQUE.

 

Voici la note sur le vers. 1 du chap. VII : « Saint Paul loue le célibat
à cause de la commodité qu'il y a de vivre sans femme, et hors les
embarras du mariage. » C'est toute la froide louange que M. Simon donne
au célibat, où les saints Pères ont cru voir la vie des anges. Ce que
saint Paul a mis dans le texte : « Il est bon, il est honnête, bon
absolument ; le traducteur dans le texte même le réduit à un « c'est bien
fait ; » et dans la note, à « être utile pour la commodité de la vie. »
Les autres avantages que saint Paul relève, comme d'être dans le célibat
plus en état de prier, plus occupé de Dieu seul et moins partagé dans
son cœur, vers. 5, 20, 32, 33, 34, 35, cet auteur, aussi bien que les
protes-tans, les compte pour peu et ne daigne les remarquer.

 

L e PASSAGE,   ET REMARQUE.

 

« Ils buvaient des eaux de la pierre qui les suivaient, I Cor., X, 4. En
lisant son texte et sa remarque, où il énonce expressément que c'étaient
les eaux qui sui voient « et accompagnaient le peuple, » on voit qu'il
traduit sans attention, et non-seulement contre la Vulgate, mais encore
contre le texte de saint Paul. Car c'est la pierre invisible,
c'est-à-dire Jésus-Christ, qui suivait partout le camp d'Israël, et lui
fournissait des eaux en abondance.

 

LIe  PASSAGE.

 

Dans la note sur le même verset : « Saint Paul, dit-il, continue son
_deras_ ou sens mystique. »

 

REMARQUE.

 

Il ne fallait pas oublier que « ce sens mystique » n'est pas une
explication arbitraire, ou une simple application que saint Paul fait de
ces passages à la nouvelle alliance comme à un objet étranger :
l'explication de l'Apôtre est du premier dessein de l'Écriture : il est,
dis-je, du dessein du Saint-Esprit, que toute la

 

563

 

loi, et en particulier tout le voyage des Israélites dans le désert,
soit la figure de l'Église et de son pèlerinage sur la terre où elle est
étrangère. Saint Paul le remarque exprès en deux endroits de ce chapitre
: « Ces choses sont arrivées pour nous servir de figures ; » et encore
plus expressément : « Toutes ces choses leur arrivaient pour nous servir
de figure, » vers. 6,11 ; ce qui déclare un dessein formé de les
rapporter aux chrétiens. Les théologiens sont soigneux à marquer ce
dessein formel et principal des anciennes Écritures : mais nos critiques
ne vont pas si loin, et voudraient bien regarder de semblables
explications comme des applications arbitraires et ingénieuses.

 

LIIe PASSAGE.

 

Au chapitre XI, vers. 19 : « Il faut qu'il y ait encore de plus grandes
partialités. »

 

REMARQUE.

 

Qui lui a donné l'autorité de retrancher de son texte les _hérésies,_
qu'il trouve également et dans le grec et dans la Vulgate ? Je veux
qu'il lui soit permis d'indiquer dans une note la petite diversité qui
se trouve ici entre les interprétations des Grecs et des Latins : mais
de décider d'abord contre les Latins et, ce qui est pis, les condamner
dès le texte, contre la Vulgate qu'il s'est obligé de suivre, c'est une
partialité trop déclarée. Un interprète modéré et pacifique aurait
plutôt travaillé à concilier ces deux interprétations, comme il est
aisé, en faisant dire à saint Paul ce qui est naturel et si véritable
d'ailleurs, qu'on ne doit pas s'étonner qu'il y ait des partialités
parmi les chrétiens, puisqu'il faut même qu'il y ait des hérésies :
_Oportet et hœreses_ ; c'est-à-dire _etiam hœreses_. Le passage est
consacré à cet usage par toute l'Église latine ; et la note du traducteur
qui remarque que « le mot d'_hérésie_ se prend ordinairement pour des
dissensions dans les dogmes, » n'est pas concluante pour l'exclusion des
véritables hérésies, puisque rien n'empêche que saint Paul n'ait
argumenté du plus au moins : ce qui au contraire est dénoté par la
particule

 

564

 

grecque _kai,_ aussi bien que par l'_et_ de la Vulgate, ainsi qu'il a
été dit.

 

LIIIe REMARQUE.

 

« Il sera coupable comme s'il avait fait mourir le Seigneur, et répandu
son sang ; » au lieu de traduire : « Il sera coupable du corps et du
sang du Seigneur, » chapitre XI, vers. 27 ; ce que l'auteur renvoie à la
note.

 

REMARQUE.

 

C'est une licence criminelle d'introduire des paraphrases dans le texte
: d'ailleurs cette expression de l'Apôtre : « Coupable du corps et du
sang, » inculque avec plus de force la réalité et l'attentat actuel et
immédiat sur la personne présente : ainsi le traducteur affaiblit le
texte, et veut mieux dire que saint Paul.

 


II  AUX CORINTHIENS.

 

LIVe   PASSAGE.

 

Sur ces paroles de la _seconde aux Corinthiens,_ chapitre I, verset 9 :
« Nous avons eu en nous-mêmes une réponse, une sentence de mort, il met
au contraire dans le texte même : « Une assurance de ne point mourir. »

 

REMARQUE.

 

Saint Chrysostome explique « cette réponse, _apokrima,_ une sentence, un
jugement, une attente certaine de sa mort, qui lui était déclarée par
toutes les circonstances (1) : » c'est à quoi le mot grec, aussi bien
que toute la suite du discours, a déterminé tous les interprètes ; et le
traducteur demeure d'accord dans sa note qu'on l'entend ainsi
ordinairement. Mais il lui faut de l'extraordinaire et de l'inouï, et il
est le seul qui change l'assurance de mourir en l'assurance de ne mourir
pas. Il dit pour toute raison que « la réponse » de saint Paul signifie
« ici une caution, ou, comme nous disons, un répondant (2) : » et sans
autre autorité que celle de Heinsius, il insère la conjecture de ce
protestant dans le texte même, et il ne craint pas de l'attribuer au
Saint-Esprit.

 

1 Hom. IV, in II _ad Cor_.— 2 Voy. I _Inst., Addit_., VIe _Rem_., n. 3.

 

565

 

Je prie le sage lecteur de s'arrêter ici un moment pour considérer ce
que deviendra l'Écriture, si elle demeure ainsi abandonnée aux
traducteurs.

 

LVe PASSAGE.

 

« La lettre cause la mort ; » et il explique « qu'elle tue, c'est-à-dire
qu'elle punit de mort, et ne propose autre chose que de sévères
châtiments à ceux qui violent ce qu'elle ordonne, » Il _Cor_., III, 6.

 

REMARQUE.

 

C'est peut-être ici un des endroits où l'on ressent davantage l'esprit
du traducteur. Outre la peine de mort que la loi prononce, , elle tue
d'une autre façon, parce que n'apportant aucun secours à notre
faiblesse, elle ne fait qu'ajouter au crime la conviction d'avoir
transgressé le commandement si expressément  proposé. Toute la théologie
a reçu cette explication, dont saint Augustin a fait un livre que tout
le monde connaît, et s'en est servi après ce Père, pour montrer la
nécessité de recourir à la grâce, c'est-à-dire à l'esprit, qui seul peut
donner la vie. Sans parler de saint Augustin, il est bien certain, et le
traducteur en convient, que cette manière dont la lettre tue est de
saint Paul, lorsqu'il enseigne _aux Romains_ « que la loi nous cause la
mort et nous tient liés ; en sorte que le péché se rend plus abondamment
péché par le commandement même : » c'est en peu de mots le fond de la
doctrine de l'Apôtre, _Rom_., VII, 5, 6, etc.

Il est certain que ces deux passages de saint Paul ont un rapport
manifeste, puisque si l'Apôtre dit ici aux Corinthiens : « La lettre
tue, et l'esprit nous donne la vie, » il avait aussi dit aux Romains «
que nous devions servir Dieu, non point dans la vieillesse de la lettre,
mais dans un nouvel esprit, » _Rom_., VII, 6.

Si donc le traducteur avait conféré ces deux passages, dont la
convenance est si sensible, au lieu de se borner, comme il a fait, à la
manière dont _la lettre tue_ en punissant de mort les transgressées, il
y aurait encore ajouté cette autre manière de donner la mort, en ce que
sans secourir notre impuissance, la loi ne fait que nous convaincre de
notre péché. C'est sans doute ce que

 

566

 

devait faire notre auteur ; et en proposant par ce moyen le système
entier de saint Paul, il en aurait pu inférer avec saint Augustin et
toute la théologie la nécessité de la grâce.

Il aurait même trouvé ce beau système dans saint Chrysostome. Il est
bien vrai que ce Père, sur cet endroit de la _Seconde aux Corinthiens_ :
« La lettre tue, » par cette lettre qui tue « entend la loi qui punit
les transgresseurs (1) ; » par où il semble avoir dicté l'explication du
traducteur. Mais il ne fallait que tourner la page pour trouver le
reste ; car on y lit «que la loi n'est qu'une pierre ; » n'est autre chose
« que des lettres écrites, qui ne donnent aucun secours et n'inspirent
rien au dedans ;» et en un mot, « quelque chose d'immobile » et d'inanimé
: tout au contraire « de l'esprit qui va partout, inspirant à tous les
cœurs une grande force (2) ; » c'est donc par là qu'il explique qu'on ne
peut rien sans la grâce, et que la loi ne peut que tuer ; c'est-à-dire
découvrir « le mal et le condamner, » au lieu que le seul esprit donne
la vie.

Il prend soin ailleurs de montrer la liaison des deux passages de saint
Paul, et que celui de l'_Epître aux Corinthiens,_ où il est dit que « la
lettre tue, » convient à ce que l'Apôtre enseigne aux Romains ; « à
cause, dit-il, que la loi ne fait que commander ; pendant que la grâce,
non contente de pardonner le passé, » nous fortifie pour l'avenir (3).

Il explique sur ce fondement de quelle sorte, comme dit saint Paul, nous
devons vivre, non plus « selon la loi qui vieillissait, mais selon le
nouvel esprit : à quoi il ajoute que la loi n'est autre chose qu'une
accusatrice, qu'elle dispose en quelque sorte au péché, qu'elle ne fait
qu'irriter le mal, et animer la cupidité par la défense (4) ; » et dit
enfin, sans rien excepter, tout ce que saint Augustin a si clairement
digéré, et si bien tourné contre les pélagiens.

On voit maintenant que le traducteur, pour expliquer que « la lettre
tue, » ne se de voit pas renfermer dans les menaces de la loi qui punit
de mort les transgresseurs, comme si la loi ne causait la mort que par
cet endroit, puisqu'il s'agit ici principalement de

 

1 Hom. VI,  in Epist. II _ad Cor_. — 2 Hom. VII. — 3 Hom. XI, _ad_
_Rom_. — 4 Hom. XII, _ad_ _Rom_.

 

567

 

la mort du péché comme opposée à la vie que la grâce donne : et si la
lettre ne tuait ici que par la mort du corps, l'esprit ne vivifierait
aussi que par la vie temporelle.

Il paraît encore que dans un passage si important contre les pélagiens,
on ne devait pas laisser à part saint Augustin, ni se tant éloigner de
lui, qu'on voulût priver les lecteurs des plus belles interprétations de
ce Père, après que toute la théologie en a fait comme un fondement de
ses dogmes les plus essentiels.

On aperçoit aisément que le traducteur a voulu selon sa coutume insinuer
secrètement de l'opposition, et comme une espèce de guerre entre saint
Chrysostome et saint Augustin, au lieu de montrer, comme nous venons de
faire, avec quelle facilité on les concilie, puisqu'il n'y a qu'à tout
lire sans s'arrêter à un seul endroit ; ce qui peut aussi servir
d'exemple à terminer en interprète catholique de semblables différends,
que le traducteur au contraire tâche d'allumer.

 


ÉPITRE AUX ÉPHÉSIENS.

 

LVIe   PASSAGE,   ET   REMARQUE.

 

Au chapitre II, verset 10 de cette _Epître,_ le texte dans son entier
porte ces mots : « Car nous sommes son ouvrage, étant créés en
Jésus-Christ dans les bonnes œuvres que Dieu a préparées, afin que nous
y marchions. » La traduction retranche ces mots, « Afin que nous y
marchions ; » ce n'est point par inadvertance, puisqu'on trouve ces
mêmes mots dans la note. Le traducteur n'en a point voulu dans le texte,
parce que cette version marque peut-être plus expressément qu'il ne
voulait, que par _cette création_ intérieure par laquelle nous sommes
créés dans les bonnes œuvres, Dieu prépare nos cœurs à les faire et y
incline au dedans nos volontés. Aussi la note dans le même esprit ne
fait-elle « Dieu créateur dans les bonnes œuvres, que par une expression
métaphorique, en nous montrant ce que nous devions faire : » ce qui
réduit la grâce chrétienne à l'opération purement extérieure de la loi,
et enseigne directement la doctrine pélagienne.

 

568

 


ÉPÎTRE AUX COLOSSIENS.

 

LVIIe   PASSAGE,   ET   REMARQUE.

 

Sur le chapitre I, verset 15. Un fidèle traducteur ne se serait jamais
permis de supprimer dans le texte le terme de premier-né ou
l'équivalent, puisqu'il est du grec et de la Vulgate et qu'il se trouve
consacré dans les versions, pour mettre à la place premier seulement,
contre la foi des originaux. Le premier objet d'un traducteur, c'est
d'être fidèle au texte sans lui ôter un seul trait, ni la plus petite
syllabe. De telles suppressions font imaginer aux ariens qui abusent de
ce passage, qu'il est véritablement pour eux, puisqu'on est contraint de
le changer : il faut éloigner de telles idées, et ne pas autoriser la
coutume de mêler son commentaire à l'original.

Pour expliquer ce mot : _Premier-né,_ l'auteur a recours à un hébraïsme,
et prétend que chez les Hébreux « ce terme signifie souvent celui qui
est éminent au-dessus des autres. » C'est peu donner au Fils de Dieu que
de le rendre « éminent au-dessus des créatures : » le sens de saint Paul
est plus profond , et veut dire que celui qui est né, _primogenitus,_
c'est-à-dire le Fils de Dieu, précède de nécessité et par sa nature,
jusqu'à l'infini, tout ce qui a été fait : ce que saint Paul exprime en
ajoutant que « toutes les créatures qui sont dans le ciel et sur la
terre ont été faites par lui, soit visibles, soit invisibles, trônes,
dominations, principautés, puissances, tout a été créé par lui et pour
lui : en sorte qu'il est avant tous, et qu'il n'y a rien qui ne subsiste
par lui,» _Col_., I,16,17.

Il ne fallait donc point hésiter à traduire ici tout du long que
Jésus-Christ est le _premier-né,_ ni appréhender que par ce moyen il se
trouvât en quelque sorte rangé avec les créatures, qui sont son ouvrage
qu'il a tiré du néant par sa puissance : puisqu'après tout, quand saint
Paul dit de Jésus-Christ qu'il est l'unique ou, ce qui est la même
chose, le _premier-né,_ sans second, avant toute créature, il ne fait
que répéter ce que Salomon a vu en esprit dans ses _Proverbes,_ que la
sagesse éternelle, qui est le Verbe, était « engendrée, conçue et
enfantée » » au sein de son Père avant

 

1 Prov., VIII, 22, 24.

 

569

 

tous les temps, lorsqu'il a commencé ses voies et produit au jour ses
ouvrages : ce qui est si grand, qu'il ne faut pas craindre que la
majesté et l'éternité du Fils de Dieu en soient rabaissées.

 


II AUX THESSALONICIENS.

 

LVIIIe   PASSAGE,   ET   REMARQUE.

 

Sur le terme d'_apostasie,_ chapitre II, la note sur le verset 3 : «
Interprète que la plupart des chrétiens abandonnent leur religion » :
c'est ajouter au texte trop visiblement et sans aucune raison. Un grand
nombre n'est pas la plupart ; et ce grand nombre suffit pour l'apostasie,
quoique d'ailleurs le corps de l'Église catholique , dont on se détache,
demeure toujours le plus grand, ainsi qu'il est arrivé dans tous les
schismes.

 

LlXe PASSAGE, ET REMARQUE.

 

Au chapitre II, verset 14. Ce ne peut être que pour contenter les
protestants, qu'on a pris plaisir de mettre avec eux _doctrine_ dans le
texte, et de reléguer à la note le mot de _tradition,_ qui est consacré
par l'usage des catholiques et par la Vulgate, aussi bien que par la
suite du discours et par le témoignage exprès des saints Pères, à la
doctrine de vive voix seulement. Cependant on n'a point de honte d'une
telle traduction, ni d'ôter à l'Église un de ses plus forts arguments
pour établir l'autorité de la tradition.

 

[Précédente]

[Accueil]

[Suivante]

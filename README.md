
# Œuvres complètes de Bossuet

Œuvres complètes de Bossuet au format texte.

Il s'agit de l'édition des œuvres complètes de Bossuet par F. Lachat.

> Œuvres complètes de Bossuet, publiées d'après les imprimés et les manuscrits originaux, purgées des interpolations et rendues à leur intégrité par F. Lachat. Édition renfermant tous les ouvrages édités et plusieurs inédits. Paris, librairie de Louis Vivès, éditeur.

# Histoire du projet

Les 31 volumes des œuvres complètes ont été numérisés et convertis au format HTML par le père Dominique Stolz, de l'Abbaye Saint Benoît de Port-Valais. Cette édition est accessible [en ligne](https://www.bibliotheque-monastique.ch/bibliotheque/bibliotheque/bossuet/index.htm), parmi d'autres trésors de la [bibliothèque monastique](https://www.bibliotheque-monastique.ch/).

À mon tour, j'ai converti (à l'aide du logiciel [pandoc](https://pandoc.org/)) les fichiers HTML en simples fichiers texte. Avec le temps, je corrige les erreurs résultant du processus de numérisation.

## Liens

- Œuvres complètes de Bossuet au format PDF sur le site de la [Bibliothèque Nationale](https://gallica.bnf.fr/services/engine/search/sru?operation=searchRetrieve&version=1.2&startRecord=0&maximumRecords=15&page=1&collapsing=disabled&query=dc.relation%20all%20%22cb33986914m%22%20%20%20%20sortby%20dc.title%2Fsort.ascending)
- [Table des ouvrages de Bossuet contenus dans chaque volume de cette édition](31/04.txt)
- [Aperçu de la même table](table-des-ouvrages.txt)
